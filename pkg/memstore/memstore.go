package memstore

import (
	"sync"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

// MemStore is an in-memory catalog storage.
type MemStore struct {
	productCards struct {
		sync.RWMutex
		data map[uint32]*catalog.ProductCard
	}
	categories struct {
		sync.RWMutex
		data map[uint32]*catalog.Category
	}
	properties struct {
		sync.RWMutex
		dataByID  map[uint32]*catalog.Property
		dataByKey map[string]*catalog.Property
	}
	propertyVariants struct {
		sync.RWMutex
		data map[uint32]*catalog.PropertyVariant
	}
}

// New creates a new MemStore.
func New() *MemStore {
	s := &MemStore{}
	s.productCards.data = make(map[uint32]*catalog.ProductCard)
	s.categories.data = make(map[uint32]*catalog.Category)
	s.properties.dataByID = make(map[uint32]*catalog.Property)
	s.properties.dataByKey = make(map[string]*catalog.Property)
	s.propertyVariants.data = make(map[uint32]*catalog.PropertyVariant)
	return s
}

// PatchProductCard patches the properties of a product card.
func (s *MemStore) PatchProductCard(id uint32, properties []*catalog.ProductProperty) {
	s.productCards.Lock()
	defer s.productCards.Unlock()

	pc, ok := s.productCards.data[id]
	if !ok {
		return
	}
	pc = pc.Clone()
	pc.ApplyProductProperties(properties)
	s.productCards.data[id] = pc
}

// DeleteProductCard deletes a product card.
func (s *MemStore) DeleteProductCard(id uint32) {
	s.productCards.Lock()
	defer s.productCards.Unlock()

	delete(s.productCards.data, id)
}

// UpsertProductCard inserts a new product card or replaces existing.
func (s *MemStore) UpsertProductCard(pc *catalog.ProductCard) {
	s.productCards.Lock()
	defer s.productCards.Unlock()

	s.productCards.data[pc.ID] = pc
}

// GetProductCardList returns product cards by the given ids.
func (s *MemStore) GetProductCardList(ids []uint32) []*catalog.ProductCard {
	s.productCards.RLock()
	defer s.productCards.RUnlock()

	out := make([]*catalog.ProductCard, 0, len(ids))
	for _, id := range ids {
		p, ok := s.productCards.data[id]
		if ok {
			out = append(out, p)
		}
	}
	return out
}

// GetProductCard returns product card by the given id.
func (s *MemStore) GetProductCard(id uint32) (*catalog.ProductCard, bool) {
	s.productCards.RLock()
	defer s.productCards.RUnlock()

	p, ok := s.productCards.data[id]
	return p, ok
}

// DeleteCategory deletes a category.
func (s *MemStore) DeleteCategory(id uint32) {
	s.categories.Lock()
	defer s.categories.Unlock()

	delete(s.categories.data, id)
}

// UpsertCategory inserts a new category or replaces existing.
func (s *MemStore) UpsertCategory(c *catalog.Category) {
	s.categories.Lock()
	defer s.categories.Unlock()

	s.categories.data[c.ID] = c
}

// GetCategory returns a category by id.
func (s *MemStore) GetCategory(id uint32) (*catalog.Category, bool) {
	s.categories.RLock()
	defer s.categories.RUnlock()

	c, ok := s.categories.data[id]
	return c, ok
}

// GetCategoryList returns all categories.
func (s *MemStore) GetCategoryList() []*catalog.Category {
	s.categories.RLock()
	defer s.categories.RUnlock()

	out := make([]*catalog.Category, 0, len(s.categories.data))
	for _, c := range s.categories.data {
		out = append(out, c)
	}
	return out
}

// DeleteProperty deletes a property.
func (s *MemStore) DeleteProperty(id uint32) {
	s.properties.Lock()
	defer s.properties.Unlock()

	p, ok := s.properties.dataByID[id]
	if !ok {
		return
	}

	delete(s.properties.dataByID, id)
	delete(s.properties.dataByKey, p.Key)
}

// UpsertProperty inserts a new property or replaces existing.
func (s *MemStore) UpsertProperty(p *catalog.Property) {
	s.properties.Lock()
	defer s.properties.Unlock()

	s.properties.dataByID[p.ID] = p
	s.properties.dataByKey[p.Key] = p
}

// GetProperty returns a property by id.
func (s *MemStore) GetProperty(id uint32) (*catalog.Property, bool) {
	s.properties.RLock()
	defer s.properties.RUnlock()

	p, ok := s.properties.dataByID[id]
	return p, ok
}

// GetPropertyByKey returns a property by key.
func (s *MemStore) GetPropertyByKey(key string) (*catalog.Property, bool) {
	s.properties.RLock()
	defer s.properties.RUnlock()

	p, ok := s.properties.dataByKey[key]
	return p, ok
}

// DeletePropertyVariant deletes a property variant.
func (s *MemStore) DeletePropertyVariant(id uint32) {
	s.propertyVariants.Lock()
	defer s.propertyVariants.Unlock()

	delete(s.propertyVariants.data, id)
}

// UpsertPropertyVariant inserts a new property variant or replaces existing.
func (s *MemStore) UpsertPropertyVariant(pv *catalog.PropertyVariant) {
	s.propertyVariants.Lock()
	defer s.propertyVariants.Unlock()

	s.propertyVariants.data[pv.ID] = pv
}

// GetPropertyVariant returns a property variant by id.
func (s *MemStore) GetPropertyVariant(id uint32) (*catalog.PropertyVariant, bool) {
	s.propertyVariants.RLock()
	defer s.propertyVariants.RUnlock()

	pv, ok := s.propertyVariants.data[id]
	return pv, ok
}
