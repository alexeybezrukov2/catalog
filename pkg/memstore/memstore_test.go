package memstore

import (
	"sort"
	"testing"

	"github.com/stretchr/testify/assert"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

func TestGetProductCardList(t *testing.T) {
	productCards := map[uint32]*catalog.ProductCard{
		1: mustNewProductCard(t, func(card *catalog.ProductCard) { card.ID = 1 }),
		2: mustNewProductCard(t, func(card *catalog.ProductCard) { card.ID = 2 }),
		3: mustNewProductCard(t, func(card *catalog.ProductCard) { card.ID = 3 }),
		4: mustNewProductCard(t, func(card *catalog.ProductCard) { card.ID = 4 }),
		5: mustNewProductCard(t, func(card *catalog.ProductCard) { card.ID = 5 }),
		6: mustNewProductCard(t, func(card *catalog.ProductCard) { card.ID = 6 }),
		7: mustNewProductCard(t, func(card *catalog.ProductCard) { card.ID = 7 }),
		8: mustNewProductCard(t, func(card *catalog.ProductCard) { card.ID = 8 }),
	}

	testCases := []struct {
		name string
		ids  []uint32
		want []*catalog.ProductCard
	}{
		{
			name: "normal response",
			ids:  []uint32{4, 7},
			want: []*catalog.ProductCard{
				mustNewProductCard(t, func(card *catalog.ProductCard) { card.ID = 4 }),
				mustNewProductCard(t, func(card *catalog.ProductCard) { card.ID = 7 }),
			},
		},
		{
			name: "no one",
			ids:  []uint32{32, 33},
			want: make([]*catalog.ProductCard, 0),
		},
		{
			name: "some",
			ids:  []uint32{5, 56},
			want: []*catalog.ProductCard{
				mustNewProductCard(t, func(card *catalog.ProductCard) { card.ID = 5 }),
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			s := New()
			for _, pc := range productCards {
				s.UpsertProductCard(pc)
			}

			resp := s.GetProductCardList(tc.ids)
			assert.Equal(t, tc.want, resp)
		})
	}
}

func TestGetProductCard(t *testing.T) {
	productCards := map[uint32]*catalog.ProductCard{
		1: mustNewProductCard(t, func(card *catalog.ProductCard) { card.ID = 1 }),
		2: mustNewProductCard(t, func(card *catalog.ProductCard) { card.ID = 2 }),
		3: mustNewProductCard(t, func(card *catalog.ProductCard) { card.ID = 3 }),
		4: mustNewProductCard(t, func(card *catalog.ProductCard) { card.ID = 4 }),
		5: mustNewProductCard(t, func(card *catalog.ProductCard) { card.ID = 5 }),
		6: mustNewProductCard(t, func(card *catalog.ProductCard) { card.ID = 6 }),
		7: mustNewProductCard(t, func(card *catalog.ProductCard) { card.ID = 7 }),
		8: mustNewProductCard(t, func(card *catalog.ProductCard) { card.ID = 8 }),
	}

	testCases := []struct {
		name string
		id   uint32
		want *catalog.ProductCard
		ok   bool
	}{
		{
			name: "normal response",
			id:   4,
			want: mustNewProductCard(t, func(card *catalog.ProductCard) { card.ID = 4 }),
			ok:   true,
		},
		{
			name: "not found",
			id:   33,
			want: nil,
			ok:   false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			s := New()
			for _, pc := range productCards {
				s.UpsertProductCard(pc)
			}

			got, ok := s.GetProductCard(tc.id)
			assert.Equal(t, tc.want, got)
			assert.Equal(t, tc.ok, ok)
		})
	}
}

func TestCategories(t *testing.T) {
	testCases := []struct {
		name             string
		upsert           []*catalog.Category
		delete           []uint32
		get              uint32
		wantCategory     *catalog.Category
		wantCategoryOK   bool
		wantCategoryList []*catalog.Category
	}{
		{
			name:             "empty",
			get:              2,
			wantCategory:     nil,
			wantCategoryOK:   false,
			wantCategoryList: []*catalog.Category{},
		},
		{
			name: "upsert-new",
			upsert: []*catalog.Category{
				mustNewCategory(t, func(c *catalog.Category) { c.ID = 1; c.Name = "1" }),
				mustNewCategory(t, func(c *catalog.Category) { c.ID = 2; c.Name = "2" }),
				mustNewCategory(t, func(c *catalog.Category) { c.ID = 3; c.Name = "3" }),
			},
			get:            2,
			wantCategory:   mustNewCategory(t, func(c *catalog.Category) { c.ID = 2; c.Name = "2" }),
			wantCategoryOK: true,
			wantCategoryList: []*catalog.Category{
				mustNewCategory(t, func(c *catalog.Category) { c.ID = 1; c.Name = "1" }),
				mustNewCategory(t, func(c *catalog.Category) { c.ID = 2; c.Name = "2" }),
				mustNewCategory(t, func(c *catalog.Category) { c.ID = 3; c.Name = "3" }),
			},
		},
		{
			name:           "delete",
			delete:         []uint32{2, 22, 222},
			get:            2,
			wantCategory:   nil,
			wantCategoryOK: false,
			wantCategoryList: []*catalog.Category{
				mustNewCategory(t, func(c *catalog.Category) { c.ID = 1; c.Name = "1" }),
				mustNewCategory(t, func(c *catalog.Category) { c.ID = 3; c.Name = "3" }),
			},
		},
		{
			name: "upsert-existing",
			upsert: []*catalog.Category{
				mustNewCategory(t, func(c *catalog.Category) { c.ID = 3; c.Name = "3-updated" }),
			},
			get:            3,
			wantCategory:   mustNewCategory(t, func(c *catalog.Category) { c.ID = 3; c.Name = "3-updated" }),
			wantCategoryOK: true,
			wantCategoryList: []*catalog.Category{
				mustNewCategory(t, func(c *catalog.Category) { c.ID = 1; c.Name = "1" }),
				mustNewCategory(t, func(c *catalog.Category) { c.ID = 3; c.Name = "3-updated" }),
			},
		},
	}

	s := New()
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			for _, c := range tc.upsert {
				s.UpsertCategory(c)
			}
			for _, id := range tc.delete {
				s.DeleteCategory(id)
			}

			gotCategory, gotCategoryOK := s.GetCategory(tc.get)
			assert.Equal(t, tc.wantCategory, gotCategory)
			assert.Equal(t, tc.wantCategoryOK, gotCategoryOK)

			gotCategoryList := s.GetCategoryList()
			sort.Slice(gotCategoryList, func(i, j int) bool { return gotCategoryList[i].ID < gotCategoryList[j].ID })
			sort.Slice(tc.wantCategoryList, func(i, j int) bool { return tc.wantCategoryList[i].ID < tc.wantCategoryList[j].ID })
			assert.Equal(t, tc.wantCategoryList, gotCategoryList)
		})
	}
}

func TestProperties(t *testing.T) {
	testCases := []struct {
		name   string
		upsert []*catalog.Property
		delete []uint32
		get    uint32
		want   *catalog.Property
		wantOK bool
	}{
		{
			name:   "empty",
			get:    2,
			want:   nil,
			wantOK: false,
		},
		{
			name: "upsert-new",
			upsert: []*catalog.Property{
				{ID: 1, Name: "1"},
				{ID: 2, Name: "2"},
				{ID: 3, Name: "3"},
			},
			get:    2,
			want:   &catalog.Property{ID: 2, Name: "2"},
			wantOK: true,
		},
		{
			name:   "delete",
			delete: []uint32{2, 22, 222},
			get:    2,
			want:   nil,
			wantOK: false,
		},
		{
			name: "upsert-existing",
			upsert: []*catalog.Property{
				{ID: 1, Name: "1-updated"},
			},
			get:    1,
			want:   &catalog.Property{ID: 1, Name: "1-updated"},
			wantOK: true,
		},
	}

	s := New()
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			for _, p := range tc.upsert {
				s.UpsertProperty(p)
			}
			for _, id := range tc.delete {
				s.DeleteProperty(id)
			}

			got, gotOK := s.GetProperty(tc.get)
			assert.Equal(t, tc.want, got)
			assert.Equal(t, tc.wantOK, gotOK)
		})
	}
}

func TestPropertyVariants(t *testing.T) {
	testCases := []struct {
		name   string
		upsert []*catalog.PropertyVariant
		delete []uint32
		get    uint32
		want   *catalog.PropertyVariant
		wantOK bool
	}{
		{
			name:   "empty",
			get:    2,
			want:   nil,
			wantOK: false,
		},
		{
			name: "upsert-new",
			upsert: []*catalog.PropertyVariant{
				{ID: 1, PropertyID: 1, Value: "1"},
				{ID: 2, PropertyID: 1, Value: "2"},
				{ID: 3, PropertyID: 2, Value: "3"},
				{ID: 4, PropertyID: 2, Value: "4"},
			},
			get:    2,
			want:   &catalog.PropertyVariant{ID: 2, PropertyID: 1, Value: "2"},
			wantOK: true,
		},
		{
			name:   "delete",
			delete: []uint32{2, 4},
			get:    2,
			want:   nil,
			wantOK: false,
		},
		{
			name: "upsert-existing",
			upsert: []*catalog.PropertyVariant{
				{ID: 4, PropertyID: 2, Value: "4-updated"},
			},
			get:    4,
			want:   &catalog.PropertyVariant{ID: 4, PropertyID: 2, Value: "4-updated"},
			wantOK: true,
		},
	}

	s := New()
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			for _, pv := range tc.upsert {
				s.UpsertPropertyVariant(pv)
			}
			for _, id := range tc.delete {
				s.DeletePropertyVariant(id)
			}

			got, gotOK := s.GetPropertyVariant(tc.get)
			assert.Equal(t, tc.want, got)
			assert.Equal(t, tc.wantOK, gotOK)
		})
	}
}

func mustNewProductCard(t *testing.T, fn func(card *catalog.ProductCard)) *catalog.ProductCard {
	pc := &catalog.ProductCard{
		ID: 65435,
		Properties: map[string]interface{}{
			"title": "Кроссовки ABC123",
			"price": float64(1999),
			"color": []interface{}{"red", "white"},
		},
	}

	if fn != nil {
		fn(pc)
	}

	return pc
}

func mustNewCategory(t *testing.T, fn func(c *catalog.Category)) *catalog.Category {
	c := &catalog.Category{
		ID:       1,
		ParentID: 2,
		Name:     "Sport",
		Order:    3,
		Filters: []*catalog.IndexFilter{
			{
				Type:         catalog.IndexTypeEnum,
				Key:          11,
				EnumVariants: []uint32{22, 33},
			},
		},
		FacetKeys: []uint32{4, 5, 6},
	}
	if fn != nil {
		fn(c)
	}
	return c
}
