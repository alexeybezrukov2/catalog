package filtersvc

import (
	"context"
	"errors"
	"net/http"
	"net/url"
	"time"

	"github.com/go-kit/kit/endpoint"
	kithttp "github.com/go-kit/kit/transport/http"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

// ClientConfig is an Client configuration.
type ClientConfig struct {
	ServiceURL string
	Timeout    time.Duration
}

func (cfg ClientConfig) validate() error {
	if cfg.ServiceURL == "" {
		return errors.New("must provide ServiceURL")
	}
	if cfg.Timeout <= 0 {
		return errors.New("invalid Timeout")
	}
	return nil
}

var _ Service = (*Client)(nil)

// Client is a filters service client.
type Client struct {
	getProductsEndpoint endpoint.Endpoint
}

// NewClient creates a new client.
func NewClient(cfg ClientConfig) (*Client, error) {
	err := cfg.validate()
	if err != nil {
		return nil, err
	}

	baseURL, err := url.Parse(cfg.ServiceURL)
	if err != nil {
		return nil, err
	}

	options := []kithttp.ClientOption{
		kithttp.SetClient(&http.Client{
			Timeout: cfg.Timeout,
		}),
	}

	c := &Client{
		getProductsEndpoint: kithttp.NewClient(
			http.MethodGet,
			baseURL,
			encodeGetProductsRequest,
			decodeGetProductsResponse,
			options...,
		).Endpoint(),
	}

	return c, nil
}

// GetProducts filters products.
func (c *Client) GetProducts(ctx context.Context, q *catalog.IndexQuery) (*catalog.IndexQueryResult, error) {
	response, err := c.getProductsEndpoint(ctx, getProductsRequest{query: q})
	if err != nil {
		return nil, err
	}
	return response.(getProductsResponse).result, nil
}
