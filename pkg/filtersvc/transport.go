package filtersvc

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

type getProductsRequest struct {
	query *catalog.IndexQuery
}

type getProductsResponse struct {
	result *catalog.IndexQueryResult
}

func encodeGetProductsRequest(ctx context.Context, r *http.Request, request interface{}) error {
	req := request.(getProductsRequest)
	r.URL.Path = "/api/v1/products"
	query := r.URL.Query()

	catalog.EncodeIndexFilters(query, "filter", req.query.Filters)

	query.Set("sort_key", strconv.FormatUint(uint64(req.query.SortKey), 10))
	query.Set("sort_desc", strconv.FormatBool(req.query.SortDesc))
	query.Set("limit", strconv.FormatUint(uint64(req.query.Limit), 10))
	query.Set("offset", strconv.FormatUint(uint64(req.query.Offset), 10))

	if len(req.query.FacetKeys) > 0 {
		var buf []byte
		for i, key := range req.query.FacetKeys {
			if i > 0 {
				buf = append(buf, ',')
			}
			buf = strconv.AppendUint(buf, uint64(key), 10)
		}
		query.Set("facet_keys", string(buf))
	}

	r.URL.RawQuery = query.Encode()
	return nil
}

func decodeGetProductsRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	req := getProductsRequest{query: &catalog.IndexQuery{}}
	q := r.URL.Query()

	filters, err := catalog.DecodeIndexFilters(q, "filter")
	if err != nil {
		return nil, catalog.ErrBadRequest("invalid filter parameter: %s", err)
	}
	req.query.Filters = filters

	if val := q.Get("sort_key"); val != "" {
		sortKey, err := strconv.ParseUint(val, 10, 32)
		if err != nil {
			return nil, catalog.ErrBadRequest("invalid sort_key parameter: %q", val)
		}
		req.query.SortKey = uint32(sortKey)
	}

	if val := q.Get("sort_desc"); val != "" {
		sortDesc, err := strconv.ParseBool(val)
		if err != nil {
			return nil, catalog.ErrBadRequest("invalid sort_desc parameter: %q", val)
		}
		req.query.SortDesc = sortDesc
	}

	if val := q.Get("limit"); val != "" {
		limit, err := strconv.ParseUint(val, 10, 32)
		if err != nil {
			return nil, catalog.ErrBadRequest("invalid limit parameter: %q", val)
		}
		req.query.Limit = uint32(limit)
	}

	if val := q.Get("offset"); val != "" {
		offset, err := strconv.ParseUint(val, 10, 32)
		if err != nil {
			return nil, catalog.ErrBadRequest("invalid offset parameter: %q", val)
		}
		req.query.Offset = uint32(offset)
	}

	if keysStr := q.Get("facet_keys"); keysStr != "" {
		var keys []uint32
		for _, keyStr := range strings.Split(keysStr, ",") {
			key, err := strconv.ParseUint(keyStr, 10, 32)
			if err != nil {
				return nil, catalog.ErrBadRequest("invalid filter parameter: invalid facet key: %q", keyStr)
			}
			keys = append(keys, uint32(key))
		}
		req.query.FacetKeys = keys
	}

	return req, nil
}

func encodeGetProductsResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(getProductsResponse)
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(res.result); err != nil {
		return catalog.ErrInternal("failed to encode JSON response: %s", err)
	}
	return nil
}

func decodeGetProductsResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return nil, decodeError(r)
	}
	res := getProductsResponse{result: &catalog.IndexQueryResult{}}
	if err := json.NewDecoder(r.Body).Decode(res.result); err != nil {
		return nil, fmt.Errorf("failed to decode JSON response: %w", err)
	}
	return res, nil
}

func encodeError(ctx context.Context, err error, w http.ResponseWriter) {
	e, ok := err.(*catalog.ServiceError)
	if !ok {
		e = &catalog.ServiceError{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		}
	}
	e.Encode(w)
}

func decodeError(r *http.Response) error {
	e := &catalog.ServiceError{}
	e.Decode(r)
	return e
}
