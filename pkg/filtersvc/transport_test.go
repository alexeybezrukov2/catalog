package filtersvc

import (
	"context"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

type mockService struct {
	onGetProducts func(ctx context.Context, q *catalog.IndexQuery) (*catalog.IndexQueryResult, error)
}

func (m *mockService) GetProducts(ctx context.Context, q *catalog.IndexQuery) (*catalog.IndexQueryResult, error) {
	return m.onGetProducts(ctx, q)
}

func initTransportTest(t *testing.T) (*httptest.Server, *Client, *mockService) {
	svc := &mockService{}
	handler := makeHandler(svc)
	server := httptest.NewServer(handler)
	client, err := NewClient(ClientConfig{
		ServiceURL: server.URL,
		Timeout:    time.Second,
	})
	if err != nil {
		t.Fatal(err)
	}
	return server, client, svc
}

func TestTransportGetProducts(t *testing.T) {
	server, client, svc := initTransportTest(t)
	defer server.Close()

	testCases := []struct {
		name   string
		query  *catalog.IndexQuery
		result *catalog.IndexQueryResult
		err    error
	}{
		{
			name: "ok",
			query: &catalog.IndexQuery{
				Filters: []*catalog.IndexFilter{
					{
						Type:         catalog.IndexTypeEnum,
						Key:          1,
						EnumVariants: []uint32{11},
					},
					{
						Type:         catalog.IndexTypeEnum,
						Key:          2,
						EnumVariants: []uint32{21, 22, 23},
					},
					{
						Type:     catalog.IndexTypeRange,
						Key:      3,
						RangeMin: 31,
						RangeMax: 39,
					},
				},
				SortKey:   3,
				SortDesc:  true,
				Limit:     123,
				Offset:    456,
				FacetKeys: []uint32{1, 2, 3},
			},
			result: &catalog.IndexQueryResult{
				IDs:   []uint32{1, 3, 5, 7, 9, 2, 4, 6, 8, 10},
				Total: 12345,
				Facets: []*catalog.IndexFacet{
					{
						Type: catalog.IndexTypeEnum,
						Key:  1,
						EnumVariants: []*catalog.IndexFacetVariant{
							{Value: 11, Count: 10},
						},
					},
					{
						Type: catalog.IndexTypeEnum,
						Key:  2,
						EnumVariants: []*catalog.IndexFacetVariant{
							{Value: 21, Count: 5},
							{Value: 22, Count: 3},
							{Value: 23, Count: 2},
						},
					},
					{
						Type:     catalog.IndexTypeRange,
						Key:      3,
						RangeMin: uint64ptr(123),
						RangeMax: uint64ptr(456),
					},
				},
			},
			err: nil,
		},
		{
			name: "error bad request",
			query: &catalog.IndexQuery{
				SortKey: 1,
				Limit:   10,
				Offset:  0,
			},
			result: nil,
			err:    catalog.ErrBadRequest("some validation error"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var gotQuery *catalog.IndexQuery
			svc.onGetProducts = func(ctx context.Context, q *catalog.IndexQuery) (*catalog.IndexQueryResult, error) {
				gotQuery = q
				return tc.result, tc.err
			}

			gotResult, gotErr := client.GetProducts(context.Background(), tc.query)

			assert.Equal(t, tc.err, gotErr)
			assert.Equal(t, tc.query, gotQuery)
			assert.Equal(t, tc.result, gotResult)
		})
	}
}

func uint64ptr(v uint64) *uint64 {
	return &v
}
