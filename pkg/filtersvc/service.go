package filtersvc

import (
	"context"

	"github.com/go-kit/kit/log"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

// Service provides product filtering functionality.
type Service interface {
	GetProducts(ctx context.Context, q *catalog.IndexQuery) (*catalog.IndexQueryResult, error)
}

type service struct {
	logger log.Logger
	index  Index
}

func newService(logger log.Logger, index Index) *service {
	return &service{logger: logger, index: index}
}

func (s *service) GetProducts(ctx context.Context, q *catalog.IndexQuery) (*catalog.IndexQueryResult, error) {
	if err := q.Validate(); err != nil {
		return nil, catalog.ErrBadRequest("invalid query params: %s", err)
	}
	qr, err := s.index.Query(q)
	if err != nil {
		return nil, catalog.ErrBadRequest("failed to query index: %s", err)
	}
	return qr, nil
}
