package catalog

import (
	"errors"
	"fmt"
	"time"
)

// IndexKeySchemaID is the index key used for product schema ID.
const IndexKeySchemaID = 0

const productQueryMaxLimit = 1000

// Product is a lifestyle product.
// It holds property keys & values as concrete types (strings, dates, etc).
type Product struct {
	ID         uint32             `json:"id" bson:"_id"`
	SupplierID uint32             `json:"supplier_id" bson:"supplier_id"`
	SchemaID   uint32             `json:"schema_id" bson:"schema_id"`
	Properties []*ProductProperty `json:"properties" bson:"properties"`
}

// ProductProperty describes property of a product along with its value.
type ProductProperty struct {
	ID       uint32       `json:"id" bson:"id"`
	Key      string       `json:"key" bson:"key"`
	Name     string       `json:"name" bson:"name"`
	Type     PropertyType `json:"type" bson:"type"`
	Value    interface{}  `json:"value" bson:"value"`
	ValueIDs []uint32     `json:"value_ids" bson:"value_ids"`
	Indexed  bool         `json:"indexed" bson:"indexed"`
}

// Validate validates the given product property.
func (p *ProductProperty) Validate() error {
	if p == nil {
		return errors.New("nil product property")
	}
	if p.ID == 0 {
		return errors.New("zero ID")
	}
	if p.Key == "" {
		return errors.New("empty Key")
	}
	if p.Name == "" {
		return errors.New("empty Name")
	}

	if !p.Type.IsValid() {
		return fmt.Errorf("invalid Type: %q", p.Type)
	}

	switch p.Type {
	case PropertyTypeEnum:
		if len(p.ValueIDs) != 1 {
			return fmt.Errorf("invalid count of value ids for enum property: %d", len(p.ValueIDs))
		}
		value, ok := p.Value.(string)
		if !ok {
			return fmt.Errorf("invalid enum property value type: %T", p.Value)
		}
		if value == "" {
			return errors.New("empty enum property value")
		}

	case PropertyTypeEnumList:
		if len(p.ValueIDs) == 0 {
			return errors.New("empty list of value ids for enum-list property")
		}
		values, ok := p.Value.([]interface{})
		if !ok {
			return fmt.Errorf("invalid enum-list property value type: %T", p.Value)
		}
		if len(values) != len(p.ValueIDs) {
			return fmt.Errorf("bad enum-list property value len: %d (expected %d)", len(values), len(p.ValueIDs))
		}
		for _, val := range values {
			if _, ok := val.(string); !ok {
				return fmt.Errorf("invalid enum-list property value element type: %T", val)
			}
		}

	case PropertyTypeDate:
		value, ok := p.Value.(string)
		if !ok {
			return fmt.Errorf("invalid date property value type: %T", p.Value)
		}
		date, err := time.Parse(time.RFC3339Nano, value)
		if err != nil {
			return fmt.Errorf("invalid date property value format: %q", value)
		}
		unix := date.Unix()
		if unix < 0 {
			return fmt.Errorf("negative date property value unix-time: %q", value)
		}

	case PropertyTypeInt:
		value, ok := p.Value.(float64)
		if !ok {
			return fmt.Errorf("invalid int property value type: %T", p.Value)
		}
		if value < 0 {
			return fmt.Errorf("negative int property value: %v", value)
		}

	case PropertyTypeString:
		value, ok := p.Value.(string)
		if !ok {
			return fmt.Errorf("invalid string property value type: %T", p.Value)
		}
		if value == "" {
			return errors.New("empty string property value")
		}

	case PropertyTypeStringList:
		values, ok := p.Value.([]interface{})
		if !ok {
			return fmt.Errorf("invalid string-list property value type: %T", p.Value)
		}
		if len(values) == 0 {
			return errors.New("empty string-list property value")
		}
		for _, val := range values {
			if _, ok := val.(string); !ok {
				return fmt.Errorf("invalid string-list property value element type: %T", val)
			}
		}
	}

	return nil
}

// MergeProperties merges the given list of properties into the product.
func (p *Product) MergeProperties(properties []*ProductProperty) {
loop:
	for _, newProp := range properties {
		for i, oldProp := range p.Properties {
			if oldProp.ID == newProp.ID {
				p.Properties[i] = newProp
				continue loop
			}
		}
		p.Properties = append(p.Properties, newProp)
	}
}

// ToProductCard creates a new ProductCard based on the Product.
func (p *Product) ToProductCard() *ProductCard {
	pc := &ProductCard{
		ID:         p.ID,
		Properties: make(map[string]interface{}),
	}

	pc.ApplyProductProperties(p.Properties)

	return pc
}

// ToIndexDocument creates a new index document based on the given Product.
func (p *Product) ToIndexDocument() (*IndexDocument, error) {
	doc := &IndexDocument{
		EnumFields:  make(map[uint32][]uint32),
		RangeFields: make(map[uint32]uint64),
	}

	doc.EnumFields[IndexKeySchemaID] = []uint32{p.SchemaID}

	for _, prop := range p.Properties {
		if prop.Indexed {
			switch prop.Type {
			case PropertyTypeEnum, PropertyTypeEnumList:
				doc.EnumFields[prop.ID] = prop.ValueIDs

			case PropertyTypeDate:
				value, ok := prop.Value.(string)
				if !ok {
					return nil, fmt.Errorf("invalid date property value type: %T", prop.Value)
				}
				date, err := time.Parse(time.RFC3339Nano, value)
				if err != nil {
					return nil, fmt.Errorf("invalid date property value format: %q", value)
				}
				unix := date.Unix()
				if unix < 0 {
					return nil, fmt.Errorf("negative date property value unix-time: %q", value)
				}
				doc.RangeFields[prop.ID] = uint64(unix)

			case PropertyTypeInt:
				value, ok := prop.Value.(float64)
				if !ok {
					return nil, fmt.Errorf("invalid int property value type: %T", prop.Value)
				}
				if value < 0 {
					return nil, fmt.Errorf("negative int property value: %v", value)
				}
				doc.RangeFields[prop.ID] = uint64(value)

			default:
				return nil, fmt.Errorf("unknown indexed property type: %q", prop.Type)
			}
		}
	}

	return doc, nil
}

// ProductInput is an input structure used to create a new product.
type ProductInput struct {
	SupplierID uint32                  `json:"supplier_id"`
	SchemaID   uint32                  `json:"schema_id"`
	Properties []*ProductPropertyInput `json:"properties"`
}

// Validate validates the given ProductInput.
func (p *ProductInput) Validate() error {
	if p == nil {
		return errors.New("nil product input")
	}
	if p.SupplierID == 0 {
		return errors.New("zero SupplierID")
	}
	if p.SchemaID == 0 {
		return errors.New("zero SchemaID")
	}
	for _, prop := range p.Properties {
		if prop == nil {
			return errors.New("nil property")
		}
		if prop.ID == 0 {
			return errors.New("zero property ID")
		}
	}
	return nil
}

// ProductPropertyInput is an input structure used to create a new product property.
type ProductPropertyInput struct {
	ID       uint32      `json:"id"`
	Value    interface{} `json:"value"`
	ValueIDs []uint32    `json:"value_ids"`
}

// ProductPatch is structure used to update a product.
type ProductPatch struct {
	Properties []*ProductPropertyInput `json:"properties"`
	Partial    bool                    `json:"partial"`
}

// Validate validates the given ProductPatch.
func (p *ProductPatch) Validate() error {
	if p == nil {
		return errors.New("nil product patch")
	}
	if len(p.Properties) == 0 {
		return errors.New("empty Properties")
	}
	for _, prop := range p.Properties {
		if prop == nil {
			return errors.New("nil property")
		}
		if prop.ID == 0 {
			return errors.New("zero property ID")
		}
	}
	return nil
}

// ProductCard defines properties for each product.
type ProductCard struct {
	ID         uint32                 `json:"id"`
	Properties map[string]interface{} `json:"properties"`
}

// Clone creates a deep copy of the ProductCard.
func (pc *ProductCard) Clone() *ProductCard {
	newpc := &ProductCard{
		ID:         pc.ID,
		Properties: make(map[string]interface{}),
	}
	for k, v := range pc.Properties {
		newpc.Properties[k] = v
	}
	return newpc
}

// ApplyProductProperties applies the given product's properties to product card.
func (pc *ProductCard) ApplyProductProperties(pp []*ProductProperty) {
	for _, p := range pp {
		pc.Properties[p.Key] = p.Value
	}
}

// ProductQuery describes a catalog products request.
type ProductQuery struct {
	Category uint32
	Filters  []*IndexFilter
	Sort     string
	Desc     bool
	Limit    uint32
	Offset   uint32
}

// Validate validates the product query.
func (q *ProductQuery) Validate() error {
	if q == nil {
		return errors.New("nil product query")
	}
	if q.Category == 0 {
		return errors.New("category must be positive")
	}
	for _, filter := range q.Filters {
		if filter == nil {
			return errors.New("nil filter")
		}
		if err := filter.Validate(); err != nil {
			return fmt.Errorf("invalid filter: %w", err)
		}
	}
	if q.Sort == "" {
		return errors.New("sort must be non-empty")
	}
	if q.Limit == 0 {
		return errors.New("limit must be positive")
	}
	if q.Limit > productQueryMaxLimit {
		return fmt.Errorf("limit is too large: %d", q.Limit)
	}
	return nil
}

// ProductQueryResult describes an answer to a catalog products request.
type ProductQueryResult struct {
	Products []*ProductCard   `json:"products"`
	Total    uint32           `json:"total"`
	Facets   []*PropertyFacet `json:"facets"`
}

// StorageProductQuery describes a products request to persistent storage.
type StorageProductQuery struct {
	SupplierID uint32
	SchemaID   uint32
	Limit      uint32
	Offset     uint32
}

// Validate validates the storage product query.
func (q *StorageProductQuery) Validate() error {
	if q == nil {
		return errors.New("nil storage product query")
	}
	if q.Limit == 0 {
		return errors.New("zero limit")
	}
	if q.Limit > productQueryMaxLimit {
		return fmt.Errorf("limit is too large: %d", q.Limit)
	}
	return nil
}

// StorageProductQueryResult describes an answer to StorageProductQuery.
type StorageProductQueryResult struct {
	Products []*Product `json:"products"`
	Total    int        `json:"total"`
}
