package catalog

import (
	"encoding/json"
	"errors"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestProductToIndexDocument(t *testing.T) {
	testCases := []struct {
		name    string
		product *Product
		wantDoc *IndexDocument
		wantErr error
	}{
		{
			name: "product with many properties",
			product: &Product{
				ID:       12345,
				SchemaID: 89,
				Properties: []*ProductProperty{
					{Indexed: true, Type: PropertyTypeEnumList, ID: 1, ValueIDs: []uint32{2}},
					{Indexed: false, Type: PropertyTypeEnumList, ID: 3, ValueIDs: []uint32{4}},
					{Indexed: true, Type: PropertyTypeEnumList, ID: 5, ValueIDs: []uint32{6, 7}},
					{Indexed: true, Type: PropertyTypeInt, ID: 12, Value: 34},
					{Indexed: false, Type: PropertyTypeInt, ID: 23, Value: 45},
					{Indexed: true, Type: PropertyTypeInt, ID: 34, Value: 56},
					{Indexed: true, Type: PropertyTypeDate, ID: 56, Value: time.Date(2020, 1, 2, 3, 4, 5, 6, time.UTC)},
					{Indexed: false, Type: PropertyTypeDate, ID: 67, Value: time.Date(2020, 2, 3, 4, 5, 6, 7, time.UTC)},
					{Indexed: true, Type: PropertyTypeDate, ID: 78, Value: time.Date(2020, 3, 4, 5, 6, 7, 8, time.UTC)},
					{Indexed: true, Type: PropertyTypeEnum, ID: 101, ValueIDs: []uint32{111}},
					{Indexed: false, Type: PropertyTypeEnum, ID: 102, ValueIDs: []uint32{222}},
					{Indexed: true, Type: PropertyTypeEnum, ID: 103, ValueIDs: []uint32{333}},
					{Indexed: false, Type: PropertyTypeString, ID: 1},
					{Indexed: false, Type: PropertyTypeStringList, ID: 1},
				},
			},
			wantDoc: &IndexDocument{
				EnumFields: map[uint32][]uint32{
					IndexKeySchemaID: {89},
					1:                {2},
					5:                {6, 7},
					101:              {111},
					103:              {333},
				},
				RangeFields: map[uint32]uint64{
					12: 34,
					34: 56,
					56: uint64(time.Date(2020, 1, 2, 3, 4, 5, 6, time.UTC).Unix()),
					78: uint64(time.Date(2020, 3, 4, 5, 6, 7, 8, time.UTC).Unix()),
				},
			},
		},
		{
			name: "unknown indexed type string",
			product: &Product{
				ID:         1,
				SchemaID:   2,
				Properties: []*ProductProperty{{Indexed: true, Type: PropertyTypeString, ID: 1}},
			},
			wantErr: fmt.Errorf("unknown indexed property type: %q", PropertyTypeString),
		},
		{
			name: "unknown indexed type string-list",
			product: &Product{
				ID:         1,
				SchemaID:   2,
				Properties: []*ProductProperty{{Indexed: true, Type: PropertyTypeStringList, ID: 1}},
			},
			wantErr: fmt.Errorf("unknown indexed property type: %q", PropertyTypeStringList),
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			data, err := json.Marshal(tc.product)
			if err != nil {
				t.Fatal(err)
			}

			var p Product
			if err := json.Unmarshal(data, &p); err != nil {
				t.Fatal(err)
			}

			gotDoc, gotErr := p.ToIndexDocument()

			assert.Equal(t, tc.wantDoc, gotDoc)
			assert.Equal(t, tc.wantErr, gotErr)
		})
	}
}

func TestProductPropertyValidate(t *testing.T) {
	testCases := []struct {
		name    string
		pp      *ProductProperty
		wantErr error
	}{
		{
			name:    "ok",
			pp:      mustNewProductProperty(nil),
			wantErr: nil,
		},
		{
			name:    "nil",
			pp:      nil,
			wantErr: errors.New("nil product property"),
		},
		{
			name: "zero ID",
			pp: mustNewProductProperty(func(p *ProductProperty) {
				p.ID = 0
			}),
			wantErr: errors.New("zero ID"),
		},
		{
			name: "empty key",
			pp: mustNewProductProperty(func(p *ProductProperty) {
				p.Key = ""
			}),
			wantErr: errors.New("empty Key"),
		},
		{
			name: "empty Name",
			pp: mustNewProductProperty(func(p *ProductProperty) {
				p.Name = ""
			}),
			wantErr: errors.New("empty Name"),
		},
		{
			name: "invalid type",
			pp: mustNewProductProperty(func(p *ProductProperty) {
				p.Type = "Type"
			}),
			wantErr: fmt.Errorf("invalid Type: %q", "Type"),
		},
		{
			name: "valid enum",
			pp: mustNewProductProperty(func(p *ProductProperty) {
				p.Type = PropertyTypeEnum
				p.Value = "test"
				p.ValueIDs = []uint32{123}
			}),
			wantErr: nil,
		},
		{
			name: "invalid count of value ids for enum",
			pp: mustNewProductProperty(func(p *ProductProperty) {
				p.Type = PropertyTypeEnum
				p.ValueIDs = nil
			}),
			wantErr: fmt.Errorf("invalid count of value ids for enum property: %d", 0),
		},
		{
			name: "invalid enum property value type",
			pp: mustNewProductProperty(func(p *ProductProperty) {
				p.Type = PropertyTypeEnum
				p.ValueIDs = []uint32{1}
				p.Value = float64(1488)
			}),
			wantErr: fmt.Errorf("invalid enum property value type: %T", float64(1488)),
		},
		{
			name: "empty enum property value",
			pp: mustNewProductProperty(func(p *ProductProperty) {
				p.Type = PropertyTypeEnum
				p.ValueIDs = []uint32{1}
				p.Value = ""
			}),
			wantErr: errors.New("empty enum property value"),
		},
		{
			name: "valid enum-list",
			pp: mustNewProductProperty(func(p *ProductProperty) {
				p.Type = PropertyTypeEnumList
				p.Value = []interface{}{"test1", "test2"}
				p.ValueIDs = []uint32{123, 321}
			}),
			wantErr: nil,
		},
		{
			name: "empty list of value ids for enum-list",
			pp: mustNewProductProperty(func(p *ProductProperty) {
				p.Type = PropertyTypeEnumList
				p.ValueIDs = nil
			}),
			wantErr: errors.New("empty list of value ids for enum-list property"),
		},
		{
			name: "invalid enum-list property value type",
			pp: mustNewProductProperty(func(p *ProductProperty) {
				p.Type = PropertyTypeEnumList
				p.ValueIDs = []uint32{1, 2, 4}
				p.Value = float64(1488)
			}),
			wantErr: fmt.Errorf("invalid enum-list property value type: %T", float64(1488)),
		},
		{
			name: "bad enum-list property value len",
			pp: mustNewProductProperty(func(p *ProductProperty) {
				p.Type = PropertyTypeEnumList
				p.Value = []interface{}{"Hello", "world"}
				p.ValueIDs = []uint32{1, 2, 4}
			}),
			wantErr: fmt.Errorf("bad enum-list property value len: %d (expected %d)", 2, 3),
		},
		{
			name: "bad enum-list property value element type",
			pp: mustNewProductProperty(func(p *ProductProperty) {
				p.Type = PropertyTypeEnumList
				p.Value = []interface{}{"Hello", "world", float64(123)}
				p.ValueIDs = []uint32{1, 2, 4}
			}),
			wantErr: fmt.Errorf("invalid enum-list property value element type: %T", float64(123)),
		},
		{
			name: "valid date",
			pp: mustNewProductProperty(func(p *ProductProperty) {
				p.Type = PropertyTypeDate
				p.Value = "2020-01-02T03:04:05Z"
				p.ValueIDs = nil
			}),
			wantErr: nil,
		},
		{
			name: "invalid date property value type",
			pp: mustNewProductProperty(func(p *ProductProperty) {
				p.Type = PropertyTypeDate
				p.Value = float64(1488)
			}),
			wantErr: fmt.Errorf("invalid date property value type: %T", float64(1488)),
		},
		{
			name: "invalid date property value format",
			pp: mustNewProductProperty(func(p *ProductProperty) {
				p.Type = PropertyTypeDate
				p.Value = "wrong"
			}),
			wantErr: fmt.Errorf("invalid date property value format: %q", "wrong"),
		},
		{
			name: "negative date property value unix-time",
			pp: mustNewProductProperty(func(p *ProductProperty) {
				p.Type = PropertyTypeDate
				p.Value = "1920-09-20T06:40:38+03:00"
			}),
			wantErr: fmt.Errorf("negative date property value unix-time: %q", "1920-09-20T06:40:38+03:00"),
		},
		{
			name: "valid int",
			pp: mustNewProductProperty(func(p *ProductProperty) {
				p.Type = PropertyTypeInt
				p.Value = float64(123)
				p.ValueIDs = nil
			}),
			wantErr: nil,
		},
		{
			name: "wrong int type",
			pp: mustNewProductProperty(func(p *ProductProperty) {
				p.Type = PropertyTypeInt
				p.Value = 1488
			}),
			wantErr: fmt.Errorf("invalid int property value type: %T", 1488),
		},
		{
			name: "negative int",
			pp: mustNewProductProperty(func(p *ProductProperty) {
				p.Type = PropertyTypeInt
				p.Value = float64(-1488)
			}),
			wantErr: fmt.Errorf("negative int property value: %v", float64(-1488)),
		},
		{
			name: "valid string",
			pp: mustNewProductProperty(func(p *ProductProperty) {
				p.Type = PropertyTypeString
				p.Value = "test"
				p.ValueIDs = nil
			}),
			wantErr: nil,
		},
		{
			name: "invalid string property value type",
			pp: mustNewProductProperty(func(p *ProductProperty) {
				p.Type = PropertyTypeString
				p.Value = 123
			}),
			wantErr: fmt.Errorf("invalid string property value type: %T", 123),
		},
		{
			name: "empty string property value",
			pp: mustNewProductProperty(func(p *ProductProperty) {
				p.Type = PropertyTypeString
				p.Value = ""
			}),
			wantErr: errors.New("empty string property value"),
		},
		{
			name: "valid string-list",
			pp: mustNewProductProperty(func(p *ProductProperty) {
				p.Type = PropertyTypeStringList
				p.Value = []interface{}{"a", "b", "c"}
				p.ValueIDs = nil
			}),
			wantErr: nil,
		},
		{
			name: "invalid string-list property value type",
			pp: mustNewProductProperty(func(p *ProductProperty) {
				p.Type = PropertyTypeStringList
				p.Value = 123
			}),
			wantErr: fmt.Errorf("invalid string-list property value type: %T", 123),
		},
		{
			name: "empty string-list property value",
			pp: mustNewProductProperty(func(p *ProductProperty) {
				p.Type = PropertyTypeStringList
				p.Value = ([]interface{})(nil)
			}),
			wantErr: errors.New("empty string-list property value"),
		},
		{
			name: "invalid string-list property value element type",
			pp: mustNewProductProperty(func(p *ProductProperty) {
				p.Type = PropertyTypeStringList
				p.Value = []interface{}{"test", 123.456}
			}),
			wantErr: fmt.Errorf("invalid string-list property value element type: %T", 123.456),
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			gotErr := tc.pp.Validate()
			assert.Equal(t, tc.wantErr, gotErr)
		})
	}
}

func TestProductInputValidate(t *testing.T) {
	testCases := []struct {
		name    string
		pi      *ProductInput
		wantErr error
	}{
		{
			name:    "ok",
			pi:      mustNewProductInput(nil),
			wantErr: nil,
		},
		{
			name: "zero SupplierID",
			pi: mustNewProductInput(func(p *ProductInput) {
				p.SupplierID = 0
			}),
			wantErr: errors.New("zero SupplierID"),
		},
		{
			name: "zero SchemaID",
			pi: mustNewProductInput(func(p *ProductInput) {
				p.SchemaID = 0
			}),
			wantErr: errors.New("zero SchemaID"),
		},
		{
			name: "nil property",
			pi: mustNewProductInput(func(p *ProductInput) {
				p.Properties = []*ProductPropertyInput{nil}
			}),
			wantErr: errors.New("nil property"),
		},
		{
			name: "zero property ID",
			pi: mustNewProductInput(func(p *ProductInput) {
				p.Properties = []*ProductPropertyInput{{
					ID:       0,
					Value:    1,
					ValueIDs: nil,
				}}
			}),
			wantErr: errors.New("zero property ID"),
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			gotErr := tc.pi.Validate()
			assert.Equal(t, tc.wantErr, gotErr)
		})
	}
}

func TestProductPatchValidate(t *testing.T) {
	testCases := []struct {
		name    string
		pc      *ProductPatch
		wantErr error
	}{
		{
			name:    "ok",
			pc:      mustNewProductPatch(nil),
			wantErr: nil,
		},
		{
			name: "empty Properties",
			pc: mustNewProductPatch(func(p *ProductPatch) {
				p.Properties = []*ProductPropertyInput{}
			}),
			wantErr: errors.New("empty Properties"),
		},
		{
			name: "nil property",
			pc: mustNewProductPatch(func(p *ProductPatch) {
				p.Properties = []*ProductPropertyInput{nil}
			}),
			wantErr: errors.New("nil property"),
		},
		{
			name: "zero property ID",
			pc: mustNewProductPatch(func(p *ProductPatch) {
				p.Properties = []*ProductPropertyInput{{
					ID:       0,
					Value:    1,
					ValueIDs: nil,
				}}
			}),
			wantErr: errors.New("zero property ID"),
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			gotErr := tc.pc.Validate()
			assert.Equal(t, tc.wantErr, gotErr)
		})
	}
}

func TestProductQueryValidate(t *testing.T) {
	testCases := []struct {
		name    string
		pq      *ProductQuery
		wantErr error
	}{
		{
			name:    "ok",
			pq:      mustNewProductQuery(nil),
			wantErr: nil,
		},
		{
			name: "nil filter",
			pq: mustNewProductQuery(func(p *ProductQuery) {
				p.Filters = []*IndexFilter{nil}
			}),
			wantErr: errors.New("nil filter"),
		},
		{
			name: "not positive category",
			pq: mustNewProductQuery(func(p *ProductQuery) {
				p.Category = 0
			}),
			wantErr: errors.New("category must be positive"),
		},
		{
			name: "invalid filter type",
			pq: mustNewProductQuery(func(p *ProductQuery) {
				p.Filters = []*IndexFilter{{
					Type:         "Type",
					Key:          1,
					EnumVariants: nil,
					RangeMin:     1,
					RangeMax:     4,
				}}
			}),
			wantErr: fmt.Errorf("invalid filter: %w", fmt.Errorf("invalid type: %q", "Type")),
		},
		{
			name: "invalid filter empty enum variants",
			pq: mustNewProductQuery(func(p *ProductQuery) {
				p.Filters = []*IndexFilter{{
					Type:         IndexTypeEnum,
					Key:          1,
					EnumVariants: nil,
					RangeMin:     0,
					RangeMax:     0,
				}}
			}),
			wantErr: fmt.Errorf("invalid filter: %w", errors.New("empty enum variants")),
		},
		{
			name: "invalid filter range",
			pq: mustNewProductQuery(func(p *ProductQuery) {
				p.Filters = []*IndexFilter{{
					Type:         IndexTypeRange,
					Key:          1,
					EnumVariants: nil,
					RangeMin:     22,
					RangeMax:     8,
				}}
			}),
			wantErr: fmt.Errorf("invalid filter: %w", fmt.Errorf("invalid range: %d - %d", 22, 8)),
		},
		{
			name: "empty sort",
			pq: mustNewProductQuery(func(p *ProductQuery) {
				p.Sort = ""
			}),
			wantErr: errors.New("sort must be non-empty"),
		},
		{
			name: "negative limit",
			pq: mustNewProductQuery(func(p *ProductQuery) {
				p.Limit = 0
			}),
			wantErr: errors.New("limit must be positive"),
		},
		{
			name: "big limit",
			pq: mustNewProductQuery(func(p *ProductQuery) {
				p.Limit = productQueryMaxLimit + 1
			}),
			wantErr: fmt.Errorf("limit is too large: %d", productQueryMaxLimit+1),
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			gotErr := tc.pq.Validate()
			assert.Equal(t, tc.wantErr, gotErr)
		})
	}
}

func TestStorageProductQueryValidate(t *testing.T) {
	testCases := []struct {
		name    string
		spq     *StorageProductQuery
		wantErr error
	}{
		{
			name:    "ok",
			spq:     mustNewStorageProductQuery(nil),
			wantErr: nil,
		},
		{
			name: "zero limit",
			spq: mustNewStorageProductQuery(func(p *StorageProductQuery) {
				p.Limit = 0
			}),
			wantErr: errors.New("zero limit"),
		},
		{
			name: "big limit",
			spq: mustNewStorageProductQuery(func(p *StorageProductQuery) {
				p.Limit = productQueryMaxLimit + 1
			}),
			wantErr: fmt.Errorf("limit is too large: %d", productQueryMaxLimit+1),
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			gotErr := tc.spq.Validate()
			assert.Equal(t, tc.wantErr, gotErr)
		})
	}
}

func mustNewProductProperty(fn func(p *ProductProperty)) *ProductProperty {
	pp := &ProductProperty{
		ID:       1,
		Key:      "price",
		Name:     "цена",
		Type:     PropertyTypeInt,
		Value:    float64(1488),
		ValueIDs: nil,
		Indexed:  true,
	}
	if fn != nil {
		fn(pp)
	}
	return pp
}

func mustNewProductInput(fn func(p *ProductInput)) *ProductInput {
	p := &ProductInput{
		SchemaID:   uint32(1),
		SupplierID: 2,
		Properties: []*ProductPropertyInput{
			{
				ID:       1,
				Value:    "v1",
				ValueIDs: []uint32{1},
			},
		},
	}
	if fn != nil {
		fn(p)
	}
	return p
}

func mustNewProductPatch(fn func(p *ProductPatch)) *ProductPatch {
	p := &ProductPatch{
		Properties: []*ProductPropertyInput{
			{
				ID:       2,
				Value:    "v1",
				ValueIDs: []uint32{1},
			},
		},
		Partial: true,
	}
	if fn != nil {
		fn(p)
	}
	return p
}

func mustNewProductQuery(fn func(p *ProductQuery)) *ProductQuery {
	p := &ProductQuery{
		Category: 1,
		Filters: []*IndexFilter{{
			Type:         IndexTypeRange,
			Key:          1,
			EnumVariants: nil,
			RangeMin:     1,
			RangeMax:     4,
		}},
		Sort:  "desc",
		Desc:  true,
		Limit: 1,
	}
	if fn != nil {
		fn(p)
	}
	return p
}

func mustNewStorageProductQuery(fn func(p *StorageProductQuery)) *StorageProductQuery {
	p := &StorageProductQuery{
		SupplierID: 1,
		SchemaID:   1,
		Limit:      1,
		Offset:     1,
	}
	if fn != nil {
		fn(p)
	}
	return p
}
