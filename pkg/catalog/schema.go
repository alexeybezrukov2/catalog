package catalog

import "errors"

// Schema describes a schema for a particular product type.
type Schema struct {
	ID         uint32            `json:"id" bson:"_id"`
	Name       string            `json:"name" bson:"name"`
	Properties []*SchemaProperty `json:"properties" bson:"properties"`
}

// SchemaName holds schema ID along with its Name.
type SchemaName struct {
	ID   uint32 `json:"id"`
	Name string `json:"name"`
}

// SchemaProperty is a single property of a product schema.
type SchemaProperty struct {
	ID       uint32       `json:"id" bson:"id"`
	Key      string       `json:"key" bson:"key"`
	Name     string       `json:"name" bson:"name"`
	Type     PropertyType `json:"type" bson:"type"`
	Indexed  bool         `json:"indexed" bson:"indexed"`
	Required bool         `json:"required" bson:"required"`
}

// SchemaInput is an input structure used to create a new schema.
type SchemaInput struct {
	Name       string                 `json:"name"`
	Properties []*SchemaPropertyInput `json:"properties"`
}

// Validate validates the given schema input.
func (si *SchemaInput) Validate() error {
	if si == nil {
		return errors.New("nil schema input")
	}
	if si.Name == "" {
		return errors.New("empty Name")
	}
	if len(si.Properties) == 0 {
		return errors.New("empty Properties")
	}
	for _, p := range si.Properties {
		if p == nil {
			return errors.New("nil property input")
		}
		if p.ID == 0 {
			return errors.New("zero property ID")
		}
	}
	return nil
}

// SchemaPatch is an input structure used to update a schema.
type SchemaPatch struct {
	Name       *string                `json:"name"`
	Properties []*SchemaPropertyInput `json:"properties"`
}

// Validate validates the given schema patch.
func (sp *SchemaPatch) Validate() error {
	if sp == nil {
		return errors.New("nil schema patch")
	}
	if sp.Name != nil && *sp.Name == "" {
		return errors.New("empty Name")
	}
	if sp.Properties != nil && len(sp.Properties) == 0 {
		return errors.New("empty Properties")
	}
	for _, p := range sp.Properties {
		if p == nil {
			return errors.New("nil property input")
		}
		if p.ID == 0 {
			return errors.New("zero property ID")
		}
	}
	return nil
}

// SchemaPropertyInput is an input structure used to create a new schema property.
type SchemaPropertyInput struct {
	ID       uint32 `json:"id"`
	Required bool   `json:"required"`
}
