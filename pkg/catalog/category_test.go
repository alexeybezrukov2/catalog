package catalog

import (
	"errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBuildCategoryTree(t *testing.T) {
	testCases := []struct {
		name       string
		categories []*Category
		want       []*CategoryNode
	}{
		{
			name:       "empty",
			categories: nil,
			want:       []*CategoryNode{},
		},
		{
			name: "detached",
			categories: []*Category{
				{ID: 1, ParentID: 0, Name: "connected with root"},
				{ID: 2, ParentID: 3, Name: "not connected with root"},
			},
			want: []*CategoryNode{
				{ID: 1, Name: "connected with root"},
			},
		},
		{
			name: "multi-level",
			categories: []*Category{
				{ID: 22, ParentID: 2, Name: "category22", Order: 22},
				{ID: 31, ParentID: 3, Name: "category31", Order: 31},
				{ID: 121, ParentID: 12, Name: "category121", Order: 121},
				{ID: 1, ParentID: 0, Name: "category1", Order: 1},
				{ID: 13, ParentID: 1, Name: "category13", Order: 13},
				{ID: 21, ParentID: 2, Name: "category21", Order: 21},
				{ID: 999, ParentID: 99, Name: "category999", Order: 999},
				{ID: 2, ParentID: 0, Name: "category2", Order: 2},
				{ID: 11, ParentID: 1, Name: "category11", Order: 11},
				{ID: 3, ParentID: 0, Name: "category3", Order: 3},
				{ID: 12, ParentID: 1, Name: "category12", Order: 12},
			},
			want: []*CategoryNode{
				{
					ID:   1,
					Name: "category1",
					Children: []*CategoryNode{
						{
							ID:   11,
							Name: "category11",
						},
						{
							ID:   12,
							Name: "category12",
							Children: []*CategoryNode{
								{
									ID:   121,
									Name: "category121",
								},
							},
						},
						{
							ID:   13,
							Name: "category13",
						},
					},
				},
				{
					ID:   2,
					Name: "category2",
					Children: []*CategoryNode{
						{
							ID:   21,
							Name: "category21",
						},
						{
							ID:   22,
							Name: "category22",
						},
					},
				},
				{
					ID:   3,
					Name: "category3",
					Children: []*CategoryNode{
						{
							ID:   31,
							Name: "category31",
						},
					},
				},
			},
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			got := BuildCategoryTree(tc.categories)
			assert.Equal(t, tc.want, got)
		})
	}
}

func TestCategoryPatchValidate(t *testing.T) {
	emptyName := ""
	testCases := []struct {
		name    string
		cp      *CategoryPatch
		wantErr error
	}{
		{
			name:    "ok",
			cp:      mustNewCategoryPatch(nil),
			wantErr: nil,
		},
		{
			name: "empty patch name",
			cp: mustNewCategoryPatch(func(cp *CategoryPatch) {
				cp.Name = &emptyName
			}),
			wantErr: errors.New("empty Name"),
		},
		{
			name:    "nil patch",
			cp:      nil,
			wantErr: errors.New("nil category patch"),
		},
		{
			name: "nil category filter in patch",
			cp: mustNewCategoryPatch(func(cp *CategoryPatch) {
				cp.Filters = []*IndexFilter{
					nil,
				}
			}),
			wantErr: fmt.Errorf("nil category filter"),
		},
		{
			name: "invalid category filter in patch",
			cp: mustNewCategoryPatch(func(cp *CategoryPatch) {
				cp.Filters = []*IndexFilter{
					{
						Type:         "Type",
						Key:          1,
						EnumVariants: []uint32{1},
						RangeMin:     0,
						RangeMax:     0,
					},
				}
			}),
			wantErr: fmt.Errorf("invalid filter: %w", fmt.Errorf("invalid type: %q", "Type")),
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			gotErr := tc.cp.Validate()
			assert.Equal(t, tc.wantErr, gotErr)
		})
	}
}

func TestCategoryPatchApply(t *testing.T) {
	testCases := []struct {
		name         string
		cp           *CategoryPatch
		category     *Category
		wantCategory *Category
	}{
		{
			name:     "ok",
			cp:       mustNewCategoryPatch(nil),
			category: mustNewCategory(nil),
			wantCategory: mustNewCategory(func(c *Category) {
				c.Name = *mustNewCategoryPatch(nil).Name
				c.Filters = mustNewCategoryPatch(nil).Filters
				c.FacetKeys = mustNewCategoryPatch(nil).FacetKeys
				c.Order = *mustNewCategoryPatch(nil).Order
				c.ParentID = *mustNewCategoryPatch(nil).ParentID
			}),
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			tc.category.ApplyPatch(tc.cp)
			assert.Equal(t, tc.wantCategory, tc.category)
		})
	}
}

func TestCategoryInputValidate(t *testing.T) {
	testCases := []struct {
		name    string
		ci      *CategoryInput
		wantErr error
	}{
		{
			name:    "ok",
			ci:      mustNewCategoryInput(nil),
			wantErr: nil,
		},
		{
			name: "empty patch name",
			ci: mustNewCategoryInput(func(ci *CategoryInput) {
				ci.Name = ""
			}),
			wantErr: errors.New("empty Name"),
		},
		{
			name:    "nil input",
			ci:      nil,
			wantErr: errors.New("nil category input"),
		},
		{
			name: "nil category filter in input",
			ci: mustNewCategoryInput(func(ci *CategoryInput) {
				ci.Filters = []*IndexFilter{
					nil,
				}
			}),
			wantErr: fmt.Errorf("nil category filter"),
		},
		{
			name: "invalid category filter in input",
			ci: mustNewCategoryInput(func(ci *CategoryInput) {
				ci.Filters = []*IndexFilter{
					{
						Type:         "Type",
						Key:          1,
						EnumVariants: []uint32{1},
						RangeMin:     0,
						RangeMax:     0,
					},
				}
			}),
			wantErr: fmt.Errorf("invalid filter: %w", fmt.Errorf("invalid type: %q", "Type")),
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			gotErr := tc.ci.Validate()
			assert.Equal(t, tc.wantErr, gotErr)

		})
	}
}

func mustNewCategory(fn func(c *Category)) *Category {
	c := &Category{
		ID:       1,
		ParentID: 0,
		Name:     "Name",
		Order:    0,
		Filters: []*IndexFilter{{
			Type:         "enum",
			Key:          1,
			EnumVariants: []uint32{11},
		}},
		FacetKeys: []uint32{2, 3},
	}
	if fn != nil {
		fn(c)
	}
	return c
}

func mustNewCategoryInput(fn func(ci *CategoryInput)) *CategoryInput {
	ci := &CategoryInput{
		ParentID: 0,
		Name:     "Name",
		Order:    0,
		Filters: []*IndexFilter{{
			Type:         "enum",
			Key:          1,
			EnumVariants: []uint32{11},
		}},
		FacetKeys: []uint32{2, 3},
	}
	if fn != nil {
		fn(ci)
	}
	return ci
}

func mustNewCategoryPatch(fn func(cp *CategoryPatch)) *CategoryPatch {
	parent := uint32(12)
	name := "PatchName"
	order := uint32(1)
	cp := &CategoryPatch{
		ParentID: &parent,
		Name:     &name,
		Order:    &order,
		Filters: []*IndexFilter{{
			Type:         "enum",
			Key:          1,
			EnumVariants: []uint32{121},
		}},
		FacetKeys: []uint32{12, 13},
	}
	if fn != nil {
		fn(cp)
	}
	return cp
}
