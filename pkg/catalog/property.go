package catalog

import (
	"errors"
	"fmt"
)

// Property describes a product property.
type Property struct {
	ID      uint32       `json:"id" bson:"_id"`
	Key     string       `json:"key" bson:"key"`
	Name    string       `json:"name" bson:"name"`
	Type    PropertyType `json:"type" bson:"type"`
	Indexed bool         `json:"indexed" bson:"indexed"`
}

// PropertyInput is an input structure used to create a new property.
type PropertyInput struct {
	Key     string       `json:"key" bson:"key"`
	Name    string       `json:"name" bson:"name"`
	Type    PropertyType `json:"type" bson:"type"`
	Indexed bool         `json:"indexed" bson:"indexed"`
}

// Validate validates the given PropertyInput structure.
func (pi *PropertyInput) Validate() error {
	if pi == nil {
		return errors.New("nil property input")
	}
	if pi.Key == "" {
		return errors.New("empty Key")
	}
	if pi.Name == "" {
		return errors.New("empty Name")
	}
	if !pi.Type.IsValid() {
		return fmt.Errorf("invalid Type: %q", pi.Type)
	}
	return nil
}

// PropertyVariant describes a single value of an enum property.
type PropertyVariant struct {
	ID         uint32 `json:"id" bson:"_id"`
	PropertyID uint32 `json:"property_id" bson:"property_id"`
	Value      string `json:"value" bson:"value"`
}

// PropertyVariantInput is an input structure used to create a new property variant.
type PropertyVariantInput struct {
	PropertyID uint32 `json:"property_id" bson:"property_id"`
	Value      string `json:"value" bson:"value"`
}

// Validate validates the given PropertyVariantInput structure.
func (pvi *PropertyVariantInput) Validate() error {
	if pvi == nil {
		return errors.New("nil property variant input")
	}
	if pvi.PropertyID == 0 {
		return errors.New("PropertyID must be positive")
	}
	if pvi.Value == "" {
		return errors.New("empty Value")
	}
	return nil
}

// PropertyType is the type of a product property.
type PropertyType string

// Supported property types.
const (
	PropertyTypeEnum       PropertyType = "enum"
	PropertyTypeEnumList   PropertyType = "enum_list"
	PropertyTypeInt        PropertyType = "int"
	PropertyTypeDate       PropertyType = "date"
	PropertyTypeString     PropertyType = "string"
	PropertyTypeStringList PropertyType = "string_list"
)

// IsValid checks if the given PropertyType is valid.
func (pt PropertyType) IsValid() bool {
	return pt == PropertyTypeEnum ||
		pt == PropertyTypeEnumList ||
		pt == PropertyTypeInt ||
		pt == PropertyTypeDate ||
		pt == PropertyTypeString ||
		pt == PropertyTypeStringList
}

// PropertyFacet provides information on the available values of a property filter.
type PropertyFacet struct {
	ID           uint32                  `json:"id"`
	Name         string                  `json:"name"`
	Type         PropertyType            `json:"type"`
	EnumVariants []*PropertyFacetVariant `json:"enum_variants,omitempty"`
	RangeMin     *uint64                 `json:"range_min,omitempty"`
	RangeMax     *uint64                 `json:"range_max,omitempty"`
}

// PropertyFacetVariant holds a value id and the count of products having the given property value.
type PropertyFacetVariant struct {
	ID    uint32 `json:"id"`
	Value string `json:"value"`
	Count uint32 `json:"count"`
}
