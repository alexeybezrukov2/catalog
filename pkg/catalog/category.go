package catalog

import (
	"errors"
	"fmt"
	"sort"
)

// Category is a single catalog menu tree item.
type Category struct {
	ID        uint32         `json:"id" bson:"_id"`
	ParentID  uint32         `json:"parent_id" bson:"parent_id"`
	Name      string         `json:"name" bson:"name"`
	Order     uint32         `json:"order" bson:"order"`
	Filters   []*IndexFilter `json:"filters" bson:"filters"`
	FacetKeys []uint32       `json:"facet_keys" bson:"facet_keys"`
}

// CategoryInput is an input structure used to create a new category.
type CategoryInput struct {
	ParentID  uint32         `json:"parent_id" bson:"parent_id"`
	Name      string         `json:"name" bson:"name"`
	Order     uint32         `json:"order" bson:"order"`
	Filters   []*IndexFilter `json:"filters" bson:"filters"`
	FacetKeys []uint32       `json:"facet_keys" bson:"facet_keys"`
}

type CategoryPatch struct {
	ParentID  *uint32        `json:"parent_id" bson:"parent_id"`
	Name      *string        `json:"name" bson:"name"`
	Order     *uint32        `json:"order" bson:"order"`
	Filters   []*IndexFilter `json:"filters" bson:"filters"`
	FacetKeys []uint32       `json:"facet_keys" bson:"facet_keys"`
}

// ApplyPatch updates Category according to existing CategoryPatch fields
func (c *Category) ApplyPatch(p *CategoryPatch) {
	if p.ParentID != nil {
		c.ParentID = *p.ParentID
	}
	if p.Name != nil {
		c.Name = *p.Name
	}
	if p.Order != nil {
		c.Order = *p.Order
	}
	if p.Filters != nil {
		c.Filters = p.Filters
	}
	if p.FacetKeys != nil {
		c.FacetKeys = p.FacetKeys
	}
}

// Validate validates the given CategoryInput structure.
func (ci *CategoryInput) Validate() error {
	if ci == nil {
		return errors.New("nil category input")
	}
	if ci.Name == "" {
		return errors.New("empty Name")
	}
	if err := validateFilters(ci.Filters); err != nil {
		return err
	}
	return nil
}

// Validate validates the given CategoryPatch structure.
func (cp *CategoryPatch) Validate() error {
	if cp == nil {
		return errors.New("nil category patch")
	}
	if cp.Name != nil && *cp.Name == "" {
		return errors.New("empty Name")
	}
	if err := validateFilters(cp.Filters); err != nil {
		return err
	}
	return nil
}

func validateFilters(ff []*IndexFilter) error {
	for _, f := range ff {
		if f == nil {
			return fmt.Errorf("nil category filter")
		}
		if err := f.Validate(); err != nil {
			return fmt.Errorf("invalid filter: %w", err)
		}
	}
	return nil
}

// CategoryNode is a single node in the category tree.
type CategoryNode struct {
	ID       uint32          `json:"id"`
	Name     string          `json:"name"`
	Children []*CategoryNode `json:"children,omitempty"`
}

// BuildCategoryTree builds a category tree from the given categories
// and returns all the top-level nodes of the tree.
func BuildCategoryTree(categories []*Category) []*CategoryNode {
	categories = sortCategories(categories)

	nodes := make(map[uint32]*CategoryNode, len(categories)+1)
	nodes[0] = &CategoryNode{Children: []*CategoryNode{}}

	for _, c := range categories {
		nodes[c.ID] = &CategoryNode{
			ID:   c.ID,
			Name: c.Name,
		}
	}
	for _, c := range categories {
		if parent, ok := nodes[c.ParentID]; ok {
			parent.Children = append(parent.Children, nodes[c.ID])
		}
	}

	return nodes[0].Children
}

func sortCategories(categories []*Category) []*Category {
	sorted := make([]*Category, len(categories))
	copy(sorted, categories)

	sort.Slice(sorted, func(i, j int) bool {
		if sorted[i].Order == sorted[j].Order {
			return sorted[i].Name < sorted[j].Name
		}
		return sorted[i].Order < sorted[j].Order
	})

	return sorted
}
