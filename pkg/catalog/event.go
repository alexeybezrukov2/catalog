package catalog

import (
	"errors"
	"fmt"
)

// Event holds the operation to be processed and provides
// the Ack method to confirm it successful processing.
type Event interface {
	Operation() *Operation
	Sequence() uint64
	Ack() error
}

// Operation is a container with event data. When changes occur on one of the replicas,
// it packs them into Operation and shares them with others replicas.
type Operation struct {
	// Type defines the type of options that event contains.
	Type OperationType `json:"type"`
	// Method defines the options processing method.
	Method OperationMethod `json:"method"`
	// Product contains product info that need to be processed if Type is OperationTypeProduct.
	Product *Product `json:"product,omitempty"`
	// Property contains information that need to be processed if Type is OperationTypeProperty.
	Property *Property `json:"property,omitempty"`
	// PropertyVariant contains information that need to be processed if Type is OperationTypePropertyVariant.
	PropertyVariant *PropertyVariant `json:"property_variant,omitempty"`
	// Category contains information that need to be processed if Type is OperationTypeCategory.
	Category *Category `json:"category,omitempty"`
}

// Validate validates the given operation.
func (op *Operation) Validate() error {
	if op == nil {
		return errors.New("nil operation")
	}
	if !op.Type.IsValid() {
		return fmt.Errorf("invalid type: %q", op.Type)
	}
	if !op.Method.IsValid() {
		return fmt.Errorf("invalid method: %q", op.Method)
	}
	switch op.Type {
	case OperationTypeProduct:
		if op.Product == nil {
			return errors.New("nil product")
		}
	case OperationTypeProperty:
		if op.Property == nil {
			return errors.New("nil property")
		}
	case OperationTypePropertyVariant:
		if op.PropertyVariant == nil {
			return errors.New("nil property variant")
		}
	case OperationTypeCategory:
		if op.Category == nil {
			return errors.New("nil category")
		}
	}
	return nil
}

// OperationType defines event options type.
type OperationType string

// Supported operation types.
const (
	OperationTypeProduct         OperationType = "product"
	OperationTypeProperty        OperationType = "property"
	OperationTypePropertyVariant OperationType = "property_variant"
	OperationTypeCategory        OperationType = "category"
)

// IsValid checks if OperationType is valid.
func (t OperationType) IsValid() bool {
	return t == OperationTypeProduct ||
		t == OperationTypeProperty ||
		t == OperationTypePropertyVariant ||
		t == OperationTypeCategory
}

// OperationMethod defines the allowed write methods.
type OperationMethod string

// Supported operation methods.
const (
	OperationMethodUpsert OperationMethod = "upsert"
	OperationMethodPatch  OperationMethod = "patch"
	OperationMethodDelete OperationMethod = "delete"
)

// IsValid checks if OperationMethod is valid.
func (m OperationMethod) IsValid() bool {
	return m == OperationMethodUpsert || m == OperationMethodPatch || m == OperationMethodDelete
}
