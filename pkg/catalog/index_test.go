package catalog

import (
	"math"
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEncodeDecodeIndexFilters(t *testing.T) {
	param := "test-param"

	testCases := []struct {
		name    string
		filters []*IndexFilter
		query   url.Values
	}{
		{
			name:    "empty",
			filters: nil,
			query:   url.Values{},
		},
		{
			name: "non-empty",
			filters: []*IndexFilter{
				{
					Type:         IndexTypeEnum,
					Key:          1,
					EnumVariants: []uint32{2},
				},
				{
					Type:         IndexTypeEnum,
					Key:          3,
					EnumVariants: []uint32{4, 5, 6, 7},
				},
				{
					Type:         IndexTypeEnum,
					Key:          math.MaxUint32,
					EnumVariants: []uint32{0, math.MaxUint32},
				},
				{
					Type:     IndexTypeRange,
					Key:      8,
					RangeMin: 9,
					RangeMax: 10,
				},
				{
					Type:     IndexTypeRange,
					Key:      math.MaxUint32,
					RangeMin: 0,
					RangeMax: math.MaxUint64,
				},
			},
			query: url.Values{
				param: {
					"1:2",
					"3:4,5,6,7",
					"4294967295:0,4294967295",
					"8:9-10",
					"4294967295:0-18446744073709551615",
				},
			},
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			query := url.Values{}
			EncodeIndexFilters(query, param, tc.filters)
			filters, err := DecodeIndexFilters(query, param)
			assert.NoError(t, err)
			assert.Equal(t, tc.query, query)
			assert.Equal(t, tc.filters, filters)
		})
	}
}
