package catalog

import (
	"errors"
	"fmt"
	"net/url"
	"strconv"
	"strings"
)

const indexQueryMaxLimit = 1000

// IndexType is the type of a document field index.
type IndexType string

// Supported index types.
const (
	IndexTypeEnum  IndexType = "enum"
	IndexTypeRange IndexType = "range"
)

// IsValid checks if the given index type is valid.
func (t IndexType) IsValid() bool {
	return t == IndexTypeEnum || t == IndexTypeRange
}

// IndexMapping describes the type of indexing for a particular document key.
type IndexMapping struct {
	Key  uint32
	Type IndexType
}

// Validate validates the index mapping.
func (m *IndexMapping) Validate() error {
	if m == nil {
		return errors.New("nil index mapping")
	}
	if !m.Type.IsValid() {
		return fmt.Errorf("invalid type: %q", m.Type)
	}
	return nil
}

// IndexDocument is a single document to be indexed.
type IndexDocument struct {
	EnumFields  map[uint32][]uint32
	RangeFields map[uint32]uint64
}

// IndexQuery is a query structure used to get matched document ids and facets from the index.
type IndexQuery struct {
	Filters   []*IndexFilter `json:"filters"`
	SortKey   uint32         `json:"sort_key"`
	SortDesc  bool           `json:"sort_desc"`
	Limit     uint32         `json:"limit"`
	Offset    uint32         `json:"offset"`
	FacetKeys []uint32       `json:"facet_keys"`
}

// Validate validates the index query.
func (q *IndexQuery) Validate() error {
	if q == nil {
		return errors.New("nil index query")
	}
	for _, filter := range q.Filters {
		if err := filter.Validate(); err != nil {
			return fmt.Errorf("invalid filter: %w", err)
		}
	}
	if q.SortKey == 0 {
		return errors.New("sort key must be positive")
	}
	if q.Limit == 0 {
		return errors.New("limit must be positive")
	}
	if q.Limit > indexQueryMaxLimit {
		return fmt.Errorf("limit is too large: %d", q.Limit)
	}
	return nil
}

// IndexFilter is a document index filter.
type IndexFilter struct {
	Type         IndexType `json:"type"`
	Key          uint32    `json:"key"`
	EnumVariants []uint32  `json:"enum_variants,omitempty"`
	RangeMin     uint64    `json:"range_min,omitempty"`
	RangeMax     uint64    `json:"range_max,omitempty"`
}

// Validate validates the index filter.
func (f *IndexFilter) Validate() error {
	if f == nil {
		return errors.New("nil index filter")
	}
	if !f.Type.IsValid() {
		return fmt.Errorf("invalid type: %q", f.Type)
	}
	switch f.Type {
	case IndexTypeEnum:
		if len(f.EnumVariants) == 0 {
			return errors.New("empty enum variants")
		}
	case IndexTypeRange:
		if f.RangeMin > f.RangeMax {
			return fmt.Errorf("invalid range: %d - %d", f.RangeMin, f.RangeMax)
		}
	}
	return nil
}

// EncodeIndexFilters encodes the given filters into the URL query.
func EncodeIndexFilters(query url.Values, param string, filters []*IndexFilter) {
	for _, filter := range filters {
		var buf []byte
		buf = strconv.AppendUint(buf, uint64(filter.Key), 10)
		buf = append(buf, ':')
		switch filter.Type {
		case IndexTypeEnum:
			for i, val := range filter.EnumVariants {
				if i > 0 {
					buf = append(buf, ',')
				}
				buf = strconv.AppendUint(buf, uint64(val), 10)
			}
		case IndexTypeRange:
			buf = strconv.AppendUint(buf, filter.RangeMin, 10)
			buf = append(buf, '-')
			buf = strconv.AppendUint(buf, filter.RangeMax, 10)
		}
		query.Add(param, string(buf))
	}
}

// DecodeIndexFilters decodes index filters from the URL query.
func DecodeIndexFilters(query url.Values, param string) ([]*IndexFilter, error) {
	var filters []*IndexFilter
	for _, filter := range query[param] {
		kv := strings.SplitN(filter, ":", 2)
		if len(kv) != 2 {
			return nil, fmt.Errorf("could not split filter value: %q", filter)
		}
		key, err := strconv.ParseUint(kv[0], 10, 32)
		if err != nil {
			return nil, fmt.Errorf("invalid filter key: %q", kv[0])
		}
		valsStr := kv[1]

		switch {
		// Check if it's a range filter (must contain a "-" character).
		case strings.Contains(valsStr, "-"):
			rangeVals := strings.SplitN(valsStr, "-", 2)
			min, err := strconv.ParseUint(rangeVals[0], 10, 64)
			if err != nil {
				return nil, fmt.Errorf("invalid range min value: %q", rangeVals[0])
			}
			max, err := strconv.ParseUint(rangeVals[1], 10, 64)
			if err != nil {
				return nil, fmt.Errorf("invalid range max value: %q", rangeVals[1])
			}
			filters = append(filters, &IndexFilter{
				Type:     IndexTypeRange,
				Key:      uint32(key),
				RangeMin: min,
				RangeMax: max,
			})

		// Otherwise its an enum filter.
		default:
			var variants []uint32
			for _, valStr := range strings.Split(valsStr, ",") {
				val, err := strconv.ParseUint(valStr, 10, 32)
				if err != nil {
					return nil, fmt.Errorf("invalid enum variant value: %q", valStr)
				}
				variants = append(variants, uint32(val))
			}
			filters = append(filters, &IndexFilter{
				Type:         IndexTypeEnum,
				Key:          uint32(key),
				EnumVariants: variants,
			})
		}
	}
	return filters, nil
}

// IndexQueryResult holds the result of an index query.
type IndexQueryResult struct {
	IDs    []uint32      `json:"ids"`
	Total  uint32        `json:"total"`
	Facets []*IndexFacet `json:"facets"`
}

// IndexFacet provides information on the available values for a filter.
type IndexFacet struct {
	Type         IndexType            `json:"type"`
	Key          uint32               `json:"key"`
	EnumVariants []*IndexFacetVariant `json:"enum_variants,omitempty"`
	RangeMin     *uint64              `json:"range_min,omitempty"`
	RangeMax     *uint64              `json:"range_max,omitempty"`
}

// IndexFacetVariant holds a value id and the count of products having the given property value.
type IndexFacetVariant struct {
	Value uint32 `json:"value"`
	Count uint32 `json:"count"`
}
