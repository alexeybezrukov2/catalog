package catalogsvc

import (
	"context"
	"strconv"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/go-kit/kit/metrics"
	kitprometheus "github.com/go-kit/kit/metrics/prometheus"
	"github.com/prometheus/client_golang/prometheus"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

// InstrumentingMiddleware wraps Service and records metrics.
type InstrumentingMiddleware struct {
	svc       Service
	histogram metrics.Histogram
}

func NewInstrumentingMiddleware(svc Service, prefix string) *InstrumentingMiddleware {
	return &InstrumentingMiddleware{
		svc: svc,
		histogram: kitprometheus.NewHistogramFrom(
			prometheus.HistogramOpts{
				Name:    prefix + "_requests",
				Buckets: prometheus.ExponentialBuckets(0.001, 2, 14),
			},
			[]string{"method", "error"},
		),
	}
}

func (mw *InstrumentingMiddleware) GetCategories(ctx context.Context) (categories []*catalog.CategoryNode, err error) {
	defer mw.record(time.Now(), "GetCategories", &err)
	return mw.svc.GetCategories(ctx)
}

func (mw *InstrumentingMiddleware) GetProducts(ctx context.Context, query *catalog.ProductQuery) (r *catalog.ProductQueryResult, err error) {
	defer mw.record(time.Now(), "GetProducts", &err)
	return mw.svc.GetProducts(ctx, query)
}

func (mw *InstrumentingMiddleware) GetProduct(ctx context.Context, id uint32) (p *catalog.ProductCard, err error) {
	defer mw.record(time.Now(), "GetProduct", &err)
	return mw.svc.GetProduct(ctx, id)
}

func (mw *InstrumentingMiddleware) record(beginTime time.Time, method string, err *error) {
	labels := []string{"method", method, "error", strconv.FormatBool(*err != nil)}
	mw.histogram.With(labels...).Observe(time.Since(beginTime).Seconds())
}

// LoggingMiddleware wraps Service and logs errors.
type LoggingMiddleware struct {
	svc    Service
	logger log.Logger
}

func NewLoggingMiddleware(svc Service, logger log.Logger) *LoggingMiddleware {
	return &LoggingMiddleware{
		svc:    svc,
		logger: logger,
	}
}

func (mw *LoggingMiddleware) GetCategories(ctx context.Context) (categories []*catalog.CategoryNode, err error) {
	defer mw.log(time.Now(), "GetCategories", &err)
	return mw.svc.GetCategories(ctx)
}

func (mw *LoggingMiddleware) GetProducts(ctx context.Context, query *catalog.ProductQuery) (r *catalog.ProductQueryResult, err error) {
	defer mw.log(time.Now(), "GetProducts", &err)
	return mw.svc.GetProducts(ctx, query)
}

func (mw *LoggingMiddleware) GetProduct(ctx context.Context, id uint32) (p *catalog.ProductCard, err error) {
	defer mw.log(time.Now(), "GetProduct", &err)
	return mw.svc.GetProduct(ctx, id)
}

func (mw *LoggingMiddleware) log(beginTime time.Time, method string, err *error) {
	if *err != nil {
		level.Error(mw.logger).Log("method", method, "err", *err, "took", time.Since(beginTime))
	}
}
