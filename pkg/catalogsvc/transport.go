package catalogsvc

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

type getCategoriesRequest struct {
}

type getCategoriesResponse struct {
	categories []*catalog.CategoryNode
}

func encodeGetCategoriesRequest(ctx context.Context, r *http.Request, request interface{}) error {
	r.URL.Path = "/api/v1/categories"
	return nil
}

func decodeGetCategoriesRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	return getCategoriesRequest{}, nil
}

func encodeGetCategoriesResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(getCategoriesResponse)
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(res.categories); err != nil {
		return catalog.ErrInternal("failed to encode JSON response: %s", err)
	}

	return nil
}

func decodeGetCategoriesResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return nil, decodeError(r)
	}
	res := getCategoriesResponse{}
	if err := json.NewDecoder(r.Body).Decode(&res.categories); err != nil {
		return nil, fmt.Errorf("failed to decode JSON response: %w", err)
	}
	return res, nil
}

type getProductsRequest struct {
	query *catalog.ProductQuery
}

type getProductsResponse struct {
	result *catalog.ProductQueryResult
}

func encodeGetProductsRequest(ctx context.Context, r *http.Request, request interface{}) error {
	req := request.(getProductsRequest)
	r.URL.Path = "/api/v1/products"
	query := r.URL.Query()

	query.Set("category", strconv.FormatUint(uint64(req.query.Category), 10))

	catalog.EncodeIndexFilters(query, "filter", req.query.Filters)

	query.Set("sort", req.query.Sort)
	query.Set("desc", strconv.FormatBool(req.query.Desc))
	query.Set("limit", strconv.FormatUint(uint64(req.query.Limit), 10))
	query.Set("offset", strconv.FormatUint(uint64(req.query.Offset), 10))

	r.URL.RawQuery = query.Encode()
	return nil
}

func decodeGetProductsRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	req := getProductsRequest{query: &catalog.ProductQuery{}}
	q := r.URL.Query()

	if val := q.Get("category"); val != "" {
		category, err := strconv.ParseUint(val, 10, 32)
		if err != nil {
			return nil, catalog.ErrBadRequest("invalid category parameter: %q", val)
		}
		req.query.Category = uint32(category)
	}

	filters, err := catalog.DecodeIndexFilters(q, "filter")
	if err != nil {
		return nil, catalog.ErrBadRequest("invalid filter parameter: %s", err)
	}
	req.query.Filters = filters

	if val := q.Get("sort"); val != "" {
		req.query.Sort = val
	}

	if val := q.Get("desc"); val != "" {
		desc, err := strconv.ParseBool(val)
		if err != nil {
			return nil, catalog.ErrBadRequest("invalid desc parameter: %q", val)
		}
		req.query.Desc = desc
	}

	if val := q.Get("limit"); val != "" {
		limit, err := strconv.ParseUint(val, 10, 32)
		if err != nil {
			return nil, catalog.ErrBadRequest("invalid limit parameter: %q", val)
		}
		req.query.Limit = uint32(limit)
	}

	if val := q.Get("offset"); val != "" {
		offset, err := strconv.ParseUint(val, 10, 32)
		if err != nil {
			return nil, catalog.ErrBadRequest("invalid offset parameter: %q", val)
		}
		req.query.Offset = uint32(offset)
	}

	return req, nil
}

func encodeGetProductsResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(getProductsResponse)
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(res.result); err != nil {
		return catalog.ErrInternal("failed to encode JSON response: %s", err)
	}
	return nil
}

func decodeGetProductsResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return nil, decodeError(r)
	}
	res := getProductsResponse{}
	if err := json.NewDecoder(r.Body).Decode(&res.result); err != nil {
		return nil, fmt.Errorf("failed to decode JSON response: %w", err)
	}
	return res, nil
}

type getProductRequest struct {
	id uint32
}

type getProductResponse struct {
	product *catalog.ProductCard
}

func encodeGetProductRequest(ctx context.Context, r *http.Request, request interface{}) error {
	req := request.(getProductRequest)
	r.URL.Path = "/api/v1/products/" + strconv.FormatUint(uint64(req.id), 10)
	return nil
}

func decodeGetProductRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 32)
	if err != nil {
		return nil, catalog.ErrBadRequest("failed to parse product id: %v", err)
	}
	return getProductRequest{id: uint32(id)}, nil
}

func encodeGetProductResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(getProductResponse)
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(res.product); err != nil {
		return catalog.ErrInternal("failed to encode JSON response: %s", err)
	}
	return nil
}

func decodeGetProductResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return nil, decodeError(r)
	}
	res := getProductResponse{}
	if err := json.NewDecoder(r.Body).Decode(&res.product); err != nil {
		return nil, fmt.Errorf("failed to decode JSON response: %w", err)
	}
	return res, nil
}

func encodeError(ctx context.Context, err error, w http.ResponseWriter) {
	e, ok := err.(*catalog.ServiceError)
	if !ok {
		e = &catalog.ServiceError{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		}
	}
	e.Encode(w)
}

func decodeError(r *http.Response) error {
	e := &catalog.ServiceError{}
	e.Decode(r)
	return e
}
