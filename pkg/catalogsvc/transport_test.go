package catalogsvc

import (
	"context"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

type mockService struct {
	onGetCategories func(ctx context.Context) ([]*catalog.CategoryNode, error)
	onGetProducts   func(ctx context.Context, query *catalog.ProductQuery) (*catalog.ProductQueryResult, error)
	onGetProduct    func(ctx context.Context, id uint32) (*catalog.ProductCard, error)
}

func (m *mockService) GetCategories(ctx context.Context) ([]*catalog.CategoryNode, error) {
	return m.onGetCategories(ctx)
}

func (m *mockService) GetProducts(ctx context.Context, query *catalog.ProductQuery) (*catalog.ProductQueryResult, error) {
	return m.onGetProducts(ctx, query)
}

func (m *mockService) GetProduct(ctx context.Context, id uint32) (*catalog.ProductCard, error) {
	return m.onGetProduct(ctx, id)
}

func initTransportTest(t *testing.T) (*httptest.Server, *Client, *mockService) {
	svc := &mockService{}
	handler := makeHandler(svc)
	server := httptest.NewServer(handler)
	client, err := NewClient(ClientConfig{
		ServiceURL: server.URL,
		Timeout:    time.Second,
	})
	if err != nil {
		t.Fatal(err)
	}
	return server, client, svc
}

func TestTransportGetCategories(t *testing.T) {
	server, client, svc := initTransportTest(t)
	defer server.Close()

	testCases := []struct {
		name   string
		result []*catalog.CategoryNode
		err    error
	}{
		{
			name: "ok",
			result: []*catalog.CategoryNode{
				{
					ID:   1,
					Name: "Стиль жизни",
					Children: []*catalog.CategoryNode{
						{
							ID:   2,
							Name: "Путешествия",
						},
						{
							ID:   3,
							Name: "Образование",
							Children: []*catalog.CategoryNode{
								{
									ID:   4,
									Name: "Мастер-класс",
								},
								{
									ID:   5,
									Name: "Семинар",
								},
							},
						},
					},
				},
			},
			err: nil,
		},
		{
			name:   "error bad request",
			result: nil,
			err:    catalog.ErrBadRequest("some validation error"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			svc.onGetCategories = func(ctx context.Context) ([]*catalog.CategoryNode, error) {
				return tc.result, tc.err
			}

			gotResult, gotErr := client.GetCategories(context.Background())

			assert.Equal(t, tc.err, gotErr)
			assert.Equal(t, tc.result, gotResult)
		})
	}
}

func TestTransportGetProducts(t *testing.T) {
	server, client, svc := initTransportTest(t)
	defer server.Close()

	testCases := []struct {
		name   string
		query  *catalog.ProductQuery
		result *catalog.ProductQueryResult
		err    error
	}{
		{
			name: "ok",
			query: &catalog.ProductQuery{
				Category: 321,
				Filters: []*catalog.IndexFilter{
					{
						Type:         catalog.IndexTypeEnum,
						Key:          1,
						EnumVariants: []uint32{11},
					},
					{
						Type:         catalog.IndexTypeEnum,
						Key:          2,
						EnumVariants: []uint32{21, 22, 23},
					},
					{
						Type:     catalog.IndexTypeRange,
						Key:      3,
						RangeMin: 31,
						RangeMax: 39,
					},
				},
				Sort:   "popularity",
				Desc:   true,
				Limit:  123,
				Offset: 456,
			},
			result: &catalog.ProductQueryResult{
				Products: []*catalog.ProductCard{
					mustNewProductCard(t, func(card *catalog.ProductCard) { card.ID = 1 }),
					mustNewProductCard(t, func(card *catalog.ProductCard) { card.ID = 2 }),
				},
				Total: 12345,
				Facets: []*catalog.PropertyFacet{
					{
						ID:   1,
						Name: "color",
						Type: catalog.PropertyTypeEnumList,
						EnumVariants: []*catalog.PropertyFacetVariant{
							{ID: 1, Value: "blue", Count: 10},
							{ID: 2, Value: "green", Count: 20},
							{ID: 3, Value: "red", Count: 30},
						},
					},
					{
						ID:       2,
						Name:     "price",
						Type:     catalog.PropertyTypeInt,
						RangeMin: uint64ptr(10),
						RangeMax: uint64ptr(1000),
					},
				},
			},
			err: nil,
		},
		{
			name:   "error bad request",
			query:  &catalog.ProductQuery{},
			result: nil,
			err:    catalog.ErrBadRequest("some validation error"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var gotQuery *catalog.ProductQuery
			svc.onGetProducts = func(ctx context.Context, query *catalog.ProductQuery) (*catalog.ProductQueryResult, error) {
				gotQuery = query
				return tc.result, tc.err
			}

			gotResult, gotErr := client.GetProducts(context.Background(), tc.query)

			assert.Equal(t, tc.err, gotErr)
			assert.Equal(t, tc.query, gotQuery)
			assert.Equal(t, tc.result, gotResult)
		})
	}
}

func TestTransportGetProduct(t *testing.T) {
	server, client, svc := initTransportTest(t)
	defer server.Close()

	testCases := []struct {
		name    string
		id      uint32
		product *catalog.ProductCard
		err     error
	}{
		{
			name:    "ok",
			id:      8943242,
			product: mustNewProductCard(t, nil),
			err:     nil,
		},
		{
			name:    "error not found",
			id:      589954,
			product: nil,
			err:     catalog.ErrNotFound("no such product id: %d", 589954),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var gotID uint32
			svc.onGetProduct = func(ctx context.Context, id uint32) (*catalog.ProductCard, error) {
				gotID = id
				return tc.product, tc.err
			}

			gotProduct, gotErr := client.GetProduct(context.Background(), tc.id)

			assert.Equal(t, tc.err, gotErr)
			assert.Equal(t, tc.id, gotID)
			assert.Equal(t, tc.product, gotProduct)
		})
	}
}

func mustNewProductCard(t *testing.T, fn func(card *catalog.ProductCard)) *catalog.ProductCard {
	pc := &catalog.ProductCard{
		ID: 65435,
		Properties: map[string]interface{}{
			"title": "Кроссовки ABC123",
			"price": float64(1999),
			"color": []interface{}{"red", "white"},
		},
	}

	if fn != nil {
		fn(pc)
	}

	return pc
}

func uint64ptr(v uint64) *uint64 {
	return &v
}
