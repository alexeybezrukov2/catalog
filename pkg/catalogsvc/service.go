package catalogsvc

import (
	"context"
	"fmt"

	"github.com/go-kit/kit/log"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

// Service provides catalog functionality.
type Service interface {
	GetCategories(ctx context.Context) ([]*catalog.CategoryNode, error)
	GetProducts(ctx context.Context, query *catalog.ProductQuery) (*catalog.ProductQueryResult, error)
	GetProduct(ctx context.Context, id uint32) (*catalog.ProductCard, error)
}

type service struct {
	logger         log.Logger
	memStore       MemStore
	filterService  FilterService
	productService ProductService
}

func newService(logger log.Logger, memStore MemStore, fsvc FilterService, psvc ProductService) *service {
	return &service{
		logger:         logger,
		memStore:       memStore,
		filterService:  fsvc,
		productService: psvc,
	}
}

func (s *service) GetCategories(ctx context.Context) ([]*catalog.CategoryNode, error) {
	categories := s.memStore.GetCategoryList()
	return catalog.BuildCategoryTree(categories), nil
}

func (s *service) GetProducts(ctx context.Context, query *catalog.ProductQuery) (*catalog.ProductQueryResult, error) {
	if err := query.Validate(); err != nil {
		return nil, catalog.ErrBadRequest("invalid query params: %s", err)
	}

	sortProperty, ok := s.memStore.GetPropertyByKey(query.Sort)
	if !ok {
		return nil, catalog.ErrBadRequest("unknown sort property key: %q", query.Sort)
	}

	cat, ok := s.memStore.GetCategory(query.Category)
	if !ok {
		return nil, catalog.ErrBadRequest("category not found: %d", query.Category)
	}

	indexRes, err := s.filterService.GetProducts(ctx, &catalog.IndexQuery{
		Filters:   append(query.Filters, cat.Filters...),
		SortKey:   sortProperty.ID,
		SortDesc:  query.Desc,
		Limit:     query.Limit,
		Offset:    query.Offset,
		FacetKeys: cat.FacetKeys,
	})
	if err != nil {
		return nil, err
	}

	products := []*catalog.ProductCard{}
	if len(indexRes.IDs) > 0 {
		products, err = s.productService.GetProductCardList(ctx, indexRes.IDs)
		if err != nil {
			return nil, err
		}
	}

	facets, err := s.enrichFacets(indexRes.Facets)
	if err != nil {
		return nil, catalog.ErrInternal("failed to enrich facets: %s", err)
	}

	res := &catalog.ProductQueryResult{
		Products: products,
		Total:    indexRes.Total,
		Facets:   facets,
	}

	return res, nil
}

func (s *service) enrichFacets(input []*catalog.IndexFacet) ([]*catalog.PropertyFacet, error) {
	output := make([]*catalog.PropertyFacet, len(input))
	for i, f := range input {
		facet := &catalog.PropertyFacet{ID: f.Key}

		p, ok := s.memStore.GetProperty(f.Key)
		if !ok {
			return nil, fmt.Errorf("property not found: %d", f.Key)
		}
		facet.Name = p.Name
		facet.Type = p.Type

		switch f.Type {
		case catalog.IndexTypeEnum:
			variants := make([]*catalog.PropertyFacetVariant, len(f.EnumVariants))
			for j, v := range f.EnumVariants {
				pv, ok := s.memStore.GetPropertyVariant(v.Value)
				if !ok {
					return nil, fmt.Errorf("property variant not found: %d", v.Value)
				}
				variants[j] = &catalog.PropertyFacetVariant{
					Value: pv.Value,
					ID:    v.Value,
					Count: v.Count,
				}
			}
			facet.EnumVariants = variants

		case catalog.IndexTypeRange:
			facet.RangeMin = f.RangeMin
			facet.RangeMax = f.RangeMax
		}

		output[i] = facet
	}
	return output, nil
}

func (s *service) GetProduct(ctx context.Context, id uint32) (*catalog.ProductCard, error) {
	return s.productService.GetProductCard(ctx, id)
}
