package catalogsvc

import (
	"context"
	"fmt"
	"net/http"
	"net/http/pprof"
	"time"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

// ServerConfig is a server configuration.
type ServerConfig struct {
	AllowedOrigins  []string
	Logger          log.Logger
	MemStore        MemStore
	FilterService   FilterService
	ProductService  ProductService
	Port            string
	ReadTimeout     time.Duration
	WriteTimeout    time.Duration
	ShutdownTimeout time.Duration
	ReadyFunc       func() bool
	MetricPrefix    string
}

// MemStore stores catalog data.
type MemStore interface {
	GetCategoryList() []*catalog.Category
	GetCategory(id uint32) (*catalog.Category, bool)
	GetProperty(id uint32) (*catalog.Property, bool)
	GetPropertyByKey(key string) (*catalog.Property, bool)
	GetPropertyVariant(id uint32) (*catalog.PropertyVariant, bool)
}

// FilterService filters products and collects facets.
type FilterService interface {
	GetProducts(ctx context.Context, q *catalog.IndexQuery) (*catalog.IndexQueryResult, error)
}

// ProductService provides product information.
type ProductService interface {
	GetProductCardList(ctx context.Context, ids []uint32) ([]*catalog.ProductCard, error)
	GetProductCard(ctx context.Context, id uint32) (*catalog.ProductCard, error)
}

// Server is a filters service server.
type Server struct {
	cfg *ServerConfig
	srv *http.Server
}

// NewServer creates a new server.
func NewServer(cfg ServerConfig) (*Server, error) {
	var svc Service
	svc = newService(cfg.Logger, cfg.MemStore, cfg.FilterService, cfg.ProductService)
	svc = NewLoggingMiddleware(svc, cfg.Logger)
	svc = NewInstrumentingMiddleware(svc, cfg.MetricPrefix)

	router := http.NewServeMux()
	router.Handle("/metrics", promhttp.Handler())
	router.Handle("/ready", readyHandler(cfg.ReadyFunc))
	router.Handle("/debug/pprof/profile", http.HandlerFunc(pprof.Profile))
	router.Handle("/api/v1/", makeHandler(svc))

	srv := &http.Server{
		Handler:      handlers.CORS(handlers.AllowedOrigins(cfg.AllowedOrigins))(router),
		Addr:         ":" + cfg.Port,
		ReadTimeout:  cfg.ReadTimeout,
		WriteTimeout: cfg.WriteTimeout,
	}

	s := &Server{
		cfg: &cfg,
		srv: srv,
	}
	return s, nil
}

// Serve starts HTTP server and stops it when the provided context is canceled.
func (s *Server) Serve(ctx context.Context) error {
	errChan := make(chan error, 1)
	go func() {
		errChan <- s.srv.ListenAndServe()
	}()

	select {
	case err := <-errChan:
		return err

	case <-ctx.Done():
		ctxShutdown, cancel := context.WithTimeout(context.Background(), s.cfg.ShutdownTimeout)
		defer cancel()
		if err := s.srv.Shutdown(ctxShutdown); err != nil {
			return fmt.Errorf("failed to shutdown server: %w", err)
		}
		return nil
	}
}

func makeHandler(svc Service) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorEncoder(encodeError),
	}

	router := mux.NewRouter()

	router.Path("/api/v1/categories").Methods(http.MethodGet).Handler(kithttp.NewServer(
		makeGetCategoriesEndpoint(svc),
		decodeGetCategoriesRequest,
		encodeGetCategoriesResponse,
		opts...,
	))

	router.Path("/api/v1/products").Methods(http.MethodGet).Handler(kithttp.NewServer(
		makeGetProductsEndpoint(svc),
		decodeGetProductsRequest,
		encodeGetProductsResponse,
		opts...,
	))

	router.Path("/api/v1/products/{id}").Methods(http.MethodGet).Handler(kithttp.NewServer(
		makeGetProductEndpoint(svc),
		decodeGetProductRequest,
		encodeGetProductResponse,
		opts...,
	))

	return router
}

func makeGetCategoriesEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		categories, err := svc.GetCategories(ctx)
		return getCategoriesResponse{categories: categories}, err
	}
}

func makeGetProductsEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getProductsRequest)
		result, err := svc.GetProducts(ctx, req.query)
		return getProductsResponse{result: result}, err
	}
}

func makeGetProductEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getProductRequest)
		product, err := svc.GetProduct(ctx, req.id)
		return getProductResponse{product: product}, err
	}
}

func readyHandler(readyFunc func() bool) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if readyFunc() {
			w.WriteHeader(http.StatusOK)
		} else {
			w.WriteHeader(http.StatusServiceUnavailable)
		}
	})
}
