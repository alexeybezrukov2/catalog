package catalogsvc

import (
	"context"
	"errors"
	"net/http"
	"net/url"
	"time"

	"github.com/go-kit/kit/endpoint"
	kithttp "github.com/go-kit/kit/transport/http"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

// ClientConfig is an Client configuration.
type ClientConfig struct {
	ServiceURL string
	Timeout    time.Duration
}

func (cfg ClientConfig) validate() error {
	if cfg.ServiceURL == "" {
		return errors.New("must provide ServiceURL")
	}
	if cfg.Timeout <= 0 {
		return errors.New("invalid Timeout")
	}
	return nil
}

var _ Service = (*Client)(nil)

// Client is a filters service client.
type Client struct {
	getCategoriesEndpoint endpoint.Endpoint
	getProductsEndpoint   endpoint.Endpoint
	getProductEndpoint    endpoint.Endpoint
}

// NewClient creates a new client.
func NewClient(cfg ClientConfig) (*Client, error) {
	err := cfg.validate()
	if err != nil {
		return nil, err
	}

	baseURL, err := url.Parse(cfg.ServiceURL)
	if err != nil {
		return nil, err
	}

	options := []kithttp.ClientOption{
		kithttp.SetClient(&http.Client{
			Timeout: cfg.Timeout,
		}),
	}

	c := &Client{
		getCategoriesEndpoint: kithttp.NewClient(
			http.MethodGet,
			baseURL,
			encodeGetCategoriesRequest,
			decodeGetCategoriesResponse,
			options...,
		).Endpoint(),

		getProductsEndpoint: kithttp.NewClient(
			http.MethodGet,
			baseURL,
			encodeGetProductsRequest,
			decodeGetProductsResponse,
			options...,
		).Endpoint(),

		getProductEndpoint: kithttp.NewClient(
			http.MethodGet,
			baseURL,
			encodeGetProductRequest,
			decodeGetProductResponse,
			options...,
		).Endpoint(),
	}

	return c, nil
}

// GetCategories filters products.
func (c *Client) GetCategories(ctx context.Context) ([]*catalog.CategoryNode, error) {
	response, err := c.getCategoriesEndpoint(ctx, getCategoriesRequest{})
	if err != nil {
		return nil, err
	}
	return response.(getCategoriesResponse).categories, nil
}

// GetProducts returns products satisfying the given query.
func (c *Client) GetProducts(ctx context.Context, query *catalog.ProductQuery) (*catalog.ProductQueryResult, error) {
	response, err := c.getProductsEndpoint(ctx, getProductsRequest{query: query})
	if err != nil {
		return nil, err
	}
	return response.(getProductsResponse).result, nil
}

// GetProduct returns a product by ID.
func (c *Client) GetProduct(ctx context.Context, id uint32) (*catalog.ProductCard, error) {
	response, err := c.getProductEndpoint(ctx, getProductRequest{id: id})
	if err != nil {
		return nil, err
	}
	return response.(getProductResponse).product, nil
}
