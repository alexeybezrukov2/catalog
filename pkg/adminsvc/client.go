package adminsvc

import (
	"context"
	"errors"
	"net/http"
	"net/url"
	"time"

	"github.com/go-kit/kit/endpoint"
	kithttp "github.com/go-kit/kit/transport/http"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

// ClientConfig is an Client configuration.
type ClientConfig struct {
	ServiceURL string
	Timeout    time.Duration
}

func (cfg ClientConfig) validate() error {
	if cfg.ServiceURL == "" {
		return errors.New("must provide ServiceURL")
	}
	if cfg.Timeout <= 0 {
		return errors.New("invalid Timeout")
	}
	return nil
}

var _ Service = (*Client)(nil)

// Client is a filters service client.
type Client struct {
	getPropertiesEndpoint  endpoint.Endpoint
	createPropertyEndpoint endpoint.Endpoint
	getPropertyEndpoint    endpoint.Endpoint
	deletePropertyEndpoint endpoint.Endpoint

	getPropertyVariantsEndpoint   endpoint.Endpoint
	createPropertyVariantEndpoint endpoint.Endpoint
	getPropertyVariantEndpoint    endpoint.Endpoint
	deletePropertyVariantEndpoint endpoint.Endpoint

	getProductsEndpoint   endpoint.Endpoint
	createProductEndpoint endpoint.Endpoint
	updateProductEndpoint endpoint.Endpoint
	getProductEndpoint    endpoint.Endpoint
	deleteProductEndpoint endpoint.Endpoint

	getCategoriesEndpoint  endpoint.Endpoint
	createCategoryEndpoint endpoint.Endpoint
	getCategoryEndpoint    endpoint.Endpoint
	updateCategoryEndpoint endpoint.Endpoint
	deleteCategoryEndpoint endpoint.Endpoint

	getSchemasEndpoint   endpoint.Endpoint
	createSchemaEndpoint endpoint.Endpoint
	updateSchemaEndpoint endpoint.Endpoint
	getSchemaEndpoint    endpoint.Endpoint
	deleteSchemaEndpoint endpoint.Endpoint
}

// NewClient creates a new client.
func NewClient(cfg ClientConfig) (*Client, error) {
	err := cfg.validate()
	if err != nil {
		return nil, err
	}

	baseURL, err := url.Parse(cfg.ServiceURL)
	if err != nil {
		return nil, err
	}

	options := []kithttp.ClientOption{
		kithttp.SetClient(&http.Client{
			Timeout: cfg.Timeout,
		}),
	}

	c := &Client{
		getPropertiesEndpoint: kithttp.NewClient(
			http.MethodGet,
			baseURL,
			encodeGetPropertiesRequest,
			decodeGetPropertiesResponse,
			options...,
		).Endpoint(),

		createPropertyEndpoint: kithttp.NewClient(
			http.MethodPost,
			baseURL,
			encodeCreatePropertyRequest,
			decodeCreatePropertyResponse,
			options...,
		).Endpoint(),

		getPropertyEndpoint: kithttp.NewClient(
			http.MethodGet,
			baseURL,
			encodeGetPropertyRequest,
			decodeGetPropertyResponse,
			options...,
		).Endpoint(),

		deletePropertyEndpoint: kithttp.NewClient(
			http.MethodDelete,
			baseURL,
			encodeDeletePropertyRequest,
			decodeDeletePropertyResponse,
			options...,
		).Endpoint(),

		getPropertyVariantsEndpoint: kithttp.NewClient(
			http.MethodGet,
			baseURL,
			encodeGetPropertyVariantsRequest,
			decodeGetPropertyVariantsResponse,
			options...,
		).Endpoint(),

		createPropertyVariantEndpoint: kithttp.NewClient(
			http.MethodPost,
			baseURL,
			encodeCreatePropertyVariantRequest,
			decodeCreatePropertyVariantResponse,
			options...,
		).Endpoint(),

		getPropertyVariantEndpoint: kithttp.NewClient(
			http.MethodGet,
			baseURL,
			encodeGetPropertyVariantRequest,
			decodeGetPropertyVariantResponse,
			options...,
		).Endpoint(),

		deletePropertyVariantEndpoint: kithttp.NewClient(
			http.MethodDelete,
			baseURL,
			encodeDeletePropertyVariantRequest,
			decodeDeletePropertyVariantResponse,
			options...,
		).Endpoint(),

		getProductsEndpoint: kithttp.NewClient(
			http.MethodGet,
			baseURL,
			encodeGetProductsRequest,
			decodeGetProductsResponse,
			options...,
		).Endpoint(),

		createProductEndpoint: kithttp.NewClient(
			http.MethodPost,
			baseURL,
			encodeCreateProductRequest,
			decodeCreateProductResponse,
			options...,
		).Endpoint(),

		updateProductEndpoint: kithttp.NewClient(
			http.MethodPatch,
			baseURL,
			encodeUpdateProductRequest,
			decodeUpdateProductResponse,
			options...,
		).Endpoint(),

		getProductEndpoint: kithttp.NewClient(
			http.MethodGet,
			baseURL,
			encodeGetProductRequest,
			decodeGetProductResponse,
			options...,
		).Endpoint(),

		deleteProductEndpoint: kithttp.NewClient(
			http.MethodDelete,
			baseURL,
			encodeDeleteProductRequest,
			decodeDeleteProductResponse,
			options...,
		).Endpoint(),

		getCategoriesEndpoint: kithttp.NewClient(
			http.MethodGet,
			baseURL,
			encodeGetCategoriesRequest,
			decodeGetCategoriesResponse,
			options...,
		).Endpoint(),

		createCategoryEndpoint: kithttp.NewClient(
			http.MethodPost,
			baseURL,
			encodeCreateCategoryRequest,
			decodeCreateCategoryResponse,
			options...,
		).Endpoint(),

		getCategoryEndpoint: kithttp.NewClient(
			http.MethodGet,
			baseURL,
			encodeGetCategoryRequest,
			decodeGetCategoryResponse,
			options...,
		).Endpoint(),

		updateCategoryEndpoint: kithttp.NewClient(
			http.MethodPatch,
			baseURL,
			encodeUpdateCategoryRequest,
			decodeUpdateCategoryResponse,
			options...,
		).Endpoint(),

		deleteCategoryEndpoint: kithttp.NewClient(
			http.MethodDelete,
			baseURL,
			encodeDeleteCategoryRequest,
			decodeDeleteCategoryResponse,
			options...,
		).Endpoint(),

		getSchemasEndpoint: kithttp.NewClient(
			http.MethodGet,
			baseURL,
			encodeGetSchemasRequest,
			decodeGetSchemasResponse,
			options...,
		).Endpoint(),

		createSchemaEndpoint: kithttp.NewClient(
			http.MethodPost,
			baseURL,
			encodeCreateSchemaRequest,
			decodeCreateSchemaResponse,
			options...,
		).Endpoint(),

		updateSchemaEndpoint: kithttp.NewClient(
			http.MethodPatch,
			baseURL,
			encodeUpdateSchemaRequest,
			decodeUpdateSchemaResponse,
			options...,
		).Endpoint(),

		getSchemaEndpoint: kithttp.NewClient(
			http.MethodGet,
			baseURL,
			encodeGetSchemaRequest,
			decodeGetSchemaResponse,
			options...,
		).Endpoint(),

		deleteSchemaEndpoint: kithttp.NewClient(
			http.MethodDelete,
			baseURL,
			encodeDeleteSchemaRequest,
			decodeDeleteSchemaResponse,
			options...,
		).Endpoint(),
	}

	return c, nil
}

// GetProperties returns a list of existing properties.
func (c *Client) GetProperties(ctx context.Context) ([]*catalog.Property, error) {
	response, err := c.getPropertiesEndpoint(ctx, getPropertiesRequest{})
	if err != nil {
		return nil, err
	}
	return response.(getPropertiesResponse).properties, nil
}

// CreateProperty creates a new property.
func (c *Client) CreateProperty(ctx context.Context, input *catalog.PropertyInput) (*catalog.Property, error) {
	response, err := c.createPropertyEndpoint(ctx, createPropertyRequest{input: input})
	if err != nil {
		return nil, err
	}
	return response.(createPropertyResponse).property, nil
}

// GetProperty returns a property by ID.
func (c *Client) GetProperty(ctx context.Context, id uint32) (*catalog.Property, error) {
	response, err := c.getPropertyEndpoint(ctx, getPropertyRequest{id: id})
	if err != nil {
		return nil, err
	}
	return response.(getPropertyResponse).property, nil
}

// DeleteProperty deletes a property by ID.
func (c *Client) DeleteProperty(ctx context.Context, id uint32) error {
	_, err := c.deletePropertyEndpoint(ctx, deletePropertyRequest{id: id})
	return err
}

// GetPropertyVariants returns all variants of enum property.
func (c *Client) GetPropertyVariants(ctx context.Context, id uint32) ([]*catalog.PropertyVariant, error) {
	response, err := c.getPropertyVariantsEndpoint(ctx, getPropertyVariantsRequest{id: id})
	if err != nil {
		return nil, err
	}
	return response.(getPropertyVariantsResponse).variants, nil
}

// CreatePropertyVariant creates a new property variant.
func (c *Client) CreatePropertyVariant(ctx context.Context, input *catalog.PropertyVariantInput) (*catalog.PropertyVariant, error) {
	response, err := c.createPropertyVariantEndpoint(ctx, createPropertyVariantRequest{input: input})
	if err != nil {
		return nil, err
	}
	return response.(createPropertyVariantResponse).variant, nil
}

// GetPropertyVariant returns a property variant by ID
func (c *Client) GetPropertyVariant(ctx context.Context, id uint32) (*catalog.PropertyVariant, error) {
	response, err := c.getPropertyVariantEndpoint(ctx, getPropertyVariantRequest{id: id})
	if err != nil {
		return nil, err
	}
	return response.(getPropertyVariantResponse).variant, nil
}

// DeletePropertyVariant deletes a property variant by ID.
func (c *Client) DeletePropertyVariant(ctx context.Context, id uint32) error {
	_, err := c.deletePropertyVariantEndpoint(ctx, deletePropertyVariantRequest{id: id})
	return err
}

// GetProducts returns products satisfying the given query.
func (c *Client) GetProducts(ctx context.Context, query *catalog.StorageProductQuery) (*catalog.StorageProductQueryResult, error) {
	response, err := c.getProductsEndpoint(ctx, getProductsRequest{query: query})
	if err != nil {
		return nil, err
	}
	return response.(getProductsResponse).result, nil
}

// CreateProduct creates a new product.
func (c *Client) CreateProduct(ctx context.Context, input *catalog.ProductInput) (*catalog.Product, error) {
	response, err := c.createProductEndpoint(ctx, createProductRequest{input: input})
	if err != nil {
		return nil, err
	}
	return response.(createProductResponse).product, nil
}

// UpdateProduct updates a product.
func (c *Client) UpdateProduct(ctx context.Context, id uint32, p *catalog.ProductPatch) (*catalog.Product, error) {
	response, err := c.updateProductEndpoint(ctx, updateProductRequest{id: id, patch: p})
	if err != nil {
		return nil, err
	}
	return response.(updateProductResponse).product, nil
}

// GetProduct returns a product by ID.
func (c *Client) GetProduct(ctx context.Context, id uint32) (*catalog.Product, error) {
	response, err := c.getProductEndpoint(ctx, getProductRequest{id: id})
	if err != nil {
		return nil, err
	}
	return response.(getProductResponse).product, nil
}

// DeleteProduct deletes a product by ID.
func (c *Client) DeleteProduct(ctx context.Context, id uint32) error {
	_, err := c.deleteProductEndpoint(ctx, deleteProductRequest{id: id})
	return err
}

// GetCategories returns all categories.
func (c *Client) GetCategories(ctx context.Context) ([]*catalog.Category, error) {
	response, err := c.getCategoriesEndpoint(ctx, getCategoriesRequest{})
	if err != nil {
		return nil, err
	}
	return response.(getCategoriesResponse).categories, nil
}

// CreateCategory creates a new category.
func (c *Client) CreateCategory(ctx context.Context, input *catalog.CategoryInput) (*catalog.Category, error) {
	response, err := c.createCategoryEndpoint(ctx, createCategoryRequest{input: input})
	if err != nil {
		return nil, err
	}
	return response.(createCategoryResponse).category, nil
}

// GetCategory returns a category by ID
func (c *Client) GetCategory(ctx context.Context, id uint32) (*catalog.Category, error) {
	response, err := c.getCategoryEndpoint(ctx, getCategoryRequest{id: id})
	if err != nil {
		return nil, err
	}
	return response.(getCategoryResponse).category, nil
}

// UpdateCategory updates a category
func (c *Client) UpdateCategory(ctx context.Context, id uint32, p *catalog.CategoryPatch) (*catalog.Category, error) {
	response, err := c.updateCategoryEndpoint(ctx, updateCategoryRequest{id: id, patch: p})
	if err != nil {
		return nil, err
	}
	return response.(updateCategoryResponse).category, nil
}

// DeleteCategory deletes a category by ID.
func (c *Client) DeleteCategory(ctx context.Context, id uint32) error {
	_, err := c.deleteCategoryEndpoint(ctx, deleteCategoryRequest{id: id})
	return err
}

// GetSchemas lists all the available schemas.
func (c *Client) GetSchemas(ctx context.Context) ([]*catalog.SchemaName, error) {
	response, err := c.getSchemasEndpoint(ctx, getSchemasRequest{})
	if err != nil {
		return nil, err
	}
	return response.(getSchemasResponse).schemas, nil
}

// CreateSchema creates a new schema.
func (c *Client) CreateSchema(ctx context.Context, input *catalog.SchemaInput) (*catalog.Schema, error) {
	response, err := c.createSchemaEndpoint(ctx, createSchemaRequest{input: input})
	if err != nil {
		return nil, err
	}
	return response.(createSchemaResponse).schema, nil
}

// UpdateSchema updates a schema.
func (c *Client) UpdateSchema(ctx context.Context, id uint32, p *catalog.SchemaPatch) (*catalog.Schema, error) {
	response, err := c.updateSchemaEndpoint(ctx, updateSchemaRequest{id: id, patch: p})
	if err != nil {
		return nil, err
	}
	return response.(updateSchemaResponse).schema, nil
}

// GetSchema returns a schema by ID.
func (c *Client) GetSchema(ctx context.Context, id uint32) (*catalog.Schema, error) {
	response, err := c.getSchemaEndpoint(ctx, getSchemaRequest{id: id})
	if err != nil {
		return nil, err
	}
	return response.(getSchemaResponse).schema, nil
}

// DeleteSchema deletes a schema by ID.
func (c *Client) DeleteSchema(ctx context.Context, id uint32) error {
	_, err := c.deleteSchemaEndpoint(ctx, deleteSchemaRequest{id: id})
	return err
}
