package adminsvc

import (
	"context"
	"errors"
	"fmt"

	"github.com/go-kit/kit/log"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

// Service provides catalog functionality.
type Service interface {
	GetProperties(ctx context.Context) ([]*catalog.Property, error)
	CreateProperty(ctx context.Context, input *catalog.PropertyInput) (*catalog.Property, error)
	GetProperty(ctx context.Context, id uint32) (*catalog.Property, error)
	DeleteProperty(ctx context.Context, id uint32) error

	GetPropertyVariants(ctx context.Context, id uint32) ([]*catalog.PropertyVariant, error)
	CreatePropertyVariant(ctx context.Context, input *catalog.PropertyVariantInput) (*catalog.PropertyVariant, error)
	GetPropertyVariant(ctx context.Context, id uint32) (*catalog.PropertyVariant, error)
	DeletePropertyVariant(ctx context.Context, id uint32) error

	GetProducts(ctx context.Context, query *catalog.StorageProductQuery) (*catalog.StorageProductQueryResult, error)
	CreateProduct(ctx context.Context, input *catalog.ProductInput) (*catalog.Product, error)
	UpdateProduct(ctx context.Context, id uint32, patch *catalog.ProductPatch) (*catalog.Product, error)
	GetProduct(ctx context.Context, id uint32) (*catalog.Product, error)
	DeleteProduct(ctx context.Context, id uint32) error

	GetCategories(ctx context.Context) ([]*catalog.Category, error)
	CreateCategory(ctx context.Context, input *catalog.CategoryInput) (*catalog.Category, error)
	GetCategory(ctx context.Context, id uint32) (*catalog.Category, error)
	UpdateCategory(ctx context.Context, id uint32, input *catalog.CategoryPatch) (*catalog.Category, error)
	DeleteCategory(ctx context.Context, id uint32) error

	GetSchemas(ctx context.Context) ([]*catalog.SchemaName, error)
	CreateSchema(ctx context.Context, input *catalog.SchemaInput) (*catalog.Schema, error)
	UpdateSchema(ctx context.Context, id uint32, patch *catalog.SchemaPatch) (*catalog.Schema, error)
	GetSchema(ctx context.Context, id uint32) (*catalog.Schema, error)
	DeleteSchema(ctx context.Context, id uint32) error
}

type service struct {
	logger     log.Logger
	storage    Storage
	eventQueue EventQueue
}

func newService(logger log.Logger, storage Storage, eventQueue EventQueue) *service {
	return &service{
		logger:     logger,
		storage:    storage,
		eventQueue: eventQueue,
	}
}

func (s *service) GetProperties(ctx context.Context) ([]*catalog.Property, error) {
	var properties []*catalog.Property
	err := s.storage.ForeachProperty(ctx, func(p *catalog.Property) error {
		properties = append(properties, p)
		return nil
	})
	if err != nil {
		return nil, catalog.ErrInternal("failed to get properties: %s", err)
	}
	return properties, nil
}

func (s *service) CreateProperty(ctx context.Context, input *catalog.PropertyInput) (*catalog.Property, error) {
	if err := input.Validate(); err != nil {
		return nil, catalog.ErrBadRequest("invalid input: %s", err)
	}

	property := &catalog.Property{
		Key:     input.Key,
		Name:    input.Name,
		Type:    input.Type,
		Indexed: input.Indexed,
	}
	if err := s.storage.InsertProperty(ctx, property); err != nil {
		if errors.Is(err, catalog.ErrDuplicateKeyInStorage) {
			return nil, catalog.ErrConflict("duplicate property Key")
		}
		return nil, catalog.ErrInternal("failed to insert property: %s", err)
	}

	op := &catalog.Operation{
		Type:     catalog.OperationTypeProperty,
		Method:   catalog.OperationMethodUpsert,
		Property: property,
	}
	if err := s.eventQueue.Publish(op); err != nil {
		return nil, catalog.ErrInternal("failed to publish replication event: %s", err)
	}

	return property, nil
}

func (s *service) GetProperty(ctx context.Context, id uint32) (*catalog.Property, error) {
	property, err := s.storage.GetProperty(ctx, id)
	if err != nil {
		if errors.Is(err, catalog.ErrNotFoundInStorage) {
			return nil, catalog.ErrNotFound("property not found: %d", id)
		}
		return nil, catalog.ErrInternal("failed to get property: %s", err)
	}
	return property, nil
}

func (s *service) DeleteProperty(ctx context.Context, id uint32) error {
	property, err := s.storage.GetProperty(ctx, id)
	if err != nil {
		if errors.Is(err, catalog.ErrNotFoundInStorage) {
			return catalog.ErrNotFound("property not found: %d", id)
		}
		return catalog.ErrInternal("failed to get property: %s", err)
	}

	err = s.storage.DeleteProperty(ctx, id)
	if err != nil {
		if errors.Is(err, catalog.ErrNotFoundInStorage) {
			return catalog.ErrNotFound("property not found: %d", id)
		}
		return catalog.ErrInternal("failed to delete property: %s", err)
	}

	op := &catalog.Operation{
		Type:     catalog.OperationTypeProperty,
		Method:   catalog.OperationMethodDelete,
		Property: property,
	}
	if err := s.eventQueue.Publish(op); err != nil {
		return catalog.ErrInternal("failed to publish replication event: %s", err)
	}
	return nil
}

func (s *service) GetPropertyVariants(ctx context.Context, id uint32) ([]*catalog.PropertyVariant, error) {
	_, err := s.storage.GetProperty(ctx, id)
	if err != nil {
		if errors.Is(err, catalog.ErrNotFoundInStorage) {
			return nil, catalog.ErrNotFound("property not found: %d", id)
		}
		return nil, catalog.ErrInternal("failed to get property: %s", err)
	}
	variants, err := s.storage.GetPropertyVariantsByProperty(ctx, id)
	if err != nil {
		return nil, catalog.ErrInternal("failed to get property variants by property: %s", err)
	}
	return variants, nil
}

func (s *service) CreatePropertyVariant(ctx context.Context, input *catalog.PropertyVariantInput) (*catalog.PropertyVariant, error) {
	if err := input.Validate(); err != nil {
		return nil, catalog.ErrBadRequest("invalid input: %s", err)
	}

	property, err := s.storage.GetProperty(ctx, input.PropertyID)
	if err != nil {
		if errors.Is(err, catalog.ErrNotFoundInStorage) {
			return nil, catalog.ErrNotFound("property not found: %d", input.PropertyID)
		}
		return nil, catalog.ErrInternal("failed to get property: %s", err)
	}

	if property.Type != catalog.PropertyTypeEnum && property.Type != catalog.PropertyTypeEnumList {
		return nil, fmt.Errorf("unable to create property variant for %v property type", property.Type)
	}

	variant := &catalog.PropertyVariant{
		PropertyID: input.PropertyID,
		Value:      input.Value,
	}
	if err := s.storage.InsertPropertyVariant(ctx, variant); err != nil {
		return nil, catalog.ErrInternal("failed to insert property variant: %s", err)
	}

	op := &catalog.Operation{
		Type:            catalog.OperationTypePropertyVariant,
		Method:          catalog.OperationMethodUpsert,
		PropertyVariant: variant,
	}
	if err := s.eventQueue.Publish(op); err != nil {
		return nil, catalog.ErrInternal("failed to publish replication event: %s", err)
	}

	return variant, nil
}

func (s *service) GetPropertyVariant(ctx context.Context, id uint32) (*catalog.PropertyVariant, error) {
	variant, err := s.storage.GetPropertyVariant(ctx, id)
	if err != nil {
		if errors.Is(err, catalog.ErrNotFoundInStorage) {
			return nil, catalog.ErrNotFound("property variant not found: %d", id)
		}
		return nil, catalog.ErrInternal("failed to get property variant: %s", err)
	}
	return variant, nil
}

func (s *service) DeletePropertyVariant(ctx context.Context, id uint32) error {
	variant, err := s.storage.GetPropertyVariant(ctx, id)
	if err != nil {
		if errors.Is(err, catalog.ErrNotFoundInStorage) {
			return catalog.ErrNotFound("property variant not found: %d", id)
		}
		return catalog.ErrInternal("failed to get property variant: %s", err)
	}

	err = s.storage.DeletePropertyVariant(ctx, id)
	if err != nil {
		if errors.Is(err, catalog.ErrNotFoundInStorage) {
			return catalog.ErrNotFound("property variant not found: %d", id)
		}
		return catalog.ErrInternal("failed to delete property variant: %s", err)
	}

	op := &catalog.Operation{
		Type:            catalog.OperationTypePropertyVariant,
		Method:          catalog.OperationMethodDelete,
		PropertyVariant: variant,
	}
	if err := s.eventQueue.Publish(op); err != nil {
		return catalog.ErrInternal("failed to publish replication event: %s", err)
	}

	return nil
}

func (s *service) GetProducts(ctx context.Context, query *catalog.StorageProductQuery) (*catalog.StorageProductQueryResult, error) {
	if err := query.Validate(); err != nil {
		return nil, catalog.ErrBadRequest("invalid query: %s", err)
	}
	result, err := s.storage.GetProducts(ctx, query)
	if err != nil {
		return nil, catalog.ErrInternal("failed to get products: %s", err)
	}
	return result, nil
}

func (s *service) CreateProduct(ctx context.Context, input *catalog.ProductInput) (*catalog.Product, error) {
	if err := input.Validate(); err != nil {
		return nil, catalog.ErrBadRequest("invalid input: %s", err)
	}

	schema, err := s.storage.GetSchema(ctx, input.SchemaID)
	if err != nil {
		if errors.Is(err, catalog.ErrNotFoundInStorage) {
			return nil, catalog.ErrBadRequest("unknown schema id: %d", input.SchemaID)
		}
		return nil, catalog.ErrInternal("failed to get schema: %s", err)
	}

	product := &catalog.Product{
		SupplierID: input.SupplierID,
		SchemaID:   input.SchemaID,
	}

	allowed := make(map[uint32]bool)
	for _, p := range schema.Properties {
		allowed[p.ID] = true
	}

	inputProperties := make(map[uint32]*catalog.ProductPropertyInput)
	for _, p := range input.Properties {
		if !allowed[p.ID] {
			return nil, catalog.ErrBadRequest("property %d is not allowed for schema %d", p.ID, input.SchemaID)
		}
		inputProperties[p.ID] = p
	}

	for _, sp := range schema.Properties {
		ppi, ok := inputProperties[sp.ID]
		if !ok {
			if sp.Required {
				return nil, catalog.ErrBadRequest("missing required property %d for schema %d", sp.ID, input.SchemaID)
			}
			continue
		}
		pp, err := s.makeProductProperty(ctx, sp, ppi)
		if err != nil {
			return nil, err
		}
		product.Properties = append(product.Properties, pp)
	}

	if err := s.storage.InsertProduct(ctx, product); err != nil {
		return nil, catalog.ErrInternal("failed to insert product: %s", err)
	}

	op := &catalog.Operation{
		Type:    catalog.OperationTypeProduct,
		Method:  catalog.OperationMethodUpsert,
		Product: product,
	}
	if err := s.eventQueue.Publish(op); err != nil {
		return nil, catalog.ErrInternal("failed to publish replication event: %s", err)
	}

	return product, nil
}

func (s *service) UpdateProduct(ctx context.Context, id uint32, patch *catalog.ProductPatch) (*catalog.Product, error) {
	if err := patch.Validate(); err != nil {
		return nil, catalog.ErrBadRequest("invalid patch: %s", err)
	}

	product, err := s.storage.GetProduct(ctx, id)
	if err != nil {
		if errors.Is(err, catalog.ErrNotFoundInStorage) {
			return nil, catalog.ErrNotFound("product not found: %d", id)
		}
		return nil, catalog.ErrInternal("failed to get product: %s", err)
	}

	schema, err := s.storage.GetSchema(ctx, product.SchemaID)
	if err != nil {
		if errors.Is(err, catalog.ErrNotFoundInStorage) {
			return nil, catalog.ErrNotFound("schema not found: %d", product.SchemaID)
		}
		return nil, catalog.ErrInternal("failed to get schema: %s", err)
	}

	schemaProperties := make(map[uint32]*catalog.SchemaProperty)
	for _, p := range schema.Properties {
		schemaProperties[p.ID] = p
	}

	patchProperties := make(map[uint32]*catalog.ProductPropertyInput)
	for _, p := range patch.Properties {
		if _, ok := schemaProperties[p.ID]; !ok {
			return nil, catalog.ErrBadRequest("property %d is not allowed for schema %d", p.ID, product.SchemaID)
		}
		patchProperties[p.ID] = p
	}

	var op *catalog.Operation

	if patch.Partial {
		newProperties := make([]*catalog.ProductProperty, 0)
		for _, ppi := range patch.Properties {
			sp := schemaProperties[ppi.ID]
			pp, err := s.makeProductProperty(ctx, sp, ppi)
			if err != nil {
				return nil, err
			}
			newProperties = append(newProperties, pp)
		}

		product.MergeProperties(newProperties)

		op = &catalog.Operation{
			Type:   catalog.OperationTypeProduct,
			Method: catalog.OperationMethodPatch,
			Product: &catalog.Product{
				ID:         product.ID,
				SupplierID: product.SupplierID,
				SchemaID:   product.SchemaID,
				Properties: newProperties,
			},
		}
	} else {
		product.Properties = make([]*catalog.ProductProperty, 0)

		for _, sp := range schema.Properties {
			ppi, ok := patchProperties[sp.ID]
			if !ok {
				if sp.Required {
					return nil, catalog.ErrBadRequest("missing required property %d for schema %d", sp.ID, product.SchemaID)
				}
				continue
			}
			pp, err := s.makeProductProperty(ctx, sp, ppi)
			if err != nil {
				return nil, err
			}
			product.Properties = append(product.Properties, pp)
		}

		op = &catalog.Operation{
			Type:    catalog.OperationTypeProduct,
			Method:  catalog.OperationMethodUpsert,
			Product: product,
		}
	}

	if err := s.storage.UpdateProduct(ctx, product); err != nil {
		return nil, catalog.ErrInternal("failed to update product: %s", err)
	}

	if err := s.eventQueue.Publish(op); err != nil {
		return nil, catalog.ErrInternal("failed to publish replication event: %s", err)
	}

	return product, nil
}

func (s *service) makeProductProperty(ctx context.Context, sp *catalog.SchemaProperty, ppi *catalog.ProductPropertyInput) (*catalog.ProductProperty, error) {
	pp := &catalog.ProductProperty{
		ID:      sp.ID,
		Key:     sp.Key,
		Name:    sp.Name,
		Type:    sp.Type,
		Indexed: sp.Indexed,
	}

	switch sp.Type {
	case catalog.PropertyTypeEnum, catalog.PropertyTypeEnumList:
		pp.ValueIDs = ppi.ValueIDs
		values := make([]interface{}, len(ppi.ValueIDs))
		for i, id := range ppi.ValueIDs {
			variant, err := s.storage.GetPropertyVariant(ctx, id)
			if err != nil {
				if errors.Is(err, catalog.ErrNotFoundInStorage) {
					return nil, catalog.ErrBadRequest("property variant not found: %d", id)
				}
				return nil, catalog.ErrInternal("failed to get property variant: %s", err)
			}
			if variant.PropertyID != sp.ID {
				return nil, catalog.ErrBadRequest("ivalid property variant %d for property %d", id, sp.ID)
			}
			values[i] = variant.Value
		}
		if sp.Type == catalog.PropertyTypeEnum {
			if len(values) != 1 {
				return nil, catalog.ErrBadRequest("ivalid count of property variants (%d) for enum property %d", len(values), sp.ID)
			}
			pp.Value = values[0]
		} else {
			if len(values) < 1 {
				return nil, catalog.ErrBadRequest("ivalid count of property variants (%d) for enum-list property %d", len(values), sp.ID)
			}
			pp.Value = values
		}

	default:
		if len(ppi.ValueIDs) != 0 {
			return nil, catalog.ErrBadRequest("non-empty list of value ids for non-enum property")
		}
		pp.Value = ppi.Value
	}

	if err := pp.Validate(); err != nil {
		return nil, catalog.ErrBadRequest("invalid product property %d: %s", pp.ID, err)
	}

	return pp, nil
}

func (s *service) GetProduct(ctx context.Context, id uint32) (*catalog.Product, error) {
	product, err := s.storage.GetProduct(ctx, id)
	if err != nil {
		if errors.Is(err, catalog.ErrNotFoundInStorage) {
			return nil, catalog.ErrNotFound("product not found: %d", id)
		}
		return nil, catalog.ErrInternal("failed to get product: %s", err)
	}
	return product, nil
}

func (s *service) DeleteProduct(ctx context.Context, id uint32) error {
	product, err := s.storage.GetProduct(ctx, id)
	if err != nil {
		if errors.Is(err, catalog.ErrNotFoundInStorage) {
			return catalog.ErrNotFound("product not found: %d", id)
		}
		return catalog.ErrInternal("failed to get product: %s", err)
	}

	err = s.storage.DeleteProduct(ctx, id)
	if err != nil {
		if errors.Is(err, catalog.ErrNotFoundInStorage) {
			return catalog.ErrNotFound("product not found: %d", id)
		}
		return catalog.ErrInternal("failed to delete product: %s", err)
	}

	op := &catalog.Operation{
		Type:    catalog.OperationTypeProduct,
		Method:  catalog.OperationMethodDelete,
		Product: product,
	}
	if err := s.eventQueue.Publish(op); err != nil {
		return catalog.ErrInternal("failed to publish replication event: %s", err)
	}
	return nil
}

func (s *service) GetCategories(ctx context.Context) ([]*catalog.Category, error) {
	var categories []*catalog.Category
	err := s.storage.ForeachCategory(ctx, func(c *catalog.Category) error {
		categories = append(categories, c)
		return nil
	})
	if err != nil {
		return nil, catalog.ErrInternal("failed to get categories: %s", err)
	}
	return categories, nil
}

func (s *service) CreateCategory(ctx context.Context, input *catalog.CategoryInput) (*catalog.Category, error) {
	if err := input.Validate(); err != nil {
		return nil, catalog.ErrBadRequest("invalid input: %s", err)
	}

	category := &catalog.Category{
		ParentID:  input.ParentID,
		Name:      input.Name,
		Order:     input.Order,
		Filters:   input.Filters,
		FacetKeys: input.FacetKeys,
	}
	if err := s.storage.InsertCategory(ctx, category); err != nil {
		return nil, catalog.ErrInternal("failed to insert category: %s", err)
	}

	op := &catalog.Operation{
		Type:     catalog.OperationTypeCategory,
		Method:   catalog.OperationMethodUpsert,
		Category: category,
	}
	if err := s.eventQueue.Publish(op); err != nil {
		return nil, catalog.ErrInternal("failed to publish replication event: %s", err)
	}

	return category, nil
}

func (s *service) GetCategory(ctx context.Context, id uint32) (*catalog.Category, error) {
	category, err := s.storage.GetCategory(ctx, id)
	if err != nil {
		if errors.Is(err, catalog.ErrNotFoundInStorage) {
			return nil, catalog.ErrNotFound("category not found: %d", id)
		}
		return nil, catalog.ErrInternal("failed to get category: %s", err)
	}
	return category, nil
}

func (s *service) UpdateCategory(ctx context.Context, id uint32, p *catalog.CategoryPatch) (*catalog.Category, error) {
	if err := p.Validate(); err != nil {
		return nil, catalog.ErrBadRequest("invalid patch: %s", err)
	}

	category, err := s.storage.GetCategory(ctx, id)
	if err != nil {
		if errors.Is(err, catalog.ErrNotFoundInStorage) {
			return nil, catalog.ErrNotFound("category not found: %d", id)
		}
		return nil, catalog.ErrInternal("failed to get category: %s", err)
	}
	category.ApplyPatch(p)

	if err := s.storage.UpdateCategory(ctx, category); err != nil {
		if errors.Is(err, catalog.ErrNotFoundInStorage) {
			return nil, catalog.ErrNotFound("category not found: %d", id)
		}
		return nil, catalog.ErrInternal("failed to update category: %s", err)
	}

	op := &catalog.Operation{
		Type:     catalog.OperationTypeCategory,
		Method:   catalog.OperationMethodUpsert,
		Category: category,
	}
	if err := s.eventQueue.Publish(op); err != nil {
		return nil, catalog.ErrInternal("failed to publish replication event: %s", err)
	}

	return category, nil
}

func (s *service) DeleteCategory(ctx context.Context, id uint32) error {
	category, err := s.storage.GetCategory(ctx, id)
	if err != nil {
		if errors.Is(err, catalog.ErrNotFoundInStorage) {
			return catalog.ErrNotFound("category not found: %d", id)
		}
		return catalog.ErrInternal("failed to get category: %s", err)
	}

	err = s.storage.DeleteCategory(ctx, id)
	if err != nil {
		if errors.Is(err, catalog.ErrNotFoundInStorage) {
			return catalog.ErrNotFound("category not found: %d", id)
		}
		return catalog.ErrInternal("failed to delete category: %s", err)
	}

	op := &catalog.Operation{
		Type:     catalog.OperationTypeCategory,
		Method:   catalog.OperationMethodDelete,
		Category: category,
	}
	if err := s.eventQueue.Publish(op); err != nil {
		return catalog.ErrInternal("failed to publish replication event: %s", err)
	}

	return nil
}

func (s *service) GetSchemas(ctx context.Context) ([]*catalog.SchemaName, error) {
	var schemas []*catalog.SchemaName
	err := s.storage.ForeachSchema(ctx, func(s *catalog.Schema) error {
		schemas = append(schemas, &catalog.SchemaName{
			ID:   s.ID,
			Name: s.Name,
		})
		return nil
	})
	if err != nil {
		return nil, catalog.ErrInternal("failed to get schemas: %s", err)
	}
	return schemas, nil
}

func (s *service) CreateSchema(ctx context.Context, input *catalog.SchemaInput) (*catalog.Schema, error) {
	if err := input.Validate(); err != nil {
		return nil, catalog.ErrBadRequest("invlaid input: %s", err)
	}

	properties, err := s.makeSchemaProperties(ctx, input.Properties)
	if err != nil {
		return nil, err
	}

	schema := &catalog.Schema{
		Name:       input.Name,
		Properties: properties,
	}
	if err := s.storage.InsertSchema(ctx, schema); err != nil {
		return nil, catalog.ErrInternal("failed to insert schema: %s", err)
	}

	return schema, nil
}

func (s *service) UpdateSchema(ctx context.Context, id uint32, patch *catalog.SchemaPatch) (*catalog.Schema, error) {
	if err := patch.Validate(); err != nil {
		return nil, catalog.ErrBadRequest("invlaid patch: %s", err)
	}

	schema, err := s.storage.GetSchema(ctx, id)
	if err != nil {
		if errors.Is(err, catalog.ErrNotFoundInStorage) {
			return nil, catalog.ErrNotFound("schema not found: %d", id)
		}
		return nil, catalog.ErrInternal("failed to get schema: %s", err)
	}

	if patch.Name != nil {
		schema.Name = *patch.Name
	}
	if patch.Properties != nil {
		properties, err := s.makeSchemaProperties(ctx, patch.Properties)
		if err != nil {
			return nil, err
		}
		schema.Properties = properties
	}

	if err := s.storage.UpdateSchema(ctx, schema); err != nil {
		if errors.Is(err, catalog.ErrNotFoundInStorage) {
			return nil, catalog.ErrNotFound("schema not found: %d", id)
		}
		return nil, catalog.ErrInternal("failed to update schema: %s", err)
	}

	return schema, nil
}

func (s *service) makeSchemaProperties(ctx context.Context, pp []*catalog.SchemaPropertyInput) ([]*catalog.SchemaProperty, error) {
	properties := make([]*catalog.SchemaProperty, 0, len(pp))
	unique := make(map[uint32]bool)
	for _, p := range pp {
		if unique[p.ID] {
			return nil, catalog.ErrBadRequest("property is duplicated: %d", p.ID)
		}
		unique[p.ID] = true

		prop, err := s.storage.GetProperty(ctx, p.ID)
		if err != nil {
			if errors.Is(err, catalog.ErrNotFoundInStorage) {
				return nil, catalog.ErrBadRequest("property not found: %d", p.ID)
			}
			return nil, catalog.ErrInternal("failed to get property: %s", err)
		}

		properties = append(properties, &catalog.SchemaProperty{
			ID:       prop.ID,
			Key:      prop.Key,
			Name:     prop.Name,
			Type:     prop.Type,
			Indexed:  prop.Indexed,
			Required: p.Required,
		})
	}
	return properties, nil
}

func (s *service) GetSchema(ctx context.Context, id uint32) (*catalog.Schema, error) {
	schema, err := s.storage.GetSchema(ctx, id)
	if err != nil {
		if errors.Is(err, catalog.ErrNotFoundInStorage) {
			return nil, catalog.ErrNotFound("schema not found: %d", id)
		}
		return nil, catalog.ErrInternal("failed to get schema: %s", err)
	}
	return schema, nil
}

func (s *service) DeleteSchema(ctx context.Context, id uint32) error {
	if err := s.storage.DeleteSchema(ctx, id); err != nil {
		if errors.Is(err, catalog.ErrNotFoundInStorage) {
			return catalog.ErrNotFound("schema not found: %d", id)
		}
		return catalog.ErrInternal("failed to delete schema: %s", err)
	}
	return nil
}
