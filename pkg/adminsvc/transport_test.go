package adminsvc

import (
	"context"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

type mockService struct {
	onGetProperties  func(ctx context.Context) ([]*catalog.Property, error)
	onCreateProperty func(ctx context.Context, input *catalog.PropertyInput) (*catalog.Property, error)
	onGetProperty    func(ctx context.Context, id uint32) (*catalog.Property, error)
	onDeleteProperty func(ctx context.Context, id uint32) error

	onGetPropertyVariants   func(ctx context.Context, id uint32) ([]*catalog.PropertyVariant, error)
	onCreatePropertyVariant func(ctx context.Context, input *catalog.PropertyVariantInput) (*catalog.PropertyVariant, error)
	onGetPropertyVariant    func(ctx context.Context, id uint32) (*catalog.PropertyVariant, error)
	onDeletePropertyVariant func(ctx context.Context, id uint32) error

	onGetProducts   func(ctx context.Context, query *catalog.StorageProductQuery) (*catalog.StorageProductQueryResult, error)
	onCreateProduct func(ctx context.Context, input *catalog.ProductInput) (*catalog.Product, error)
	onUpdateProduct func(ctx context.Context, id uint32, patch *catalog.ProductPatch) (*catalog.Product, error)
	onGetProduct    func(ctx context.Context, id uint32) (*catalog.Product, error)
	onDeleteProduct func(ctx context.Context, id uint32) error

	onGetCategories  func(ctx context.Context) ([]*catalog.Category, error)
	onCreateCategory func(ctx context.Context, input *catalog.CategoryInput) (*catalog.Category, error)
	onGetCategory    func(ctx context.Context, id uint32) (*catalog.Category, error)
	onUpdateCategory func(ctx context.Context, id uint32, input *catalog.CategoryPatch) (*catalog.Category, error)
	onDeleteCategory func(ctx context.Context, id uint32) error

	onGetSchemas   func(ctx context.Context) ([]*catalog.SchemaName, error)
	onCreateSchema func(ctx context.Context, input *catalog.SchemaInput) (*catalog.Schema, error)
	onUpdateSchema func(ctx context.Context, id uint32, patch *catalog.SchemaPatch) (*catalog.Schema, error)
	onGetSchema    func(ctx context.Context, id uint32) (*catalog.Schema, error)
	onDeleteSchema func(ctx context.Context, id uint32) error
}

func (m *mockService) GetProperties(ctx context.Context) ([]*catalog.Property, error) {
	return m.onGetProperties(ctx)
}

func (m *mockService) CreateProperty(ctx context.Context, input *catalog.PropertyInput) (*catalog.Property, error) {
	return m.onCreateProperty(ctx, input)
}

func (m *mockService) GetProperty(ctx context.Context, id uint32) (*catalog.Property, error) {
	return m.onGetProperty(ctx, id)
}

func (m *mockService) DeleteProperty(ctx context.Context, id uint32) error {
	return m.onDeleteProperty(ctx, id)
}

func (m *mockService) GetPropertyVariants(ctx context.Context, id uint32) ([]*catalog.PropertyVariant, error) {
	return m.onGetPropertyVariants(ctx, id)
}

func (m *mockService) CreatePropertyVariant(ctx context.Context, input *catalog.PropertyVariantInput) (*catalog.PropertyVariant, error) {
	return m.onCreatePropertyVariant(ctx, input)
}

func (m *mockService) GetPropertyVariant(ctx context.Context, id uint32) (*catalog.PropertyVariant, error) {
	return m.onGetPropertyVariant(ctx, id)
}

func (m *mockService) DeletePropertyVariant(ctx context.Context, id uint32) error {
	return m.onDeletePropertyVariant(ctx, id)
}

func (m *mockService) GetProducts(ctx context.Context, query *catalog.StorageProductQuery) (*catalog.StorageProductQueryResult, error) {
	return m.onGetProducts(ctx, query)
}

func (m *mockService) CreateProduct(ctx context.Context, input *catalog.ProductInput) (*catalog.Product, error) {
	return m.onCreateProduct(ctx, input)
}

func (m *mockService) UpdateProduct(ctx context.Context, id uint32, patch *catalog.ProductPatch) (*catalog.Product, error) {
	return m.onUpdateProduct(ctx, id, patch)
}

func (m *mockService) GetProduct(ctx context.Context, id uint32) (*catalog.Product, error) {
	return m.onGetProduct(ctx, id)
}

func (m *mockService) DeleteProduct(ctx context.Context, id uint32) error {
	return m.onDeleteProduct(ctx, id)
}

func (m *mockService) GetCategories(ctx context.Context) ([]*catalog.Category, error) {
	return m.onGetCategories(ctx)
}

func (m *mockService) CreateCategory(ctx context.Context, input *catalog.CategoryInput) (*catalog.Category, error) {
	return m.onCreateCategory(ctx, input)
}

func (m *mockService) GetCategory(ctx context.Context, id uint32) (*catalog.Category, error) {
	return m.onGetCategory(ctx, id)
}

func (m *mockService) UpdateCategory(ctx context.Context, id uint32, p *catalog.CategoryPatch) (*catalog.Category, error) {
	return m.onUpdateCategory(ctx, id, p)
}

func (m *mockService) DeleteCategory(ctx context.Context, id uint32) error {
	return m.onDeleteCategory(ctx, id)
}

func (m *mockService) GetSchemas(ctx context.Context) ([]*catalog.SchemaName, error) {
	return m.onGetSchemas(ctx)
}

func (m *mockService) CreateSchema(ctx context.Context, input *catalog.SchemaInput) (*catalog.Schema, error) {
	return m.onCreateSchema(ctx, input)
}

func (m *mockService) UpdateSchema(ctx context.Context, id uint32, patch *catalog.SchemaPatch) (*catalog.Schema, error) {
	return m.onUpdateSchema(ctx, id, patch)
}

func (m *mockService) GetSchema(ctx context.Context, id uint32) (*catalog.Schema, error) {
	return m.onGetSchema(ctx, id)
}

func (m *mockService) DeleteSchema(ctx context.Context, id uint32) error {
	return m.onDeleteSchema(ctx, id)
}

func initTransportTest(t *testing.T) (*httptest.Server, *Client, *mockService) {
	svc := &mockService{}
	handler := makeHandler(svc)
	server := httptest.NewServer(handler)
	client, err := NewClient(ClientConfig{
		ServiceURL: server.URL,
		Timeout:    time.Second,
	})
	if err != nil {
		t.Fatal(err)
	}
	return server, client, svc
}

func TestTransportGetProperties(t *testing.T) {
	server, client, svc := initTransportTest(t)
	defer server.Close()

	testCases := []struct {
		name   string
		result []*catalog.Property
		err    error
	}{
		{
			name: "ok",
			result: []*catalog.Property{
				mustNewProperty(func(p *catalog.Property) { p.ID = 1 }),
				mustNewProperty(func(p *catalog.Property) { p.ID = 2 }),
				mustNewProperty(func(p *catalog.Property) { p.ID = 3 }),
			},
			err: nil,
		},
		{
			name:   "error bad request",
			result: nil,
			err:    catalog.ErrBadRequest("some validation error"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			svc.onGetProperties = func(ctx context.Context) ([]*catalog.Property, error) {
				return tc.result, tc.err
			}

			gotResult, gotErr := client.GetProperties(context.Background())

			assert.Equal(t, tc.err, gotErr)
			assert.Equal(t, tc.result, gotResult)
		})
	}
}

func TestTransportCreateProperty(t *testing.T) {
	server, client, svc := initTransportTest(t)
	defer server.Close()

	testCases := []struct {
		name     string
		input    *catalog.PropertyInput
		property *catalog.Property
		err      error
	}{
		{
			name:     "ok",
			input:    mustNewPropertyInput(nil),
			property: mustNewProperty(nil),
			err:      nil,
		},
		{
			name:  "error bad request",
			input: mustNewPropertyInput(nil),
			err:   catalog.ErrBadRequest("some validation error"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var gotInput *catalog.PropertyInput
			svc.onCreateProperty = func(ctx context.Context, input *catalog.PropertyInput) (*catalog.Property, error) {
				gotInput = input
				return tc.property, tc.err
			}

			gotProperty, gotErr := client.CreateProperty(context.Background(), tc.input)

			assert.Equal(t, tc.input, gotInput)
			assert.Equal(t, tc.err, gotErr)
			assert.Equal(t, tc.property, gotProperty)
		})
	}
}

func TestTransportGetProperty(t *testing.T) {
	server, client, svc := initTransportTest(t)
	defer server.Close()

	testCases := []struct {
		name     string
		id       uint32
		property *catalog.Property
		err      error
	}{
		{
			name:     "ok",
			id:       123,
			property: mustNewProperty(nil),
			err:      nil,
		},
		{
			name: "error not found",
			id:   123,
			err:  catalog.ErrNotFound("property not found: %d", 123),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var gotID uint32
			svc.onGetProperty = func(ctx context.Context, id uint32) (*catalog.Property, error) {
				gotID = id
				return tc.property, tc.err
			}

			gotProperty, gotErr := client.GetProperty(context.Background(), tc.id)

			assert.Equal(t, tc.id, gotID)
			assert.Equal(t, tc.err, gotErr)
			assert.Equal(t, tc.property, gotProperty)
		})
	}
}

func TestTransportDeleteProperty(t *testing.T) {
	server, client, svc := initTransportTest(t)
	defer server.Close()

	testCases := []struct {
		name string
		id   uint32
		err  error
	}{
		{
			name: "ok",
			id:   123,
			err:  nil,
		},
		{
			name: "error bad request",
			id:   123,
			err:  catalog.ErrBadRequest("some validation error"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var gotID uint32
			svc.onDeleteProperty = func(ctx context.Context, id uint32) error {
				gotID = id
				return tc.err
			}

			gotErr := client.DeleteProperty(context.Background(), tc.id)

			assert.Equal(t, tc.id, gotID)
			assert.Equal(t, tc.err, gotErr)
		})
	}
}

func TestTransportGetPropertyVariants(t *testing.T) {
	server, client, svc := initTransportTest(t)
	defer server.Close()

	testCases := []struct {
		name   string
		id     uint32
		result []*catalog.PropertyVariant
		err    error
	}{
		{
			name: "ok",
			result: []*catalog.PropertyVariant{
				mustNewPropertyVariant(func(p *catalog.PropertyVariant) { p.ID = 1 }),
				mustNewPropertyVariant(func(p *catalog.PropertyVariant) { p.ID = 2 }),
				mustNewPropertyVariant(func(p *catalog.PropertyVariant) { p.ID = 3 }),
			},
			id:  1,
			err: nil,
		},
		{
			name:   "error bad request",
			result: nil,
			id:     0,
			err:    catalog.ErrBadRequest("some validation error"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var gotID uint32
			svc.onGetPropertyVariants = func(ctx context.Context, id uint32) ([]*catalog.PropertyVariant, error) {
				gotID = id
				return tc.result, tc.err
			}

			gotResult, gotErr := client.GetPropertyVariants(context.Background(), tc.id)

			assert.Equal(t, tc.id, gotID)
			assert.Equal(t, tc.err, gotErr)
			assert.Equal(t, tc.result, gotResult)
		})
	}
}

func TestTransportCreatePropertyVariant(t *testing.T) {
	server, client, svc := initTransportTest(t)
	defer server.Close()

	testCases := []struct {
		name    string
		input   *catalog.PropertyVariantInput
		variant *catalog.PropertyVariant
		err     error
	}{
		{
			name:    "ok",
			input:   mustNewPropertyVariantInput(nil),
			variant: mustNewPropertyVariant(nil),
			err:     nil,
		},
		{
			name:  "error bad request",
			input: mustNewPropertyVariantInput(nil),
			err:   catalog.ErrBadRequest("some validation error"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var gotInput *catalog.PropertyVariantInput
			svc.onCreatePropertyVariant = func(ctx context.Context, input *catalog.PropertyVariantInput) (*catalog.PropertyVariant, error) {
				gotInput = input
				return tc.variant, tc.err
			}

			gotProperty, gotErr := client.CreatePropertyVariant(context.Background(), tc.input)

			assert.Equal(t, tc.input, gotInput)
			assert.Equal(t, tc.err, gotErr)
			assert.Equal(t, tc.variant, gotProperty)
		})
	}
}

func TestTransportGetPropertyVariant(t *testing.T) {
	server, client, svc := initTransportTest(t)
	defer server.Close()

	testCases := []struct {
		name    string
		id      uint32
		variant *catalog.PropertyVariant
		err     error
	}{
		{
			name:    "ok",
			id:      123,
			variant: mustNewPropertyVariant(nil),
			err:     nil,
		},
		{
			name: "error not found",
			id:   123,
			err:  catalog.ErrNotFound("variant not found: %d", 123),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var gotID uint32
			svc.onGetPropertyVariant = func(ctx context.Context, id uint32) (*catalog.PropertyVariant, error) {
				gotID = id
				return tc.variant, tc.err
			}

			gotPropertyVariant, gotErr := client.GetPropertyVariant(context.Background(), tc.id)

			assert.Equal(t, tc.id, gotID)
			assert.Equal(t, tc.err, gotErr)
			assert.Equal(t, tc.variant, gotPropertyVariant)
		})
	}
}

func TestTransportDeletePropertyVariant(t *testing.T) {
	server, client, svc := initTransportTest(t)
	defer server.Close()

	testCases := []struct {
		name string
		id   uint32
		err  error
	}{
		{
			name: "ok",
			id:   123,
			err:  nil,
		},
		{
			name: "error bad request",
			id:   123,
			err:  catalog.ErrBadRequest("some validation error"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var gotID uint32
			svc.onDeletePropertyVariant = func(ctx context.Context, id uint32) error {
				gotID = id
				return tc.err
			}

			gotErr := client.DeletePropertyVariant(context.Background(), tc.id)

			assert.Equal(t, tc.id, gotID)
			assert.Equal(t, tc.err, gotErr)
		})
	}
}

func TestTransportGetProducts(t *testing.T) {
	server, client, svc := initTransportTest(t)
	defer server.Close()

	testCases := []struct {
		name   string
		query  *catalog.StorageProductQuery
		result *catalog.StorageProductQueryResult
		err    error
	}{
		{
			name: "ok",
			query: &catalog.StorageProductQuery{
				SupplierID: 123,
				SchemaID:   321,
				Limit:      456,
				Offset:     789,
			},
			result: &catalog.StorageProductQueryResult{
				Products: []*catalog.Product{
					mustNewProduct(func(p *catalog.Product) { p.ID = 1 }),
					mustNewProduct(func(p *catalog.Product) { p.ID = 2 }),
				},
				Total: 12345,
			},
			err: nil,
		},
		{
			name:   "error bad request",
			query:  &catalog.StorageProductQuery{},
			result: nil,
			err:    catalog.ErrBadRequest("some validation error"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var gotQuery *catalog.StorageProductQuery
			svc.onGetProducts = func(ctx context.Context, query *catalog.StorageProductQuery) (*catalog.StorageProductQueryResult, error) {
				gotQuery = query
				return tc.result, tc.err
			}

			gotResult, gotErr := client.GetProducts(context.Background(), tc.query)

			assert.Equal(t, tc.err, gotErr)
			assert.Equal(t, tc.query, gotQuery)
			assert.Equal(t, tc.result, gotResult)
		})
	}
}

func TestTransportCreateProduct(t *testing.T) {
	server, client, svc := initTransportTest(t)
	defer server.Close()

	testCases := []struct {
		name    string
		input   *catalog.ProductInput
		product *catalog.Product
		err     error
	}{
		{
			name:    "ok",
			input:   mustNewProductInput(nil),
			product: mustNewProduct(nil),
			err:     nil,
		},
		{
			name:  "error bad request",
			input: mustNewProductInput(nil),
			err:   catalog.ErrBadRequest("some validation error"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var gotInput *catalog.ProductInput
			svc.onCreateProduct = func(ctx context.Context, input *catalog.ProductInput) (*catalog.Product, error) {
				gotInput = input
				return tc.product, tc.err
			}

			gotProduct, gotErr := client.CreateProduct(context.Background(), tc.input)

			assert.Equal(t, tc.input, gotInput)
			assert.Equal(t, tc.err, gotErr)
			assert.Equal(t, tc.product, gotProduct)
		})
	}
}

func TestTransportUpdateProduct(t *testing.T) {
	server, client, svc := initTransportTest(t)
	defer server.Close()

	testCases := []struct {
		name    string
		id      uint32
		patch   *catalog.ProductPatch
		product *catalog.Product
		err     error
	}{
		{
			name:    "ok",
			id:      1,
			patch:   mustNewProductPatch(nil),
			product: mustNewProduct(nil),
			err:     nil,
		},
		{
			name:  "error not found",
			id:    0,
			patch: mustNewProductPatch(nil),
			err:   catalog.ErrNotFound("product not found: %d", 123),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var gotPatch *catalog.ProductPatch
			svc.onUpdateProduct = func(ctx context.Context, id uint32, patch *catalog.ProductPatch) (*catalog.Product, error) {
				gotPatch = patch
				return tc.product, tc.err
			}

			gotProduct, gotErr := client.UpdateProduct(context.Background(), tc.id, tc.patch)

			assert.Equal(t, tc.patch, gotPatch)
			assert.Equal(t, tc.err, gotErr)
			assert.Equal(t, tc.product, gotProduct)
		})
	}
}

func TestTransportGetProduct(t *testing.T) {
	server, client, svc := initTransportTest(t)
	defer server.Close()

	testCases := []struct {
		name    string
		id      uint32
		product *catalog.Product
		err     error
	}{
		{
			name:    "ok",
			id:      123,
			product: mustNewProduct(nil),
			err:     nil,
		},
		{
			name: "error not found",
			id:   123,
			err:  catalog.ErrNotFound("product not found: %d", 123),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var gotID uint32
			svc.onGetProduct = func(ctx context.Context, id uint32) (*catalog.Product, error) {
				gotID = id
				return tc.product, tc.err
			}

			gotProduct, gotErr := client.GetProduct(context.Background(), tc.id)

			assert.Equal(t, tc.id, gotID)
			assert.Equal(t, tc.err, gotErr)
			assert.Equal(t, tc.product, gotProduct)
		})
	}
}

func TestTransportDeleteProduct(t *testing.T) {
	server, client, svc := initTransportTest(t)
	defer server.Close()

	testCases := []struct {
		name string
		id   uint32
		err  error
	}{
		{
			name: "ok",
			id:   123,
			err:  nil,
		},
		{
			name: "error bad request",
			id:   123,
			err:  catalog.ErrBadRequest("some validation error"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var gotID uint32
			svc.onDeleteProduct = func(ctx context.Context, id uint32) error {
				gotID = id
				return tc.err
			}

			gotErr := client.DeleteProduct(context.Background(), tc.id)

			assert.Equal(t, tc.id, gotID)
			assert.Equal(t, tc.err, gotErr)
		})
	}
}

func TestTransportGetCategories(t *testing.T) {
	server, client, svc := initTransportTest(t)
	defer server.Close()

	testCases := []struct {
		name   string
		result []*catalog.Category
		err    error
	}{
		{
			name: "ok",
			result: []*catalog.Category{
				mustNewCategory(func(p *catalog.Category) { p.ID = 1 }),
				mustNewCategory(func(p *catalog.Category) { p.ID = 2 }),
				mustNewCategory(func(p *catalog.Category) { p.ID = 3 }),
			},
			err: nil,
		},
		{
			name:   "error bad request",
			result: nil,
			err:    catalog.ErrBadRequest("some validation error"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			svc.onGetCategories = func(ctx context.Context) ([]*catalog.Category, error) {
				return tc.result, tc.err
			}

			gotResult, gotErr := client.GetCategories(context.Background())

			assert.Equal(t, tc.err, gotErr)
			assert.Equal(t, tc.result, gotResult)
		})
	}
}

func TestTransportCreateCategory(t *testing.T) {
	server, client, svc := initTransportTest(t)
	defer server.Close()

	testCases := []struct {
		name     string
		input    *catalog.CategoryInput
		category *catalog.Category
		err      error
	}{
		{
			name:     "ok",
			input:    mustNewCategoryInput(nil),
			category: mustNewCategory(nil),
			err:      nil,
		},
		{
			name:  "error bad request",
			input: mustNewCategoryInput(nil),
			err:   catalog.ErrBadRequest("some validation error"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var gotInput *catalog.CategoryInput
			svc.onCreateCategory = func(ctx context.Context, input *catalog.CategoryInput) (*catalog.Category, error) {
				gotInput = input
				return tc.category, tc.err
			}

			gotCategory, gotErr := client.CreateCategory(context.Background(), tc.input)

			assert.Equal(t, tc.input, gotInput)
			assert.Equal(t, tc.err, gotErr)
			assert.Equal(t, tc.category, gotCategory)
		})
	}
}

func TestTransportGetCategory(t *testing.T) {
	server, client, svc := initTransportTest(t)
	defer server.Close()

	testCases := []struct {
		name     string
		id       uint32
		category *catalog.Category
		err      error
	}{
		{
			name:     "ok",
			id:       123,
			category: mustNewCategory(nil),
			err:      nil,
		},
		{
			name: "error not found",
			id:   123,
			err:  catalog.ErrNotFound("category not found: %d", 123),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var gotID uint32
			svc.onGetCategory = func(ctx context.Context, id uint32) (*catalog.Category, error) {
				gotID = id
				return tc.category, tc.err
			}

			gotCategory, gotErr := client.GetCategory(context.Background(), tc.id)

			assert.Equal(t, tc.id, gotID)
			assert.Equal(t, tc.err, gotErr)
			assert.Equal(t, tc.category, gotCategory)
		})
	}
}

func TestTransportUpdateCategory(t *testing.T) {
	server, client, svc := initTransportTest(t)
	defer server.Close()

	testCases := []struct {
		name     string
		id       uint32
		p        *catalog.CategoryPatch
		category *catalog.Category
		err      error
	}{
		{
			name:     "ok",
			id:       1,
			p:        mustNewCategoryPatch(nil),
			category: mustNewCategory(nil),
			err:      nil,
		},
		{
			name: "error bad request",
			id:   0,
			p:    mustNewCategoryPatch(nil),
			err:  catalog.ErrNotFound("category not found: %d", 123),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var gotPatch *catalog.CategoryPatch
			svc.onUpdateCategory = func(ctx context.Context, id uint32, p *catalog.CategoryPatch) (*catalog.Category, error) {
				gotPatch = p
				return tc.category, tc.err
			}

			gotCategory, gotErr := client.UpdateCategory(context.Background(), tc.id, tc.p)

			assert.Equal(t, tc.p, gotPatch)
			assert.Equal(t, tc.err, gotErr)
			assert.Equal(t, tc.category, gotCategory)
		})
	}
}

func TestTransportDeleteCategory(t *testing.T) {
	server, client, svc := initTransportTest(t)
	defer server.Close()

	testCases := []struct {
		name string
		id   uint32
		err  error
	}{
		{
			name: "ok",
			id:   123,
			err:  nil,
		},
		{
			name: "error bad request",
			id:   123,
			err:  catalog.ErrBadRequest("some validation error"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var gotID uint32
			svc.onDeleteCategory = func(ctx context.Context, id uint32) error {
				gotID = id
				return tc.err
			}

			gotErr := client.DeleteCategory(context.Background(), tc.id)

			assert.Equal(t, tc.id, gotID)
			assert.Equal(t, tc.err, gotErr)
		})
	}
}

func TestTransportGetSchemas(t *testing.T) {
	server, client, svc := initTransportTest(t)
	defer server.Close()

	testCases := []struct {
		name    string
		schemas []*catalog.SchemaName
		err     error
	}{
		{
			name: "ok",
			schemas: []*catalog.SchemaName{
				{ID: 1, Name: "name 1"},
				{ID: 2, Name: "name 2"},
			},
			err: nil,
		},
		{
			name: "error bad request",
			err:  catalog.ErrBadRequest("some validation error"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			svc.onGetSchemas = func(ctx context.Context) ([]*catalog.SchemaName, error) {
				return tc.schemas, tc.err
			}

			gotSchemas, gotErr := client.GetSchemas(context.Background())

			assert.Equal(t, tc.err, gotErr)
			assert.Equal(t, tc.schemas, gotSchemas)
		})
	}
}

func TestTransportCreateSchema(t *testing.T) {
	server, client, svc := initTransportTest(t)
	defer server.Close()

	testCases := []struct {
		name   string
		input  *catalog.SchemaInput
		schema *catalog.Schema
		err    error
	}{
		{
			name:   "ok",
			input:  mustNewSchemaInput(nil),
			schema: mustNewSchema(nil),
			err:    nil,
		},
		{
			name:  "error bad request",
			input: mustNewSchemaInput(nil),
			err:   catalog.ErrBadRequest("some validation error"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var gotInput *catalog.SchemaInput
			svc.onCreateSchema = func(ctx context.Context, input *catalog.SchemaInput) (*catalog.Schema, error) {
				gotInput = input
				return tc.schema, tc.err
			}

			gotSchema, gotErr := client.CreateSchema(context.Background(), tc.input)

			assert.Equal(t, tc.input, gotInput)
			assert.Equal(t, tc.err, gotErr)
			assert.Equal(t, tc.schema, gotSchema)
		})
	}
}

func TestTransportUpdateSchema(t *testing.T) {
	server, client, svc := initTransportTest(t)
	defer server.Close()

	testCases := []struct {
		name   string
		id     uint32
		patch  *catalog.SchemaPatch
		schema *catalog.Schema
		err    error
	}{
		{
			name:   "ok",
			id:     1,
			patch:  mustNewSchemaPatch(nil),
			schema: mustNewSchema(nil),
			err:    nil,
		},
		{
			name:  "error not found",
			id:    0,
			patch: mustNewSchemaPatch(nil),
			err:   catalog.ErrNotFound("schema not found: %d", 123),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var gotPatch *catalog.SchemaPatch
			svc.onUpdateSchema = func(ctx context.Context, id uint32, patch *catalog.SchemaPatch) (*catalog.Schema, error) {
				gotPatch = patch
				return tc.schema, tc.err
			}

			gotSchema, gotErr := client.UpdateSchema(context.Background(), tc.id, tc.patch)

			assert.Equal(t, tc.patch, gotPatch)
			assert.Equal(t, tc.err, gotErr)
			assert.Equal(t, tc.schema, gotSchema)
		})
	}
}

func TestTransportGetSchema(t *testing.T) {
	server, client, svc := initTransportTest(t)
	defer server.Close()

	testCases := []struct {
		name   string
		id     uint32
		schema *catalog.Schema
		err    error
	}{
		{
			name:   "ok",
			id:     123,
			schema: mustNewSchema(nil),
			err:    nil,
		},
		{
			name: "error not found",
			id:   123,
			err:  catalog.ErrNotFound("schema not found: %d", 123),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var gotID uint32
			svc.onGetSchema = func(ctx context.Context, id uint32) (*catalog.Schema, error) {
				gotID = id
				return tc.schema, tc.err
			}

			gotSchema, gotErr := client.GetSchema(context.Background(), tc.id)

			assert.Equal(t, tc.id, gotID)
			assert.Equal(t, tc.err, gotErr)
			assert.Equal(t, tc.schema, gotSchema)
		})
	}
}

func TestTransportDeleteSchema(t *testing.T) {
	server, client, svc := initTransportTest(t)
	defer server.Close()

	testCases := []struct {
		name string
		id   uint32
		err  error
	}{
		{
			name: "ok",
			id:   123,
			err:  nil,
		},
		{
			name: "error bad request",
			id:   123,
			err:  catalog.ErrBadRequest("some validation error"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var gotID uint32
			svc.onDeleteSchema = func(ctx context.Context, id uint32) error {
				gotID = id
				return tc.err
			}

			gotErr := client.DeleteSchema(context.Background(), tc.id)

			assert.Equal(t, tc.id, gotID)
			assert.Equal(t, tc.err, gotErr)
		})
	}
}

func mustNewProperty(fn func(p *catalog.Property)) *catalog.Property {
	p := &catalog.Property{
		ID:      1,
		Key:     "key",
		Name:    "name",
		Type:    catalog.PropertyTypeEnumList,
		Indexed: true,
	}
	if fn != nil {
		fn(p)
	}
	return p
}

func mustNewPropertyInput(fn func(pi *catalog.PropertyInput)) *catalog.PropertyInput {
	pi := &catalog.PropertyInput{
		Key:     "key",
		Name:    "name",
		Type:    catalog.PropertyTypeEnumList,
		Indexed: true,
	}
	if fn != nil {
		fn(pi)
	}
	return pi
}

func mustNewPropertyVariant(fn func(pv *catalog.PropertyVariant)) *catalog.PropertyVariant {
	pv := &catalog.PropertyVariant{
		ID:         1,
		PropertyID: 1,
		Value:      "value",
	}
	if fn != nil {
		fn(pv)
	}
	return pv
}

func mustNewPropertyVariantInput(fn func(pvi *catalog.PropertyVariantInput)) *catalog.PropertyVariantInput {
	pvi := &catalog.PropertyVariantInput{
		PropertyID: 1,
		Value:      "value",
	}
	if fn != nil {
		fn(pvi)
	}
	return pvi
}

func mustNewCategory(fn func(c *catalog.Category)) *catalog.Category {
	c := &catalog.Category{
		ID:       1,
		ParentID: 0,
		Name:     "Name",
		Order:    0,
		Filters: []*catalog.IndexFilter{{
			Type:         "enum",
			Key:          1,
			EnumVariants: []uint32{11},
		}},
		FacetKeys: []uint32{2, 3},
	}
	if fn != nil {
		fn(c)
	}
	return c
}

func mustNewCategoryInput(fn func(ci *catalog.CategoryInput)) *catalog.CategoryInput {
	ci := &catalog.CategoryInput{
		ParentID: 0,
		Name:     "Name",
		Order:    0,
		Filters: []*catalog.IndexFilter{{
			Type:         "enum",
			Key:          1,
			EnumVariants: []uint32{11},
		}},
		FacetKeys: []uint32{2, 3},
	}
	if fn != nil {
		fn(ci)
	}
	return ci
}

func mustNewCategoryPatch(fn func(cp *catalog.CategoryPatch)) *catalog.CategoryPatch {
	parent := uint32(0)
	name := "PatchName"
	order := uint32(0)
	cp := &catalog.CategoryPatch{
		ParentID: &parent,
		Name:     &name,
		Order:    &order,
		Filters: []*catalog.IndexFilter{{
			Type:         "enum",
			Key:          1,
			EnumVariants: []uint32{11},
		}},
		FacetKeys: []uint32{2, 3},
	}
	if fn != nil {
		fn(cp)
	}
	return cp
}

func mustNewProduct(fn func(p *catalog.Product)) *catalog.Product {
	p := &catalog.Product{
		ID:         1,
		SupplierID: 2,
		Properties: []*catalog.ProductProperty{
			{
				ID:       1,
				Key:      "key",
				Name:     "name",
				Type:     catalog.PropertyTypeEnumList,
				Value:    "v1",
				ValueIDs: []uint32{1},
				Indexed:  true,
			},
		},
	}
	if fn != nil {
		fn(p)
	}
	return p
}

func mustNewProductInput(fn func(p *catalog.ProductInput)) *catalog.ProductInput {
	p := &catalog.ProductInput{
		SupplierID: 2,
		Properties: []*catalog.ProductPropertyInput{
			{
				ID:       1,
				Value:    "v1",
				ValueIDs: []uint32{1},
			},
		},
	}
	if fn != nil {
		fn(p)
	}
	return p
}

func mustNewProductPatch(fn func(p *catalog.ProductPatch)) *catalog.ProductPatch {
	p := &catalog.ProductPatch{
		Properties: []*catalog.ProductPropertyInput{
			{
				ID:       1,
				Value:    "v1",
				ValueIDs: []uint32{1},
			},
		},
		Partial: true,
	}
	if fn != nil {
		fn(p)
	}
	return p
}

func mustNewSchema(fn func(p *catalog.Schema)) *catalog.Schema {
	p := &catalog.Schema{
		ID:   1,
		Name: "schema name",
		Properties: []*catalog.SchemaProperty{
			{
				ID:       1,
				Key:      "key 1",
				Name:     "name 1",
				Type:     catalog.PropertyTypeEnumList,
				Indexed:  false,
				Required: true,
			},
			{
				ID:       2,
				Key:      "key 2",
				Name:     "name 2",
				Type:     catalog.PropertyTypeString,
				Indexed:  true,
				Required: false,
			},
		},
	}
	if fn != nil {
		fn(p)
	}
	return p
}

func mustNewSchemaInput(fn func(p *catalog.SchemaInput)) *catalog.SchemaInput {
	p := &catalog.SchemaInput{
		Name: "schema name",
		Properties: []*catalog.SchemaPropertyInput{
			{
				ID:       1,
				Required: true,
			},
			{
				ID:       2,
				Required: false,
			},
		},
	}
	if fn != nil {
		fn(p)
	}
	return p
}

func mustNewSchemaPatch(fn func(p *catalog.SchemaPatch)) *catalog.SchemaPatch {
	name := "schema name"
	p := &catalog.SchemaPatch{
		Name: &name,
		Properties: []*catalog.SchemaPropertyInput{
			{
				ID:       1,
				Required: true,
			},
			{
				ID:       2,
				Required: false,
			},
		},
	}
	if fn != nil {
		fn(p)
	}
	return p
}
