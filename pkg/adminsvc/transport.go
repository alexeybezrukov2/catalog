package adminsvc

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
	"github.com/gorilla/mux"
)

type getPropertiesRequest struct {
}

type getPropertiesResponse struct {
	properties []*catalog.Property
}

func encodeGetPropertiesRequest(ctx context.Context, r *http.Request, request interface{}) error {
	r.URL.Path = "/api/v1/properties"
	return nil
}

func decodeGetPropertiesRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	return getPropertiesRequest{}, nil
}

func encodeGetPropertiesResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(getPropertiesResponse)
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(res.properties); err != nil {
		return catalog.ErrInternal("failed to encode JSON response: %s", err)
	}
	return nil
}

func decodeGetPropertiesResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return nil, decodeError(r)
	}
	var properties []*catalog.Property
	if err := json.NewDecoder(r.Body).Decode(&properties); err != nil {
		return nil, fmt.Errorf("failed to decode JSON response: %w", err)
	}
	return getPropertiesResponse{properties: properties}, nil
}

type createPropertyRequest struct {
	input *catalog.PropertyInput
}

type createPropertyResponse struct {
	property *catalog.Property
}

func encodeCreatePropertyRequest(ctx context.Context, r *http.Request, request interface{}) error {
	req := request.(createPropertyRequest)
	r.URL.Path = "/api/v1/properties"
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(req.input); err != nil {
		return err
	}
	r.Body = ioutil.NopCloser(&buf)
	return nil
}

func decodeCreatePropertyRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var input catalog.PropertyInput
	if err := json.NewDecoder(r.Body).Decode(&input); err != nil {
		return nil, catalog.ErrBadRequest("failed to decode JSON request: %v", err)
	}
	return createPropertyRequest{input: &input}, nil
}

func encodeCreatePropertyResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(createPropertyResponse)
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(res.property); err != nil {
		return catalog.ErrInternal("failed to encode JSON response: %s", err)
	}
	return nil
}

func decodeCreatePropertyResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return nil, decodeError(r)
	}
	var property catalog.Property
	if err := json.NewDecoder(r.Body).Decode(&property); err != nil {
		return nil, fmt.Errorf("failed to decode JSON response: %w", err)
	}
	return createPropertyResponse{property: &property}, nil
}

type getPropertyRequest struct {
	id uint32
}

type getPropertyResponse struct {
	property *catalog.Property
}

func encodeGetPropertyRequest(ctx context.Context, r *http.Request, request interface{}) error {
	req := request.(getPropertyRequest)
	r.URL.Path = "/api/v1/properties/" + strconv.FormatUint(uint64(req.id), 10)
	return nil
}

func decodeGetPropertyRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 32)
	if err != nil {
		return nil, catalog.ErrBadRequest("failed to parse id: %v", err)
	}
	return getPropertyRequest{id: uint32(id)}, nil
}

func encodeGetPropertyResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(getPropertyResponse)
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(res.property); err != nil {
		return catalog.ErrInternal("failed to encode JSON response: %s", err)
	}
	return nil
}

func decodeGetPropertyResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return nil, decodeError(r)
	}
	var property catalog.Property
	if err := json.NewDecoder(r.Body).Decode(&property); err != nil {
		return nil, fmt.Errorf("failed to decode JSON response: %w", err)
	}
	return getPropertyResponse{property: &property}, nil
}

type deletePropertyRequest struct {
	id uint32
}

type deletePropertyResponse struct {
}

func encodeDeletePropertyRequest(ctx context.Context, r *http.Request, request interface{}) error {
	req := request.(deletePropertyRequest)
	r.URL.Path = "/api/v1/properties/" + strconv.FormatUint(uint64(req.id), 10)
	return nil
}

func decodeDeletePropertyRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 32)
	if err != nil {
		return nil, catalog.ErrBadRequest("failed to parse id: %v", err)
	}
	return deletePropertyRequest{id: uint32(id)}, nil
}

func encodeDeletePropertyResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	w.WriteHeader(http.StatusNoContent)
	return nil
}

func decodeDeletePropertyResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return nil, decodeError(r)
	}
	return deletePropertyResponse{}, nil
}

type getPropertyVariantsRequest struct {
	id uint32
}

type getPropertyVariantsResponse struct {
	variants []*catalog.PropertyVariant
}

func encodeGetPropertyVariantsRequest(ctx context.Context, r *http.Request, request interface{}) error {
	req := request.(getPropertyVariantsRequest)
	r.URL.Path = "/api/v1/property_variants"
	r.URL.RawQuery = "property_id=" + strconv.FormatUint(uint64(req.id), 10)
	return nil
}

func decodeGetPropertyVariantsRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	id, err := strconv.ParseUint(r.URL.Query().Get("property_id"), 10, 32)
	if err != nil {
		return nil, catalog.ErrBadRequest("failed to parse id: %v", err)
	}
	return getPropertyVariantsRequest{id: uint32(id)}, nil
}

func encodeGetPropertyVariantsResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(getPropertyVariantsResponse)
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(res.variants); err != nil {
		return catalog.ErrInternal("failed to encode JSON response: %s", err)
	}
	return nil
}

func decodeGetPropertyVariantsResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return nil, decodeError(r)
	}
	var variants []*catalog.PropertyVariant
	if err := json.NewDecoder(r.Body).Decode(&variants); err != nil {
		return nil, fmt.Errorf("failed to decode JSON response: %w", err)
	}
	return getPropertyVariantsResponse{variants: variants}, nil
}

type createPropertyVariantRequest struct {
	input *catalog.PropertyVariantInput
}

type createPropertyVariantResponse struct {
	variant *catalog.PropertyVariant
}

func encodeCreatePropertyVariantRequest(ctx context.Context, r *http.Request, request interface{}) error {
	req := request.(createPropertyVariantRequest)
	r.URL.Path = "/api/v1/property_variants"
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(req.input); err != nil {
		return err
	}
	r.Body = ioutil.NopCloser(&buf)
	return nil
}

func decodeCreatePropertyVariantRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var input catalog.PropertyVariantInput
	if err := json.NewDecoder(r.Body).Decode(&input); err != nil {
		return nil, catalog.ErrBadRequest("failed to decode JSON request: %v", err)
	}
	return createPropertyVariantRequest{input: &input}, nil
}

func encodeCreatePropertyVariantResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(createPropertyVariantResponse)
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(res.variant); err != nil {
		return catalog.ErrInternal("failed to encode JSON response: %s", err)
	}
	return nil
}

func decodeCreatePropertyVariantResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return nil, decodeError(r)
	}
	var variant catalog.PropertyVariant
	if err := json.NewDecoder(r.Body).Decode(&variant); err != nil {
		return nil, fmt.Errorf("failed to decode JSON response: %w", err)
	}
	return createPropertyVariantResponse{variant: &variant}, nil
}

type getPropertyVariantRequest struct {
	id uint32
}

type getPropertyVariantResponse struct {
	variant *catalog.PropertyVariant
}

func encodeGetPropertyVariantRequest(ctx context.Context, r *http.Request, request interface{}) error {
	req := request.(getPropertyVariantRequest)
	r.URL.Path = "/api/v1/property_variants/" + strconv.FormatUint(uint64(req.id), 10)
	return nil
}

func decodeGetPropertyVariantRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 32)
	if err != nil {
		return nil, catalog.ErrBadRequest("failed to parse id: %v", err)
	}
	return getPropertyVariantRequest{id: uint32(id)}, nil
}

func encodeGetPropertyVariantResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(getPropertyVariantResponse)
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(res.variant); err != nil {
		return catalog.ErrInternal("failed to encode JSON response: %s", err)
	}
	return nil
}

func decodeGetPropertyVariantResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return nil, decodeError(r)
	}
	var variant *catalog.PropertyVariant
	if err := json.NewDecoder(r.Body).Decode(&variant); err != nil {
		return nil, fmt.Errorf("failed to decode JSON response: %w", err)
	}
	return getPropertyVariantResponse{variant: variant}, nil
}

type deletePropertyVariantRequest struct {
	id uint32
}

type deletePropertyVariantResponse struct {
}

func encodeDeletePropertyVariantRequest(ctx context.Context, r *http.Request, request interface{}) error {
	req := request.(deletePropertyVariantRequest)
	r.URL.Path = "/api/v1/property_variants/" + strconv.FormatUint(uint64(req.id), 10)
	return nil
}

func decodeDeletePropertyVariantRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 32)
	if err != nil {
		return nil, catalog.ErrBadRequest("failed to parse id: %v", err)
	}
	return deletePropertyVariantRequest{id: uint32(id)}, nil
}

func encodeDeletePropertyVariantResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	w.WriteHeader(http.StatusNoContent)
	return nil
}

func decodeDeletePropertyVariantResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return nil, decodeError(r)
	}
	return deletePropertyVariantResponse{}, nil
}

type getProductsRequest struct {
	query *catalog.StorageProductQuery
}

type getProductsResponse struct {
	result *catalog.StorageProductQueryResult
}

func encodeGetProductsRequest(ctx context.Context, r *http.Request, request interface{}) error {
	req := request.(getProductsRequest)
	r.URL.Path = "/api/v1/products"
	query := r.URL.Query()
	query.Set("supplier_id", strconv.FormatUint(uint64(req.query.SupplierID), 10))
	query.Set("schema_id", strconv.FormatUint(uint64(req.query.SchemaID), 10))
	query.Set("limit", strconv.FormatUint(uint64(req.query.Limit), 10))
	query.Set("offset", strconv.FormatUint(uint64(req.query.Offset), 10))
	r.URL.RawQuery = query.Encode()
	return nil
}

func decodeGetProductsRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	req := getProductsRequest{query: &catalog.StorageProductQuery{}}
	q := r.URL.Query()

	if val := q.Get("supplier_id"); val != "" {
		supplierID, err := strconv.ParseUint(val, 10, 32)
		if err != nil {
			return nil, catalog.ErrBadRequest("invalid supplier_id parameter: %q", val)
		}
		req.query.SupplierID = uint32(supplierID)
	}

	if val := q.Get("schema_id"); val != "" {
		schemaID, err := strconv.ParseUint(val, 10, 32)
		if err != nil {
			return nil, catalog.ErrBadRequest("invalid schema_id parameter: %q", val)
		}
		req.query.SchemaID = uint32(schemaID)
	}

	if val := q.Get("limit"); val != "" {
		limit, err := strconv.ParseUint(val, 10, 32)
		if err != nil {
			return nil, catalog.ErrBadRequest("invalid limit parameter: %q", val)
		}
		req.query.Limit = uint32(limit)
	}

	if val := q.Get("offset"); val != "" {
		offset, err := strconv.ParseUint(val, 10, 32)
		if err != nil {
			return nil, catalog.ErrBadRequest("invalid offset parameter: %q", val)
		}
		req.query.Offset = uint32(offset)
	}

	return req, nil
}

func encodeGetProductsResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(getProductsResponse)
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(res.result); err != nil {
		return catalog.ErrInternal("failed to encode JSON response: %s", err)
	}
	return nil
}

func decodeGetProductsResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return nil, decodeError(r)
	}
	var result catalog.StorageProductQueryResult
	if err := json.NewDecoder(r.Body).Decode(&result); err != nil {
		return nil, fmt.Errorf("failed to decode JSON response: %w", err)
	}
	return getProductsResponse{result: &result}, nil
}

type createProductRequest struct {
	input *catalog.ProductInput
}

type createProductResponse struct {
	product *catalog.Product
}

func encodeCreateProductRequest(ctx context.Context, r *http.Request, request interface{}) error {
	req := request.(createProductRequest)
	r.URL.Path = "/api/v1/products"
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(req.input); err != nil {
		return err
	}
	r.Body = ioutil.NopCloser(&buf)
	return nil
}

func decodeCreateProductRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var input catalog.ProductInput
	if err := json.NewDecoder(r.Body).Decode(&input); err != nil {
		return nil, catalog.ErrBadRequest("failed to decode JSON request: %v", err)
	}
	return createProductRequest{input: &input}, nil
}

func encodeCreateProductResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(createProductResponse)
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(res.product); err != nil {
		return catalog.ErrInternal("failed to encode JSON response: %s", err)
	}
	return nil
}

func decodeCreateProductResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return nil, decodeError(r)
	}
	var product catalog.Product
	if err := json.NewDecoder(r.Body).Decode(&product); err != nil {
		return nil, fmt.Errorf("failed to decode JSON response: %w", err)
	}
	return createProductResponse{product: &product}, nil
}

type updateProductRequest struct {
	id    uint32
	patch *catalog.ProductPatch
}

type updateProductResponse struct {
	product *catalog.Product
}

func encodeUpdateProductRequest(ctx context.Context, r *http.Request, request interface{}) error {
	req := request.(updateProductRequest)
	r.URL.Path = "/api/v1/products/" + strconv.FormatUint(uint64(req.id), 10)
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(req.patch); err != nil {
		return err
	}
	r.Body = ioutil.NopCloser(&buf)
	return nil
}

func decodeUpdateProductRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 32)
	if err != nil {
		return nil, catalog.ErrBadRequest("failed to parse id: %v", err)
	}
	var patch catalog.ProductPatch
	if err := json.NewDecoder(r.Body).Decode(&patch); err != nil {
		return nil, catalog.ErrBadRequest("failed to decode JSON request: %v", err)
	}
	return updateProductRequest{id: uint32(id), patch: &patch}, nil
}

func encodeUpdateProductResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(updateProductResponse)
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(res.product); err != nil {
		return catalog.ErrInternal("failed to encode JSON response: %s", err)
	}
	return nil
}

func decodeUpdateProductResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return nil, decodeError(r)
	}
	var product catalog.Product
	if err := json.NewDecoder(r.Body).Decode(&product); err != nil {
		return nil, fmt.Errorf("failed to decode JSON response: %w", err)
	}
	return updateProductResponse{product: &product}, nil
}

type getProductRequest struct {
	id uint32
}

type getProductResponse struct {
	product *catalog.Product
}

func encodeGetProductRequest(ctx context.Context, r *http.Request, request interface{}) error {
	req := request.(getProductRequest)
	r.URL.Path = "/api/v1/products/" + strconv.FormatUint(uint64(req.id), 10)
	return nil
}

func decodeGetProductRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 32)
	if err != nil {
		return nil, catalog.ErrBadRequest("failed to parse id: %v", err)
	}
	return getProductRequest{id: uint32(id)}, nil
}

func encodeGetProductResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(getProductResponse)
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(res.product); err != nil {
		return catalog.ErrInternal("failed to encode JSON response: %s", err)
	}
	return nil
}

func decodeGetProductResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return nil, decodeError(r)
	}
	var product catalog.Product
	if err := json.NewDecoder(r.Body).Decode(&product); err != nil {
		return nil, fmt.Errorf("failed to decode JSON response: %w", err)
	}
	return getProductResponse{product: &product}, nil
}

type deleteProductRequest struct {
	id uint32
}

type deleteProductResponse struct {
}

func encodeDeleteProductRequest(ctx context.Context, r *http.Request, request interface{}) error {
	req := request.(deleteProductRequest)
	r.URL.Path = "/api/v1/products/" + strconv.FormatUint(uint64(req.id), 10)
	return nil
}

func decodeDeleteProductRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 32)
	if err != nil {
		return nil, catalog.ErrBadRequest("failed to parse id: %v", err)
	}
	return deleteProductRequest{id: uint32(id)}, nil
}

func encodeDeleteProductResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	w.WriteHeader(http.StatusNoContent)
	return nil
}

func decodeDeleteProductResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return nil, decodeError(r)
	}
	return deleteProductResponse{}, nil
}

type getCategoriesRequest struct {
}

type getCategoriesResponse struct {
	categories []*catalog.Category
}

func encodeGetCategoriesRequest(ctx context.Context, r *http.Request, request interface{}) error {
	r.URL.Path = "/api/v1/categories"
	return nil
}

func decodeGetCategoriesRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	return getCategoriesRequest{}, nil
}

func encodeGetCategoriesResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(getCategoriesResponse)
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(res.categories); err != nil {
		return catalog.ErrInternal("failed to encode JSON response: %s", err)
	}

	return nil
}

func decodeGetCategoriesResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return nil, decodeError(r)
	}
	res := getCategoriesResponse{}
	if err := json.NewDecoder(r.Body).Decode(&res.categories); err != nil {
		return nil, fmt.Errorf("failed to decode JSON response: %w", err)
	}
	return res, nil
}

type createCategoryRequest struct {
	input *catalog.CategoryInput
}

type createCategoryResponse struct {
	category *catalog.Category
}

func encodeCreateCategoryRequest(ctx context.Context, r *http.Request, request interface{}) error {
	req := request.(createCategoryRequest)
	r.URL.Path = "/api/v1/categories"
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(req.input); err != nil {
		return err
	}
	r.Body = ioutil.NopCloser(&buf)
	return nil
}

func decodeCreateCategoryRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var input catalog.CategoryInput
	if err := json.NewDecoder(r.Body).Decode(&input); err != nil {
		return nil, catalog.ErrBadRequest("failed to decode JSON request: %v", err)
	}
	return createCategoryRequest{input: &input}, nil
}

func encodeCreateCategoryResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(createCategoryResponse)
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(res.category); err != nil {
		return catalog.ErrInternal("failed to encode JSON response: %s", err)
	}
	return nil
}

func decodeCreateCategoryResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return nil, decodeError(r)
	}
	var category catalog.Category
	if err := json.NewDecoder(r.Body).Decode(&category); err != nil {
		return nil, fmt.Errorf("failed to decode JSON response: %w", err)
	}
	return createCategoryResponse{category: &category}, nil
}

type getCategoryRequest struct {
	id uint32
}

type getCategoryResponse struct {
	category *catalog.Category
}

func encodeGetCategoryRequest(ctx context.Context, r *http.Request, request interface{}) error {
	req := request.(getCategoryRequest)
	r.URL.Path = "/api/v1/categories/" + strconv.FormatUint(uint64(req.id), 10)
	return nil
}

func decodeGetCategoryRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 32)
	if err != nil {
		return nil, catalog.ErrBadRequest("failed to parse id: %v", err)
	}
	return getCategoryRequest{id: uint32(id)}, nil
}

func encodeGetCategoryResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(getCategoryResponse)
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(res.category); err != nil {
		return catalog.ErrInternal("failed to encode JSON response: %s", err)
	}
	return nil
}

func decodeGetCategoryResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return nil, decodeError(r)
	}
	var category *catalog.Category
	if err := json.NewDecoder(r.Body).Decode(&category); err != nil {
		return nil, fmt.Errorf("failed to decode JSON response: %w", err)
	}
	return getCategoryResponse{category: category}, nil
}

type updateCategoryRequest struct {
	id    uint32
	patch *catalog.CategoryPatch
}

type updateCategoryResponse struct {
	category *catalog.Category
}

func encodeUpdateCategoryRequest(ctx context.Context, r *http.Request, request interface{}) error {
	req := request.(updateCategoryRequest)
	r.URL.Path = "/api/v1/categories/" + strconv.FormatUint(uint64(req.id), 10)
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(req.patch); err != nil {
		return err
	}
	r.Body = ioutil.NopCloser(&buf)
	return nil
}

func decodeUpdateCategoryRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var input catalog.CategoryPatch
	id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 32)
	if err != nil {
		return nil, catalog.ErrBadRequest("failed to parse id: %v", err)
	}
	if err := json.NewDecoder(r.Body).Decode(&input); err != nil {
		return nil, catalog.ErrBadRequest("failed to decode JSON request: %v", err)
	}
	return updateCategoryRequest{patch: &input, id: uint32(id)}, nil
}

func encodeUpdateCategoryResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(updateCategoryResponse)
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(res.category); err != nil {
		return catalog.ErrInternal("failed to encode JSON response: %s", err)
	}
	return nil
}

func decodeUpdateCategoryResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return nil, decodeError(r)
	}
	var category catalog.Category
	if err := json.NewDecoder(r.Body).Decode(&category); err != nil {
		return nil, fmt.Errorf("failed to decode JSON response: %w", err)
	}
	return updateCategoryResponse{category: &category}, nil
}

type deleteCategoryRequest struct {
	id uint32
}

type deleteCategoryResponse struct {
}

func encodeDeleteCategoryRequest(ctx context.Context, r *http.Request, request interface{}) error {
	req := request.(deleteCategoryRequest)
	r.URL.Path = "/api/v1/categories/" + strconv.FormatUint(uint64(req.id), 10)
	return nil
}

func decodeDeleteCategoryRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 32)
	if err != nil {
		return nil, catalog.ErrBadRequest("failed to parse id: %v", err)
	}
	return deleteCategoryRequest{id: uint32(id)}, nil
}

func encodeDeleteCategoryResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	w.WriteHeader(http.StatusNoContent)
	return nil
}

func decodeDeleteCategoryResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return nil, decodeError(r)
	}
	return deleteCategoryResponse{}, nil
}

type getSchemasRequest struct {
}

type getSchemasResponse struct {
	schemas []*catalog.SchemaName
}

func encodeGetSchemasRequest(ctx context.Context, r *http.Request, request interface{}) error {
	r.URL.Path = "/api/v1/schemas"
	return nil
}

func decodeGetSchemasRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	return getSchemasRequest{}, nil
}

func encodeGetSchemasResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(getSchemasResponse)
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(res.schemas); err != nil {
		return catalog.ErrInternal("failed to encode JSON response: %s", err)
	}
	return nil
}

func decodeGetSchemasResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return nil, decodeError(r)
	}
	var schemas []*catalog.SchemaName
	if err := json.NewDecoder(r.Body).Decode(&schemas); err != nil {
		return nil, fmt.Errorf("failed to decode JSON response: %w", err)
	}
	return getSchemasResponse{schemas: schemas}, nil
}

type createSchemaRequest struct {
	input *catalog.SchemaInput
}

type createSchemaResponse struct {
	schema *catalog.Schema
}

func encodeCreateSchemaRequest(ctx context.Context, r *http.Request, request interface{}) error {
	req := request.(createSchemaRequest)
	r.URL.Path = "/api/v1/schemas"
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(req.input); err != nil {
		return err
	}
	r.Body = ioutil.NopCloser(&buf)
	return nil
}

func decodeCreateSchemaRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var input catalog.SchemaInput
	if err := json.NewDecoder(r.Body).Decode(&input); err != nil {
		return nil, catalog.ErrBadRequest("failed to decode JSON request: %v", err)
	}
	return createSchemaRequest{input: &input}, nil
}

func encodeCreateSchemaResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(createSchemaResponse)
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(res.schema); err != nil {
		return catalog.ErrInternal("failed to encode JSON response: %s", err)
	}
	return nil
}

func decodeCreateSchemaResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return nil, decodeError(r)
	}
	var schema catalog.Schema
	if err := json.NewDecoder(r.Body).Decode(&schema); err != nil {
		return nil, fmt.Errorf("failed to decode JSON response: %w", err)
	}
	return createSchemaResponse{schema: &schema}, nil
}

type updateSchemaRequest struct {
	id    uint32
	patch *catalog.SchemaPatch
}

type updateSchemaResponse struct {
	schema *catalog.Schema
}

func encodeUpdateSchemaRequest(ctx context.Context, r *http.Request, request interface{}) error {
	req := request.(updateSchemaRequest)
	r.URL.Path = "/api/v1/schemas/" + strconv.FormatUint(uint64(req.id), 10)
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(req.patch); err != nil {
		return err
	}
	r.Body = ioutil.NopCloser(&buf)
	return nil
}

func decodeUpdateSchemaRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 32)
	if err != nil {
		return nil, catalog.ErrBadRequest("failed to parse id: %v", err)
	}
	var patch catalog.SchemaPatch
	if err := json.NewDecoder(r.Body).Decode(&patch); err != nil {
		return nil, catalog.ErrBadRequest("failed to decode JSON request: %v", err)
	}
	return updateSchemaRequest{id: uint32(id), patch: &patch}, nil
}

func encodeUpdateSchemaResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(updateSchemaResponse)
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(res.schema); err != nil {
		return catalog.ErrInternal("failed to encode JSON response: %s", err)
	}
	return nil
}

func decodeUpdateSchemaResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return nil, decodeError(r)
	}
	var schema catalog.Schema
	if err := json.NewDecoder(r.Body).Decode(&schema); err != nil {
		return nil, fmt.Errorf("failed to decode JSON response: %w", err)
	}
	return updateSchemaResponse{schema: &schema}, nil
}

type getSchemaRequest struct {
	id uint32
}

type getSchemaResponse struct {
	schema *catalog.Schema
}

func encodeGetSchemaRequest(ctx context.Context, r *http.Request, request interface{}) error {
	req := request.(getSchemaRequest)
	r.URL.Path = "/api/v1/schemas/" + strconv.FormatUint(uint64(req.id), 10)
	return nil
}

func decodeGetSchemaRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 32)
	if err != nil {
		return nil, catalog.ErrBadRequest("failed to parse id: %v", err)
	}
	return getSchemaRequest{id: uint32(id)}, nil
}

func encodeGetSchemaResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(getSchemaResponse)
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(res.schema); err != nil {
		return catalog.ErrInternal("failed to encode JSON response: %s", err)
	}
	return nil
}

func decodeGetSchemaResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return nil, decodeError(r)
	}
	var schema catalog.Schema
	if err := json.NewDecoder(r.Body).Decode(&schema); err != nil {
		return nil, fmt.Errorf("failed to decode JSON response: %w", err)
	}
	return getSchemaResponse{schema: &schema}, nil
}

type deleteSchemaRequest struct {
	id uint32
}

type deleteSchemaResponse struct {
}

func encodeDeleteSchemaRequest(ctx context.Context, r *http.Request, request interface{}) error {
	req := request.(deleteSchemaRequest)
	r.URL.Path = "/api/v1/schemas/" + strconv.FormatUint(uint64(req.id), 10)
	return nil
}

func decodeDeleteSchemaRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 32)
	if err != nil {
		return nil, catalog.ErrBadRequest("failed to parse id: %v", err)
	}
	return deleteSchemaRequest{id: uint32(id)}, nil
}

func encodeDeleteSchemaResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	w.WriteHeader(http.StatusNoContent)
	return nil
}

func decodeDeleteSchemaResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return nil, decodeError(r)
	}
	return deleteSchemaResponse{}, nil
}

func encodeError(ctx context.Context, err error, w http.ResponseWriter) {
	e, ok := err.(*catalog.ServiceError)
	if !ok {
		e = &catalog.ServiceError{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		}
	}
	e.Encode(w)
}

func decodeError(r *http.Response) error {
	e := &catalog.ServiceError{}
	e.Decode(r)
	return e
}
