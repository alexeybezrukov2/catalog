package adminsvc

import (
	"context"
	"errors"
	"fmt"
	"testing"

	"github.com/go-kit/kit/log"
	"github.com/stretchr/testify/assert"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

type mockStorage struct {
	onInsertProperty  func(ctx context.Context, p *catalog.Property) error
	onGetProperty     func(ctx context.Context, id uint32) (*catalog.Property, error)
	onForeachProperty func(ctx context.Context, fn func(*catalog.Property) error) error
	onDeleteProperty  func(ctx context.Context, id uint32) error

	onGetPropertyVariantsByProperty func(ctx context.Context, propertyID uint32) ([]*catalog.PropertyVariant, error)
	onInsertPropertyVariant         func(ctx context.Context, pv *catalog.PropertyVariant) error
	onGetPropertyVariant            func(ctx context.Context, id uint32) (*catalog.PropertyVariant, error)
	onDeletePropertyVariant         func(ctx context.Context, id uint32) error

	onGetProducts   func(ctx context.Context, query *catalog.StorageProductQuery) (*catalog.StorageProductQueryResult, error)
	onInsertProduct func(ctx context.Context, p *catalog.Product) error
	onUpdateProduct func(ctx context.Context, p *catalog.Product) error
	onGetProduct    func(ctx context.Context, id uint32) (*catalog.Product, error)
	onDeleteProduct func(ctx context.Context, id uint32) error

	onForeachCategory func(ctx context.Context, fn func(*catalog.Category) error) error
	onInsertCategory  func(ctx context.Context, c *catalog.Category) error
	onGetCategory     func(ctx context.Context, id uint32) (*catalog.Category, error)
	onUpdateCategory  func(ctx context.Context, c *catalog.Category) error
	onDeleteCategory  func(ctx context.Context, id uint32) error

	onInsertSchema  func(ctx context.Context, schema *catalog.Schema) error
	onUpdateSchema  func(ctx context.Context, schema *catalog.Schema) error
	onGetSchema     func(ctx context.Context, id uint32) (*catalog.Schema, error)
	onForeachSchema func(ctx context.Context, fn func(*catalog.Schema) error) error
	onDeleteSchema  func(ctx context.Context, id uint32) error
}

func (m *mockStorage) InsertProperty(ctx context.Context, p *catalog.Property) error {
	return m.onInsertProperty(ctx, p)
}
func (m *mockStorage) GetProperty(ctx context.Context, id uint32) (*catalog.Property, error) {
	return m.onGetProperty(ctx, id)
}
func (m *mockStorage) ForeachProperty(ctx context.Context, fn func(*catalog.Property) error) error {
	return m.onForeachProperty(ctx, fn)
}
func (m *mockStorage) DeleteProperty(ctx context.Context, id uint32) error {
	return m.onDeleteProperty(ctx, id)
}
func (m *mockStorage) GetPropertyVariantsByProperty(ctx context.Context, propertyID uint32) ([]*catalog.PropertyVariant, error) {
	return m.onGetPropertyVariantsByProperty(ctx, propertyID)
}
func (m *mockStorage) InsertPropertyVariant(ctx context.Context, pv *catalog.PropertyVariant) error {
	return m.onInsertPropertyVariant(ctx, pv)
}
func (m *mockStorage) GetPropertyVariant(ctx context.Context, id uint32) (*catalog.PropertyVariant, error) {
	return m.onGetPropertyVariant(ctx, id)
}
func (m *mockStorage) DeletePropertyVariant(ctx context.Context, id uint32) error {
	return m.onDeletePropertyVariant(ctx, id)
}
func (m *mockStorage) GetProducts(ctx context.Context, query *catalog.StorageProductQuery) (*catalog.StorageProductQueryResult, error) {
	return m.onGetProducts(ctx, query)
}
func (m *mockStorage) InsertProduct(ctx context.Context, p *catalog.Product) error {
	return m.onInsertProduct(ctx, p)
}
func (m *mockStorage) UpdateProduct(ctx context.Context, p *catalog.Product) error {
	return m.onUpdateProduct(ctx, p)
}
func (m *mockStorage) GetProduct(ctx context.Context, id uint32) (*catalog.Product, error) {
	return m.onGetProduct(ctx, id)
}
func (m *mockStorage) DeleteProduct(ctx context.Context, id uint32) error {
	return m.onDeleteProduct(ctx, id)
}
func (m *mockStorage) ForeachCategory(ctx context.Context, fn func(*catalog.Category) error) error {
	return m.onForeachCategory(ctx, fn)
}
func (m *mockStorage) InsertCategory(ctx context.Context, c *catalog.Category) error {
	return m.onInsertCategory(ctx, c)
}
func (m *mockStorage) GetCategory(ctx context.Context, id uint32) (*catalog.Category, error) {
	return m.onGetCategory(ctx, id)
}
func (m *mockStorage) UpdateCategory(ctx context.Context, c *catalog.Category) error {
	return m.onUpdateCategory(ctx, c)
}
func (m *mockStorage) DeleteCategory(ctx context.Context, id uint32) error {
	return m.onDeleteCategory(ctx, id)
}
func (m *mockStorage) InsertSchema(ctx context.Context, schema *catalog.Schema) error {
	return m.onInsertSchema(ctx, schema)
}
func (m *mockStorage) UpdateSchema(ctx context.Context, schema *catalog.Schema) error {
	return m.onUpdateSchema(ctx, schema)
}
func (m *mockStorage) GetSchema(ctx context.Context, id uint32) (*catalog.Schema, error) {
	return m.onGetSchema(ctx, id)
}
func (m *mockStorage) ForeachSchema(ctx context.Context, fn func(*catalog.Schema) error) error {
	return m.onForeachSchema(ctx, fn)
}
func (m *mockStorage) DeleteSchema(ctx context.Context, id uint32) error {
	return m.onDeleteSchema(ctx, id)
}

type mockEventQueue struct {
	onPublish func(op *catalog.Operation) error
}

func (m *mockEventQueue) Publish(op *catalog.Operation) error {
	return m.onPublish(op)
}

func newTestService() *service {
	return &service{
		logger: log.NewNopLogger(),
		eventQueue: &mockEventQueue{
			onPublish: func(op *catalog.Operation) error {
				return nil
			}}}
}

func TestServiceGetProperties(t *testing.T) {
	s := newTestService()
	testCases := []struct {
		name         string
		wantProperty []*catalog.Property
		wantErr      error
		storageErr   error
	}{
		{
			name: "ok",
			wantProperty: []*catalog.Property{
				mustNewProperty(func(p *catalog.Property) { p.ID = 1 }),
				mustNewProperty(func(p *catalog.Property) { p.ID = 2 }),
				mustNewProperty(func(p *catalog.Property) { p.ID = 3 })},
			wantErr:    nil,
			storageErr: nil,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			store := &mockStorage{
				onForeachProperty: func(ctx context.Context, fn func(property *catalog.Property) error) error {
					for _, p := range tc.wantProperty {
						if err := fn(p); err != nil {
							return err
						}
					}
					return tc.storageErr
				},
			}
			s.storage = store
			gotProperty, gotErr := s.GetProperties(context.Background())
			assert.Equal(t, tc.wantProperty, gotProperty)
			assert.Equal(t, tc.wantErr, gotErr)
		})
	}
}

func TestServiceCreateProperty(t *testing.T) {
	s := newTestService()
	testCases := []struct {
		name         string
		id           uint32
		input        *catalog.PropertyInput
		wantProperty *catalog.Property
		wantErr      error
		storageErr   error
	}{
		{
			name:         "ok",
			id:           1,
			input:        mustNewPropertyInput(nil),
			wantProperty: mustNewProperty(nil),
			wantErr:      nil,
			storageErr:   nil,
		},
		{
			name: "empty name",
			id:   1,
			input: mustNewPropertyInput(func(pi *catalog.PropertyInput) {
				pi.Name = ""
			}),
			wantProperty: nil,
			wantErr:      catalog.ErrBadRequest("invalid input: %s", errors.New("empty Name")),
			storageErr:   nil,
		},
		{
			name: "invalid key",
			id:   1,
			input: mustNewPropertyInput(func(pi *catalog.PropertyInput) {
				pi.Key = ""
			}),
			wantProperty: nil,
			wantErr:      catalog.ErrBadRequest("invalid input: %s", errors.New("empty Key")),
			storageErr:   nil,
		},
		{
			name: "invalid type",
			id:   1,
			input: mustNewPropertyInput(func(pi *catalog.PropertyInput) {
				pi.Type = "some"
			}),
			wantProperty: nil,
			wantErr:      catalog.ErrBadRequest("invalid input: %s", fmt.Errorf("invalid Type: %q", "some")),
			storageErr:   nil,
		},
		{
			name:         "nil input",
			input:        nil,
			id:           1,
			wantProperty: nil,
			wantErr:      catalog.ErrBadRequest("invalid input: %s", errors.New("nil property input")),
			storageErr:   nil,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			store := &mockStorage{
				onInsertProperty: func(ctx context.Context, p *catalog.Property) error {
					p.ID = tc.id
					return tc.storageErr
				},
			}
			s.storage = store
			gotProperty, gotErr := s.CreateProperty(context.Background(), tc.input)
			assert.Equal(t, tc.wantProperty, gotProperty)
			assert.Equal(t, tc.wantErr, gotErr)
		})
	}
}

func TestServiceGetProperty(t *testing.T) {
	s := newTestService()
	testCases := []struct {
		name string
		id   uint32

		wantProperty *catalog.Property
		wantErr      error
		storageErr   error
	}{
		{
			name:         "ok",
			id:           1,
			wantProperty: mustNewProperty(nil),
			wantErr:      nil,
			storageErr:   nil,
		},
		{
			name:         "not found in storage",
			id:           999999999,
			wantProperty: nil,
			wantErr:      catalog.ErrNotFound("property not found: %d", 999999999),
			storageErr:   catalog.ErrNotFoundInStorage,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			store := &mockStorage{
				onGetProperty: func(ctx context.Context, id uint32) (*catalog.Property, error) {
					return tc.wantProperty, tc.storageErr
				},
			}
			s.storage = store
			gotProperty, gotErr := s.GetProperty(context.Background(), tc.id)
			assert.Equal(t, tc.wantProperty, gotProperty)
			assert.Equal(t, tc.wantErr, gotErr)
		})
	}
}

func TestServiceDeleteProperty(t *testing.T) {
	s := newTestService()
	testCases := []struct {
		name       string
		id         uint32
		wantErr    error
		storageErr error
	}{
		{
			name:       "ok",
			id:         1,
			wantErr:    nil,
			storageErr: nil,
		},
		{
			name:       "not found in storage",
			id:         999999999,
			wantErr:    catalog.ErrNotFound("property not found: %d", 999999999),
			storageErr: catalog.ErrNotFoundInStorage,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			store := &mockStorage{
				onGetProperty: func(ctx context.Context, id uint32) (*catalog.Property, error) {
					return &catalog.Property{
						ID: id,
					}, tc.storageErr
				},
				onDeleteProperty: func(ctx context.Context, id uint32) error {
					return tc.storageErr
				},
			}
			s.storage = store
			gotErr := s.DeleteProperty(context.Background(), tc.id)
			assert.Equal(t, tc.wantErr, gotErr)
		})
	}
}

func TestServiceGetPropertyVariants(t *testing.T) {
	s := newTestService()
	testCases := []struct {
		name                string
		id                  uint32
		wantPropertyVariant []*catalog.PropertyVariant
		wantErr             error
		storageErr          error
	}{
		{
			name: "ok",
			id:   1,
			wantPropertyVariant: []*catalog.PropertyVariant{
				mustNewPropertyVariant(func(pv *catalog.PropertyVariant) { pv.ID = 1 }),
				mustNewPropertyVariant(func(pv *catalog.PropertyVariant) { pv.ID = 2 }),
				mustNewPropertyVariant(func(pv *catalog.PropertyVariant) { pv.ID = 3 }),
			},
			wantErr:    nil,
			storageErr: nil,
		},
		{
			name:                "property not found",
			id:                  999,
			wantPropertyVariant: nil,
			wantErr:             catalog.ErrNotFound("property not found: %d", 999),
			storageErr:          catalog.ErrNotFoundInStorage,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			store := &mockStorage{
				onGetProperty: func(ctx context.Context, id uint32) (*catalog.Property, error) {
					return mustNewProperty(func(p *catalog.Property) {
						p.ID = id
					}), tc.storageErr
				},
				onGetPropertyVariantsByProperty: func(ctx context.Context, propertyID uint32) ([]*catalog.PropertyVariant, error) {
					return tc.wantPropertyVariant, tc.storageErr
				},
			}
			s.storage = store
			gotPropertyVariant, gotErr := s.GetPropertyVariants(context.Background(), tc.id)
			assert.Equal(t, tc.wantPropertyVariant, gotPropertyVariant)
			assert.Equal(t, tc.wantErr, gotErr)
		})
	}
}

func TestServiceCreatePropertyVariant(t *testing.T) {
	s := newTestService()
	testCases := []struct {
		name                string
		input               *catalog.PropertyVariantInput
		id                  uint32
		wantPropertyVariant *catalog.PropertyVariant
		wantErr             error
		storageErr          error
		propertyType        catalog.PropertyType
	}{
		{
			name:                "ok",
			input:               mustNewPropertyVariantInput(nil),
			id:                  1,
			wantPropertyVariant: mustNewPropertyVariant(nil),
			wantErr:             nil,
			storageErr:          nil,
			propertyType:        catalog.PropertyTypeEnumList,
		},
		{
			name: "property not found",
			input: mustNewPropertyVariantInput(func(pvi *catalog.PropertyVariantInput) {
				pvi.PropertyID = 999
			}),
			id:                  999,
			wantPropertyVariant: nil,
			wantErr:             catalog.ErrNotFound("property not found: %d", 999),
			storageErr:          catalog.ErrNotFoundInStorage,
			propertyType:        catalog.PropertyTypeEnumList,
		},
		{
			name: "empty value",
			input: mustNewPropertyVariantInput(func(pvi *catalog.PropertyVariantInput) {
				pvi.Value = ""
			}),
			id:                  1,
			wantPropertyVariant: nil,
			wantErr:             catalog.ErrBadRequest("invalid input: %s", errors.New("empty Value")),
			storageErr:          nil,
			propertyType:        catalog.PropertyTypeEnumList,
		},
		{
			name:                "nil input",
			input:               nil,
			id:                  1,
			wantPropertyVariant: nil,
			wantErr:             catalog.ErrBadRequest("invalid input: %s", errors.New("nil property variant input")),
			storageErr:          nil,
			propertyType:        catalog.PropertyTypeEnumList,
		},
		{
			name:                "wrong type",
			input:               mustNewPropertyVariantInput(nil),
			id:                  1,
			wantPropertyVariant: nil,
			wantErr:             fmt.Errorf("unable to create property variant for %v property type", catalog.PropertyTypeInt),
			storageErr:          nil,
			propertyType:        catalog.PropertyTypeInt,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			store := &mockStorage{
				onGetProperty: func(ctx context.Context, id uint32) (*catalog.Property, error) {
					return mustNewProperty(func(p *catalog.Property) {
						p.ID = id
						p.Type = tc.propertyType
					}), tc.storageErr
				},
				onInsertPropertyVariant: func(ctx context.Context, pv *catalog.PropertyVariant) error {
					pv.ID = tc.id
					return tc.storageErr
				},
			}
			s.storage = store
			gotPropertyVariant, gotErr := s.CreatePropertyVariant(context.Background(), tc.input)
			assert.Equal(t, tc.wantPropertyVariant, gotPropertyVariant)
			assert.Equal(t, tc.wantErr, gotErr)
		})
	}
}

func TestServiceGetPropertyVariant(t *testing.T) {
	s := newTestService()
	testCases := []struct {
		name string
		id   uint32

		wantPropertyVariant *catalog.PropertyVariant
		wantErr             error
		storageErr          error
	}{
		{
			name:                "ok",
			id:                  1,
			wantPropertyVariant: mustNewPropertyVariant(nil),
			wantErr:             nil,
			storageErr:          nil,
		},
		{
			name:                "not found in storage",
			id:                  999999999,
			wantPropertyVariant: nil,
			wantErr:             catalog.ErrNotFound("property variant not found: %d", 999999999),
			storageErr:          catalog.ErrNotFoundInStorage,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			store := &mockStorage{
				onGetPropertyVariant: func(ctx context.Context, id uint32) (*catalog.PropertyVariant, error) {
					return tc.wantPropertyVariant, tc.storageErr
				},
			}
			s.storage = store
			gotPropertyVariant, gotErr := s.GetPropertyVariant(context.Background(), tc.id)
			assert.Equal(t, tc.wantPropertyVariant, gotPropertyVariant)
			assert.Equal(t, tc.wantErr, gotErr)
		})
	}
}

func TestServiceDeletePropertyVariant(t *testing.T) {
	s := newTestService()
	testCases := []struct {
		name       string
		id         uint32
		wantErr    error
		storageErr error
	}{
		{
			name:       "ok",
			id:         1,
			wantErr:    nil,
			storageErr: nil,
		},
		{
			name:       "not found in storage",
			id:         999999999,
			wantErr:    catalog.ErrNotFound("property variant not found: %d", 999999999),
			storageErr: catalog.ErrNotFoundInStorage,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			store := &mockStorage{
				onGetPropertyVariant: func(ctx context.Context, id uint32) (*catalog.PropertyVariant, error) {
					return &catalog.PropertyVariant{ID: id}, tc.storageErr
				},
				onDeletePropertyVariant: func(ctx context.Context, id uint32) error {
					return tc.storageErr
				},
			}
			s.storage = store
			gotErr := s.DeletePropertyVariant(context.Background(), tc.id)
			assert.Equal(t, tc.wantErr, gotErr)
		})
	}
}

func TestServiceGetProducts(t *testing.T) {
	s := newTestService()
	testCases := []struct {
		name          string
		query         *catalog.StorageProductQuery
		storageResult *catalog.StorageProductQueryResult
		storageErr    error
		wantResult    *catalog.StorageProductQueryResult
		wantErr       error
	}{
		{
			name: "ok",
			query: &catalog.StorageProductQuery{
				SupplierID: 123,
				Limit:      100,
			},
			storageResult: &catalog.StorageProductQueryResult{
				Products: []*catalog.Product{{ID: 1}, {ID: 2}, {ID: 3}},
				Total:    3,
			},
			storageErr: nil,
			wantResult: &catalog.StorageProductQueryResult{
				Products: []*catalog.Product{{ID: 1}, {ID: 2}, {ID: 3}},
				Total:    3,
			},
			wantErr: nil,
		},
		{
			name: "error",
			query: &catalog.StorageProductQuery{
				SupplierID: 123,
				Limit:      100,
			},
			storageErr: errors.New("something's wrong"),
			wantErr:    catalog.ErrInternal("failed to get products: something's wrong"),
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			s.storage = &mockStorage{
				onGetProducts: func(ctx context.Context, query *catalog.StorageProductQuery) (*catalog.StorageProductQueryResult, error) {
					if tc.storageErr != nil {
						return nil, tc.storageErr
					}
					return tc.storageResult, nil
				},
			}
			gotResult, gotErr := s.GetProducts(context.Background(), tc.query)
			assert.Equal(t, tc.wantResult, gotResult)
			assert.Equal(t, tc.wantErr, gotErr)
		})
	}
}

func TestServiceCreateProduct(t *testing.T) {
	s := newTestService()
	testCases := []struct {
		name                    string
		input                   *catalog.ProductInput
		storageSchemas          map[uint32]*catalog.Schema
		storagePropertyVariants map[uint32]*catalog.PropertyVariant
		storageInsertID         uint32
		storageInsertErr        error
		wantOp                  *catalog.Operation
		wantProduct             *catalog.Product
		wantErr                 error
	}{
		{
			name: "ok",
			input: &catalog.ProductInput{
				SupplierID: 123,
				SchemaID:   321,
				Properties: []*catalog.ProductPropertyInput{
					{ID: 1, Value: "Test"},
					{ID: 2, Value: float64(222)},
					{ID: 3, Value: "2020-01-02T03:04:05Z"},
					{ID: 4, ValueIDs: []uint32{41}},
					{ID: 5, ValueIDs: []uint32{51, 52, 53}},
				},
			},
			storageSchemas: map[uint32]*catalog.Schema{
				321: {
					Properties: []*catalog.SchemaProperty{
						{ID: 1, Key: "key1", Name: "name1", Type: catalog.PropertyTypeString, Indexed: false, Required: true},
						{ID: 2, Key: "key2", Name: "name2", Type: catalog.PropertyTypeInt, Indexed: true, Required: true},
						{ID: 3, Key: "key3", Name: "name3", Type: catalog.PropertyTypeDate, Indexed: false, Required: false},
						{ID: 4, Key: "key4", Name: "name4", Type: catalog.PropertyTypeEnum, Indexed: true, Required: true},
						{ID: 5, Key: "key5", Name: "name5", Type: catalog.PropertyTypeEnumList, Indexed: true, Required: true},
						{ID: 9, Key: "key9", Name: "name9", Type: catalog.PropertyTypeString, Indexed: false, Required: false},
					},
				},
			},
			storagePropertyVariants: map[uint32]*catalog.PropertyVariant{
				41: {ID: 41, PropertyID: 4, Value: "v41"},
				51: {ID: 51, PropertyID: 5, Value: "v51"},
				52: {ID: 52, PropertyID: 5, Value: "v52"},
				53: {ID: 53, PropertyID: 5, Value: "v53"},
			},
			storageInsertID:  54321,
			storageInsertErr: nil,
			wantOp: &catalog.Operation{
				Type:   catalog.OperationTypeProduct,
				Method: catalog.OperationMethodUpsert,
				Product: &catalog.Product{
					ID:         54321,
					SupplierID: 123,
					SchemaID:   321,
					Properties: []*catalog.ProductProperty{
						{ID: 1, Key: "key1", Name: "name1", Type: catalog.PropertyTypeString, Value: "Test", Indexed: false},
						{ID: 2, Key: "key2", Name: "name2", Type: catalog.PropertyTypeInt, Value: float64(222), Indexed: true},
						{ID: 3, Key: "key3", Name: "name3", Type: catalog.PropertyTypeDate, Value: "2020-01-02T03:04:05Z", Indexed: false},
						{ID: 4, Key: "key4", Name: "name4", Type: catalog.PropertyTypeEnum, Value: "v41", ValueIDs: []uint32{41}, Indexed: true},
						{ID: 5, Key: "key5", Name: "name5", Type: catalog.PropertyTypeEnumList, Value: []interface{}{"v51", "v52", "v53"}, ValueIDs: []uint32{51, 52, 53}, Indexed: true},
					},
				},
			},
			wantProduct: &catalog.Product{
				ID:         54321,
				SupplierID: 123,
				SchemaID:   321,
				Properties: []*catalog.ProductProperty{
					{ID: 1, Key: "key1", Name: "name1", Type: catalog.PropertyTypeString, Value: "Test", Indexed: false},
					{ID: 2, Key: "key2", Name: "name2", Type: catalog.PropertyTypeInt, Value: float64(222), Indexed: true},
					{ID: 3, Key: "key3", Name: "name3", Type: catalog.PropertyTypeDate, Value: "2020-01-02T03:04:05Z", Indexed: false},
					{ID: 4, Key: "key4", Name: "name4", Type: catalog.PropertyTypeEnum, Value: "v41", ValueIDs: []uint32{41}, Indexed: true},
					{ID: 5, Key: "key5", Name: "name5", Type: catalog.PropertyTypeEnumList, Value: []interface{}{"v51", "v52", "v53"}, ValueIDs: []uint32{51, 52, 53}, Indexed: true},
				},
			},
			wantErr: nil,
		},
		{
			name: "unknown schema",
			input: &catalog.ProductInput{
				SupplierID: 123,
				SchemaID:   321,
				Properties: []*catalog.ProductPropertyInput{{ID: 1, Value: "Test"}},
			},
			storageSchemas: map[uint32]*catalog.Schema{},
			wantErr:        catalog.ErrBadRequest("unknown schema id: 321"),
		},
		{
			name: "property not allowed",
			input: &catalog.ProductInput{
				SupplierID: 123,
				SchemaID:   321,
				Properties: []*catalog.ProductPropertyInput{
					{ID: 1, Value: "Test"},
					{ID: 2, Value: float64(222)},
				},
			},
			storageSchemas: map[uint32]*catalog.Schema{
				321: {
					Properties: []*catalog.SchemaProperty{
						{ID: 1, Key: "key1", Name: "name1", Type: catalog.PropertyTypeString, Indexed: false, Required: true},
					},
				},
			},
			wantErr: catalog.ErrBadRequest("property 2 is not allowed for schema 321"),
		},
		{
			name: "missing required property",
			input: &catalog.ProductInput{
				SupplierID: 123,
				SchemaID:   321,
				Properties: []*catalog.ProductPropertyInput{
					{ID: 1, Value: "Test"},
					{ID: 2, Value: float64(222)},
				},
			},
			storageSchemas: map[uint32]*catalog.Schema{
				321: {
					Properties: []*catalog.SchemaProperty{
						{ID: 1, Key: "key1", Name: "name1", Type: catalog.PropertyTypeString, Indexed: false, Required: true},
						{ID: 2, Key: "key2", Name: "name2", Type: catalog.PropertyTypeInt, Indexed: true, Required: true},
						{ID: 3, Key: "key3", Name: "name3", Type: catalog.PropertyTypeDate, Indexed: false, Required: false},
						{ID: 4, Key: "key4", Name: "name4", Type: catalog.PropertyTypeEnumList, Indexed: true, Required: true},
						{ID: 9, Key: "key9", Name: "name9", Type: catalog.PropertyTypeString, Indexed: false, Required: false},
					},
				},
			},
			wantErr: catalog.ErrBadRequest("missing required property 4 for schema 321"),
		},
		{
			name: "property variant not found",
			input: &catalog.ProductInput{
				SupplierID: 123,
				SchemaID:   321,
				Properties: []*catalog.ProductPropertyInput{
					{ID: 4, ValueIDs: []uint32{41, 42, 43}},
				},
			},
			storageSchemas: map[uint32]*catalog.Schema{
				321: {
					Properties: []*catalog.SchemaProperty{
						{ID: 4, Key: "key4", Name: "name4", Type: catalog.PropertyTypeEnumList, Indexed: true, Required: true},
					},
				},
			},
			storagePropertyVariants: map[uint32]*catalog.PropertyVariant{
				41: {ID: 41, PropertyID: 4, Value: "v41"},
				43: {ID: 43, PropertyID: 4, Value: "v43"},
			},
			wantErr: catalog.ErrBadRequest("property variant not found: 42"),
		},
		{
			name: "ivalid property variant",
			input: &catalog.ProductInput{
				SupplierID: 123,
				SchemaID:   321,
				Properties: []*catalog.ProductPropertyInput{
					{ID: 4, ValueIDs: []uint32{41, 42, 43}},
				},
			},
			storageSchemas: map[uint32]*catalog.Schema{
				321: {
					Properties: []*catalog.SchemaProperty{
						{ID: 4, Key: "key4", Name: "name4", Type: catalog.PropertyTypeEnumList, Indexed: true, Required: true},
					},
				},
			},
			storagePropertyVariants: map[uint32]*catalog.PropertyVariant{
				41: {ID: 41, PropertyID: 4, Value: "v41"},
				42: {ID: 41, PropertyID: 3, Value: "v41"},
				43: {ID: 43, PropertyID: 4, Value: "v43"},
			},
			wantErr: catalog.ErrBadRequest("ivalid property variant 42 for property 4"),
		},
		{
			name: "insert error",
			input: &catalog.ProductInput{
				SupplierID: 123,
				SchemaID:   321,
				Properties: []*catalog.ProductPropertyInput{
					{ID: 1, Value: "Test"},
				},
			},
			storageSchemas: map[uint32]*catalog.Schema{
				321: {
					Properties: []*catalog.SchemaProperty{
						{ID: 1, Key: "key1", Name: "name1", Type: catalog.PropertyTypeString, Indexed: false, Required: true},
					},
				},
			},
			storageInsertErr: errors.New("bad luck"),
			wantErr:          catalog.ErrInternal("failed to insert product: bad luck"),
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var gotInsertedProduct *catalog.Product
			s.storage = &mockStorage{
				onGetSchema: func(ctx context.Context, id uint32) (*catalog.Schema, error) {
					if x, ok := tc.storageSchemas[id]; ok {
						return x, nil
					}
					return nil, catalog.ErrNotFoundInStorage
				},
				onGetPropertyVariant: func(ctx context.Context, id uint32) (*catalog.PropertyVariant, error) {
					if x, ok := tc.storagePropertyVariants[id]; ok {
						return x, nil
					}
					return nil, catalog.ErrNotFoundInStorage
				},
				onInsertProduct: func(ctx context.Context, p *catalog.Product) error {
					if tc.storageInsertErr != nil {
						return tc.storageInsertErr
					}
					p.ID = tc.storageInsertID
					gotInsertedProduct = p
					return nil
				},
			}
			var gotOp *catalog.Operation
			s.eventQueue = &mockEventQueue{
				onPublish: func(op *catalog.Operation) error {
					gotOp = op
					return nil
				},
			}
			gotProduct, gotErr := s.CreateProduct(context.Background(), tc.input)
			assert.Equal(t, tc.wantProduct, gotProduct)
			assert.Equal(t, tc.wantProduct, gotInsertedProduct)
			assert.Equal(t, tc.wantErr, gotErr)
			assert.Equal(t, tc.wantOp, gotOp)
		})
	}
}

func TestServiceUpdateProduct(t *testing.T) {
	s := newTestService()
	testCases := []struct {
		name                    string
		id                      uint32
		patch                   *catalog.ProductPatch
		storageProducts         map[uint32]*catalog.Product
		storageSchemas          map[uint32]*catalog.Schema
		storagePropertyVariants map[uint32]*catalog.PropertyVariant
		storageUpdateErr        error
		wantOp                  *catalog.Operation
		wantProduct             *catalog.Product
		wantErr                 error
	}{
		{
			name: "full ok",
			id:   54321,
			patch: &catalog.ProductPatch{
				Properties: []*catalog.ProductPropertyInput{
					{ID: 1, Value: "Test"},
					{ID: 2, Value: float64(222)},
					{ID: 3, Value: "2020-01-02T03:04:05Z"},
					{ID: 4, ValueIDs: []uint32{41, 42, 43}},
				},
				Partial: false,
			},
			storageProducts: map[uint32]*catalog.Product{
				54321: {
					ID:         54321,
					SupplierID: 123,
					SchemaID:   321,
					Properties: []*catalog.ProductProperty{
						{ID: 1, Key: "key1", Name: "name1", Type: catalog.PropertyTypeString, Value: "OldVal", Indexed: false},
						{ID: 99, Key: "key99", Name: "name99", Type: catalog.PropertyTypeString, Value: "v99", Indexed: false},
					},
				},
			},
			storageSchemas: map[uint32]*catalog.Schema{
				321: {
					Properties: []*catalog.SchemaProperty{
						{ID: 1, Key: "key1", Name: "name1", Type: catalog.PropertyTypeString, Indexed: false, Required: true},
						{ID: 2, Key: "key2", Name: "name2", Type: catalog.PropertyTypeInt, Indexed: true, Required: true},
						{ID: 3, Key: "key3", Name: "name3", Type: catalog.PropertyTypeDate, Indexed: false, Required: false},
						{ID: 4, Key: "key4", Name: "name4", Type: catalog.PropertyTypeEnumList, Indexed: true, Required: true},
						{ID: 9, Key: "key9", Name: "name9", Type: catalog.PropertyTypeString, Indexed: false, Required: false},
					},
				},
			},
			storagePropertyVariants: map[uint32]*catalog.PropertyVariant{
				41: {ID: 41, PropertyID: 4, Value: "v41"},
				42: {ID: 42, PropertyID: 4, Value: "v42"},
				43: {ID: 43, PropertyID: 4, Value: "v43"},
			},
			storageUpdateErr: nil,
			wantOp: &catalog.Operation{
				Type:   catalog.OperationTypeProduct,
				Method: catalog.OperationMethodUpsert,
				Product: &catalog.Product{
					ID:         54321,
					SupplierID: 123,
					SchemaID:   321,
					Properties: []*catalog.ProductProperty{
						{ID: 1, Key: "key1", Name: "name1", Type: catalog.PropertyTypeString, Value: "Test", Indexed: false},
						{ID: 2, Key: "key2", Name: "name2", Type: catalog.PropertyTypeInt, Value: float64(222), Indexed: true},
						{ID: 3, Key: "key3", Name: "name3", Type: catalog.PropertyTypeDate, Value: "2020-01-02T03:04:05Z", Indexed: false},
						{ID: 4, Key: "key4", Name: "name4", Type: catalog.PropertyTypeEnumList, Value: []interface{}{"v41", "v42", "v43"}, ValueIDs: []uint32{41, 42, 43}, Indexed: true},
					},
				},
			},
			wantProduct: &catalog.Product{
				ID:         54321,
				SupplierID: 123,
				SchemaID:   321,
				Properties: []*catalog.ProductProperty{
					{ID: 1, Key: "key1", Name: "name1", Type: catalog.PropertyTypeString, Value: "Test", Indexed: false},
					{ID: 2, Key: "key2", Name: "name2", Type: catalog.PropertyTypeInt, Value: float64(222), Indexed: true},
					{ID: 3, Key: "key3", Name: "name3", Type: catalog.PropertyTypeDate, Value: "2020-01-02T03:04:05Z", Indexed: false},
					{ID: 4, Key: "key4", Name: "name4", Type: catalog.PropertyTypeEnumList, Value: []interface{}{"v41", "v42", "v43"}, ValueIDs: []uint32{41, 42, 43}, Indexed: true},
				},
			},
			wantErr: nil,
		},
		{
			name: "partial ok",
			id:   54321,
			patch: &catalog.ProductPatch{
				Properties: []*catalog.ProductPropertyInput{
					{ID: 1, Value: "Test1"},
					{ID: 9, Value: "Test9"},
				},
				Partial: true,
			},
			storageProducts: map[uint32]*catalog.Product{
				54321: {
					ID:         54321,
					SupplierID: 123,
					SchemaID:   321,
					Properties: []*catalog.ProductProperty{
						{ID: 1, Key: "key1", Name: "name1", Type: catalog.PropertyTypeString, Value: "OldVal", Indexed: false},
						{ID: 2, Key: "key2", Name: "name2", Type: catalog.PropertyTypeInt, Value: float64(222), Indexed: true},
						{ID: 4, Key: "key4", Name: "name4", Type: catalog.PropertyTypeEnumList, Value: []interface{}{"v41", "v42", "v43"}, ValueIDs: []uint32{41, 42, 43}, Indexed: true},
					},
				},
			},
			storageSchemas: map[uint32]*catalog.Schema{
				321: {
					Properties: []*catalog.SchemaProperty{
						{ID: 1, Key: "key1", Name: "name1", Type: catalog.PropertyTypeString, Indexed: false, Required: true},
						{ID: 2, Key: "key2", Name: "name2", Type: catalog.PropertyTypeInt, Indexed: true, Required: true},
						{ID: 3, Key: "key3", Name: "name3", Type: catalog.PropertyTypeDate, Indexed: false, Required: false},
						{ID: 4, Key: "key4", Name: "name4", Type: catalog.PropertyTypeEnumList, Indexed: true, Required: true},
						{ID: 9, Key: "key9", Name: "name9", Type: catalog.PropertyTypeString, Indexed: false, Required: false},
					},
				},
			},
			storageUpdateErr: nil,
			wantOp: &catalog.Operation{
				Type:   catalog.OperationTypeProduct,
				Method: catalog.OperationMethodPatch,
				Product: &catalog.Product{
					ID:         54321,
					SupplierID: 123,
					SchemaID:   321,
					Properties: []*catalog.ProductProperty{
						{ID: 1, Key: "key1", Name: "name1", Type: catalog.PropertyTypeString, Value: "Test1", Indexed: false},
						{ID: 9, Key: "key9", Name: "name9", Type: catalog.PropertyTypeString, Value: "Test9", Indexed: false},
					},
				},
			},
			wantProduct: &catalog.Product{
				ID:         54321,
				SupplierID: 123,
				SchemaID:   321,
				Properties: []*catalog.ProductProperty{
					{ID: 1, Key: "key1", Name: "name1", Type: catalog.PropertyTypeString, Value: "Test1", Indexed: false},
					{ID: 2, Key: "key2", Name: "name2", Type: catalog.PropertyTypeInt, Value: float64(222), Indexed: true},
					{ID: 4, Key: "key4", Name: "name4", Type: catalog.PropertyTypeEnumList, Value: []interface{}{"v41", "v42", "v43"}, ValueIDs: []uint32{41, 42, 43}, Indexed: true},
					{ID: 9, Key: "key9", Name: "name9", Type: catalog.PropertyTypeString, Value: "Test9", Indexed: false},
				},
			},
			wantErr: nil,
		},
		{
			name: "missing required property",
			id:   54321,
			patch: &catalog.ProductPatch{
				Properties: []*catalog.ProductPropertyInput{
					{ID: 1, Value: "Test"},
				},
				Partial: false,
			},
			storageProducts: map[uint32]*catalog.Product{
				54321: {
					ID:         54321,
					SupplierID: 123,
					SchemaID:   321,
					Properties: []*catalog.ProductProperty{
						{ID: 1, Key: "key1", Name: "name1", Type: catalog.PropertyTypeString, Value: "OldVal", Indexed: false},
						{ID: 2, Key: "key2", Name: "name2", Type: catalog.PropertyTypeInt, Value: float64(222), Indexed: true},
					},
				},
			},
			storageSchemas: map[uint32]*catalog.Schema{
				321: {
					Properties: []*catalog.SchemaProperty{
						{ID: 1, Key: "key1", Name: "name1", Type: catalog.PropertyTypeString, Indexed: false, Required: true},
						{ID: 2, Key: "key2", Name: "name2", Type: catalog.PropertyTypeInt, Indexed: true, Required: true},
					},
				},
			},
			wantErr: catalog.ErrBadRequest("missing required property 2 for schema 321"),
		},
		{
			name: "property not allowed",
			id:   54321,
			patch: &catalog.ProductPatch{
				Properties: []*catalog.ProductPropertyInput{
					{ID: 1, Value: "Test"},
					{ID: 99, Value: "Test"},
				},
				Partial: true,
			},
			storageProducts: map[uint32]*catalog.Product{
				54321: {
					ID:         54321,
					SupplierID: 123,
					SchemaID:   321,
					Properties: []*catalog.ProductProperty{
						{ID: 1, Key: "key1", Name: "name1", Type: catalog.PropertyTypeString, Value: "OldVal", Indexed: false},
					},
				},
			},
			storageSchemas: map[uint32]*catalog.Schema{
				321: {
					Properties: []*catalog.SchemaProperty{
						{ID: 1, Key: "key1", Name: "name1", Type: catalog.PropertyTypeString, Indexed: false, Required: true},
					},
				},
			},
			wantErr: catalog.ErrBadRequest("property 99 is not allowed for schema 321"),
		},
		{
			name: "product not found",
			id:   54321,
			patch: &catalog.ProductPatch{
				Properties: []*catalog.ProductPropertyInput{
					{ID: 1, Value: "Test"},
				},
				Partial: true,
			},
			storageProducts: map[uint32]*catalog.Product{},
			storageSchemas:  map[uint32]*catalog.Schema{},
			wantErr:         catalog.ErrNotFound("product not found: 54321"),
		},
		{
			name: "schema not found",
			id:   54321,
			patch: &catalog.ProductPatch{
				Properties: []*catalog.ProductPropertyInput{
					{ID: 1, Value: "Test"},
				},
				Partial: true,
			},
			storageProducts: map[uint32]*catalog.Product{
				54321: {
					ID:         54321,
					SupplierID: 123,
					SchemaID:   321,
					Properties: []*catalog.ProductProperty{
						{ID: 1, Key: "key1", Name: "name1", Type: catalog.PropertyTypeString, Value: "OldVal", Indexed: false},
					},
				},
			},
			storageSchemas: map[uint32]*catalog.Schema{},
			wantErr:        catalog.ErrNotFound("schema not found: 321"),
		},
		{
			name: "update error",
			id:   54321,
			patch: &catalog.ProductPatch{
				Properties: []*catalog.ProductPropertyInput{
					{ID: 1, Value: "Test"},
				},
				Partial: true,
			},
			storageProducts: map[uint32]*catalog.Product{
				54321: {
					ID:         54321,
					SupplierID: 123,
					SchemaID:   321,
					Properties: []*catalog.ProductProperty{
						{ID: 1, Key: "key1", Name: "name1", Type: catalog.PropertyTypeString, Value: "OldVal", Indexed: false},
					},
				},
			},
			storageSchemas: map[uint32]*catalog.Schema{
				321: {
					Properties: []*catalog.SchemaProperty{
						{ID: 1, Key: "key1", Name: "name1", Type: catalog.PropertyTypeString, Indexed: false, Required: true},
					},
				},
			},
			storageUpdateErr: errors.New("bad luck"),
			wantErr:          catalog.ErrInternal("failed to update product: bad luck"),
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var gotUpdatedProduct *catalog.Product
			s.storage = &mockStorage{
				onGetProduct: func(ctx context.Context, id uint32) (*catalog.Product, error) {
					if x, ok := tc.storageProducts[id]; ok {
						return x, nil
					}
					return nil, catalog.ErrNotFoundInStorage
				},
				onGetSchema: func(ctx context.Context, id uint32) (*catalog.Schema, error) {
					if x, ok := tc.storageSchemas[id]; ok {
						return x, nil
					}
					return nil, catalog.ErrNotFoundInStorage
				},
				onGetPropertyVariant: func(ctx context.Context, id uint32) (*catalog.PropertyVariant, error) {
					if x, ok := tc.storagePropertyVariants[id]; ok {
						return x, nil
					}
					return nil, catalog.ErrNotFoundInStorage
				},
				onUpdateProduct: func(ctx context.Context, p *catalog.Product) error {
					if tc.storageUpdateErr != nil {
						return tc.storageUpdateErr
					}
					gotUpdatedProduct = p
					return nil
				},
			}
			var gotOp *catalog.Operation
			s.eventQueue = &mockEventQueue{
				onPublish: func(op *catalog.Operation) error {
					gotOp = op
					return nil
				},
			}
			gotProduct, gotErr := s.UpdateProduct(context.Background(), tc.id, tc.patch)
			assert.Equal(t, tc.wantProduct, gotProduct)
			assert.Equal(t, tc.wantProduct, gotUpdatedProduct)
			assert.Equal(t, tc.wantErr, gotErr)
			assert.Equal(t, tc.wantOp, gotOp)
		})
	}
}

func TestServiceGetProduct(t *testing.T) {
	s := newTestService()
	testCases := []struct {
		name           string
		id             uint32
		storageProduct *catalog.Product
		storageErr     error
		wantProduct    *catalog.Product
		wantErr        error
	}{
		{
			name:           "ok",
			id:             1,
			storageProduct: mustNewProduct(nil),
			storageErr:     nil,
			wantProduct:    mustNewProduct(nil),
			wantErr:        nil,
		},
		{
			name:           "not found",
			id:             1,
			storageProduct: nil,
			storageErr:     catalog.ErrNotFoundInStorage,
			wantProduct:    nil,
			wantErr:        catalog.ErrNotFound("product not found: 1"),
		},
		{
			name:           "error",
			id:             1,
			storageProduct: nil,
			storageErr:     errors.New("something's wrong"),
			wantProduct:    nil,
			wantErr:        catalog.ErrInternal("failed to get product: something's wrong"),
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			s.storage = &mockStorage{
				onGetProduct: func(ctx context.Context, id uint32) (*catalog.Product, error) {
					if tc.storageErr != nil {
						return nil, tc.storageErr
					}
					return tc.storageProduct, nil
				},
			}
			gotProduct, gotErr := s.GetProduct(context.Background(), tc.id)
			assert.Equal(t, tc.wantProduct, gotProduct)
			assert.Equal(t, tc.wantErr, gotErr)
		})
	}
}

func TestServiceDeleteProduct(t *testing.T) {
	s := newTestService()
	testCases := []struct {
		name             string
		id               uint32
		storageProducts  map[uint32]*catalog.Product
		storageDeleteErr error
		wantErr          error
		wantOp           *catalog.Operation
	}{
		{
			name: "ok",
			id:   123,
			storageProducts: map[uint32]*catalog.Product{
				123: mustNewProduct(func(p *catalog.Product) { p.ID = 123 }),
			},
			storageDeleteErr: nil,
			wantErr:          nil,
			wantOp: &catalog.Operation{
				Type:    catalog.OperationTypeProduct,
				Method:  catalog.OperationMethodDelete,
				Product: mustNewProduct(func(p *catalog.Product) { p.ID = 123 }),
			},
		},
		{
			name:            "not found",
			id:              123,
			storageProducts: map[uint32]*catalog.Product{},
			wantErr:         catalog.ErrNotFound("product not found: 123"),
		},
		{
			name: "not found on delete",
			id:   123,
			storageProducts: map[uint32]*catalog.Product{
				123: mustNewProduct(func(p *catalog.Product) { p.ID = 123 }),
			},
			storageDeleteErr: catalog.ErrNotFoundInStorage,
			wantErr:          catalog.ErrNotFound("product not found: 123"),
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			s.storage = &mockStorage{
				onGetProduct: func(ctx context.Context, id uint32) (*catalog.Product, error) {
					if x, ok := tc.storageProducts[id]; ok {
						return x, nil
					}
					return nil, catalog.ErrNotFoundInStorage
				},
				onDeleteProduct: func(ctx context.Context, id uint32) error {
					return tc.storageDeleteErr
				},
			}
			var gotOp *catalog.Operation
			s.eventQueue = &mockEventQueue{
				onPublish: func(op *catalog.Operation) error {
					gotOp = op
					return nil
				},
			}
			gotErr := s.DeleteProduct(context.Background(), tc.id)
			assert.Equal(t, tc.wantErr, gotErr)
			assert.Equal(t, tc.wantOp, gotOp)
		})
	}
}

func TestServiceGetCategories(t *testing.T) {
	s := newTestService()
	testCases := []struct {
		name           string
		wantCategories []*catalog.Category
		wantErr        error
		storageErr     error
	}{
		{
			name: "ok",
			wantCategories: []*catalog.Category{
				mustNewCategory(func(c *catalog.Category) { c.ID = 1 }),
				mustNewCategory(func(c *catalog.Category) { c.ID = 2 }),
				mustNewCategory(func(c *catalog.Category) { c.ID = 3 }),
			},
			wantErr:    nil,
			storageErr: nil,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			store := &mockStorage{
				onForeachCategory: func(ctx context.Context, fn func(*catalog.Category) error) error {
					for _, c := range tc.wantCategories {
						if err := fn(c); err != nil {
							return err
						}
					}
					return tc.storageErr
				},
			}
			s.storage = store
			gotCategory, gotErr := s.GetCategories(context.Background())
			assert.Equal(t, tc.wantCategories, gotCategory)
			assert.Equal(t, tc.wantErr, gotErr)
		})
	}
}

func TestServiceCreateCategory(t *testing.T) {
	s := newTestService()
	testCases := []struct {
		name         string
		input        *catalog.CategoryInput
		id           uint32
		wantCategory *catalog.Category
		wantErr      error
		storageErr   error
	}{
		{
			name:         "ok",
			id:           1,
			input:        mustNewCategoryInput(nil),
			wantCategory: mustNewCategory(nil),
			wantErr:      nil,
			storageErr:   nil,
		},
		{
			name: "empty name",
			id:   1,
			input: mustNewCategoryInput(func(ci *catalog.CategoryInput) {
				ci.Name = ""
			}),
			wantCategory: nil,
			wantErr:      catalog.ErrBadRequest("invalid input: %s", errors.New("empty Name")),
			storageErr:   nil,
		},
		{
			name: "invalid filters",
			id:   1,
			input: mustNewCategoryInput(func(ci *catalog.CategoryInput) {
				ci.Filters[0].Type = "invalid"
			}),
			wantCategory: nil,
			wantErr:      catalog.ErrBadRequest("invalid input: %s", fmt.Errorf("invalid filter: %s", fmt.Errorf("invalid type: %q", "invalid"))),
			storageErr:   nil,
		},
		{
			name:         "nil input",
			input:        nil,
			id:           1,
			wantCategory: nil,
			wantErr:      catalog.ErrBadRequest("invalid input: %s", errors.New("nil category input")),
			storageErr:   nil,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			store := &mockStorage{
				onInsertCategory: func(ctx context.Context, c *catalog.Category) error {
					c.ID = tc.id
					return tc.storageErr
				},
			}
			s.storage = store
			gotCategory, gotErr := s.CreateCategory(context.Background(), tc.input)
			assert.Equal(t, tc.wantCategory, gotCategory)
			assert.Equal(t, tc.wantErr, gotErr)
		})
	}
}

func TestServiceGetCategory(t *testing.T) {
	s := newTestService()
	testCases := []struct {
		name         string
		id           uint32
		wantCategory *catalog.Category
		wantErr      error
		storageErr   error
	}{
		{
			name:         "ok",
			id:           1,
			wantCategory: mustNewCategory(nil),
			wantErr:      nil,
			storageErr:   nil,
		},
		{
			name:         "not found in storage",
			id:           999999999,
			wantCategory: nil,
			wantErr:      catalog.ErrNotFound("category not found: %d", 999999999),
			storageErr:   catalog.ErrNotFoundInStorage,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			store := &mockStorage{
				onGetCategory: func(ctx context.Context, id uint32) (*catalog.Category, error) {
					return tc.wantCategory, tc.storageErr
				},
			}
			s.storage = store
			gotCategory, gotErr := s.GetCategory(context.Background(), tc.id)
			assert.Equal(t, tc.wantCategory, gotCategory)
			assert.Equal(t, tc.wantErr, gotErr)
		})
	}
}

func TestServiceUpdateCategory(t *testing.T) {
	s := newTestService()
	testCases := []struct {
		name             string
		id               uint32
		wasCategory      *catalog.Category
		patch            *catalog.CategoryPatch
		wantCategory     *catalog.Category
		wantErr          error
		storageGetErr    error
		storageUpdateErr error
	}{
		{
			name: "name",
			id:   1,
			wasCategory: mustNewCategory(func(c *catalog.Category) {
				c.Name = "old"
			}),
			patch: mustNewCategoryPatch(func(cp *catalog.CategoryPatch) {
				*cp.Name = "new"
			}),
			wantCategory: mustNewCategory(func(c *catalog.Category) {
				c.Name = "new"
			}),
			wantErr:          nil,
			storageGetErr:    nil,
			storageUpdateErr: nil,
		},
		{
			name:        "invalid patch",
			id:          1,
			wasCategory: mustNewCategory(nil),
			patch: mustNewCategoryPatch(func(cp *catalog.CategoryPatch) {
				*cp.Name = ""
			}),
			wantCategory:     nil,
			wantErr:          catalog.ErrBadRequest("invalid patch: %s", errors.New("empty Name")),
			storageGetErr:    nil,
			storageUpdateErr: nil,
		},
		{
			name:             "not found in storage",
			id:               999999999,
			wasCategory:      nil,
			patch:            mustNewCategoryPatch(nil),
			wantCategory:     nil,
			wantErr:          catalog.ErrNotFound("category not found: %d", 999999999),
			storageGetErr:    catalog.ErrNotFoundInStorage,
			storageUpdateErr: nil,
		},
		{
			name:             "nil patch",
			patch:            nil,
			id:               1,
			wantCategory:     nil,
			wantErr:          catalog.ErrBadRequest("invalid patch: %s", errors.New("nil category patch")),
			storageGetErr:    nil,
			storageUpdateErr: nil,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			store := &mockStorage{
				onGetCategory: func(ctx context.Context, id uint32) (*catalog.Category, error) {
					return tc.wasCategory, tc.storageGetErr
				},
				onUpdateCategory: func(ctx context.Context, c *catalog.Category) error {
					return tc.storageUpdateErr
				},
			}
			s.storage = store
			gotCategory, gotErr := s.UpdateCategory(context.Background(), tc.id, tc.patch)
			assert.Equal(t, tc.wantCategory, gotCategory)
			assert.Equal(t, tc.wantErr, gotErr)
		})
	}
}

func TestServiceDeleteCategory(t *testing.T) {
	s := newTestService()
	testCases := []struct {
		name       string
		id         uint32
		wantErr    error
		storageErr error
	}{
		{
			name:       "ok",
			id:         1,
			wantErr:    nil,
			storageErr: nil,
		},
		{
			name:       "not found in storage",
			id:         999999999,
			wantErr:    catalog.ErrNotFound("category not found: %d", 999999999),
			storageErr: catalog.ErrNotFoundInStorage,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			store := &mockStorage{
				onGetCategory: func(ctx context.Context, id uint32) (*catalog.Category, error) {
					return &catalog.Category{
						ID: id,
					}, tc.storageErr
				},
				onDeleteCategory: func(ctx context.Context, id uint32) error {
					return tc.storageErr
				},
			}
			s.storage = store
			gotErr := s.DeleteCategory(context.Background(), tc.id)
			assert.Equal(t, tc.wantErr, gotErr)
		})
	}
}

func TestServiceGetSchemas(t *testing.T) {
	s := newTestService()
	testCases := []struct {
		name           string
		storageSchemas []*catalog.Schema
		storageErr     error
		wantSchemas    []*catalog.SchemaName
		wantErr        error
	}{
		{
			name:           "ok",
			storageSchemas: []*catalog.Schema{{ID: 1, Name: "n1"}, {ID: 2, Name: "n2"}},
			wantSchemas:    []*catalog.SchemaName{{ID: 1, Name: "n1"}, {ID: 2, Name: "n2"}},
		},
		{
			name:       "error",
			storageErr: errors.New("something's wrong"),
			wantErr:    catalog.ErrInternal("failed to get schemas: something's wrong"),
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			s.storage = &mockStorage{
				onForeachSchema: func(ctx context.Context, fn func(*catalog.Schema) error) error {
					for _, schema := range tc.storageSchemas {
						if err := fn(schema); err != nil {
							return err
						}
					}
					if tc.storageErr != nil {
						return tc.storageErr
					}
					return nil
				},
			}
			gotSchemas, gotErr := s.GetSchemas(context.Background())
			assert.Equal(t, tc.wantSchemas, gotSchemas)
			assert.Equal(t, tc.wantErr, gotErr)
		})
	}
}

func TestServiceCreateSchema(t *testing.T) {
	s := newTestService()
	testCases := []struct {
		name              string
		input             *catalog.SchemaInput
		storageProperties map[uint32]*catalog.Property
		storageInsertID   uint32
		storageInsertErr  error
		wantSchema        *catalog.Schema
		wantErr           error
	}{
		{
			name: "ok",
			input: &catalog.SchemaInput{
				Name: "Test Schema",
				Properties: []*catalog.SchemaPropertyInput{
					{ID: 1, Required: true},
					{ID: 2, Required: false},
					{ID: 3, Required: true},
					{ID: 4, Required: false},
				},
			},
			storageProperties: map[uint32]*catalog.Property{
				1: {ID: 1, Key: "k1", Name: "n1", Type: catalog.PropertyTypeEnumList, Indexed: true},
				2: {ID: 2, Key: "k2", Name: "n2", Type: catalog.PropertyTypeString, Indexed: false},
				3: {ID: 3, Key: "k3", Name: "n3", Type: catalog.PropertyTypeInt, Indexed: true},
				4: {ID: 4, Key: "k4", Name: "n4", Type: catalog.PropertyTypeDate, Indexed: true},
			},
			storageInsertID:  54321,
			storageInsertErr: nil,
			wantSchema: &catalog.Schema{
				ID:   54321,
				Name: "Test Schema",
				Properties: []*catalog.SchemaProperty{
					{ID: 1, Key: "k1", Name: "n1", Type: catalog.PropertyTypeEnumList, Indexed: true, Required: true},
					{ID: 2, Key: "k2", Name: "n2", Type: catalog.PropertyTypeString, Indexed: false, Required: false},
					{ID: 3, Key: "k3", Name: "n3", Type: catalog.PropertyTypeInt, Indexed: true, Required: true},
					{ID: 4, Key: "k4", Name: "n4", Type: catalog.PropertyTypeDate, Indexed: true, Required: false},
				},
			},
			wantErr: nil,
		},
		{
			name: "property not found",
			input: &catalog.SchemaInput{
				Name: "Test Schema",
				Properties: []*catalog.SchemaPropertyInput{
					{ID: 1, Required: true},
					{ID: 2, Required: false},
				},
			},
			storageProperties: map[uint32]*catalog.Property{
				1: {ID: 1, Key: "k1", Name: "n1", Type: catalog.PropertyTypeEnumList, Indexed: true},
			},
			wantErr: catalog.ErrBadRequest("property not found: 2"),
		},
		{
			name: "property duplicated",
			input: &catalog.SchemaInput{
				Name: "Test Schema",
				Properties: []*catalog.SchemaPropertyInput{
					{ID: 1, Required: true},
					{ID: 2, Required: false},
					{ID: 1, Required: true},
				},
			},
			storageProperties: map[uint32]*catalog.Property{
				1: {ID: 1, Key: "k1", Name: "n1", Type: catalog.PropertyTypeEnumList, Indexed: true},
				2: {ID: 2, Key: "k2", Name: "n2", Type: catalog.PropertyTypeString, Indexed: false},
			},
			wantErr: catalog.ErrBadRequest("property is duplicated: 1"),
		},
		{
			name: "insert error",
			input: &catalog.SchemaInput{
				Name: "Test Schema",
				Properties: []*catalog.SchemaPropertyInput{
					{ID: 1, Required: true},
					{ID: 2, Required: false},
				},
			},
			storageProperties: map[uint32]*catalog.Property{
				1: {ID: 1, Key: "k1", Name: "n1", Type: catalog.PropertyTypeEnumList, Indexed: true},
				2: {ID: 2, Key: "k2", Name: "n2", Type: catalog.PropertyTypeString, Indexed: false},
			},
			storageInsertErr: errors.New("bad luck"),
			wantErr:          catalog.ErrInternal("failed to insert schema: bad luck"),
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var gotInsertedSchema *catalog.Schema
			s.storage = &mockStorage{
				onGetProperty: func(ctx context.Context, id uint32) (*catalog.Property, error) {
					if x, ok := tc.storageProperties[id]; ok {
						return x, nil
					}
					return nil, catalog.ErrNotFoundInStorage
				},
				onInsertSchema: func(ctx context.Context, s *catalog.Schema) error {
					if tc.storageInsertErr != nil {
						return tc.storageInsertErr
					}
					s.ID = tc.storageInsertID
					gotInsertedSchema = s
					return nil
				},
			}
			gotSchema, gotErr := s.CreateSchema(context.Background(), tc.input)
			assert.Equal(t, tc.wantSchema, gotSchema)
			assert.Equal(t, tc.wantSchema, gotInsertedSchema)
			assert.Equal(t, tc.wantErr, gotErr)
		})
	}
}

func TestServiceUpdateSchema(t *testing.T) {
	s := newTestService()
	testCases := []struct {
		name              string
		id                uint32
		patch             *catalog.SchemaPatch
		storageSchemas    map[uint32]*catalog.Schema
		storageProperties map[uint32]*catalog.Property
		storageUpdateErr  error
		wantSchema        *catalog.Schema
		wantErr           error
	}{
		{
			name: "ok",
			id:   321,
			patch: &catalog.SchemaPatch{
				Name: stringptr("Test Schema Updated"),
				Properties: []*catalog.SchemaPropertyInput{
					{ID: 1, Required: false},
					{ID: 3, Required: true},
					{ID: 4, Required: true},
				},
			},
			storageSchemas: map[uint32]*catalog.Schema{
				321: {
					ID:   321,
					Name: "Test Schema",
					Properties: []*catalog.SchemaProperty{
						{ID: 1, Key: "k1", Name: "n1", Type: catalog.PropertyTypeEnumList, Indexed: true, Required: true},
						{ID: 2, Key: "k2", Name: "n2", Type: catalog.PropertyTypeString, Indexed: false, Required: false},
						{ID: 3, Key: "k3", Name: "n3", Type: catalog.PropertyTypeInt, Indexed: true, Required: true},
					},
				},
			},
			storageProperties: map[uint32]*catalog.Property{
				1: {ID: 1, Key: "k1", Name: "n1", Type: catalog.PropertyTypeEnumList, Indexed: true},
				3: {ID: 3, Key: "k3", Name: "n3", Type: catalog.PropertyTypeInt, Indexed: true},
				4: {ID: 4, Key: "k4", Name: "n4", Type: catalog.PropertyTypeDate, Indexed: true},
			},
			storageUpdateErr: nil,
			wantSchema: &catalog.Schema{
				ID:   321,
				Name: "Test Schema Updated",
				Properties: []*catalog.SchemaProperty{
					{ID: 1, Key: "k1", Name: "n1", Type: catalog.PropertyTypeEnumList, Indexed: true, Required: false},
					{ID: 3, Key: "k3", Name: "n3", Type: catalog.PropertyTypeInt, Indexed: true, Required: true},
					{ID: 4, Key: "k4", Name: "n4", Type: catalog.PropertyTypeDate, Indexed: true, Required: true},
				},
			},
			wantErr: nil,
		},
		{
			name: "ok - only name",
			id:   321,
			patch: &catalog.SchemaPatch{
				Name: stringptr("Test Schema Updated"),
			},
			storageSchemas: map[uint32]*catalog.Schema{
				321: {
					ID:   321,
					Name: "Test Schema",
					Properties: []*catalog.SchemaProperty{
						{ID: 1, Key: "k1", Name: "n1", Type: catalog.PropertyTypeEnumList, Indexed: true, Required: true},
						{ID: 2, Key: "k2", Name: "n2", Type: catalog.PropertyTypeString, Indexed: false, Required: false},
						{ID: 3, Key: "k3", Name: "n3", Type: catalog.PropertyTypeInt, Indexed: true, Required: true},
					},
				},
			},
			storageUpdateErr: nil,
			wantSchema: &catalog.Schema{
				ID:   321,
				Name: "Test Schema Updated",
				Properties: []*catalog.SchemaProperty{
					{ID: 1, Key: "k1", Name: "n1", Type: catalog.PropertyTypeEnumList, Indexed: true, Required: true},
					{ID: 2, Key: "k2", Name: "n2", Type: catalog.PropertyTypeString, Indexed: false, Required: false},
					{ID: 3, Key: "k3", Name: "n3", Type: catalog.PropertyTypeInt, Indexed: true, Required: true},
				},
			},
			wantErr: nil,
		},
		{
			name: "ok - only properties",
			id:   321,
			patch: &catalog.SchemaPatch{
				Properties: []*catalog.SchemaPropertyInput{
					{ID: 1, Required: false},
					{ID: 3, Required: true},
					{ID: 4, Required: true},
				},
			},
			storageSchemas: map[uint32]*catalog.Schema{
				321: {
					ID:   321,
					Name: "Test Schema",
					Properties: []*catalog.SchemaProperty{
						{ID: 1, Key: "k1", Name: "n1", Type: catalog.PropertyTypeEnumList, Indexed: true, Required: true},
						{ID: 2, Key: "k2", Name: "n2", Type: catalog.PropertyTypeString, Indexed: false, Required: false},
						{ID: 3, Key: "k3", Name: "n3", Type: catalog.PropertyTypeInt, Indexed: true, Required: true},
					},
				},
			},
			storageProperties: map[uint32]*catalog.Property{
				1: {ID: 1, Key: "k1", Name: "n1", Type: catalog.PropertyTypeEnumList, Indexed: true},
				3: {ID: 3, Key: "k3", Name: "n3", Type: catalog.PropertyTypeInt, Indexed: true},
				4: {ID: 4, Key: "k4", Name: "n4", Type: catalog.PropertyTypeDate, Indexed: true},
			},
			storageUpdateErr: nil,
			wantSchema: &catalog.Schema{
				ID:   321,
				Name: "Test Schema",
				Properties: []*catalog.SchemaProperty{
					{ID: 1, Key: "k1", Name: "n1", Type: catalog.PropertyTypeEnumList, Indexed: true, Required: false},
					{ID: 3, Key: "k3", Name: "n3", Type: catalog.PropertyTypeInt, Indexed: true, Required: true},
					{ID: 4, Key: "k4", Name: "n4", Type: catalog.PropertyTypeDate, Indexed: true, Required: true},
				},
			},
			wantErr: nil,
		},
		{
			name: "schema not found",
			id:   321,
			patch: &catalog.SchemaPatch{
				Name: stringptr("Test Schema Updated"),
				Properties: []*catalog.SchemaPropertyInput{
					{ID: 1, Required: false},
					{ID: 3, Required: true},
					{ID: 4, Required: true},
				},
			},
			storageSchemas: map[uint32]*catalog.Schema{},
			wantErr:        catalog.ErrNotFound("schema not found: 321"),
		},
		{
			name: "update error",
			id:   321,
			patch: &catalog.SchemaPatch{
				Name: stringptr("Test Schema Updated"),
			},
			storageSchemas: map[uint32]*catalog.Schema{
				321: {
					ID:   321,
					Name: "Test Schema",
					Properties: []*catalog.SchemaProperty{
						{ID: 1, Key: "k1", Name: "n1", Type: catalog.PropertyTypeEnumList, Indexed: true, Required: true},
					},
				},
			},
			storageUpdateErr: errors.New("bad luck"),
			wantErr:          catalog.ErrInternal("failed to update schema: bad luck"),
		},
		{
			name: "update error - not found",
			id:   321,
			patch: &catalog.SchemaPatch{
				Name: stringptr("Test Schema Updated"),
			},
			storageSchemas: map[uint32]*catalog.Schema{
				321: {
					ID:   321,
					Name: "Test Schema",
					Properties: []*catalog.SchemaProperty{
						{ID: 1, Key: "k1", Name: "n1", Type: catalog.PropertyTypeEnumList, Indexed: true, Required: true},
					},
				},
			},
			storageUpdateErr: catalog.ErrNotFoundInStorage,
			wantErr:          catalog.ErrNotFound("schema not found: 321"),
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var gotUpdatedSchema *catalog.Schema
			s.storage = &mockStorage{
				onGetSchema: func(ctx context.Context, id uint32) (*catalog.Schema, error) {
					if x, ok := tc.storageSchemas[id]; ok {
						return x, nil
					}
					return nil, catalog.ErrNotFoundInStorage
				},
				onGetProperty: func(ctx context.Context, id uint32) (*catalog.Property, error) {
					if x, ok := tc.storageProperties[id]; ok {
						return x, nil
					}
					return nil, catalog.ErrNotFoundInStorage
				},
				onUpdateSchema: func(ctx context.Context, s *catalog.Schema) error {
					if tc.storageUpdateErr != nil {
						return tc.storageUpdateErr
					}
					gotUpdatedSchema = s
					return nil
				},
			}
			gotSchema, gotErr := s.UpdateSchema(context.Background(), tc.id, tc.patch)
			assert.Equal(t, tc.wantSchema, gotSchema)
			assert.Equal(t, tc.wantSchema, gotUpdatedSchema)
			assert.Equal(t, tc.wantErr, gotErr)
		})
	}
}

func TestServiceGetSchema(t *testing.T) {
	s := newTestService()
	testCases := []struct {
		name           string
		id             uint32
		storageSchemas map[uint32]*catalog.Schema
		wantSchema     *catalog.Schema
		wantErr        error
	}{
		{
			name: "ok",
			id:   123,
			storageSchemas: map[uint32]*catalog.Schema{
				123: mustNewSchema(func(s *catalog.Schema) { s.ID = 123 }),
			},
			wantSchema: mustNewSchema(func(s *catalog.Schema) { s.ID = 123 }),
		},
		{
			name:           "not found",
			id:             123,
			storageSchemas: map[uint32]*catalog.Schema{},
			wantErr:        catalog.ErrNotFound("schema not found: 123"),
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			s.storage = &mockStorage{
				onGetSchema: func(ctx context.Context, id uint32) (*catalog.Schema, error) {
					if x, ok := tc.storageSchemas[id]; ok {
						return x, nil
					}
					return nil, catalog.ErrNotFoundInStorage
				},
			}
			gotSchema, gotErr := s.GetSchema(context.Background(), tc.id)
			assert.Equal(t, tc.wantSchema, gotSchema)
			assert.Equal(t, tc.wantErr, gotErr)
		})
	}
}

func TestServiceDeleteSchema(t *testing.T) {
	s := newTestService()
	testCases := []struct {
		name           string
		id             uint32
		storageSchemas map[uint32]bool
		wantErr        error
	}{
		{
			name:           "ok",
			id:             123,
			storageSchemas: map[uint32]bool{123: true},
			wantErr:        nil,
		},
		{
			name:           "not found",
			id:             123,
			storageSchemas: map[uint32]bool{},
			wantErr:        catalog.ErrNotFound("schema not found: 123"),
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			s.storage = &mockStorage{
				onDeleteSchema: func(ctx context.Context, id uint32) error {
					if !tc.storageSchemas[id] {
						return catalog.ErrNotFoundInStorage
					}
					return nil
				},
			}
			gotErr := s.DeleteSchema(context.Background(), tc.id)
			assert.Equal(t, tc.wantErr, gotErr)
		})
	}
}

func stringptr(v string) *string { return &v }
