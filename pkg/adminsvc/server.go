package adminsvc

import (
	"context"
	"fmt"
	"net/http"
	"net/http/pprof"
	"time"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

// ServerConfig is a server configuration.
type ServerConfig struct {
	Logger          log.Logger
	Storage         Storage
	EventQueue      EventQueue
	Port            string
	ReadTimeout     time.Duration
	WriteTimeout    time.Duration
	ShutdownTimeout time.Duration
	MetricPrefix    string
}

// Storage is a persistent catalog data storage.
type Storage interface {
	InsertProperty(ctx context.Context, p *catalog.Property) error
	GetProperty(ctx context.Context, id uint32) (*catalog.Property, error)
	ForeachProperty(ctx context.Context, fn func(*catalog.Property) error) error
	DeleteProperty(ctx context.Context, id uint32) error

	GetPropertyVariantsByProperty(ctx context.Context, propertyID uint32) ([]*catalog.PropertyVariant, error)
	InsertPropertyVariant(ctx context.Context, pv *catalog.PropertyVariant) error
	GetPropertyVariant(ctx context.Context, id uint32) (*catalog.PropertyVariant, error)
	DeletePropertyVariant(ctx context.Context, id uint32) error

	GetProducts(ctx context.Context, query *catalog.StorageProductQuery) (*catalog.StorageProductQueryResult, error)
	InsertProduct(ctx context.Context, p *catalog.Product) error
	UpdateProduct(ctx context.Context, p *catalog.Product) error
	GetProduct(ctx context.Context, id uint32) (*catalog.Product, error)
	DeleteProduct(ctx context.Context, id uint32) error

	ForeachCategory(ctx context.Context, fn func(*catalog.Category) error) error
	InsertCategory(ctx context.Context, c *catalog.Category) error
	GetCategory(ctx context.Context, id uint32) (*catalog.Category, error)
	UpdateCategory(ctx context.Context, c *catalog.Category) error
	DeleteCategory(ctx context.Context, id uint32) error

	InsertSchema(ctx context.Context, schema *catalog.Schema) error
	UpdateSchema(ctx context.Context, schema *catalog.Schema) error
	GetSchema(ctx context.Context, id uint32) (*catalog.Schema, error)
	ForeachSchema(ctx context.Context, fn func(*catalog.Schema) error) error
	DeleteSchema(ctx context.Context, id uint32) error
}

// EventQueue is a catalog data replication queue.
type EventQueue interface {
	Publish(op *catalog.Operation) error
}

// Server is a filters service server.
type Server struct {
	cfg *ServerConfig
	srv *http.Server
}

// NewServer creates a new server.
func NewServer(cfg ServerConfig) (*Server, error) {
	var svc Service
	svc = newService(cfg.Logger, cfg.Storage, cfg.EventQueue)
	svc = NewLoggingMiddleware(svc, cfg.Logger)
	svc = NewInstrumentingMiddleware(svc, cfg.MetricPrefix)

	router := http.NewServeMux()
	router.Handle("/metrics", promhttp.Handler())
	router.Handle("/debug/pprof/profile", http.HandlerFunc(pprof.Profile))
	router.Handle("/api/v1/", makeHandler(svc))

	srv := &http.Server{
		Handler:      router,
		Addr:         ":" + cfg.Port,
		ReadTimeout:  cfg.ReadTimeout,
		WriteTimeout: cfg.WriteTimeout,
	}

	s := &Server{
		cfg: &cfg,
		srv: srv,
	}
	return s, nil
}

// Serve starts HTTP server and stops it when the provided context is canceled.
func (s *Server) Serve(ctx context.Context) error {
	errChan := make(chan error, 1)
	go func() {
		errChan <- s.srv.ListenAndServe()
	}()

	select {
	case err := <-errChan:
		return err

	case <-ctx.Done():
		ctxShutdown, cancel := context.WithTimeout(context.Background(), s.cfg.ShutdownTimeout)
		defer cancel()
		if err := s.srv.Shutdown(ctxShutdown); err != nil {
			return fmt.Errorf("failed to shutdown server: %w", err)
		}
		return nil
	}
}

func makeHandler(svc Service) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorEncoder(encodeError),
	}

	router := mux.NewRouter()

	router.Path("/api/v1/properties").Methods(http.MethodGet).Handler(kithttp.NewServer(
		makeGetPropertiesEndpoint(svc),
		decodeGetPropertiesRequest,
		encodeGetPropertiesResponse,
		opts...,
	))

	router.Path("/api/v1/properties").Methods(http.MethodPost).Handler(kithttp.NewServer(
		makeCreatePropertyEndpoint(svc),
		decodeCreatePropertyRequest,
		encodeCreatePropertyResponse,
		opts...,
	))

	router.Path("/api/v1/properties/{id}").Methods(http.MethodGet).Handler(kithttp.NewServer(
		makeGetPropertyEndpoint(svc),
		decodeGetPropertyRequest,
		encodeGetPropertyResponse,
		opts...,
	))

	router.Path("/api/v1/properties/{id}").Methods(http.MethodDelete).Handler(kithttp.NewServer(
		makeDeletePropertyEndpoint(svc),
		decodeDeletePropertyRequest,
		encodeDeletePropertyResponse,
		opts...,
	))

	router.Path("/api/v1/property_variants").Methods(http.MethodGet).Handler(kithttp.NewServer(
		makeGetPropertyVariantsEndpoint(svc),
		decodeGetPropertyVariantsRequest,
		encodeGetPropertyVariantsResponse,
		opts...,
	))

	router.Path("/api/v1/property_variants").Methods(http.MethodPost).Handler(kithttp.NewServer(
		makeCreatePropertyVariantEndpoint(svc),
		decodeCreatePropertyVariantRequest,
		encodeCreatePropertyVariantResponse,
		opts...,
	))

	router.Path("/api/v1/property_variants/{id}").Methods(http.MethodGet).Handler(kithttp.NewServer(
		makeGetPropertyVariantEndpoint(svc),
		decodeGetPropertyVariantRequest,
		encodeGetPropertyVariantResponse,
		opts...,
	))

	router.Path("/api/v1/property_variants/{id}").Methods(http.MethodDelete).Handler(kithttp.NewServer(
		makeDeletePropertyVariantEndpoint(svc),
		decodeDeletePropertyVariantRequest,
		encodeDeletePropertyVariantResponse,
		opts...,
	))

	router.Path("/api/v1/products").Methods(http.MethodGet).Handler(kithttp.NewServer(
		makeGetProductsEndpoint(svc),
		decodeGetProductsRequest,
		encodeGetProductsResponse,
		opts...,
	))

	router.Path("/api/v1/products").Methods(http.MethodPost).Handler(kithttp.NewServer(
		makeCreateProductEndpoint(svc),
		decodeCreateProductRequest,
		encodeCreateProductResponse,
		opts...,
	))

	router.Path("/api/v1/products/{id}").Methods(http.MethodPatch).Handler(kithttp.NewServer(
		makeUpdateProductEndpoint(svc),
		decodeUpdateProductRequest,
		encodeUpdateProductResponse,
		opts...,
	))

	router.Path("/api/v1/products/{id}").Methods(http.MethodGet).Handler(kithttp.NewServer(
		makeGetProductEndpoint(svc),
		decodeGetProductRequest,
		encodeGetProductResponse,
		opts...,
	))

	router.Path("/api/v1/products/{id}").Methods(http.MethodDelete).Handler(kithttp.NewServer(
		makeDeleteProductEndpoint(svc),
		decodeDeleteProductRequest,
		encodeDeleteProductResponse,
		opts...,
	))

	router.Path("/api/v1/categories").Methods(http.MethodGet).Handler(kithttp.NewServer(
		makeGetCategoriesEndpoint(svc),
		decodeGetCategoriesRequest,
		encodeGetCategoriesResponse,
		opts...,
	))

	router.Path("/api/v1/categories").Methods(http.MethodPost).Handler(kithttp.NewServer(
		makeCreateCategoryEndpoint(svc),
		decodeCreateCategoryRequest,
		encodeCreateCategoryResponse,
		opts...,
	))

	router.Path("/api/v1/categories/{id}").Methods(http.MethodGet).Handler(kithttp.NewServer(
		makeGetCategoryEndpoint(svc),
		decodeGetCategoryRequest,
		encodeGetCategoryResponse,
		opts...,
	))

	router.Path("/api/v1/categories/{id}").Methods(http.MethodPatch).Handler(kithttp.NewServer(
		makeUpdateCategoryEndpoint(svc),
		decodeUpdateCategoryRequest,
		encodeUpdateCategoryResponse,
		opts...,
	))

	router.Path("/api/v1/categories/{id}").Methods(http.MethodDelete).Handler(kithttp.NewServer(
		makeDeleteCategoryEndpoint(svc),
		decodeDeleteCategoryRequest,
		encodeDeleteCategoryResponse,
		opts...,
	))

	router.Path("/api/v1/schemas").Methods(http.MethodGet).Handler(kithttp.NewServer(
		makeGetSchemasEndpoint(svc),
		decodeGetSchemasRequest,
		encodeGetSchemasResponse,
		opts...,
	))

	router.Path("/api/v1/schemas").Methods(http.MethodPost).Handler(kithttp.NewServer(
		makeCreateSchemaEndpoint(svc),
		decodeCreateSchemaRequest,
		encodeCreateSchemaResponse,
		opts...,
	))

	router.Path("/api/v1/schemas/{id}").Methods(http.MethodPatch).Handler(kithttp.NewServer(
		makeUpdateSchemaEndpoint(svc),
		decodeUpdateSchemaRequest,
		encodeUpdateSchemaResponse,
		opts...,
	))

	router.Path("/api/v1/schemas/{id}").Methods(http.MethodGet).Handler(kithttp.NewServer(
		makeGetSchemaEndpoint(svc),
		decodeGetSchemaRequest,
		encodeGetSchemaResponse,
		opts...,
	))

	router.Path("/api/v1/schemas/{id}").Methods(http.MethodDelete).Handler(kithttp.NewServer(
		makeDeleteSchemaEndpoint(svc),
		decodeDeleteSchemaRequest,
		encodeDeleteSchemaResponse,
		opts...,
	))

	return router
}

func makeGetPropertiesEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		properties, err := svc.GetProperties(ctx)
		return getPropertiesResponse{properties: properties}, err
	}
}

func makeCreatePropertyEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(createPropertyRequest)
		property, err := svc.CreateProperty(ctx, req.input)
		return createPropertyResponse{property: property}, err
	}
}

func makeGetPropertyEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getPropertyRequest)
		property, err := svc.GetProperty(ctx, req.id)
		return getPropertyResponse{property: property}, err
	}
}

func makeDeletePropertyEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(deletePropertyRequest)
		err := svc.DeleteProperty(ctx, req.id)
		return deletePropertyResponse{}, err
	}
}

func makeGetPropertyVariantsEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getPropertyVariantsRequest)
		variants, err := svc.GetPropertyVariants(ctx, req.id)
		return getPropertyVariantsResponse{variants: variants}, err
	}
}

func makeCreatePropertyVariantEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(createPropertyVariantRequest)
		variant, err := svc.CreatePropertyVariant(ctx, req.input)
		return createPropertyVariantResponse{variant: variant}, err
	}
}

func makeGetPropertyVariantEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getPropertyVariantRequest)
		variant, err := svc.GetPropertyVariant(ctx, req.id)
		return getPropertyVariantResponse{variant: variant}, err
	}
}

func makeDeletePropertyVariantEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(deletePropertyVariantRequest)
		err := svc.DeletePropertyVariant(ctx, req.id)
		return deletePropertyVariantResponse{}, err
	}
}

func makeGetProductsEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getProductsRequest)
		result, err := svc.GetProducts(ctx, req.query)
		return getProductsResponse{result: result}, err
	}
}

func makeCreateProductEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(createProductRequest)
		product, err := svc.CreateProduct(ctx, req.input)
		return createProductResponse{product: product}, err
	}
}

func makeUpdateProductEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateProductRequest)
		product, err := svc.UpdateProduct(ctx, req.id, req.patch)
		return updateProductResponse{product: product}, err
	}
}

func makeGetProductEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getProductRequest)
		product, err := svc.GetProduct(ctx, req.id)
		return getProductResponse{product: product}, err
	}
}

func makeDeleteProductEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(deleteProductRequest)
		err := svc.DeleteProduct(ctx, req.id)
		return deleteProductResponse{}, err
	}
}

func makeGetCategoriesEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		categories, err := svc.GetCategories(ctx)
		return getCategoriesResponse{categories: categories}, err
	}
}

func makeCreateCategoryEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(createCategoryRequest)
		category, err := svc.CreateCategory(ctx, req.input)
		return createCategoryResponse{category: category}, err
	}
}

func makeGetCategoryEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getCategoryRequest)
		category, err := svc.GetCategory(ctx, req.id)
		return getCategoryResponse{category: category}, err
	}
}

func makeUpdateCategoryEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateCategoryRequest)
		category, err := svc.UpdateCategory(ctx, req.id, req.patch)
		return updateCategoryResponse{category: category}, err
	}
}

func makeDeleteCategoryEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(deleteCategoryRequest)
		err := svc.DeleteCategory(ctx, req.id)
		return deleteCategoryResponse{}, err
	}
}

func makeGetSchemasEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		schemas, err := svc.GetSchemas(ctx)
		return getSchemasResponse{schemas: schemas}, err
	}
}

func makeCreateSchemaEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(createSchemaRequest)
		schema, err := svc.CreateSchema(ctx, req.input)
		return createSchemaResponse{schema: schema}, err
	}
}

func makeUpdateSchemaEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateSchemaRequest)
		schema, err := svc.UpdateSchema(ctx, req.id, req.patch)
		return updateSchemaResponse{schema: schema}, err
	}
}

func makeGetSchemaEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getSchemaRequest)
		schema, err := svc.GetSchema(ctx, req.id)
		return getSchemaResponse{schema: schema}, err
	}
}

func makeDeleteSchemaEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(deleteSchemaRequest)
		err := svc.DeleteSchema(ctx, req.id)
		return deleteSchemaResponse{}, err
	}
}
