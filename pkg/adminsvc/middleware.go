package adminsvc

import (
	"context"
	"strconv"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/go-kit/kit/metrics"
	kitprometheus "github.com/go-kit/kit/metrics/prometheus"
	"github.com/prometheus/client_golang/prometheus"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

// InstrumentingMiddleware wraps Service and records metrics.
type InstrumentingMiddleware struct {
	svc       Service
	histogram metrics.Histogram
}

func NewInstrumentingMiddleware(svc Service, prefix string) *InstrumentingMiddleware {
	return &InstrumentingMiddleware{
		svc: svc,
		histogram: kitprometheus.NewHistogramFrom(
			prometheus.HistogramOpts{
				Name:    prefix + "_requests",
				Buckets: prometheus.ExponentialBuckets(0.001, 2, 14),
			},
			[]string{"method", "error"},
		),
	}
}

func (mw *InstrumentingMiddleware) GetProperties(ctx context.Context) (properties []*catalog.Property, err error) {
	defer mw.record(time.Now(), "GetProperties", &err)
	return mw.svc.GetProperties(ctx)
}

func (mw *InstrumentingMiddleware) CreateProperty(ctx context.Context, input *catalog.PropertyInput) (property *catalog.Property, err error) {
	defer mw.record(time.Now(), "CreateProperty", &err)
	return mw.svc.CreateProperty(ctx, input)
}

func (mw *InstrumentingMiddleware) GetProperty(ctx context.Context, id uint32) (property *catalog.Property, err error) {
	defer mw.record(time.Now(), "GetProperty", &err)
	return mw.svc.GetProperty(ctx, id)
}

func (mw *InstrumentingMiddleware) DeleteProperty(ctx context.Context, id uint32) (err error) {
	defer mw.record(time.Now(), "DeleteProperty", &err)
	return mw.svc.DeleteProperty(ctx, id)
}

func (mw *InstrumentingMiddleware) GetPropertyVariants(ctx context.Context, id uint32) (variants []*catalog.PropertyVariant, err error) {
	defer mw.record(time.Now(), "GetPropertyVariants", &err)
	return mw.svc.GetPropertyVariants(ctx, id)
}

func (mw *InstrumentingMiddleware) CreatePropertyVariant(ctx context.Context, input *catalog.PropertyVariantInput) (variant *catalog.PropertyVariant, err error) {
	defer mw.record(time.Now(), "CreatePropertyVariant", &err)
	return mw.svc.CreatePropertyVariant(ctx, input)
}

func (mw *InstrumentingMiddleware) GetPropertyVariant(ctx context.Context, id uint32) (variant *catalog.PropertyVariant, err error) {
	defer mw.record(time.Now(), "GetPropertyVariant", &err)
	return mw.svc.GetPropertyVariant(ctx, id)
}

func (mw *InstrumentingMiddleware) DeletePropertyVariant(ctx context.Context, id uint32) (err error) {
	defer mw.record(time.Now(), "DeletePropertyVariant", &err)
	return mw.svc.DeletePropertyVariant(ctx, id)
}

func (mw *InstrumentingMiddleware) GetProducts(ctx context.Context, query *catalog.StorageProductQuery) (r *catalog.StorageProductQueryResult, err error) {
	defer mw.record(time.Now(), "GetProducts", &err)
	return mw.svc.GetProducts(ctx, query)
}

func (mw *InstrumentingMiddleware) CreateProduct(ctx context.Context, input *catalog.ProductInput) (product *catalog.Product, err error) {
	defer mw.record(time.Now(), "CreateProduct", &err)
	return mw.svc.CreateProduct(ctx, input)
}

func (mw *InstrumentingMiddleware) UpdateProduct(ctx context.Context, id uint32, patch *catalog.ProductPatch) (product *catalog.Product, err error) {
	defer mw.record(time.Now(), "UpdateProduct", &err)
	return mw.svc.UpdateProduct(ctx, id, patch)
}

func (mw *InstrumentingMiddleware) GetProduct(ctx context.Context, id uint32) (product *catalog.Product, err error) {
	defer mw.record(time.Now(), "GetProduct", &err)
	return mw.svc.GetProduct(ctx, id)
}

func (mw *InstrumentingMiddleware) DeleteProduct(ctx context.Context, id uint32) (err error) {
	defer mw.record(time.Now(), "DeleteProduct", &err)
	return mw.svc.DeleteProduct(ctx, id)
}

func (mw *InstrumentingMiddleware) GetCategories(ctx context.Context) (cc []*catalog.Category, err error) {
	defer mw.record(time.Now(), "GetCategories", &err)
	return mw.svc.GetCategories(ctx)
}

func (mw *InstrumentingMiddleware) CreateCategory(ctx context.Context, input *catalog.CategoryInput) (c *catalog.Category, err error) {
	defer mw.record(time.Now(), "CreateCategory", &err)
	return mw.svc.CreateCategory(ctx, input)
}

func (mw *InstrumentingMiddleware) GetCategory(ctx context.Context, id uint32) (c *catalog.Category, err error) {
	defer mw.record(time.Now(), "GetCategory", &err)
	return mw.svc.GetCategory(ctx, id)
}

func (mw *InstrumentingMiddleware) UpdateCategory(ctx context.Context, id uint32, p *catalog.CategoryPatch) (c *catalog.Category, err error) {
	defer mw.record(time.Now(), "UpdateCategory", &err)
	return mw.svc.UpdateCategory(ctx, id, p)
}

func (mw *InstrumentingMiddleware) DeleteCategory(ctx context.Context, id uint32) (err error) {
	defer mw.record(time.Now(), "DeleteCategory", &err)
	return mw.svc.DeleteCategory(ctx, id)
}

func (mw *InstrumentingMiddleware) GetSchemas(ctx context.Context) (schemas []*catalog.SchemaName, err error) {
	defer mw.record(time.Now(), "GetSchemas", &err)
	return mw.svc.GetSchemas(ctx)
}

func (mw *InstrumentingMiddleware) CreateSchema(ctx context.Context, input *catalog.SchemaInput) (schema *catalog.Schema, err error) {
	defer mw.record(time.Now(), "CreateSchema", &err)
	return mw.svc.CreateSchema(ctx, input)
}

func (mw *InstrumentingMiddleware) UpdateSchema(ctx context.Context, id uint32, patch *catalog.SchemaPatch) (schema *catalog.Schema, err error) {
	defer mw.record(time.Now(), "UpdateSchema", &err)
	return mw.svc.UpdateSchema(ctx, id, patch)
}

func (mw *InstrumentingMiddleware) GetSchema(ctx context.Context, id uint32) (schema *catalog.Schema, err error) {
	defer mw.record(time.Now(), "GetSchema", &err)
	return mw.svc.GetSchema(ctx, id)
}

func (mw *InstrumentingMiddleware) DeleteSchema(ctx context.Context, id uint32) (err error) {
	defer mw.record(time.Now(), "DeleteSchema", &err)
	return mw.svc.DeleteSchema(ctx, id)
}

func (mw *InstrumentingMiddleware) record(beginTime time.Time, method string, err *error) {
	labels := []string{"method", method, "error", strconv.FormatBool(*err != nil)}
	mw.histogram.With(labels...).Observe(time.Since(beginTime).Seconds())
}

// LoggingMiddleware wraps Service and logs errors.
type LoggingMiddleware struct {
	svc    Service
	logger log.Logger
}

func NewLoggingMiddleware(svc Service, logger log.Logger) *LoggingMiddleware {
	return &LoggingMiddleware{
		svc:    svc,
		logger: logger,
	}
}

func (mw *LoggingMiddleware) GetProperties(ctx context.Context) (properties []*catalog.Property, err error) {
	defer mw.log(time.Now(), "GetProperties", &err)
	return mw.svc.GetProperties(ctx)
}

func (mw *LoggingMiddleware) CreateProperty(ctx context.Context, input *catalog.PropertyInput) (property *catalog.Property, err error) {
	defer mw.log(time.Now(), "CreateProperty", &err)
	return mw.svc.CreateProperty(ctx, input)
}

func (mw *LoggingMiddleware) GetProperty(ctx context.Context, id uint32) (property *catalog.Property, err error) {
	defer mw.log(time.Now(), "GetProperty", &err)
	return mw.svc.GetProperty(ctx, id)
}

func (mw *LoggingMiddleware) DeleteProperty(ctx context.Context, id uint32) (err error) {
	defer mw.log(time.Now(), "DeleteProperty", &err)
	return mw.svc.DeleteProperty(ctx, id)
}

func (mw *LoggingMiddleware) GetPropertyVariants(ctx context.Context, id uint32) (variants []*catalog.PropertyVariant, err error) {
	defer mw.log(time.Now(), "GetPropertyVariants", &err)
	return mw.svc.GetPropertyVariants(ctx, id)
}

func (mw *LoggingMiddleware) CreatePropertyVariant(ctx context.Context, input *catalog.PropertyVariantInput) (variant *catalog.PropertyVariant, err error) {
	defer mw.log(time.Now(), "CreatePropertyVariant", &err)
	return mw.svc.CreatePropertyVariant(ctx, input)
}

func (mw *LoggingMiddleware) GetPropertyVariant(ctx context.Context, id uint32) (variant *catalog.PropertyVariant, err error) {
	defer mw.log(time.Now(), "GetPropertyVariant", &err)
	return mw.svc.GetPropertyVariant(ctx, id)
}

func (mw *LoggingMiddleware) DeletePropertyVariant(ctx context.Context, id uint32) (err error) {
	defer mw.log(time.Now(), "DeletePropertyVariant", &err)
	return mw.svc.DeletePropertyVariant(ctx, id)
}

func (mw *LoggingMiddleware) GetProducts(ctx context.Context, query *catalog.StorageProductQuery) (r *catalog.StorageProductQueryResult, err error) {
	defer mw.log(time.Now(), "GetProducts", &err)
	return mw.svc.GetProducts(ctx, query)
}

func (mw *LoggingMiddleware) CreateProduct(ctx context.Context, input *catalog.ProductInput) (product *catalog.Product, err error) {
	defer mw.log(time.Now(), "CreateProduct", &err)
	return mw.svc.CreateProduct(ctx, input)
}

func (mw *LoggingMiddleware) UpdateProduct(ctx context.Context, id uint32, patch *catalog.ProductPatch) (product *catalog.Product, err error) {
	defer mw.log(time.Now(), "UpdateProduct", &err)
	return mw.svc.UpdateProduct(ctx, id, patch)
}

func (mw *LoggingMiddleware) GetProduct(ctx context.Context, id uint32) (product *catalog.Product, err error) {
	defer mw.log(time.Now(), "GetProduct", &err)
	return mw.svc.GetProduct(ctx, id)
}

func (mw *LoggingMiddleware) DeleteProduct(ctx context.Context, id uint32) (err error) {
	defer mw.log(time.Now(), "DeleteProduct", &err)
	return mw.svc.DeleteProduct(ctx, id)
}

func (mw *LoggingMiddleware) GetCategories(ctx context.Context) (cc []*catalog.Category, err error) {
	defer mw.log(time.Now(), "GetCategories", &err)
	return mw.svc.GetCategories(ctx)
}

func (mw *LoggingMiddleware) CreateCategory(ctx context.Context, input *catalog.CategoryInput) (c *catalog.Category, err error) {
	defer mw.log(time.Now(), "CreateCategory", &err)
	return mw.svc.CreateCategory(ctx, input)
}

func (mw *LoggingMiddleware) GetCategory(ctx context.Context, id uint32) (c *catalog.Category, err error) {
	defer mw.log(time.Now(), "GetCategory", &err)
	return mw.svc.GetCategory(ctx, id)
}

func (mw *LoggingMiddleware) UpdateCategory(ctx context.Context, id uint32, p *catalog.CategoryPatch) (c *catalog.Category, err error) {
	defer mw.log(time.Now(), "UpdateCategory", &err)
	return mw.svc.UpdateCategory(ctx, id, p)
}

func (mw *LoggingMiddleware) DeleteCategory(ctx context.Context, id uint32) (err error) {
	defer mw.log(time.Now(), "DeleteCategory", &err)
	return mw.svc.DeleteCategory(ctx, id)
}

func (mw *LoggingMiddleware) GetSchemas(ctx context.Context) (schemas []*catalog.SchemaName, err error) {
	defer mw.log(time.Now(), "GetSchemas", &err)
	return mw.svc.GetSchemas(ctx)
}

func (mw *LoggingMiddleware) CreateSchema(ctx context.Context, input *catalog.SchemaInput) (schema *catalog.Schema, err error) {
	defer mw.log(time.Now(), "CreateSchema", &err)
	return mw.svc.CreateSchema(ctx, input)
}

func (mw *LoggingMiddleware) UpdateSchema(ctx context.Context, id uint32, patch *catalog.SchemaPatch) (schema *catalog.Schema, err error) {
	defer mw.log(time.Now(), "UpdateSchema", &err)
	return mw.svc.UpdateSchema(ctx, id, patch)
}

func (mw *LoggingMiddleware) GetSchema(ctx context.Context, id uint32) (schema *catalog.Schema, err error) {
	defer mw.log(time.Now(), "GetSchema", &err)
	return mw.svc.GetSchema(ctx, id)
}

func (mw *LoggingMiddleware) DeleteSchema(ctx context.Context, id uint32) (err error) {
	defer mw.log(time.Now(), "DeleteSchema", &err)
	return mw.svc.DeleteSchema(ctx, id)
}

func (mw *LoggingMiddleware) log(beginTime time.Time, method string, err *error) {
	if *err != nil {
		level.Error(mw.logger).Log("method", method, "err", *err, "took", time.Since(beginTime))
	}
}
