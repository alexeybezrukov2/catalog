package event

import (
	"errors"
	"os"
	"sync"
	"testing"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/nats-io/stan.go"
	"github.com/nats-io/stan.go/pb"
	"github.com/stretchr/testify/assert"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

// Environment variables used to connect to a test NATS streaming server.
// These variables must be present in the environment for the NATS-dependent
// tests to run, otherwise they will be skipped.
const (
	testEnvURL = "TEST_NATS_URL"
)

func TestEventQueue_handleMessage(t *testing.T) {
	wantSeq := uint64(101)
	want := mustNewOperation(t, nil)

	q := &Queue{
		logger:         log.NewNopLogger(),
		input:          make(chan catalog.Event),
		sequenceNumber: 100,
	}

	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()

		task := <-q.input

		assert.Equal(t, want, task.Operation())
		assert.Equal(t, wantSeq, task.Sequence())
	}()

	q.handleMessage(&stan.Msg{
		MsgProto: pb.MsgProto{
			Sequence: 101,
			Data:     []byte(`{"Type":"product","Method":"patch","Product":{"id":65435,"properties":[{"id":3453,"name":"price","type":"int","value":8499},{"id":43215,"name":"size","type":"enum_list","value":["XS","S","M","L"]}]}}`),
		},
		Sub: nil,
	})

	wg.Wait()
}

func TestConfig_Validate(t *testing.T) {
	testCases := []struct {
		name    string
		cfg     Config
		wantErr error
	}{
		{
			name:    "normal resp",
			cfg:     mustNewConfig(t, nil),
			wantErr: nil,
		},
		{
			name: "must provide Logger",
			cfg: mustNewConfig(t, func(cfg *Config) {
				cfg.Logger = nil
			}),
			wantErr: errors.New("must provide Logger"),
		},
		{
			name: "must provide URLs",
			cfg: mustNewConfig(t, func(cfg *Config) {
				cfg.URL = ""
			}),
			wantErr: errors.New("must provide URL"),
		},
		{
			name: "must provide ClusterID",
			cfg: mustNewConfig(t, func(cfg *Config) {
				cfg.ClusterID = ""
			}),
			wantErr: errors.New("must provide ClusterID"),
		},
		{
			name: "must provide Subject",
			cfg: mustNewConfig(t, func(cfg *Config) {
				cfg.Subject = ""
			}),
			wantErr: errors.New("must provide Subject"),
		},
		{
			name: "must provide AckWait",
			cfg: mustNewConfig(t, func(cfg *Config) {
				cfg.AckWait = 0
			}),
			wantErr: errors.New("must provide AckWait"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			err := tc.cfg.validate()
			assert.Equal(t, tc.wantErr, err)
		})
	}
}

func getTestNATSURL(t *testing.T) string {
	url := os.Getenv(testEnvURL)
	if url == "" {
		t.Skipf("skipping test: environment variable not found: %s", testEnvURL)
	}

	return url
}

func TestPublishAndSubscribe(t *testing.T) {
	url := getTestNATSURL(t)

	send := func(op *catalog.Operation) {
		q, err := NewQueue(mustNewConfig(t, func(cfg *Config) {
			cfg.URL = url
		}))
		if err != nil {
			t.Fatal(err)
		}

		err = q.Publish(op)
		if err != nil {
			t.Fatal(err)
		}

		if err := q.Close(); err != nil {
			t.Fatal(err)
		}
	}

	receive := func(seq uint64) (*catalog.Operation, uint64) {
		q, err := NewQueue(mustNewConfig(t, func(cfg *Config) {
			cfg.URL = url
			cfg.SequenceNumber = seq
		}))
		if err != nil {
			t.Fatal(err)
		}

		ch, err := q.Subscribe()
		if err != nil {
			t.Fatal(err)
		}

		event := <-ch
		err = event.Ack()
		if err != nil {
			t.Fatal(err)
		}

		if err := q.Close(); err != nil {
			t.Fatal(err)
		}

		return event.Operation(), event.Sequence()
	}

	opPatch := mustNewOperation(t, nil)

	opDelete := mustNewOperation(t, func(op *catalog.Operation) {
		op.Method = catalog.OperationMethodDelete
	})

	opUpsert := mustNewOperation(t, func(op *catalog.Operation) {
		op.Method = catalog.OperationMethodUpsert
	})

	send(opPatch)
	got, seq := receive(0)
	assert.Equal(t, opPatch, got)

	send(opDelete)
	send(opUpsert)

	got, seq = receive(seq + 1)
	assert.Equal(t, opDelete, got)

	got, seq = receive(seq + 1)
	assert.Equal(t, opUpsert, got)

	got, seq = receive(0)
	assert.Equal(t, opPatch, got)
}

func mustNewConfig(_ *testing.T, fn func(cfg *Config)) Config {
	cfg := Config{
		Logger:           log.NewNopLogger(),
		NodeID:           "nodeId",
		ClusterID:        "test-cluster",
		SequenceNumber:   0,
		URL:              "127.0.0.1",
		Subject:          "subj",
		AckWait:          30 * time.Second,
		ReconnectTimeout: time.Second,
	}

	if fn != nil {
		fn(&cfg)
	}
	return cfg
}

func mustNewOperation(_ *testing.T, f func(*catalog.Operation)) *catalog.Operation {
	op := &catalog.Operation{
		Type:   catalog.OperationTypeProduct,
		Method: catalog.OperationMethodPatch,
		Product: &catalog.Product{
			ID: 65435,
			Properties: []*catalog.ProductProperty{
				{
					ID:    3453,
					Name:  "price",
					Type:  catalog.PropertyTypeInt,
					Value: float64(8499),
				},
				{
					ID:    43215,
					Name:  "size",
					Type:  catalog.PropertyTypeEnumList,
					Value: []interface{}{"XS", "S", "M", "L"},
				},
			},
		},
	}

	if f != nil {
		f(op)
	}
	return op
}
