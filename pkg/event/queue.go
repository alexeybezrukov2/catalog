package event

import (
	"encoding/json"
	"errors"
	"fmt"
	"sync"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/go-kit/kit/sd/lb"
	"github.com/nats-io/stan.go"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

var (
	errNoConnection = errors.New("no connection to NATS system")
	errQueueClosed  = errors.New("queue already closed")
)

type event struct {
	op  *catalog.Operation
	seq uint64
	ack func() error
}

func (e *event) Sequence() uint64 {
	return e.seq
}

func (e *event) Operation() *catalog.Operation {
	return e.op
}

func (e *event) Ack() error {
	return e.ack()
}

// Config is an Queue configuration.
type Config struct {
	Logger           log.Logger
	NodeID           string
	ClusterID        string
	SequenceNumber   uint64
	URL              string
	Subject          string
	AckWait          time.Duration
	ReconnectTimeout time.Duration
}

func (cfg Config) validate() error {
	if cfg.Logger == nil {
		return errors.New("must provide Logger")
	}
	if cfg.NodeID == "" {
		return errors.New("must provide NodeID")
	}
	if cfg.URL == "" {
		return errors.New("must provide URL")
	}
	if cfg.ClusterID == "" {
		return errors.New("must provide ClusterID")
	}
	if cfg.Subject == "" {
		return errors.New("must provide Subject")
	}
	if cfg.AckWait <= 0 {
		return errors.New("must provide AckWait")
	}
	if cfg.ReconnectTimeout <= 0 {
		return errors.New("must provide ReconnectTimeout")
	}
	return nil
}

// Queue is a NATS-based event queue. It allows to subscribe to events or publish them.
type Queue struct {
	logger           log.Logger
	nodeID           string
	url              string
	ackWait          time.Duration
	reconnectTimeout time.Duration
	clusterID        string
	subject          string

	mu             sync.RWMutex
	conn           stan.Conn
	stanSub        stan.Subscription
	input          chan catalog.Event
	output         chan catalog.Event
	sequenceNumber uint64
	closed         bool

	wg     sync.WaitGroup
	doneCh chan struct{}
}

// NewQueue creates a new Queue.
func NewQueue(cfg Config) (*Queue, error) {
	err := cfg.validate()
	if err != nil {
		return nil, err
	}

	q := &Queue{
		logger:           cfg.Logger,
		nodeID:           cfg.NodeID,
		url:              cfg.URL,
		ackWait:          cfg.AckWait,
		reconnectTimeout: cfg.ReconnectTimeout,
		clusterID:        cfg.ClusterID,
		subject:          cfg.Subject,
		mu:               sync.RWMutex{},
		conn:             nil,
		stanSub:          nil,
		input:            make(chan catalog.Event),
		output:           make(chan catalog.Event),
		sequenceNumber:   cfg.SequenceNumber,
		closed:           false,
		wg:               sync.WaitGroup{},
		doneCh:           make(chan struct{}),
	}

	err = q.connect()
	if err != nil {
		level.Error(q.logger).Log("msg", "failed to establish NATS connection", "err", err)
		q.dialBackground()
	}

	q.handleEvents()

	return q, nil
}

func (q *Queue) dialBackground() {
	q.wg.Add(1)
	go func() {
		tc := time.NewTicker(q.reconnectTimeout)
		defer func() {
			tc.Stop()
			q.wg.Done()
		}()

		for {
			select {
			case <-tc.C:
				err := q.connect()
				if err != nil {
					level.Error(q.logger).Log("err", err, "retry", q.reconnectTimeout)
					continue
				}
				return

			case <-q.doneCh:
				return
			}
		}
	}()
}

func (q *Queue) connect() error {
	q.mu.Lock()
	defer q.mu.Unlock()

	stanOpts := []stan.Option{
		stan.SetConnectionLostHandler(q.recoverConn),
		stan.NatsURL(q.url),
	}

	conn, err := stan.Connect(q.clusterID, q.nodeID, stanOpts...)
	if err != nil {
		return err
	}

	level.Info(q.logger).Log("msg", "established NATS Streaming connection")

	subsOpts := []stan.SubscriptionOption{
		stan.MaxInflight(1),
		stan.AckWait(q.ackWait),
		stan.SetManualAckMode(),
	}

	if q.sequenceNumber > 0 {
		subsOpts = append(subsOpts, stan.StartAtSequence(q.sequenceNumber))
	} else {
		subsOpts = append(subsOpts, stan.DeliverAllAvailable())
	}

	stanSub, err := conn.QueueSubscribe(q.subject, q.nodeID, q.handleMessage, subsOpts...)
	if err != nil {
		_ = conn.Close()
		return fmt.Errorf("failed to subscribe to queue: %w", err)
	}

	q.conn = conn
	q.stanSub = stanSub

	return nil
}

func (q *Queue) recoverConn(_ stan.Conn, reason error) {
	level.Error(q.logger).Log("msg", "NATS connection lost", "err", reason)
	q.dialBackground()
}

func (q *Queue) getConn() (stan.Conn, error) {
	q.mu.RLock()
	defer q.mu.RUnlock()
	if q.conn == nil {
		return nil, errNoConnection
	}
	return q.conn, nil
}

// Subscribe returns channel with replication events.
func (q *Queue) Subscribe() (<-chan catalog.Event, error) {
	q.mu.Lock()
	defer q.mu.Unlock()

	if q.closed {
		return nil, errQueueClosed
	}

	return q.output, nil
}

func (q *Queue) handleMessage(msg *stan.Msg) {
	op := &catalog.Operation{}
	err := json.Unmarshal(msg.Data, op)
	if err != nil {
		level.Error(q.logger).Log("msg", "failed to decode data", "err", err)
		return
	}

	q.mu.Lock()
	q.sequenceNumber = msg.Sequence
	q.mu.Unlock()

	task := &event{
		op:  op,
		seq: msg.Sequence,
		ack: msg.Ack,
	}

	select {
	case q.input <- task:
	case <-q.doneCh:
	}
}

// Publish sends operation in replication queue.
func (q *Queue) Publish(op *catalog.Operation) error {
	conn, err := q.getConn()
	if err != nil {
		return err
	}

	data, err := json.Marshal(op)
	if err != nil {
		return fmt.Errorf("failed to encode data: %w", err)
	}

	return conn.Publish(q.subject, data)
}

func (q *Queue) handleEvents() {
	q.wg.Add(1)
	go func() {
		defer q.wg.Done()
		for {
			var event catalog.Event

			select {
			case event = <-q.input:
			case <-q.doneCh:
				return
			}

			select {
			case q.output <- event:
			case <-q.doneCh:
				return
			}
		}
	}()
}

// Close closes the event queue.
func (q *Queue) Close() error {
	q.mu.Lock()
	defer q.mu.Unlock()

	if q.closed {
		return nil
	}

	close(q.doneCh)
	q.wg.Wait()

	close(q.output)

	finalErr := lb.RetryError{}

	if q.conn != nil {
		err := q.conn.Close()
		if err != nil {
			err = fmt.Errorf("failed to close connection: %w", err)
			finalErr.RawErrors = append(finalErr.RawErrors, err)
		}
	}

	q.closed = true

	if len(finalErr.RawErrors) > 0 {
		err := errors.New("unable to close queue")
		finalErr.RawErrors = append(finalErr.RawErrors, err)
		finalErr.Final = err
		return finalErr
	}

	return nil
}
