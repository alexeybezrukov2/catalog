package storage

import (
	"context"
	"errors"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

const (
	connectTimeout     = 30 * time.Second
	disconnectTimeout  = 30 * time.Second
	initIndexesTimeout = 30 * time.Second

	collectionCounters         = "counters"
	collectionCategories       = "categories"
	collectionProperties       = "properties"
	collectionPropertyVariants = "property_variants"
	collectionProducts         = "products"
	collectionSchemas          = "schemas"
)

// Config is a storage configuration.
type Config struct {
	URL      string // Mongo URL.
	Database string // Mongo database name.
}

func (cfg *Config) validate() error {
	if cfg.URL == "" {
		return errors.New("empty URL")
	}
	if cfg.Database == "" {
		return errors.New("empty Database")
	}
	return nil
}

// Storage is a mongodb-based persistent catalog storage.
type Storage struct {
	client           *mongo.Client
	counters         *mongo.Collection
	categories       *mongo.Collection
	properties       *mongo.Collection
	propertyVariants *mongo.Collection
	products         *mongo.Collection
	schemas          *mongo.Collection
}

// New creates a new storage.
func New(cfg Config) (*Storage, error) {
	if err := cfg.validate(); err != nil {
		return nil, fmt.Errorf("invalid configuration: %w", err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), connectTimeout)
	defer cancel()

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(cfg.URL))
	if err != nil {
		return nil, fmt.Errorf("failed to connect to mongo: %w", err)
	}

	s := &Storage{
		client:           client,
		counters:         client.Database(cfg.Database).Collection(collectionCounters),
		categories:       client.Database(cfg.Database).Collection(collectionCategories),
		properties:       client.Database(cfg.Database).Collection(collectionProperties),
		propertyVariants: client.Database(cfg.Database).Collection(collectionPropertyVariants),
		products:         client.Database(cfg.Database).Collection(collectionProducts),
		schemas:          client.Database(cfg.Database).Collection(collectionSchemas),
	}

	if err := s.initIndexes(); err != nil {
		_ = s.Close()
		return nil, fmt.Errorf("failed to init indexes: %w", err)
	}

	return s, nil
}

func (s *Storage) initIndexes() error {
	ctx, cancel := context.WithTimeout(context.Background(), initIndexesTimeout)
	defer cancel()

	_, err := s.properties.Indexes().CreateOne(ctx, mongo.IndexModel{
		Keys:    bson.M{"key": 1},
		Options: options.Index().SetUnique(true),
	})
	if err != nil {
		return fmt.Errorf("failed to create index for properties: %w", err)
	}

	_, err = s.propertyVariants.Indexes().CreateOne(ctx, mongo.IndexModel{
		Keys: bson.M{"property_id": 1},
	})
	if err != nil {
		return fmt.Errorf("failed to create index for property variants: %w", err)
	}

	_, err = s.products.Indexes().CreateMany(ctx, []mongo.IndexModel{
		{
			Keys: bson.M{"supplier_id": 1},
		},
		{
			Keys: bson.M{"schema_id": 1},
		},
	})
	if err != nil {
		return fmt.Errorf("failed to create indexes for products: %w", err)
	}

	return nil
}

func (s *Storage) nextID(ctx context.Context, collection string) (uint32, error) {
	res := s.counters.FindOneAndUpdate(
		ctx,
		bson.M{"_id": collection},
		bson.M{"$inc": bson.M{"value": 1}},
		options.FindOneAndUpdate().SetUpsert(true),
		options.FindOneAndUpdate().SetReturnDocument(options.After),
	)
	if err := res.Err(); err != nil {
		return 0, fmt.Errorf("failed to find one and update: %w", err)
	}

	var counter struct {
		ID    string `bson:"_id"`
		Value uint32 `bson:"value"`
	}
	if err := res.Decode(&counter); err != nil {
		return 0, fmt.Errorf("failed to decode counter: %w", err)
	}

	return counter.Value, nil
}

// InsertCategory inserts a new category into storage (ID field is auto-generated).
func (s *Storage) InsertCategory(ctx context.Context, c *catalog.Category) error {
	id, err := s.nextID(ctx, collectionCategories)
	if err != nil {
		return fmt.Errorf("failed to get next id: %w", err)
	}
	c.ID = id
	if _, err := s.categories.InsertOne(ctx, c); err != nil {
		return fmt.Errorf("failed to insert one: %w", err)
	}
	return nil
}

// UpdateCategory updates the given category in storage.
func (s *Storage) UpdateCategory(ctx context.Context, c *catalog.Category) error {
	res, err := s.categories.ReplaceOne(ctx, bson.M{"_id": c.ID}, c)
	if err != nil {
		return fmt.Errorf("failed to replace one: %w", err)
	}
	if res.MatchedCount == 0 {
		return catalog.ErrNotFoundInStorage
	}
	return nil
}

// GetCategory gets a category from storage by ID.
func (s *Storage) GetCategory(ctx context.Context, id uint32) (*catalog.Category, error) {
	var category catalog.Category
	if err := s.categories.FindOne(ctx, bson.M{"_id": id}).Decode(&category); err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, catalog.ErrNotFoundInStorage
		}
		return nil, fmt.Errorf("failed to find one: %w", err)
	}
	return &category, nil
}

// ForeachCategory iterates the stored categories calling the provided function fn for each of them.
// It stops with an error if either fn returns an error or ctx is canceled.
func (s *Storage) ForeachCategory(ctx context.Context, fn func(*catalog.Category) error) error {
	cursor, err := s.categories.Find(ctx, bson.D{})
	if err != nil {
		return fmt.Errorf("failed to find: %w", err)
	}
	defer cursor.Close(ctx)

	for cursor.Next(ctx) {
		var category *catalog.Category
		if err := cursor.Decode(&category); err != nil {
			return fmt.Errorf("failed to decode: %w", err)
		}
		if err := fn(category); err != nil {
			return err
		}
	}
	if err := cursor.Err(); err != nil {
		return fmt.Errorf("failed to iterate cursor: %w", err)
	}

	return nil
}

// DeleteCategory deletes a category from storage.
func (s *Storage) DeleteCategory(ctx context.Context, id uint32) error {
	res, err := s.categories.DeleteOne(ctx, bson.M{"_id": id})
	if err != nil {
		return fmt.Errorf("failed to delete one: %w", err)
	}
	if res.DeletedCount == 0 {
		return catalog.ErrNotFoundInStorage
	}
	return nil
}

// InsertProperty inserts a new property into storage (ID field is auto-generated).
func (s *Storage) InsertProperty(ctx context.Context, p *catalog.Property) error {
	id, err := s.nextID(ctx, collectionProperties)
	if err != nil {
		return fmt.Errorf("failed to get next id: %w", err)
	}
	p.ID = id
	if _, err := s.properties.InsertOne(ctx, p); err != nil {
		if isDuplicateKeyError(err) {
			return catalog.ErrDuplicateKeyInStorage
		}
		return fmt.Errorf("failed to insert one: %w", err)
	}
	return nil
}

// UpdateProperty updates the given property in storage.
func (s *Storage) UpdateProperty(ctx context.Context, p *catalog.Property) error {
	res, err := s.properties.ReplaceOne(ctx, bson.M{"_id": p.ID}, p)
	if err != nil {
		return fmt.Errorf("failed to replace one: %w", err)
	}
	if res.MatchedCount == 0 {
		return catalog.ErrNotFoundInStorage
	}
	return nil
}

// GetProperty gets a property from storage by ID.
func (s *Storage) GetProperty(ctx context.Context, id uint32) (*catalog.Property, error) {
	var property catalog.Property
	if err := s.properties.FindOne(ctx, bson.M{"_id": id}).Decode(&property); err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, catalog.ErrNotFoundInStorage
		}
		return nil, fmt.Errorf("failed to find one: %w", err)
	}
	return &property, nil
}

// ForeachProperty iterates the stored properties calling the provided function fn for each of them.
// It stops with an error if either fn returns an error or ctx is canceled.
func (s *Storage) ForeachProperty(ctx context.Context, fn func(*catalog.Property) error) error {
	cursor, err := s.properties.Find(ctx, bson.D{})
	if err != nil {
		return fmt.Errorf("failed to find: %w", err)
	}
	defer cursor.Close(ctx)

	for cursor.Next(ctx) {
		var property *catalog.Property
		if err := cursor.Decode(&property); err != nil {
			return fmt.Errorf("failed to decode: %w", err)
		}
		if err := fn(property); err != nil {
			return err
		}
	}
	if err := cursor.Err(); err != nil {
		return fmt.Errorf("failed to iterate cursor: %w", err)
	}

	return nil
}

// DeleteProperty deletes a property from storage.
func (s *Storage) DeleteProperty(ctx context.Context, id uint32) error {
	res, err := s.properties.DeleteOne(ctx, bson.M{"_id": id})
	if err != nil {
		return fmt.Errorf("failed to delete one: %w", err)
	}
	if res.DeletedCount == 0 {
		return catalog.ErrNotFoundInStorage
	}
	return nil
}

// InsertPropertyVariant inserts a new property variant into storage (ID field is auto-generated).
func (s *Storage) InsertPropertyVariant(ctx context.Context, pv *catalog.PropertyVariant) error {
	id, err := s.nextID(ctx, collectionPropertyVariants)
	if err != nil {
		return fmt.Errorf("failed to get next id: %w", err)
	}
	pv.ID = id
	if _, err := s.propertyVariants.InsertOne(ctx, pv); err != nil {
		return fmt.Errorf("failed to insert one: %w", err)
	}
	return nil
}

// UpdatePropertyVariant updates the given property variant in storage.
func (s *Storage) UpdatePropertyVariant(ctx context.Context, pv *catalog.PropertyVariant) error {
	res, err := s.propertyVariants.ReplaceOne(ctx, bson.M{"_id": pv.ID}, pv)
	if err != nil {
		return fmt.Errorf("failed to replace one: %w", err)
	}
	if res.MatchedCount == 0 {
		return catalog.ErrNotFoundInStorage
	}
	return nil
}

// GetPropertyVariant gets a property variant from storage by ID.
func (s *Storage) GetPropertyVariant(ctx context.Context, id uint32) (*catalog.PropertyVariant, error) {
	var propertyVariant catalog.PropertyVariant
	if err := s.propertyVariants.FindOne(ctx, bson.M{"_id": id}).Decode(&propertyVariant); err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, catalog.ErrNotFoundInStorage
		}
		return nil, fmt.Errorf("failed to find one: %w", err)
	}
	return &propertyVariant, nil
}

// GetPropertyVariantsByProperty gets all property variants from storage by property ID.
func (s *Storage) GetPropertyVariantsByProperty(ctx context.Context, propertyID uint32) ([]*catalog.PropertyVariant, error) {
	cursor, err := s.propertyVariants.Find(ctx, bson.M{"property_id": propertyID})
	if err != nil {
		return nil, fmt.Errorf("failed to find: %w", err)
	}

	var propertyVariants []*catalog.PropertyVariant
	if err := cursor.All(ctx, &propertyVariants); err != nil {
		return nil, fmt.Errorf("failed to get all documents: %w", err)
	}

	return propertyVariants, nil
}

// ForeachPropertyVariant iterates the stored property variants calling the provided function fn for each of them.
// It stops with an error if either fn returns an error or ctx is canceled.
func (s *Storage) ForeachPropertyVariant(ctx context.Context, fn func(*catalog.PropertyVariant) error) error {
	cursor, err := s.propertyVariants.Find(ctx, bson.D{})
	if err != nil {
		return fmt.Errorf("failed to find: %w", err)
	}
	defer cursor.Close(ctx)

	for cursor.Next(ctx) {
		var propertyVariant *catalog.PropertyVariant
		if err := cursor.Decode(&propertyVariant); err != nil {
			return fmt.Errorf("failed to decode: %w", err)
		}
		if err := fn(propertyVariant); err != nil {
			return err
		}
	}
	if err := cursor.Err(); err != nil {
		return fmt.Errorf("failed to iterate cursor: %w", err)
	}

	return nil
}

// DeletePropertyVariant deletes a property variant from storage.
func (s *Storage) DeletePropertyVariant(ctx context.Context, id uint32) error {
	res, err := s.propertyVariants.DeleteOne(ctx, bson.M{"_id": id})
	if err != nil {
		return fmt.Errorf("failed to delete one: %w", err)
	}
	if res.DeletedCount == 0 {
		return catalog.ErrNotFoundInStorage
	}
	return nil
}

// InsertProduct inserts a new product into storage (ID field is auto-generated).
func (s *Storage) InsertProduct(ctx context.Context, p *catalog.Product) error {
	id, err := s.nextID(ctx, collectionProducts)
	if err != nil {
		return fmt.Errorf("failed to get next id: %w", err)
	}
	p.ID = id
	if _, err := s.products.InsertOne(ctx, p); err != nil {
		return fmt.Errorf("failed to insert one: %w", err)
	}
	return nil
}

// UpdateProduct updates the given product in storage.
func (s *Storage) UpdateProduct(ctx context.Context, p *catalog.Product) error {
	res, err := s.products.ReplaceOne(ctx, bson.M{"_id": p.ID}, p)
	if err != nil {
		return fmt.Errorf("failed to replace one: %w", err)
	}
	if res.MatchedCount == 0 {
		return catalog.ErrNotFoundInStorage
	}
	return nil
}

// GetProduct gets a product from storage by ID.
func (s *Storage) GetProduct(ctx context.Context, id uint32) (*catalog.Product, error) {
	var product catalog.Product
	if err := s.products.FindOne(ctx, bson.M{"_id": id}).Decode(&product); err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, catalog.ErrNotFoundInStorage
		}
		return nil, fmt.Errorf("failed to find one: %w", err)
	}
	return &product, nil
}

// GetProducts gets products from storage by given query.
func (s *Storage) GetProducts(ctx context.Context, query *catalog.StorageProductQuery) (*catalog.StorageProductQueryResult, error) {
	filter := bson.M{}
	if query.SupplierID != 0 {
		filter["supplier_id"] = query.SupplierID
	}
	if query.SchemaID != 0 {
		filter["schema_id"] = query.SchemaID
	}

	opts := options.Find().SetSort(bson.M{"_id": 1})
	if query.Limit > 0 {
		opts.SetLimit(int64(query.Limit))
	}
	if query.Offset > 0 {
		opts.SetSkip(int64(query.Offset))
	}

	cursor, err := s.products.Find(ctx, filter, opts)
	if err != nil {
		return nil, fmt.Errorf("failed to find: %w", err)
	}

	var products []*catalog.Product
	if err := cursor.All(ctx, &products); err != nil {
		return nil, fmt.Errorf("failed to get all documents: %w", err)
	}

	count, err := s.products.CountDocuments(ctx, filter)
	if err != nil {
		return nil, fmt.Errorf("failed to count documents: %w", err)
	}

	return &catalog.StorageProductQueryResult{Products: products, Total: int(count)}, nil
}

// ForeachProduct iterates the stored products calling the provided function fn for each of them.
// It stops with an error if either fn returns an error or ctx is canceled.
func (s *Storage) ForeachProduct(ctx context.Context, fn func(*catalog.Product) error) error {
	cursor, err := s.products.Find(ctx, bson.D{})
	if err != nil {
		return fmt.Errorf("failed to find: %w", err)
	}
	defer cursor.Close(ctx)

	for cursor.Next(ctx) {
		var product *catalog.Product
		if err := cursor.Decode(&product); err != nil {
			return fmt.Errorf("failed to decode: %w", err)
		}
		if err := fn(product); err != nil {
			return err
		}
	}
	if err := cursor.Err(); err != nil {
		return fmt.Errorf("failed to iterate cursor: %w", err)
	}

	return nil
}

// DeleteProduct deletes a product from storage.
func (s *Storage) DeleteProduct(ctx context.Context, id uint32) error {
	res, err := s.products.DeleteOne(ctx, bson.M{"_id": id})
	if err != nil {
		return fmt.Errorf("failed to delete one: %w", err)
	}
	if res.DeletedCount == 0 {
		return catalog.ErrNotFoundInStorage
	}
	return nil
}

// InsertSchema inserts a new schema into storage (ID field is auto-generated).
func (s *Storage) InsertSchema(ctx context.Context, schema *catalog.Schema) error {
	id, err := s.nextID(ctx, collectionSchemas)
	if err != nil {
		return fmt.Errorf("failed to get next id: %w", err)
	}
	schema.ID = id
	if _, err := s.schemas.InsertOne(ctx, schema); err != nil {
		return fmt.Errorf("failed to insert one: %w", err)
	}
	return nil
}

// UpdateSchema updates the given schema in storage.
func (s *Storage) UpdateSchema(ctx context.Context, schema *catalog.Schema) error {
	res, err := s.schemas.ReplaceOne(ctx, bson.M{"_id": schema.ID}, schema)
	if err != nil {
		return fmt.Errorf("failed to replace one: %w", err)
	}
	if res.MatchedCount == 0 {
		return catalog.ErrNotFoundInStorage
	}
	return nil
}

// GetSchema gets a schema from storage by ID.
func (s *Storage) GetSchema(ctx context.Context, id uint32) (*catalog.Schema, error) {
	var schema catalog.Schema
	if err := s.schemas.FindOne(ctx, bson.M{"_id": id}).Decode(&schema); err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, catalog.ErrNotFoundInStorage
		}
		return nil, fmt.Errorf("failed to find one: %w", err)
	}
	return &schema, nil
}

// ForeachSchema iterates the stored schemas calling the provided function fn for each of them.
// It stops with an error if either fn returns an error or ctx is canceled.
func (s *Storage) ForeachSchema(ctx context.Context, fn func(*catalog.Schema) error) error {
	cursor, err := s.schemas.Find(ctx, bson.D{})
	if err != nil {
		return fmt.Errorf("failed to find: %w", err)
	}
	defer cursor.Close(ctx)

	for cursor.Next(ctx) {
		var schema *catalog.Schema
		if err := cursor.Decode(&schema); err != nil {
			return fmt.Errorf("failed to decode: %w", err)
		}
		if err := fn(schema); err != nil {
			return err
		}
	}
	if err := cursor.Err(); err != nil {
		return fmt.Errorf("failed to iterate cursor: %w", err)
	}

	return nil
}

// DeleteSchema deletes a schema from storage.
func (s *Storage) DeleteSchema(ctx context.Context, id uint32) error {
	res, err := s.schemas.DeleteOne(ctx, bson.M{"_id": id})
	if err != nil {
		return fmt.Errorf("failed to delete one: %w", err)
	}
	if res.DeletedCount == 0 {
		return catalog.ErrNotFoundInStorage
	}
	return nil
}

// Close closes the storage.
func (s *Storage) Close() error {
	ctx, cancel := context.WithTimeout(context.Background(), disconnectTimeout)
	defer cancel()

	return s.client.Disconnect(ctx)
}

func isDuplicateKeyError(err error) bool {
	var e mongo.WriteException
	if errors.As(err, &e) {
		for _, we := range e.WriteErrors {
			if we.Code == 11000 {
				return true
			}
		}
	}
	return false
}
