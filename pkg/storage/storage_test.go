package storage

import (
	"context"
	"os"
	"strconv"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

// Environment variables used to connect to a test mongo database.
// These variables must be present in the environment for the mongo-dependent
// tests to run, otherwise they will be skipped.
const (
	testEnvURL      = "TEST_MONGO_URL"
	testEnvDatabase = "TEST_MONGO_DATABASE"
)

func getTestStorage(tb testing.TB) (*Storage, func()) {
	url := os.Getenv(testEnvURL)
	if url == "" {
		tb.Skipf("skipping test: environment variable not found: %s", testEnvURL)
	}
	database := os.Getenv(testEnvDatabase)
	if url == "" {
		tb.Skipf("skipping test: environment variable not found: %s", testEnvDatabase)
	}

	// Drop collections.
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(url))
	if err != nil {
		tb.Fatalf("failed to connect to mongo: %s", err)
	}
	collections := []string{
		collectionCounters,
		collectionCategories,
		collectionProperties,
		collectionPropertyVariants,
		collectionProducts,
	}
	for _, c := range collections {
		if err := client.Database(database).Collection(c).Drop(ctx); err != nil {
			tb.Fatalf("failed to drop collection %s: %s", c, err)
		}
	}
	if err := client.Disconnect(ctx); err != nil {
		tb.Fatalf("failed to disconnect from mongo: %s", err)
	}

	// Create a test storage.
	s, err := New(Config{
		URL:      url,
		Database: database,
	})
	if err != nil {
		tb.Fatalf("failed to create test storage: %s", err)
	}

	teardown := func() {
		if err := s.Close(); err != nil {
			tb.Fatalf("failed to close test storage: %s", err)
		}
	}
	return s, teardown
}

func TestNextID(t *testing.T) {
	s, teardown := getTestStorage(t)
	defer teardown()

	const (
		numGoroutines      = 100
		numIDsPerGoroutine = 100
	)

	ids := make(chan uint32, numGoroutines*numIDsPerGoroutine)

	var wg sync.WaitGroup
	for g := 0; g < numGoroutines; g++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for i := 0; i < numIDsPerGoroutine; i++ {
				id, err := s.nextID(context.Background(), "test_collection")
				if err != nil {
					t.Error(err)
					return
				}
				ids <- id
			}
		}()
	}
	wg.Wait()
	close(ids)

	idCounts := make(map[uint32]int)
	for id := range ids {
		idCounts[id]++
	}

	for id := uint32(1); id <= numGoroutines*numIDsPerGoroutine; id++ {
		count := idCounts[id]
		if count != 1 {
			t.Fatalf("got count %d want 1 for id=%d", count, id)
		}
	}
}

func TestCategories(t *testing.T) {
	s, teardown := getTestStorage(t)
	defer teardown()

	categories := make(map[uint32]*catalog.Category)
	var upd, del *catalog.Category

	for i := 0; i < 10; i++ {
		c := mustNewCategory(func(c *catalog.Category) { c.ID = 0 })

		if err := s.InsertCategory(context.Background(), c); err != nil {
			t.Fatal(err)
		}
		if c.ID == 0 {
			t.Fatal("category id is not set after insert")
		}
		if _, ok := categories[c.ID]; ok {
			t.Fatalf("category id is not unique: %d", c.ID)
		}

		categories[c.ID] = c

		switch {
		case upd == nil:
			upd = c
		case del == nil:
			del = c
		}
	}

	upd.Name += "updated"
	if err := s.UpdateCategory(context.Background(), upd); err != nil {
		t.Fatal(err)
	}

	delete(categories, del.ID)
	if err := s.DeleteCategory(context.Background(), del.ID); err != nil {
		t.Fatal(err)
	}

	for _, c := range categories {
		gotCategory, err := s.GetCategory(context.Background(), c.ID)
		if err != nil {
			t.Fatal(err)
		}
		assert.Equal(t, c, gotCategory)
	}

	gotCategories := make(map[uint32]*catalog.Category)
	err := s.ForeachCategory(context.Background(), func(c *catalog.Category) error {
		gotCategories[c.ID] = c
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, categories, gotCategories)
}

func TestProperties(t *testing.T) {
	s, teardown := getTestStorage(t)
	defer teardown()

	properties := make(map[uint32]*catalog.Property)
	var upd, del *catalog.Property

	for i := 0; i < 10; i++ {
		p := mustNewProperty(func(p *catalog.Property) {
			p.ID = 0
			p.Key = strconv.Itoa(i)
		})

		if err := s.InsertProperty(context.Background(), p); err != nil {
			t.Fatal(err)
		}
		if p.ID == 0 {
			t.Fatal("property id is not set after insert")
		}
		if _, ok := properties[p.ID]; ok {
			t.Fatalf("property id is not unique: %d", p.ID)
		}

		properties[p.ID] = p

		switch {
		case upd == nil:
			upd = p
		case del == nil:
			del = p
		}
	}

	upd.Name += "updated"
	if err := s.UpdateProperty(context.Background(), upd); err != nil {
		t.Fatal(err)
	}

	delete(properties, del.ID)
	if err := s.DeleteProperty(context.Background(), del.ID); err != nil {
		t.Fatal(err)
	}

	for _, p := range properties {
		gotProperty, err := s.GetProperty(context.Background(), p.ID)
		if err != nil {
			t.Fatal(err)
		}
		assert.Equal(t, p, gotProperty)
	}

	gotProperties := make(map[uint32]*catalog.Property)
	err := s.ForeachProperty(context.Background(), func(p *catalog.Property) error {
		gotProperties[p.ID] = p
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, properties, gotProperties)

	p := mustNewProperty(func(p *catalog.Property) {
		p.ID = 0
		p.Key = "duplicate_key_test"
	})
	if err := s.InsertProperty(context.Background(), p); err != nil {
		t.Fatal(err)
	}
	if err := s.InsertProperty(context.Background(), p); err != catalog.ErrDuplicateKeyInStorage {
		t.Fatalf("got error %v want %v", err, catalog.ErrDuplicateKeyInStorage)
	}
}

func TestPropertyVariants(t *testing.T) {
	s, teardown := getTestStorage(t)
	defer teardown()

	propertyVariants := make(map[uint32]*catalog.PropertyVariant)
	var upd, del *catalog.PropertyVariant

	for i := 0; i < 10; i++ {
		pv := mustNewPropertyVariant(func(p *catalog.PropertyVariant) { p.ID = 0 })

		if err := s.InsertPropertyVariant(context.Background(), pv); err != nil {
			t.Fatal(err)
		}
		if pv.ID == 0 {
			t.Fatal("property variant id is not set after insert")
		}
		if _, ok := propertyVariants[pv.ID]; ok {
			t.Fatalf("property variant id is not unique: %d", pv.ID)
		}

		propertyVariants[pv.ID] = pv

		switch {
		case upd == nil:
			upd = pv
		case del == nil:
			del = pv
		}
	}

	upd.Value += "updated"
	if err := s.UpdatePropertyVariant(context.Background(), upd); err != nil {
		t.Fatal(err)
	}

	delete(propertyVariants, del.ID)
	if err := s.DeletePropertyVariant(context.Background(), del.ID); err != nil {
		t.Fatal(err)
	}

	for _, pv := range propertyVariants {
		gotPropertyVariant, err := s.GetPropertyVariant(context.Background(), pv.ID)
		if err != nil {
			t.Fatal(err)
		}
		assert.Equal(t, pv, gotPropertyVariant)
	}

	gotPropertyVariants := make(map[uint32]*catalog.PropertyVariant)
	err := s.ForeachPropertyVariant(context.Background(), func(pv *catalog.PropertyVariant) error {
		gotPropertyVariants[pv.ID] = pv
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, propertyVariants, gotPropertyVariants)
}

func TestGetPropertyVariantsByProperty(t *testing.T) {
	s, teardown := getTestStorage(t)
	defer teardown()

	init := []*catalog.PropertyVariant{
		{PropertyID: 1, Value: "11"},
		{PropertyID: 2, Value: "21"},
		{PropertyID: 2, Value: "22"},
		{PropertyID: 2, Value: "23"},
	}

	byProperty := make(map[uint32][]*catalog.PropertyVariant)

	for _, pv := range init {
		if err := s.InsertPropertyVariant(context.Background(), pv); err != nil {
			t.Fatal(err)
		}
		byProperty[pv.PropertyID] = append(byProperty[pv.PropertyID], pv)
	}

	byProperty[123] = nil

	for propertyID, propertyVariants := range byProperty {
		gotPropertyVariants, err := s.GetPropertyVariantsByProperty(context.Background(), propertyID)
		if err != nil {
			t.Fatal(err)
		}
		assert.Equal(t, propertyVariants, gotPropertyVariants, "propertyID=%d", propertyID)
	}
}

func TestProducts(t *testing.T) {
	s, teardown := getTestStorage(t)
	defer teardown()

	products := make(map[uint32]*catalog.Product)
	var upd, del *catalog.Product

	for i := 0; i < 10; i++ {
		p := mustNewProduct(func(p *catalog.Product) { p.ID = 0 })

		if err := s.InsertProduct(context.Background(), p); err != nil {
			t.Fatal(err)
		}
		if p.ID == 0 {
			t.Fatal("product id is not set after insert")
		}
		if _, ok := products[p.ID]; ok {
			t.Fatalf("product id is not unique: %d", p.ID)
		}

		products[p.ID] = p

		switch {
		case upd == nil:
			upd = p
		case del == nil:
			del = p
		}
	}

	upd.Properties = append(upd.Properties, &catalog.ProductProperty{ID: 12345, Name: "update"})
	if err := s.UpdateProduct(context.Background(), upd); err != nil {
		t.Fatal(err)
	}

	delete(products, del.ID)
	if err := s.DeleteProduct(context.Background(), del.ID); err != nil {
		t.Fatal(err)
	}

	for _, p := range products {
		gotProduct, err := s.GetProduct(context.Background(), p.ID)
		if err != nil {
			t.Fatal(err)
		}
		assert.Equal(t, p, gotProduct)
	}

	gotProducts := make(map[uint32]*catalog.Product)
	err := s.ForeachProduct(context.Background(), func(p *catalog.Product) error {
		gotProducts[p.ID] = p
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, products, gotProducts)
}

func TestGetProducts(t *testing.T) {
	s, teardown := getTestStorage(t)
	defer teardown()

	init := []*catalog.Product{
		0: mustNewProduct(func(p *catalog.Product) { p.ID = 0; p.SupplierID = 1; p.SchemaID = 1 }),
		1: mustNewProduct(func(p *catalog.Product) { p.ID = 0; p.SupplierID = 2; p.SchemaID = 2 }),
		2: mustNewProduct(func(p *catalog.Product) { p.ID = 0; p.SupplierID = 2; p.SchemaID = 2 }),
		3: mustNewProduct(func(p *catalog.Product) { p.ID = 0; p.SupplierID = 2; p.SchemaID = 3 }),
		4: mustNewProduct(func(p *catalog.Product) { p.ID = 0; p.SupplierID = 2; p.SchemaID = 3 }),
		5: mustNewProduct(func(p *catalog.Product) { p.ID = 0; p.SupplierID = 1; p.SchemaID = 3 }),
	}

	for _, p := range init {
		if err := s.InsertProduct(context.Background(), p); err != nil {
			t.Fatal(err)
		}
	}

	testCases := []struct {
		name       string
		query      *catalog.StorageProductQuery
		wantResult *catalog.StorageProductQueryResult
	}{
		{
			name: "supplier id",
			query: &catalog.StorageProductQuery{
				SupplierID: 1,
			},
			wantResult: &catalog.StorageProductQueryResult{
				Products: []*catalog.Product{init[0], init[5]},
				Total:    2,
			},
		},
		{
			name: "schema id",
			query: &catalog.StorageProductQuery{
				SchemaID: 3,
			},
			wantResult: &catalog.StorageProductQueryResult{
				Products: []*catalog.Product{init[3], init[4], init[5]},
				Total:    3,
			},
		},
		{
			name: "supplier id, schema id",
			query: &catalog.StorageProductQuery{
				SupplierID: 1,
				SchemaID:   3,
			},
			wantResult: &catalog.StorageProductQueryResult{
				Products: []*catalog.Product{init[5]},
				Total:    1,
			},
		},
		{
			name: "supplier id, limit, offset",
			query: &catalog.StorageProductQuery{
				SupplierID: 2,
				Limit:      2,
				Offset:     1,
			},
			wantResult: &catalog.StorageProductQueryResult{
				Products: []*catalog.Product{init[2], init[3]},
				Total:    4,
			},
		},
		{
			name: "non-existing supplier id",
			query: &catalog.StorageProductQuery{
				SupplierID: 123,
			},
			wantResult: &catalog.StorageProductQueryResult{
				Products: nil,
				Total:    0,
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			gotResult, err := s.GetProducts(context.Background(), tc.query)
			if err != nil {
				t.Fatal(err)
			}
			assert.Equal(t, tc.wantResult, gotResult)
		})
	}
}

func TestSchemas(t *testing.T) {
	s, teardown := getTestStorage(t)
	defer teardown()

	schemas := make(map[uint32]*catalog.Schema)
	var upd, del *catalog.Schema

	for i := 0; i < 10; i++ {
		p := mustNewSchema(func(s *catalog.Schema) { s.ID = 0 })

		if err := s.InsertSchema(context.Background(), p); err != nil {
			t.Fatal(err)
		}
		if p.ID == 0 {
			t.Fatal("schema id is not set after insert")
		}
		if _, ok := schemas[p.ID]; ok {
			t.Fatalf("schema id is not unique: %d", p.ID)
		}

		schemas[p.ID] = p

		switch {
		case upd == nil:
			upd = p
		case del == nil:
			del = p
		}
	}

	upd.Name += "update"
	upd.Properties = append(upd.Properties, &catalog.SchemaProperty{ID: 12345, Name: "update"})
	if err := s.UpdateSchema(context.Background(), upd); err != nil {
		t.Fatal(err)
	}

	delete(schemas, del.ID)
	if err := s.DeleteSchema(context.Background(), del.ID); err != nil {
		t.Fatal(err)
	}

	for _, p := range schemas {
		gotSchema, err := s.GetSchema(context.Background(), p.ID)
		if err != nil {
			t.Fatal(err)
		}
		assert.Equal(t, p, gotSchema)
	}

	gotSchemas := make(map[uint32]*catalog.Schema)
	err := s.ForeachSchema(context.Background(), func(p *catalog.Schema) error {
		gotSchemas[p.ID] = p
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, schemas, gotSchemas)
}

func mustNewCategory(fn func(c *catalog.Category)) *catalog.Category {
	c := &catalog.Category{
		ID:       1,
		ParentID: 2,
		Name:     "Sport",
		Order:    3,
		Filters: []*catalog.IndexFilter{
			{
				Type:         catalog.IndexTypeEnum,
				Key:          11,
				EnumVariants: []uint32{22, 33},
			},
		},
		FacetKeys: []uint32{4, 5, 6},
	}
	if fn != nil {
		fn(c)
	}
	return c
}

func mustNewProperty(fn func(p *catalog.Property)) *catalog.Property {
	p := &catalog.Property{
		ID:      1,
		Key:     "key",
		Name:    "name",
		Type:    catalog.PropertyTypeEnum,
		Indexed: true,
	}
	if fn != nil {
		fn(p)
	}
	return p
}

func mustNewPropertyVariant(fn func(pv *catalog.PropertyVariant)) *catalog.PropertyVariant {
	pv := &catalog.PropertyVariant{
		ID:         1,
		PropertyID: 2,
		Value:      "value",
	}
	if fn != nil {
		fn(pv)
	}
	return pv
}

func mustNewProduct(fn func(p *catalog.Product)) *catalog.Product {
	p := &catalog.Product{
		ID: 1,
		Properties: []*catalog.ProductProperty{
			{
				ID:       1,
				Key:      "key",
				Name:     "name",
				Type:     catalog.PropertyTypeEnum,
				Value:    "v1",
				ValueIDs: []uint32{1},
				Indexed:  true,
			},
		},
	}
	if fn != nil {
		fn(p)
	}
	return p
}

func mustNewSchema(fn func(p *catalog.Schema)) *catalog.Schema {
	s := &catalog.Schema{
		ID:   1,
		Name: "Name",
		Properties: []*catalog.SchemaProperty{
			{
				ID:       1,
				Key:      "key1",
				Name:     "name1",
				Type:     catalog.PropertyTypeEnum,
				Indexed:  true,
				Required: true,
			},
			{
				ID:       2,
				Key:      "key2",
				Name:     "name2",
				Type:     catalog.PropertyTypeString,
				Indexed:  false,
				Required: false,
			},
		},
	}
	if fn != nil {
		fn(s)
	}
	return s
}
