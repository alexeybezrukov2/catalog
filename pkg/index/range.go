package index

import (
	"math"
	"math/bits"

	"github.com/RoaringBitmap/roaring"
)

// rangeIndex implements a bit-sliced index (BSI) for uint64 values.
// It provides the ability to evaluate range predicates as well as
// apply the n-largest and n-smallest functions to result sets.
//
// See also:
//  - Denis Rinfret, Patrick E. O'Neil, Elizabeth O'Neil, "Bit-Sliced Index Arithmetic".
//  - Patrick O'Neil, Dallan Quass, "Improved query performance with variant indexes".
//
type rangeIndex struct {
	all  *roaring.Bitmap   // Contains all indexed document ids.
	bits []*roaring.Bitmap // Bit-slices containing documents ids for each bit position.
}

func newRangeIndex() *rangeIndex {
	return &rangeIndex{
		all: roaring.New(),
	}
}

// put adds the given document id and the corresponding value to the index.
func (ri *rangeIndex) put(id uint32, val uint64) {
	ri.all.Add(id)
	for i := 0; i < bits.Len64(val); i++ {
		if len(ri.bits)-1 < i {
			ri.bits = append(ri.bits, roaring.New())
		}
		if (val>>i)&1 == 1 {
			ri.bits[i].Add(id)
		}
	}
}

// delete removes the given document id from index.
func (ri *rangeIndex) delete(id uint32) {
	ri.all.Remove(id)
	for _, b := range ri.bits {
		b.Remove(id)
	}
}

// getGreaterEqual returns a bitmap containing all the document ids with values greater than or equal to val.
// The returned bitmap is safe to modify.
func (ri *rangeIndex) getGreaterEqual(val uint64) *roaring.Bitmap {
	if ri.all.IsEmpty() {
		return roaring.New()
	}
	if val == 0 {
		return ri.all.Clone()
	}
	if bits.Len64(val) > len(ri.bits) {
		return roaring.New()
	}
	var b *roaring.Bitmap
	for i, bi := range ri.bits {
		if (val>>i)&1 == 1 {
			if b == nil {
				b = bi.Clone()
			} else {
				b.And(bi)
			}
		} else {
			if b != nil {
				b.Or(bi)
			}
		}
	}
	return b
}

// getInRange returns a bitmap containing all the document ids with values within the given range.
// The returned bitmap is safe to modify.
func (ri *rangeIndex) getInRange(min, max uint64) *roaring.Bitmap {
	if ri.all.IsEmpty() {
		return roaring.New()
	}
	switch {
	case min == 0 && max == math.MaxUint64:
		return ri.all.Clone()
	case min == 0:
		return roaring.AndNot(ri.all, ri.getGreaterEqual(max+1))
	case max == math.MaxUint64:
		return ri.getGreaterEqual(min)
	default:
		b := ri.getGreaterEqual(min)
		b.AndNot(ri.getGreaterEqual(max + 1))
		return b
	}
}

// getNLargest returns a bitmap containing n document ids from the resultSet with largest values.
// If two documents have equal values indexed, the ones with the largest ids are returned first.
// The returned bitmap is safe to modify.
func (ri *rangeIndex) getNLargest(resultSet *roaring.Bitmap, n uint32) *roaring.Bitmap {
	if n == 0 {
		return roaring.New()
	}

	g := roaring.New()
	e := roaring.And(resultSet, ri.all)
	if n >= uint32(e.GetCardinality()) {
		return e
	}
	for i := len(ri.bits) - 1; i >= 0; i-- {
		bi := ri.bits[i]
		eAndBi := roaring.And(e, bi)
		x := roaring.Or(g, eAndBi)
		k := uint32(x.GetCardinality())
		if k < n {
			g = x
			e.AndNot(bi)
			continue
		}
		if k > n {
			e = eAndBi
			continue
		}
		return x
	}
	g.Or(e)

	removeCount := uint32(g.GetCardinality()) - n
	if removeCount > 0 {
		iter := e.Iterator()
		for ; removeCount > 0; removeCount-- {
			g.Remove(iter.Next())
		}
	}

	return g
}

// getNSmallest returns a bitmap containing n document ids from the resultSet with smallest values.
// If two documents have equal values indexed, the ones with the smallest ids are returned first.
// The returned bitmap is safe to modify.
func (ri *rangeIndex) getNSmallest(resultSet *roaring.Bitmap, n uint32) *roaring.Bitmap {
	if n == 0 {
		return roaring.New()
	}

	g := roaring.New()
	e := roaring.And(resultSet, ri.all)
	if n >= uint32(e.GetCardinality()) {
		return e
	}
	for i := len(ri.bits) - 1; i >= 0; i-- {
		bi := ri.bits[i]
		eAndNotBi := roaring.AndNot(e, bi)
		x := roaring.Or(g, eAndNotBi)
		k := uint32(x.GetCardinality())
		if k < n {
			g = x
			e.And(bi)
			continue
		}
		if k > n {
			e = eAndNotBi
			continue
		}
		return x
	}
	g.Or(e)

	removeCount := uint32(g.GetCardinality()) - n
	if removeCount > 0 {
		iter := e.ReverseIterator()
		for ; removeCount > 0; removeCount-- {
			g.Remove(iter.Next())
		}
	}

	return g
}

// getFacetMinMax returns facet min and max values for the given result set. It returns
// false as the second return value if there is no indexed ids in the provided result set.
func (ri *rangeIndex) getFacetMinMax(resultSet *roaring.Bitmap) (min, max uint64, ok bool) {
	minList := ri.getNSmallest(resultSet, 1).ToArray()
	maxList := ri.getNLargest(resultSet, 1).ToArray()
	if len(minList) != 1 || len(maxList) != 1 {
		return 0, 0, false
	}
	return ri.getValue(minList[0]), ri.getValue(maxList[0]), true
}

// getValue returns the indexed value for the given document id.
// It returns 0 if the provided id is not indexed.
func (ri *rangeIndex) getValue(id uint32) uint64 {
	var val uint64
	for i, bi := range ri.bits {
		if bi.Contains(id) {
			val |= 1 << i
		}
	}
	return val
}
