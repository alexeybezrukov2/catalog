package index

import (
	"sort"

	"github.com/RoaringBitmap/roaring"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

// enumIndex creates a bitmap index for each field value
// encountered, holding the list of matching document ids.
type enumIndex struct {
	vals map[uint32]*roaring.Bitmap
}

func newEnumIndex() *enumIndex {
	return &enumIndex{
		vals: make(map[uint32]*roaring.Bitmap),
	}
}

// put adds the given document id and the corresponding value to the index.
func (ei *enumIndex) put(id uint32, val uint32) {
	b, ok := ei.vals[val]
	if !ok {
		b = roaring.New()
		ei.vals[val] = b
	}
	b.Add(id)
}

// delete removes the given document id from index.
func (ei *enumIndex) delete(id uint32) {
	for _, b := range ei.vals {
		b.Remove(id)
	}
}

// getEqual returns a bitmap containing all the document ids with values equal to val.
// The returned bitmap must not be modified (must be cloned first)!
func (ei *enumIndex) getEqual(val uint32) *roaring.Bitmap {
	if b, ok := ei.vals[val]; ok {
		return b
	}
	return roaring.New()
}

// getFacetVariants returns facet values for the given result set. It returns false
// as the second return value if there is no indexed ids in the provided result set.
func (ei *enumIndex) getFacetVariants(resultSet *roaring.Bitmap) ([]*catalog.IndexFacetVariant, bool) {
	variants := []*catalog.IndexFacetVariant{}
	for val, b := range ei.vals {
		count := uint32(resultSet.AndCardinality(b))
		if count > 0 {
			variants = append(variants, &catalog.IndexFacetVariant{
				Value: val,
				Count: count,
			})
		}
	}
	sort.Slice(variants, func(i, j int) bool {
		return variants[i].Value < variants[j].Value
	})
	return variants, len(variants) > 0
}
