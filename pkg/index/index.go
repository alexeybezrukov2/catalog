package index

import (
	"errors"
	"fmt"
	"sort"
	"sync"

	"github.com/RoaringBitmap/roaring"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

// Index indexes and queries documents.
type Index struct {
	mu     sync.RWMutex
	all    *roaring.Bitmap
	enums  map[uint32]*enumIndex
	ranges map[uint32]*rangeIndex
}

// New creates a new index.
func New() *Index {
	return &Index{
		all:    roaring.New(),
		enums:  make(map[uint32]*enumIndex),
		ranges: make(map[uint32]*rangeIndex),
	}
}

// AddMapping adds a new mapping to the index.
func (idx *Index) AddMapping(m *catalog.IndexMapping) {
	idx.mu.Lock()
	defer idx.mu.Unlock()
	switch m.Type {
	case catalog.IndexTypeEnum:
		if _, ok := idx.enums[m.Key]; !ok {
			idx.enums[m.Key] = newEnumIndex()
		}
	case catalog.IndexTypeRange:
		if _, ok := idx.ranges[m.Key]; !ok {
			idx.ranges[m.Key] = newRangeIndex()
		}
	}
}

// DropMapping drops the given index mapping.
func (idx *Index) DropMapping(m *catalog.IndexMapping) {
	idx.mu.Lock()
	defer idx.mu.Unlock()
	switch m.Type {
	case catalog.IndexTypeEnum:
		delete(idx.enums, m.Key)
	case catalog.IndexTypeRange:
		delete(idx.ranges, m.Key)
	}
}

// Insert inserts the given document into the index.
// If the document is already indexed it is replaced with the new one.
func (idx *Index) Insert(id uint32, doc *catalog.IndexDocument) {
	idx.mu.Lock()
	defer idx.mu.Unlock()
	if idx.all.Contains(id) {
		for _, ei := range idx.enums {
			ei.delete(id)
		}
		for _, ri := range idx.ranges {
			ri.delete(id)
		}
	}
	for key, vals := range doc.EnumFields {
		if ei, ok := idx.enums[key]; ok {
			for _, val := range vals {
				ei.put(id, val)
			}
		}
	}
	for key, val := range doc.RangeFields {
		if ri, ok := idx.ranges[key]; ok {
			ri.put(id, val)
		}
	}
	idx.all.Add(id)
}

// Update partially updates the given document.
// Only the fields present in doc are (re-)indexed while others remain untouched.
func (idx *Index) Update(id uint32, doc *catalog.IndexDocument) {
	idx.mu.Lock()
	defer idx.mu.Unlock()
	for key, vals := range doc.EnumFields {
		if ei, ok := idx.enums[key]; ok {
			ei.delete(id)
			for _, val := range vals {
				ei.put(id, val)
			}
		}
	}
	for key, val := range doc.RangeFields {
		if ri, ok := idx.ranges[key]; ok {
			ri.delete(id)
			ri.put(id, val)
		}
	}
	idx.all.Add(id)
}

// Delete deletes the given document from the index.
func (idx *Index) Delete(id uint32) {
	idx.mu.Lock()
	defer idx.mu.Unlock()
	if idx.all.Contains(id) {
		for _, ei := range idx.enums {
			ei.delete(id)
		}
		for _, ri := range idx.ranges {
			ri.delete(id)
		}
		idx.all.Remove(id)
	}
}

// Query queries indexed documents.
func (idx *Index) Query(q *catalog.IndexQuery) (*catalog.IndexQueryResult, error) {
	idx.mu.RLock()
	defer idx.mu.RUnlock()

	sets, err := idx.applyFilters(q.Filters)
	if err != nil {
		return nil, err
	}

	resultSet := idx.computeResultSet(sets)
	total := uint32(resultSet.GetCardinality())

	ids, err := idx.applyLimits(resultSet, q.SortKey, q.SortDesc, q.Limit, q.Offset)
	if err != nil {
		return nil, err
	}

	facets, err := idx.getFacets(q.Filters, sets, resultSet, q.FacetKeys)
	if err != nil {
		return nil, err
	}

	res := &catalog.IndexQueryResult{
		IDs:    ids,
		Total:  total,
		Facets: facets,
	}
	return res, nil
}

// applyFilters applies the given filters to all the indexed documents,
// and returns a separate set of matched ids for each filter.
func (idx *Index) applyFilters(filters []*catalog.IndexFilter) ([]*roaring.Bitmap, error) {
	sets := make([]*roaring.Bitmap, len(filters))
	for i, filter := range filters {
		set, err := idx.applyFilter(filter)
		if err != nil {
			return nil, err
		}
		sets[i] = set
	}
	return sets, nil
}

func (idx *Index) applyFilter(filter *catalog.IndexFilter) (*roaring.Bitmap, error) {
	switch filter.Type {
	case catalog.IndexTypeEnum:
		return idx.applyEnumFilter(filter)
	case catalog.IndexTypeRange:
		return idx.applyRangeFilter(filter)
	default:
		return nil, fmt.Errorf("unsupported filter type: %q", filter.Type)
	}
}

func (idx *Index) applyEnumFilter(filter *catalog.IndexFilter) (*roaring.Bitmap, error) {
	ei, ok := idx.enums[filter.Key]
	if !ok {
		return nil, fmt.Errorf("enum index not found for key %d", filter.Key)
	}
	switch len(filter.EnumVariants) {
	case 0:
		return roaring.New(), nil
	case 1:
		return ei.getEqual(filter.EnumVariants[0]), nil
	default:
		sets := make([]*roaring.Bitmap, len(filter.EnumVariants))
		for i, val := range filter.EnumVariants {
			sets[i] = ei.getEqual(val)
		}
		return roaring.FastOr(sets...), nil
	}
}

func (idx *Index) applyRangeFilter(filter *catalog.IndexFilter) (*roaring.Bitmap, error) {
	ri, ok := idx.ranges[filter.Key]
	if !ok {
		return nil, fmt.Errorf("range index not found for key %q", filter.Key)
	}
	return ri.getInRange(filter.RangeMin, filter.RangeMax), nil
}

// computeResultSet takes multiple filtered sets and computes a result set of ids using
// AND logical operation. If the sets slice is empty, all the indexed ids are returned.
func (idx *Index) computeResultSet(sets []*roaring.Bitmap) *roaring.Bitmap {
	switch len(sets) {
	case 0:
		return idx.all
	case 1:
		return sets[0]
	default:
		return roaring.FastAnd(sets...)
	}
}

func (idx *Index) applyLimits(set *roaring.Bitmap, sortKey uint32, desc bool, limit, offset uint32) ([]uint32, error) {
	sortIndex, ok := idx.ranges[sortKey]
	if !ok {
		return nil, fmt.Errorf("range index not found for sort key %d", sortKey)
	}

	if limit == 0 {
		return nil, errors.New("limit must be positive")
	}

	var limitedSet *roaring.Bitmap
	if desc {
		limitedSet = sortIndex.getNLargest(set, offset+limit)
	} else {
		limitedSet = sortIndex.getNSmallest(set, offset+limit)
	}

	if offset > 0 {
		var exclude *roaring.Bitmap
		if desc {
			exclude = sortIndex.getNLargest(limitedSet, offset)
		} else {
			exclude = sortIndex.getNSmallest(limitedSet, offset)
		}
		limitedSet.AndNot(exclude)
	}

	ids := limitedSet.ToArray()

	// Now we have the requested page of results, but ids within the page are sorted by id.
	// We need to sort them by the corresponding sorting index value instead.
	type idval struct {
		id  uint32
		val uint64
	}
	idvals := make([]idval, len(ids))
	for i, id := range ids {
		idvals[i] = idval{id: id, val: sortIndex.getValue(id)}
	}
	if desc {
		sort.Slice(idvals, func(i, j int) bool {
			if idvals[j].val == idvals[i].val {
				return idvals[j].id < idvals[i].id
			}
			return idvals[j].val < idvals[i].val
		})
	} else {
		sort.Slice(idvals, func(i, j int) bool {
			if idvals[i].val == idvals[j].val {
				return idvals[i].id < idvals[j].id
			}
			return idvals[i].val < idvals[j].val
		})
	}
	for i, idv := range idvals {
		ids[i] = idv.id
	}

	return ids, nil
}

func (idx *Index) getFacets(filters []*catalog.IndexFilter, sets []*roaring.Bitmap, resultSet *roaring.Bitmap, keys []uint32) ([]*catalog.IndexFacet, error) {
	facets := []*catalog.IndexFacet{}
	for _, key := range keys {
		// We need to combine all filter sets except the ones corresponding to the given key.
		var facetSets []*roaring.Bitmap
		for i, set := range sets {
			if filters[i].Key == key {
				continue
			}
			facetSets = append(facetSets, set)
		}
		var facetSet *roaring.Bitmap
		if len(facetSets) == len(sets) {
			facetSet = resultSet
		} else {
			facetSet = idx.computeResultSet(facetSets)
		}

		if ei, ok := idx.enums[key]; ok {
			if variants, ok := ei.getFacetVariants(facetSet); ok {
				facets = append(facets, &catalog.IndexFacet{
					Type:         catalog.IndexTypeEnum,
					Key:          key,
					EnumVariants: variants,
				})
			}
			continue
		}
		if ri, ok := idx.ranges[key]; ok {
			if min, max, ok := ri.getFacetMinMax(facetSet); ok {
				facets = append(facets, &catalog.IndexFacet{
					Type:     catalog.IndexTypeRange,
					Key:      key,
					RangeMin: &min,
					RangeMax: &max,
				})
			}
			continue
		}
		return nil, fmt.Errorf("index not found for facet key %d", key)
	}
	return facets, nil
}
