package index

import (
	"fmt"
	"math"
	"math/rand"
	"reflect"
	"sort"
	"testing"

	"github.com/RoaringBitmap/roaring"
)

func TestRangeIndexGetGreaterEqual(t *testing.T) {
	const (
		numDocs     = 1000
		maxVal      = 1000000
		numRndTests = 1000
	)
	r := rand.New(rand.NewSource(0))

	docs := map[uint32]uint64{}
	ri := newRangeIndex()
	for i := 0; i < numDocs; i++ {
		id := uint32(i)
		val := uint64(r.Intn(maxVal))
		docs[id] = val
		ri.put(id, val)
	}

	testCases := []uint64{0, 1, 2, maxVal - 2, maxVal - 1, maxVal, maxVal + 1, maxVal + 2, math.MaxUint64}
	for _, val := range docs {
		testCases = append(testCases, val)
	}
	for i := 0; i < numRndTests; i++ {
		testCases = append(testCases, uint64(r.Intn(maxVal)))
	}

	simpleFilter := func(x uint64) []uint32 {
		res := []uint32{}
		for id, val := range docs {
			if val >= x {
				res = append(res, id)
			}
		}
		return res
	}

	for _, x := range testCases {
		want := simpleFilter(x)
		got := ri.getGreaterEqual(x).ToArray()
		sort.Slice(got, func(i, j int) bool { return got[i] < got[j] })
		sort.Slice(want, func(i, j int) bool { return want[i] < want[j] })
		if !reflect.DeepEqual(got, want) {
			t.Fatalf("test case x=%d: got different ids", x)
		}
	}
}

func TestRangeIndexGetNLargest(t *testing.T) {
	const (
		numDocs  = 1000
		maxVal   = 1000
		numTests = 1000
	)
	r := rand.New(rand.NewSource(0))

	type doc struct {
		id  uint32
		val uint64
	}
	docs := []doc{}
	ri := newRangeIndex()
	for i := 0; i < numDocs; i++ {
		id := uint32(i)
		val := uint64(r.Intn(maxVal))
		docs = append(docs, doc{id: id, val: val})
		ri.put(id, val)
	}

	sort.Slice(docs, func(i, j int) bool {
		if docs[i].val > docs[j].val {
			return true
		}
		if docs[i].val < docs[j].val {
			return false
		}
		return docs[i].id > docs[j].id
	})

	simpleNLargest := func(resultSet *roaring.Bitmap, n uint32) []uint32 {
		res := []uint32{}
		for _, d := range docs {
			if len(res) == int(n) {
				break
			}
			if !resultSet.Contains(d.id) {
				continue
			}
			res = append(res, d.id)
		}
		return res
	}

	for i := 0; i < numTests; i++ {
		resultSet := roaring.New()
		for j := 0; j < numDocs/2; j++ {
			resultSet.Add(uint32(r.Intn(numDocs)))
		}
		n := uint32(r.Intn(100))
		got := ri.getNLargest(resultSet, n).ToArray()
		want := simpleNLargest(resultSet, n)
		sort.Slice(got, func(i, j int) bool { return got[i] < got[j] })
		sort.Slice(want, func(i, j int) bool { return want[i] < want[j] })
		if !reflect.DeepEqual(got, want) {
			t.Fatalf("test case %d: got different ids", i)
		}
	}
}

func TestRangeIndexGetNSmallest(t *testing.T) {
	const (
		numDocs  = 1000
		maxVal   = 1000
		numTests = 1000
	)
	r := rand.New(rand.NewSource(0))

	type doc struct {
		id  uint32
		val uint64
	}
	docs := []doc{}
	ri := newRangeIndex()
	for i := 0; i < numDocs; i++ {
		id := uint32(i)
		val := uint64(r.Intn(maxVal))
		docs = append(docs, doc{id: id, val: val})
		ri.put(id, val)
	}

	sort.Slice(docs, func(i, j int) bool {
		if docs[i].val < docs[j].val {
			return true
		}
		if docs[i].val > docs[j].val {
			return false
		}
		return docs[i].id < docs[j].id
	})

	simpleNSmallest := func(resultSet *roaring.Bitmap, n uint32) []uint32 {
		res := []uint32{}
		for _, d := range docs {
			if len(res) == int(n) {
				break
			}
			if !resultSet.Contains(d.id) {
				continue
			}
			res = append(res, d.id)
		}
		return res
	}

	for i := 0; i < numTests; i++ {
		resultSet := roaring.New()
		for j := 0; j < numDocs/2; j++ {
			resultSet.Add(uint32(r.Intn(numDocs)))
		}
		n := uint32(r.Intn(100))
		got := ri.getNSmallest(resultSet, n).ToArray()
		want := simpleNSmallest(resultSet, n)
		sort.Slice(got, func(i, j int) bool { return got[i] < got[j] })
		sort.Slice(want, func(i, j int) bool { return want[i] < want[j] })
		if !reflect.DeepEqual(got, want) {
			t.Fatalf("test case %d: got different ids", i)
		}
	}
}

var testRangeIndex *rangeIndex

const (
	testRangeIndexSize   = 1e6
	testRangeIndexMaxVal = 1e9
)

func initRangeIndex() {
	if testRangeIndex != nil {
		return
	}
	testRangeIndex = newRangeIndex()
	r := rand.New(rand.NewSource(0))
	for i := 0; i < testRangeIndexSize; i++ {
		testRangeIndex.put(uint32(i), uint64(r.Intn(testRangeIndexMaxVal)))
	}
}

func BenchmarkRangeIndexGetGreaterEqual(b *testing.B) {
	initRangeIndex()
	r := rand.New(rand.NewSource(0))
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		testRangeIndex.getGreaterEqual(uint64(r.Intn(testRangeIndexMaxVal)))
	}
}

func BenchmarkRangeIndexGetNLargest(b *testing.B) {
	initRangeIndex()
	benchCases := []struct {
		rs int
		n  int
	}{
		{rs: 1000, n: 100},
		{rs: 10000, n: 100},
		{rs: 100000, n: 100},
		{rs: 100000, n: 1000},
		{rs: 100000, n: 10000},
	}
	for _, bc := range benchCases {
		b.Run(fmt.Sprintf("rs %d n %d", bc.rs, bc.n), func(b *testing.B) {
			benchmarkRangeIndexGetNLargest(b, bc.rs, bc.n)
		})
	}
}

func benchmarkRangeIndexGetNLargest(b *testing.B, rsSize, n int) {
	r := rand.New(rand.NewSource(0))
	resultSet := roaring.New()
	for i := 0; i < rsSize; i++ {
		resultSet.Add(uint32(r.Intn(testRangeIndexSize)))
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		testRangeIndex.getNLargest(resultSet, uint32(n))
	}
}

func BenchmarkRangeIndexGetNSmallest(b *testing.B) {
	initRangeIndex()
	benchCases := []struct {
		rs int
		n  int
	}{
		{rs: 1000, n: 100},
		{rs: 10000, n: 100},
		{rs: 100000, n: 100},
		{rs: 100000, n: 1000},
		{rs: 100000, n: 10000},
	}
	for _, bc := range benchCases {
		b.Run(fmt.Sprintf("rs %d n %d", bc.rs, bc.n), func(b *testing.B) {
			benchmarkRangeIndexGetNSmallest(b, bc.rs, bc.n)
		})
	}
}

func benchmarkRangeIndexGetNSmallest(b *testing.B, rsSize, n int) {
	r := rand.New(rand.NewSource(0))
	resultSet := roaring.New()
	for i := 0; i < rsSize; i++ {
		resultSet.Add(uint32(r.Intn(testRangeIndexSize)))
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		testRangeIndex.getNSmallest(resultSet, uint32(n))
	}
}
