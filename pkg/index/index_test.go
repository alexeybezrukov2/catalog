package index

import (
	"math"
	"math/rand"
	"testing"

	"github.com/stretchr/testify/assert"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

func TestIndexQueryFilters(t *testing.T) {
	idx := New()

	idx.AddMapping(&catalog.IndexMapping{Key: 1, Type: catalog.IndexTypeEnum})
	idx.AddMapping(&catalog.IndexMapping{Key: 2, Type: catalog.IndexTypeEnum})
	idx.AddMapping(&catalog.IndexMapping{Key: 3, Type: catalog.IndexTypeEnum})
	idx.AddMapping(&catalog.IndexMapping{Key: 4, Type: catalog.IndexTypeRange})

	docs := map[uint32]*catalog.IndexDocument{
		1: {EnumFields: map[uint32][]uint32{1: {11}, 2: {21}, 3: {31}}, RangeFields: map[uint32]uint64{4: 123}},
		2: {EnumFields: map[uint32][]uint32{1: {12}, 2: {22}, 3: {31}}, RangeFields: map[uint32]uint64{4: 1}},
		3: {EnumFields: map[uint32][]uint32{1: {11}, 2: {22}, 3: {32}}, RangeFields: map[uint32]uint64{4: 0}},
		4: {EnumFields: map[uint32][]uint32{1: {13}, 2: {23}, 3: {32}}, RangeFields: map[uint32]uint64{4: 987654}},
		5: {EnumFields: map[uint32][]uint32{1: {12}, 2: {23}, 3: {33}}, RangeFields: map[uint32]uint64{4: 12}},
		6: {EnumFields: map[uint32][]uint32{1: {11}, 2: {21}, 3: {33, 34, 35}}, RangeFields: map[uint32]uint64{4: 321}},
	}
	for id, doc := range docs {
		idx.Insert(id, doc)
	}

	testCases := []struct {
		name    string
		filters []*catalog.IndexFilter
		want    []uint32
	}{
		{
			name: "enum filter single value",
			filters: []*catalog.IndexFilter{
				{
					Type:         catalog.IndexTypeEnum,
					Key:          1,
					EnumVariants: []uint32{11},
				},
			},
			want: []uint32{3, 1, 6},
		},
		{
			name: "enum filter multiple values",
			filters: []*catalog.IndexFilter{
				{
					Type:         catalog.IndexTypeEnum,
					Key:          3,
					EnumVariants: []uint32{32, 34},
				},
			},
			want: []uint32{3, 6, 4},
		},
		{
			name: "enum filter missing values",
			filters: []*catalog.IndexFilter{
				{
					Type:         catalog.IndexTypeEnum,
					Key:          1,
					EnumVariants: []uint32{997, 998, 999},
				},
			},
			want: []uint32{},
		},
		{
			name: "two enum filters",
			filters: []*catalog.IndexFilter{
				{
					Type:         catalog.IndexTypeEnum,
					Key:          2,
					EnumVariants: []uint32{21, 23},
				},
				{
					Type:         catalog.IndexTypeEnum,
					Key:          3,
					EnumVariants: []uint32{31, 33},
				},
			},
			want: []uint32{5, 1, 6},
		},
		{
			name: "three enum filters",
			filters: []*catalog.IndexFilter{
				{
					Type:         catalog.IndexTypeEnum,
					Key:          1,
					EnumVariants: []uint32{13},
				},
				{
					Type:         catalog.IndexTypeEnum,
					Key:          2,
					EnumVariants: []uint32{23},
				},
				{
					Type:         catalog.IndexTypeEnum,
					Key:          3,
					EnumVariants: []uint32{32},
				},
			},
			want: []uint32{4},
		},
		{
			name: "three enum filters no results",
			filters: []*catalog.IndexFilter{
				{
					Type:         catalog.IndexTypeEnum,
					Key:          1,
					EnumVariants: []uint32{13},
				},
				{
					Type:         catalog.IndexTypeEnum,
					Key:          2,
					EnumVariants: []uint32{21},
				},
				{
					Type:         catalog.IndexTypeEnum,
					Key:          3,
					EnumVariants: []uint32{33},
				},
			},
			want: []uint32{},
		},
		{
			name: "range filter all",
			filters: []*catalog.IndexFilter{
				{
					Type:     catalog.IndexTypeRange,
					Key:      4,
					RangeMin: 0,
					RangeMax: math.MaxUint64,
				},
			},
			want: []uint32{3, 2, 5, 1, 6, 4},
		},
		{
			name: "range filter some",
			filters: []*catalog.IndexFilter{
				{
					Type:     catalog.IndexTypeRange,
					Key:      4,
					RangeMin: 10,
					RangeMax: 321,
				},
			},
			want: []uint32{5, 1, 6},
		},
		{
			name: "range filter none",
			filters: []*catalog.IndexFilter{
				{
					Type:     catalog.IndexTypeRange,
					Key:      4,
					RangeMin: 13,
					RangeMax: 122,
				},
			},
			want: []uint32{},
		},
		{
			name: "range filter 0 to x",
			filters: []*catalog.IndexFilter{
				{
					Type:     catalog.IndexTypeRange,
					Key:      4,
					RangeMin: 0,
					RangeMax: 123,
				},
			},
			want: []uint32{3, 2, 5, 1},
		},
		{
			name: "range filter x to max-uint64",
			filters: []*catalog.IndexFilter{
				{
					Type:     catalog.IndexTypeRange,
					Key:      4,
					RangeMin: 123,
					RangeMax: math.MaxUint64,
				},
			},
			want: []uint32{1, 6, 4},
		},
		{
			name: "enum and range filters",
			filters: []*catalog.IndexFilter{
				{
					Type:         catalog.IndexTypeEnum,
					Key:          1,
					EnumVariants: []uint32{11},
				},
				{
					Type:     catalog.IndexTypeRange,
					Key:      4,
					RangeMin: 321,
					RangeMax: 321,
				},
			},
			want: []uint32{6},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			res, err := idx.Query(&catalog.IndexQuery{
				Filters:   tc.filters,
				SortKey:   4,
				SortDesc:  false,
				Limit:     10,
				Offset:    0,
				FacetKeys: nil,
			})
			if err != nil {
				t.Fatal(err)
			}
			got := res.IDs
			assert.Equal(t, tc.want, got)
		})
	}
}

func TestIndexQueryLimitsFacets(t *testing.T) {
	idx := New()

	idx.AddMapping(&catalog.IndexMapping{Key: 1, Type: catalog.IndexTypeEnum})
	idx.AddMapping(&catalog.IndexMapping{Key: 2, Type: catalog.IndexTypeEnum})
	idx.AddMapping(&catalog.IndexMapping{Key: 3, Type: catalog.IndexTypeEnum})
	idx.AddMapping(&catalog.IndexMapping{Key: 4, Type: catalog.IndexTypeRange})

	docs := map[uint32]*catalog.IndexDocument{
		1:  {EnumFields: map[uint32][]uint32{1: {11}, 2: {21}, 3: {31}}, RangeFields: map[uint32]uint64{4: 41}},
		2:  {EnumFields: map[uint32][]uint32{1: {11}, 2: {22}, 3: {31}}, RangeFields: map[uint32]uint64{4: 42}},
		3:  {EnumFields: map[uint32][]uint32{1: {11}, 2: {23}, 3: {31}}, RangeFields: map[uint32]uint64{4: 43}},
		4:  {EnumFields: map[uint32][]uint32{1: {11}, 2: {24}, 3: {32}}, RangeFields: map[uint32]uint64{4: 44}},
		5:  {EnumFields: map[uint32][]uint32{1: {11}, 2: {21}, 3: {32}}, RangeFields: map[uint32]uint64{4: 45}},
		6:  {EnumFields: map[uint32][]uint32{1: {11}, 2: {22}, 3: {32}}, RangeFields: map[uint32]uint64{4: 46}},
		7:  {EnumFields: map[uint32][]uint32{1: {11}, 2: {23}, 3: {33}}, RangeFields: map[uint32]uint64{4: 47}},
		8:  {EnumFields: map[uint32][]uint32{1: {11}, 2: {21}, 3: {33}}, RangeFields: map[uint32]uint64{4: 48}},
		9:  {EnumFields: map[uint32][]uint32{1: {11}, 2: {22}, 3: {33}}, RangeFields: map[uint32]uint64{4: 49}},
		10: {EnumFields: map[uint32][]uint32{1: {11}, 2: {21}, 3: {34, 35}}, RangeFields: map[uint32]uint64{4: 50}},
		99: {EnumFields: map[uint32][]uint32{1: {12}, 2: {21}, 3: {31}}, RangeFields: map[uint32]uint64{4: 123456789}},
	}
	for id, doc := range docs {
		idx.Insert(id, doc)
	}

	testCases := []struct {
		name  string
		query *catalog.IndexQuery
		want  *catalog.IndexQueryResult
	}{
		{
			name: "limit > total",
			query: &catalog.IndexQuery{
				Filters:   []*catalog.IndexFilter{{Type: catalog.IndexTypeEnum, Key: 1, EnumVariants: []uint32{11}}},
				SortKey:   4,
				SortDesc:  false,
				Limit:     11,
				Offset:    0,
				FacetKeys: nil,
			},
			want: &catalog.IndexQueryResult{
				IDs:    []uint32{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
				Total:  10,
				Facets: []*catalog.IndexFacet{},
			},
		},
		{
			name: "limit = total",
			query: &catalog.IndexQuery{
				Filters:   []*catalog.IndexFilter{{Type: catalog.IndexTypeEnum, Key: 1, EnumVariants: []uint32{11}}},
				SortKey:   4,
				SortDesc:  false,
				Limit:     10,
				Offset:    0,
				FacetKeys: nil,
			},
			want: &catalog.IndexQueryResult{
				IDs:    []uint32{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
				Total:  10,
				Facets: []*catalog.IndexFacet{},
			},
		},
		{
			name: "limit = total desc",
			query: &catalog.IndexQuery{
				Filters:   []*catalog.IndexFilter{{Type: catalog.IndexTypeEnum, Key: 1, EnumVariants: []uint32{11}}},
				SortKey:   4,
				SortDesc:  true,
				Limit:     10,
				Offset:    0,
				FacetKeys: nil,
			},
			want: &catalog.IndexQueryResult{
				IDs:    []uint32{10, 9, 8, 7, 6, 5, 4, 3, 2, 1},
				Total:  10,
				Facets: []*catalog.IndexFacet{},
			},
		},
		{
			name: "limit < total",
			query: &catalog.IndexQuery{
				Filters:   []*catalog.IndexFilter{{Type: catalog.IndexTypeEnum, Key: 1, EnumVariants: []uint32{11}}},
				SortKey:   4,
				SortDesc:  false,
				Limit:     9,
				Offset:    0,
				FacetKeys: nil,
			},
			want: &catalog.IndexQueryResult{
				IDs:    []uint32{1, 2, 3, 4, 5, 6, 7, 8, 9},
				Total:  10,
				Facets: []*catalog.IndexFacet{},
			},
		},
		{
			name: "limit 3",
			query: &catalog.IndexQuery{
				Filters:   []*catalog.IndexFilter{{Type: catalog.IndexTypeEnum, Key: 1, EnumVariants: []uint32{11}}},
				SortKey:   4,
				SortDesc:  false,
				Limit:     3,
				Offset:    0,
				FacetKeys: nil,
			},
			want: &catalog.IndexQueryResult{
				IDs:    []uint32{1, 2, 3},
				Total:  10,
				Facets: []*catalog.IndexFacet{},
			},
		},
		{
			name: "limit 3 desc",
			query: &catalog.IndexQuery{
				Filters:   []*catalog.IndexFilter{{Type: catalog.IndexTypeEnum, Key: 1, EnumVariants: []uint32{11}}},
				SortKey:   4,
				SortDesc:  true,
				Limit:     3,
				Offset:    0,
				FacetKeys: nil,
			},
			want: &catalog.IndexQueryResult{
				IDs:    []uint32{10, 9, 8},
				Total:  10,
				Facets: []*catalog.IndexFacet{},
			},
		},
		{
			name: "limit 3 offset 3",
			query: &catalog.IndexQuery{
				Filters:   []*catalog.IndexFilter{{Type: catalog.IndexTypeEnum, Key: 1, EnumVariants: []uint32{11}}},
				SortKey:   4,
				SortDesc:  false,
				Limit:     3,
				Offset:    3,
				FacetKeys: nil,
			},
			want: &catalog.IndexQueryResult{
				IDs:    []uint32{4, 5, 6},
				Total:  10,
				Facets: []*catalog.IndexFacet{},
			},
		},
		{
			name: "limit 3 offset 3 desc",
			query: &catalog.IndexQuery{
				Filters:   []*catalog.IndexFilter{{Type: catalog.IndexTypeEnum, Key: 1, EnumVariants: []uint32{11}}},
				SortKey:   4,
				SortDesc:  true,
				Limit:     3,
				Offset:    3,
				FacetKeys: nil,
			},
			want: &catalog.IndexQueryResult{
				IDs:    []uint32{7, 6, 5},
				Total:  10,
				Facets: []*catalog.IndexFacet{},
			},
		},
		{
			name: "limit 3 offset 9",
			query: &catalog.IndexQuery{
				Filters:   []*catalog.IndexFilter{{Type: catalog.IndexTypeEnum, Key: 1, EnumVariants: []uint32{11}}},
				SortKey:   4,
				SortDesc:  false,
				Limit:     3,
				Offset:    9,
				FacetKeys: nil,
			},
			want: &catalog.IndexQueryResult{
				IDs:    []uint32{10},
				Total:  10,
				Facets: []*catalog.IndexFacet{},
			},
		},
		{
			name: "limit 3 offset 9 desc",
			query: &catalog.IndexQuery{
				Filters:   []*catalog.IndexFilter{{Type: catalog.IndexTypeEnum, Key: 1, EnumVariants: []uint32{11}}},
				SortKey:   4,
				SortDesc:  true,
				Limit:     3,
				Offset:    9,
				FacetKeys: nil,
			},
			want: &catalog.IndexQueryResult{
				IDs:    []uint32{1},
				Total:  10,
				Facets: []*catalog.IndexFacet{},
			},
		},
		{
			name: "limit 5 offset 10",
			query: &catalog.IndexQuery{
				Filters:   []*catalog.IndexFilter{{Type: catalog.IndexTypeEnum, Key: 1, EnumVariants: []uint32{11}}},
				SortKey:   4,
				SortDesc:  false,
				Limit:     5,
				Offset:    10,
				FacetKeys: nil,
			},
			want: &catalog.IndexQueryResult{
				IDs:    []uint32{},
				Total:  10,
				Facets: []*catalog.IndexFacet{},
			},
		},
		{
			name: "limit 5 offset 10 desc",
			query: &catalog.IndexQuery{
				Filters:   []*catalog.IndexFilter{{Type: catalog.IndexTypeEnum, Key: 1, EnumVariants: []uint32{11}}},
				SortKey:   4,
				SortDesc:  true,
				Limit:     5,
				Offset:    10,
				FacetKeys: nil,
			},
			want: &catalog.IndexQueryResult{
				IDs:    []uint32{},
				Total:  10,
				Facets: []*catalog.IndexFacet{},
			},
		},
		{
			name: "limit 3 offset 3 desc some facets",
			query: &catalog.IndexQuery{
				Filters:   []*catalog.IndexFilter{{Type: catalog.IndexTypeEnum, Key: 1, EnumVariants: []uint32{11}}},
				SortKey:   4,
				SortDesc:  true,
				Limit:     3,
				Offset:    3,
				FacetKeys: []uint32{1, 4},
			},
			want: &catalog.IndexQueryResult{
				IDs:   []uint32{7, 6, 5},
				Total: 10,
				Facets: []*catalog.IndexFacet{
					{
						Type: catalog.IndexTypeEnum,
						Key:  1,
						EnumVariants: []*catalog.IndexFacetVariant{
							{Value: 11, Count: 10},
							{Value: 12, Count: 1},
						},
					},
					{
						Type:     catalog.IndexTypeRange,
						Key:      4,
						RangeMin: uint64ptr(41),
						RangeMax: uint64ptr(50),
					},
				},
			},
		},
		{
			name: "limit 3 offset 3 desc all facets",
			query: &catalog.IndexQuery{
				Filters:   []*catalog.IndexFilter{{Type: catalog.IndexTypeEnum, Key: 1, EnumVariants: []uint32{11}}},
				SortKey:   4,
				SortDesc:  true,
				Limit:     3,
				Offset:    3,
				FacetKeys: []uint32{1, 2, 3, 4},
			},
			want: &catalog.IndexQueryResult{
				IDs:   []uint32{7, 6, 5},
				Total: 10,
				Facets: []*catalog.IndexFacet{
					{
						Type: catalog.IndexTypeEnum,
						Key:  1,
						EnumVariants: []*catalog.IndexFacetVariant{
							{Value: 11, Count: 10},
							{Value: 12, Count: 1},
						},
					},
					{
						Type: catalog.IndexTypeEnum,
						Key:  2,
						EnumVariants: []*catalog.IndexFacetVariant{
							{Value: 21, Count: 4},
							{Value: 22, Count: 3},
							{Value: 23, Count: 2},
							{Value: 24, Count: 1},
						},
					},
					{
						Type: catalog.IndexTypeEnum,
						Key:  3,
						EnumVariants: []*catalog.IndexFacetVariant{
							{Value: 31, Count: 3},
							{Value: 32, Count: 3},
							{Value: 33, Count: 3},
							{Value: 34, Count: 1},
							{Value: 35, Count: 1},
						},
					},
					{
						Type:     catalog.IndexTypeRange,
						Key:      4,
						RangeMin: uint64ptr(41),
						RangeMax: uint64ptr(50),
					},
				},
			},
		},
		{
			name: "all docs limit 5 offset 6 desc all facets",
			query: &catalog.IndexQuery{
				Filters:   []*catalog.IndexFilter{{Type: catalog.IndexTypeEnum, Key: 1, EnumVariants: []uint32{11, 12}}},
				SortKey:   4,
				SortDesc:  true,
				Limit:     5,
				Offset:    6,
				FacetKeys: []uint32{1, 2, 3, 4},
			},
			want: &catalog.IndexQueryResult{
				IDs:   []uint32{5, 4, 3, 2, 1},
				Total: 11,
				Facets: []*catalog.IndexFacet{
					{
						Type: catalog.IndexTypeEnum,
						Key:  1,
						EnumVariants: []*catalog.IndexFacetVariant{
							{Value: 11, Count: 10},
							{Value: 12, Count: 1},
						},
					},
					{
						Type: catalog.IndexTypeEnum,
						Key:  2,
						EnumVariants: []*catalog.IndexFacetVariant{
							{Value: 21, Count: 5},
							{Value: 22, Count: 3},
							{Value: 23, Count: 2},
							{Value: 24, Count: 1},
						},
					},
					{
						Type: catalog.IndexTypeEnum,
						Key:  3,
						EnumVariants: []*catalog.IndexFacetVariant{
							{Value: 31, Count: 4},
							{Value: 32, Count: 3},
							{Value: 33, Count: 3},
							{Value: 34, Count: 1},
							{Value: 35, Count: 1},
						},
					},
					{
						Type:     catalog.IndexTypeRange,
						Key:      4,
						RangeMin: uint64ptr(41),
						RangeMax: uint64ptr(123456789),
					},
				},
			},
		},
		{
			name: "three filters all facets",
			query: &catalog.IndexQuery{
				Filters: []*catalog.IndexFilter{
					{Type: catalog.IndexTypeEnum, Key: 2, EnumVariants: []uint32{22, 23, 24}},
					{Type: catalog.IndexTypeEnum, Key: 3, EnumVariants: []uint32{31, 32, 34}},
					{Type: catalog.IndexTypeRange, Key: 4, RangeMin: 43, RangeMax: 49},
				},
				SortKey:   4,
				SortDesc:  false,
				Limit:     10,
				Offset:    0,
				FacetKeys: []uint32{1, 2, 3, 4},
			},
			want: &catalog.IndexQueryResult{
				IDs:   []uint32{3, 4, 6},
				Total: 3,
				Facets: []*catalog.IndexFacet{
					{
						Type: catalog.IndexTypeEnum,
						Key:  1,
						EnumVariants: []*catalog.IndexFacetVariant{
							{Value: 11, Count: 3},
						},
					},
					{
						Type: catalog.IndexTypeEnum,
						Key:  2,
						EnumVariants: []*catalog.IndexFacetVariant{
							{Value: 21, Count: 1},
							{Value: 22, Count: 1},
							{Value: 23, Count: 1},
							{Value: 24, Count: 1},
						},
					},
					{
						Type: catalog.IndexTypeEnum,
						Key:  3,
						EnumVariants: []*catalog.IndexFacetVariant{
							{Value: 31, Count: 1},
							{Value: 32, Count: 2},
							{Value: 33, Count: 2},
						},
					},
					{
						Type:     catalog.IndexTypeRange,
						Key:      4,
						RangeMin: uint64ptr(42),
						RangeMax: uint64ptr(46),
					},
				},
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			got, err := idx.Query(tc.query)
			if err != nil {
				t.Fatal(err)
			}
			assert.Equal(t, tc.want, got)
		})
	}
}

func TestIndexInsertUpdateDelete(t *testing.T) {
	idx := New()
	idx.AddMapping(&catalog.IndexMapping{Key: 1, Type: catalog.IndexTypeEnum})
	idx.AddMapping(&catalog.IndexMapping{Key: 2, Type: catalog.IndexTypeRange})

	testCases := []struct {
		name   string
		fn     func(idx *Index)
		filter *catalog.IndexFilter
		want   []uint32
	}{
		{
			name: "init index",
			fn: func(idx *Index) {
				idx.Insert(1, &catalog.IndexDocument{EnumFields: map[uint32][]uint32{1: {1}}, RangeFields: map[uint32]uint64{2: 1}})
				idx.Insert(2, &catalog.IndexDocument{EnumFields: map[uint32][]uint32{1: {2}}, RangeFields: map[uint32]uint64{2: 2}})
			},
		},
		{
			name:   "check 1 is found by enum",
			filter: &catalog.IndexFilter{Type: catalog.IndexTypeEnum, Key: 1, EnumVariants: []uint32{1}},
			want:   []uint32{1},
		},
		{
			name:   "check 1 is found by range",
			filter: &catalog.IndexFilter{Type: catalog.IndexTypeRange, Key: 2, RangeMin: 1, RangeMax: 1},
			want:   []uint32{1},
		},
		{
			name:   "check 2 is found by enum",
			filter: &catalog.IndexFilter{Type: catalog.IndexTypeEnum, Key: 1, EnumVariants: []uint32{2}},
			want:   []uint32{2},
		},
		{
			name:   "check 2 is found by range",
			filter: &catalog.IndexFilter{Type: catalog.IndexTypeRange, Key: 2, RangeMin: 2, RangeMax: 2},
			want:   []uint32{2},
		},
		{
			name: "delete 1",
			fn: func(idx *Index) {
				idx.Delete(1)
			},
		},
		{
			name:   "check 1 is not found by enum",
			filter: &catalog.IndexFilter{Type: catalog.IndexTypeEnum, Key: 1, EnumVariants: []uint32{1}},
			want:   []uint32{},
		},
		{
			name:   "check 1 is not found by range",
			filter: &catalog.IndexFilter{Type: catalog.IndexTypeRange, Key: 2, RangeMin: 1, RangeMax: 1},
			want:   []uint32{},
		},
		{
			name:   "check 2 is still found by enum",
			filter: &catalog.IndexFilter{Type: catalog.IndexTypeEnum, Key: 1, EnumVariants: []uint32{2}},
			want:   []uint32{2},
		},
		{
			name:   "check 2 is still found by range",
			filter: &catalog.IndexFilter{Type: catalog.IndexTypeRange, Key: 2, RangeMin: 2, RangeMax: 2},
			want:   []uint32{2},
		},
		{
			name: "reindex 2 to 3",
			fn: func(idx *Index) {
				idx.Insert(2, &catalog.IndexDocument{EnumFields: map[uint32][]uint32{1: {3}}, RangeFields: map[uint32]uint64{2: 3}})
			},
		},
		{
			name:   "check 2 is not found by enum",
			filter: &catalog.IndexFilter{Type: catalog.IndexTypeEnum, Key: 1, EnumVariants: []uint32{2}},
			want:   []uint32{},
		},
		{
			name:   "check 2 is not found by range",
			filter: &catalog.IndexFilter{Type: catalog.IndexTypeRange, Key: 2, RangeMin: 2, RangeMax: 2},
			want:   []uint32{},
		},
		{
			name:   "check 3 is found by enum",
			filter: &catalog.IndexFilter{Type: catalog.IndexTypeEnum, Key: 1, EnumVariants: []uint32{3}},
			want:   []uint32{2},
		},
		{
			name:   "check 3 is found by range",
			filter: &catalog.IndexFilter{Type: catalog.IndexTypeRange, Key: 2, RangeMin: 3, RangeMax: 3},
			want:   []uint32{2},
		},
		{
			name: "update enum 3 to 4",
			fn: func(idx *Index) {
				idx.Update(2, &catalog.IndexDocument{EnumFields: map[uint32][]uint32{1: {4}}})
			},
		},
		{
			name:   "check 3 is not found by enum",
			filter: &catalog.IndexFilter{Type: catalog.IndexTypeEnum, Key: 1, EnumVariants: []uint32{3}},
			want:   []uint32{},
		},
		{
			name:   "check 4 is found by enum",
			filter: &catalog.IndexFilter{Type: catalog.IndexTypeEnum, Key: 1, EnumVariants: []uint32{4}},
			want:   []uint32{2},
		},
		{
			name:   "check 3 is still found by range",
			filter: &catalog.IndexFilter{Type: catalog.IndexTypeRange, Key: 2, RangeMin: 3, RangeMax: 3},
			want:   []uint32{2},
		},
		{
			name: "update range 3 to 5",
			fn: func(idx *Index) {
				idx.Update(2, &catalog.IndexDocument{RangeFields: map[uint32]uint64{2: 5}})
			},
		},
		{
			name:   "check 4 is still found by enum",
			filter: &catalog.IndexFilter{Type: catalog.IndexTypeEnum, Key: 1, EnumVariants: []uint32{4}},
			want:   []uint32{2},
		},
		{
			name:   "check 3 is not found by range",
			filter: &catalog.IndexFilter{Type: catalog.IndexTypeRange, Key: 2, RangeMin: 3, RangeMax: 3},
			want:   []uint32{},
		},
		{
			name:   "check 5 is found by range",
			filter: &catalog.IndexFilter{Type: catalog.IndexTypeRange, Key: 2, RangeMin: 5, RangeMax: 5},
			want:   []uint32{2},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if tc.fn != nil {
				tc.fn(idx)
			}
			if tc.filter != nil {
				res, err := idx.Query(&catalog.IndexQuery{
					Filters:   []*catalog.IndexFilter{tc.filter},
					SortKey:   2,
					SortDesc:  false,
					Limit:     10,
					Offset:    0,
					FacetKeys: nil,
				})
				if err != nil {
					t.Fatal(err)
				}
				got := res.IDs
				assert.Equal(t, tc.want, got)
			}
		})
	}
}

var idx *Index

const (
	idxNumDocuments    = 1e6
	idxEnumKey1        = 1
	idxEnumKey2        = 2
	idxEnumCardinality = 5
	idxRangeKey        = 3
	idxRangeMaxVal     = 1e9
)

func initIdx() {
	if idx != nil {
		return
	}

	idx = New()
	idx.AddMapping(&catalog.IndexMapping{Key: 1, Type: catalog.IndexTypeEnum})
	idx.AddMapping(&catalog.IndexMapping{Key: 2, Type: catalog.IndexTypeEnum})
	idx.AddMapping(&catalog.IndexMapping{Key: 3, Type: catalog.IndexTypeRange})

	r := rand.New(rand.NewSource(0))
	for i := 0; i < idxNumDocuments; i++ {
		idx.Insert(uint32(i), &catalog.IndexDocument{
			EnumFields: map[uint32][]uint32{
				idxEnumKey1: {uint32(r.Intn(idxEnumCardinality))},
				idxEnumKey2: {uint32(r.Intn(idxEnumCardinality))},
			},
			RangeFields: map[uint32]uint64{
				idxRangeKey: uint64(r.Intn(idxRangeMaxVal)),
			},
		})
	}
}

func BenchmarkIndex(b *testing.B) {
	initIdx()
	r := rand.New(rand.NewSource(0))
	mid := int(idxRangeMaxVal / 2)
	b.ResetTimer()
	n := 0
	for i := 0; i < b.N; i++ {
		res, err := idx.Query(&catalog.IndexQuery{
			Filters: []*catalog.IndexFilter{
				{
					Type: catalog.IndexTypeEnum,
					Key:  idxEnumKey1,
					EnumVariants: []uint32{
						uint32(r.Intn(idxEnumCardinality)),
						uint32(r.Intn(idxEnumCardinality)),
					},
				},
				{
					Type: catalog.IndexTypeEnum,
					Key:  idxEnumKey2,
					EnumVariants: []uint32{
						uint32(r.Intn(idxEnumCardinality)),
						uint32(r.Intn(idxEnumCardinality)),
					},
				},
				{
					Type:     catalog.IndexTypeRange,
					Key:      idxRangeKey,
					RangeMin: uint64(r.Intn(mid)),
					RangeMax: uint64(mid + r.Intn(mid)),
				},
			},
			SortKey:   idxRangeKey,
			SortDesc:  false,
			Limit:     100,
			Offset:    100,
			FacetKeys: []uint32{idxEnumKey1, idxEnumKey2, idxRangeKey},
		})
		if err != nil {
			b.Fatal(err)
		}
		n += int(res.Total)
	}
}

func uint64ptr(v uint64) *uint64 {
	return &v
}
