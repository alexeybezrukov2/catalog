package productsvc

import (
	"context"
	"encoding/json"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

type getProductCardListRequest struct {
	IDs []uint32
}

func decodeGetProductCardListRequest(_ context.Context, r *http.Request) (interface{}, error) {
	idsStr := strings.Split(r.URL.Query().Get("ids"), ",")
	ids := make([]uint32, 0, len(idsStr))
	for _, idStr := range idsStr {
		id, err := strconv.ParseUint(idStr, 10, 32)
		if err != nil {
			return nil, catalog.ErrBadRequest("failed to parse product id: %v", err)
		}
		ids = append(ids, uint32(id))
	}
	return getProductCardListRequest{IDs: ids}, nil
}

func encodeGetProductCardListRequest(_ context.Context, r *http.Request, request interface{}) error {
	req := request.(getProductCardListRequest)
	r.URL.Path = "/api/v1/products"

	if len(req.IDs) == 0 {
		return nil
	}

	idsStr := make([]string, 0, len(req.IDs))
	for _, id := range req.IDs {
		idsStr = append(idsStr, strconv.FormatUint(uint64(id), 10))
	}

	query := r.URL.Query()
	query.Set("ids", strings.Join(idsStr, ","))
	r.URL.RawQuery = query.Encode()

	return nil
}

type getProductCardListResponse struct {
	ProductCardList []*catalog.ProductCard
	Err             error
}

func decodeGetProductCardListResponse(_ context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return getProductCardListResponse{Err: decodeError(r)}, nil
	}
	req := getProductCardListResponse{ProductCardList: []*catalog.ProductCard{}}
	if err := json.NewDecoder(r.Body).Decode(&req.ProductCardList); err != nil {
		return nil, catalog.ErrBadRequest("failed to decode JSON request: %v", err)
	}
	return req, nil
}

func encodeGetProductCardListResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(getProductCardListResponse)
	if res.Err != nil {
		return res.Err
	}
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(res.ProductCardList); err != nil {
		return catalog.ErrInternal("failed to encode JSON response: %s", err)
	}
	return nil
}

type getProductCardRequest struct {
	ID uint32
}

func encodeGetProductCardRequest(_ context.Context, r *http.Request, request interface{}) error {
	req := request.(getProductCardRequest)
	r.URL.Path = "/api/v1/products/" + strconv.FormatUint(uint64(req.ID), 10)
	return nil
}

func decodeGetProductCardRequest(_ context.Context, r *http.Request) (interface{}, error) {
	id, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 32)
	if err != nil {
		return nil, catalog.ErrBadRequest("failed to parse product id: %v", err)
	}
	return getProductCardRequest{ID: uint32(id)}, nil
}

type getProductCardResponse struct {
	ProductCard *catalog.ProductCard
	Err         error
}

func encodeGetProductCardResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(getProductCardResponse)
	if res.Err != nil {
		return res.Err
	}
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(res.ProductCard); err != nil {
		return catalog.ErrInternal("failed to encode JSON response: %s", err)
	}
	return nil
}

func decodeGetProductCardResponse(_ context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return getProductCardResponse{Err: decodeError(r)}, nil
	}
	req := getProductCardResponse{ProductCard: &catalog.ProductCard{}}
	if err := json.NewDecoder(r.Body).Decode(req.ProductCard); err != nil {
		return nil, catalog.ErrBadRequest("failed to decode JSON request: %v", err)
	}
	return req, nil
}

func encodeError(ctx context.Context, err error, w http.ResponseWriter) {
	e, ok := err.(*catalog.ServiceError)
	if !ok {
		e = &catalog.ServiceError{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		}
	}
	e.Encode(w)
}

func decodeError(r *http.Response) error {
	e := &catalog.ServiceError{}
	e.Decode(r)
	return e
}
