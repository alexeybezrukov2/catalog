package productsvc

import (
	"context"
	"errors"
	"net/http"
	"net/url"
	"time"

	"github.com/go-kit/kit/endpoint"
	kithttp "github.com/go-kit/kit/transport/http"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

// ClientConfig is an Client configuration.
type ClientConfig struct {
	ServiceURL string
	Timeout    time.Duration
}

func (cfg ClientConfig) validate() error {
	if cfg.ServiceURL == "" {
		return errors.New("must provide ServiceURL")
	}
	if cfg.Timeout <= 0 {
		return errors.New("invalid Timeout")
	}
	return nil
}

var _ Service = (*Client)(nil)

// Client is a client for productsvc.
type Client struct {
	getProductCardListEndpoint endpoint.Endpoint
	getProductCardEndpoint     endpoint.Endpoint
}

// NewClient creates a new productsvc client.
func NewClient(cfg ClientConfig) (*Client, error) {
	err := cfg.validate()
	if err != nil {
		return nil, err
	}

	baseURL, err := url.Parse(cfg.ServiceURL)
	if err != nil {
		return nil, err
	}

	options := []kithttp.ClientOption{
		kithttp.SetClient(&http.Client{
			Timeout: cfg.Timeout,
		}),
	}

	c := &Client{
		getProductCardListEndpoint: kithttp.NewClient(
			http.MethodGet,
			baseURL,
			encodeGetProductCardListRequest,
			decodeGetProductCardListResponse,
			options...,
		).Endpoint(),
		getProductCardEndpoint: kithttp.NewClient(
			http.MethodGet,
			baseURL,
			encodeGetProductCardRequest,
			decodeGetProductCardResponse,
			options...,
		).Endpoint(),
	}

	return c, nil
}

// GetProductCardList returns the list of product cards by the given ids.
func (c *Client) GetProductCardList(ctx context.Context, ids []uint32) ([]*catalog.ProductCard, error) {
	response, err := c.getProductCardListEndpoint(ctx, getProductCardListRequest{
		IDs: ids,
	})
	if err != nil {
		return nil, err
	}
	res := response.(getProductCardListResponse)
	return res.ProductCardList, res.Err
}

// GetProductCard returns the product card with the given id.
func (c *Client) GetProductCard(ctx context.Context, id uint32) (*catalog.ProductCard, error) {
	response, err := c.getProductCardEndpoint(ctx, getProductCardRequest{
		ID: id,
	})
	if err != nil {
		return nil, err
	}
	res := response.(getProductCardResponse)
	return res.ProductCard, res.Err
}
