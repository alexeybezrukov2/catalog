package productsvc

import (
	"context"
	"fmt"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

type mockService struct {
	onGetProductCardList func(ctx context.Context, ids []uint32) ([]*catalog.ProductCard, error)
	onGetProductCard     func(ctx context.Context, id uint32) (*catalog.ProductCard, error)
}

func (m *mockService) GetProductCardList(ctx context.Context, ids []uint32) ([]*catalog.ProductCard, error) {
	return m.onGetProductCardList(ctx, ids)
}

func (m *mockService) GetProductCard(ctx context.Context, id uint32) (*catalog.ProductCard, error) {
	return m.onGetProductCard(ctx, id)
}

func initTransportTest(t *testing.T) (*httptest.Server, *Client, *mockService) {
	svc := &mockService{}
	handler := makeHandler(svc)
	server := httptest.NewServer(handler)
	client, err := NewClient(ClientConfig{
		ServiceURL: server.URL,
		Timeout:    time.Second,
	})
	if err != nil {
		t.Fatal(err)
	}
	return server, client, svc
}

func TestClient_GetProductCard(t *testing.T) {
	server, client, svc := initTransportTest(t)
	defer server.Close()

	testCases := []struct {
		name        string
		id          uint32
		productCard *catalog.ProductCard
		err         error
	}{
		{
			name:        "normal response",
			id:          8943242,
			productCard: mustNewProductCard(t, nil),
			err:         nil,
		},
		{
			name:        "error response",
			id:          589954,
			productCard: nil,
			err:         catalog.ErrNotFound("no such product id: %d", 123),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			svc.onGetProductCard = func(ctx context.Context, id uint32) (*catalog.ProductCard, error) {
				if id != tc.id {
					return nil, fmt.Errorf("want: %d; got: %d", tc.id, id)
				}
				return tc.productCard, tc.err
			}

			resp, err := client.GetProductCard(context.Background(), tc.id)
			assert.Equal(t, tc.productCard, resp)
			assert.Equal(t, tc.err, err)
		})
	}
}

func TestClient_GetProductCardList(t *testing.T) {
	server, client, svc := initTransportTest(t)
	defer server.Close()

	testCases := []struct {
		name            string
		ids             []uint32
		productCardList []*catalog.ProductCard
		err             error
	}{
		{
			name: "normal response",
			ids:  []uint32{43523, 423543},
			productCardList: []*catalog.ProductCard{
				mustNewProductCard(t, func(card *catalog.ProductCard) { card.ID = 43523 }),
				mustNewProductCard(t, func(card *catalog.ProductCard) { card.ID = 423543 }),
			},
			err: nil,
		},
		{
			name:            "error response",
			ids:             []uint32{43523, 423543},
			productCardList: nil,
			err:             catalog.ErrBadRequest("max number of ids per query: %d", 100),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			svc.onGetProductCardList = func(ctx context.Context, ids []uint32) (cards []*catalog.ProductCard, err error) {
				if len(ids) != len(tc.ids) {
					return nil, fmt.Errorf("want: %v; got: %v", tc.ids, ids)
				}
				for i, v := range ids {
					if v != tc.ids[i] {
						return nil, fmt.Errorf("want: %v; got: %v", tc.ids, ids)
					}
				}
				return tc.productCardList, tc.err
			}

			resp, err := client.GetProductCardList(context.Background(), tc.ids)
			assert.Equal(t, tc.productCardList, resp)
			assert.Equal(t, tc.err, err)
		})
	}
}

func mustNewProductCard(t *testing.T, fn func(card *catalog.ProductCard)) *catalog.ProductCard {
	pc := &catalog.ProductCard{
		ID: 65435,
		Properties: map[string]interface{}{
			"title": "Кроссовки ABC123",
			"price": float64(1999),
			"color": []interface{}{"red", "white"},
		},
	}

	if fn != nil {
		fn(pc)
	}

	return pc
}
