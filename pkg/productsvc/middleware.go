package productsvc

import (
	"context"
	"strconv"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/go-kit/kit/metrics"
	kitprometheus "github.com/go-kit/kit/metrics/prometheus"
	"github.com/prometheus/client_golang/prometheus"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

func NewLoggingMiddleware(next Service, logger log.Logger) *LoggingMiddleware {
	return &LoggingMiddleware{
		logger: logger,
		next:   next,
	}
}

type LoggingMiddleware struct {
	logger log.Logger
	next   Service
}

func (mw *LoggingMiddleware) GetProductCardList(ctx context.Context, ids []uint32) ([]*catalog.ProductCard, error) {
	begin := time.Now()
	resp, err := mw.next.GetProductCardList(ctx, ids)
	if err != nil {
		level.Error(mw.logger).Log("method", "GetProductCardList", "err", err, "took", time.Since(begin))
	}
	return resp, err
}

func (mw *LoggingMiddleware) GetProductCard(ctx context.Context, id uint32) (*catalog.ProductCard, error) {
	begin := time.Now()
	resp, err := mw.next.GetProductCard(ctx, id)
	if err != nil {
		level.Error(mw.logger).Log("method", "GetProductCard", "err", err, "took", time.Since(begin))
	}
	return resp, err
}

func NewInstrumentingMiddleware(next Service, prefix string) *InstrumentingMiddleware {
	return &InstrumentingMiddleware{
		next: next,
		reqMetrics: kitprometheus.NewHistogramFrom(
			prometheus.HistogramOpts{
				Name: prefix + "_requests",
				Help: "Requests Info",
				Buckets: []float64{
					0.001, 0.002, 0.003,
					0.004, 0.005, 0.010,
					0.015, 0.030, 0.050,
					0.100, 0.3, 0.5,
					1, 2, 5,
				},
			},
			[]string{"method", "error"},
		),
	}
}

type InstrumentingMiddleware struct {
	next       Service
	reqMetrics metrics.Histogram
}

func (mw *InstrumentingMiddleware) GetProductCardList(ctx context.Context, ids []uint32) ([]*catalog.ProductCard, error) {
	begin := time.Now()
	resp, err := mw.next.GetProductCardList(ctx, ids)
	labels := []string{"method", "GetProductCardList", "error", strconv.FormatBool(err != nil)}
	mw.reqMetrics.With(labels...).Observe(time.Since(begin).Seconds())
	return resp, err
}

func (mw *InstrumentingMiddleware) GetProductCard(ctx context.Context, id uint32) (*catalog.ProductCard, error) {
	begin := time.Now()
	resp, err := mw.next.GetProductCard(ctx, id)
	labels := []string{"method", "GetProductCard", "error", strconv.FormatBool(err != nil)}
	mw.reqMetrics.With(labels...).Observe(time.Since(begin).Seconds())
	return resp, err
}
