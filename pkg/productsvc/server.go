package productsvc

import (
	"context"
	"fmt"
	"net/http"
	"net/http/pprof"
	"time"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

// ServerConfig is a server configuration.
type ServerConfig struct {
	Logger                  log.Logger
	MemStore                MemStore
	Port                    string
	MaxProductCardsPerQuery int
	ReadTimeout             time.Duration
	WriteTimeout            time.Duration
	ShutdownTimeout         time.Duration
	ReadyFunc               func() bool
	MetricPrefix            string
}

// MemStore stores product cards.
type MemStore interface {
	GetProductCardList(ids []uint32) []*catalog.ProductCard
	GetProductCard(id uint32) (*catalog.ProductCard, bool)
}

// Server is a product service server.
type Server struct {
	cfg *ServerConfig
	srv *http.Server
}

// NewServer creates a new server.
func NewServer(cfg ServerConfig) (*Server, error) {
	var svc Service
	svc = newService(cfg.Logger, cfg.MemStore, cfg.MaxProductCardsPerQuery)
	svc = NewLoggingMiddleware(svc, cfg.Logger)
	svc = NewInstrumentingMiddleware(svc, cfg.MetricPrefix)

	router := http.NewServeMux()
	router.Handle("/metrics", promhttp.Handler())
	router.Handle("/ready", readyHandler(cfg.ReadyFunc))
	router.HandleFunc("/debug/pprof/profile", pprof.Profile)
	router.Handle("/debug/pprof/heap", pprof.Handler("heap"))
	router.Handle("/api/v1/", makeHandler(svc))

	srv := &http.Server{
		Handler:      router,
		Addr:         ":" + cfg.Port,
		ReadTimeout:  cfg.ReadTimeout,
		WriteTimeout: cfg.WriteTimeout,
	}

	s := &Server{
		cfg: &cfg,
		srv: srv,
	}

	return s, nil
}

// Serve starts HTTP server and stops it when the provided context is canceled.
func (s *Server) Serve(ctx context.Context) error {
	errChan := make(chan error, 1)
	go func() {
		errChan <- s.srv.ListenAndServe()
	}()

	select {
	case err := <-errChan:
		return err

	case <-ctx.Done():
		ctxShutdown, cancel := context.WithTimeout(context.Background(), s.cfg.ShutdownTimeout)
		defer cancel()
		if err := s.srv.Shutdown(ctxShutdown); err != nil {
			return fmt.Errorf("failed to shutdown server: %w", err)
		}
		return nil
	}
}

func makeHandler(svc Service) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorEncoder(encodeError),
	}

	router := mux.NewRouter()

	router.Path("/api/v1/products/{id}").Methods(http.MethodGet).Handler(kithttp.NewServer(
		makeGetProductCardEndpoint(svc),
		decodeGetProductCardRequest,
		encodeGetProductCardResponse,
		opts...,
	))

	router.Path("/api/v1/products").Methods(http.MethodGet).Handler(kithttp.NewServer(
		makeGetProductCardListEndpoint(svc),
		decodeGetProductCardListRequest,
		encodeGetProductCardListResponse,
		opts...,
	))

	return router
}

func makeGetProductCardEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getProductCardRequest)
		resp, err := svc.GetProductCard(ctx, req.ID)
		return getProductCardResponse{ProductCard: resp}, err
	}
}

func makeGetProductCardListEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getProductCardListRequest)
		resp, err := svc.GetProductCardList(ctx, req.IDs)
		return getProductCardListResponse{ProductCardList: resp, Err: err}, nil
	}
}

func readyHandler(readyFunc func() bool) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if readyFunc() {
			w.WriteHeader(http.StatusOK)
		} else {
			w.WriteHeader(http.StatusServiceUnavailable)
		}
	})
}
