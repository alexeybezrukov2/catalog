package productsvc

import (
	"context"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"

	"github.com/go-kit/kit/log"
)

type Service interface {
	GetProductCardList(ctx context.Context, ids []uint32) ([]*catalog.ProductCard, error)
	GetProductCard(ctx context.Context, id uint32) (*catalog.ProductCard, error)
}

type service struct {
	logger   log.Logger
	memStore MemStore

	maxProductCardsPerQuery int
}

func newService(logger log.Logger, memStore MemStore, maxProductCardsPerQuery int) *service {
	return &service{
		logger:                  logger,
		memStore:                memStore,
		maxProductCardsPerQuery: maxProductCardsPerQuery,
	}
}

func (s *service) GetProductCardList(ctx context.Context, ids []uint32) ([]*catalog.ProductCard, error) {
	if len(ids) == 0 {
		return nil, catalog.ErrBadRequest("must provide product ids")
	}

	if len(ids) > s.maxProductCardsPerQuery {
		return nil, catalog.ErrBadRequest("max number of ids per query: %d", s.maxProductCardsPerQuery)
	}

	return s.memStore.GetProductCardList(ids), nil
}

func (s *service) GetProductCard(ctx context.Context, id uint32) (*catalog.ProductCard, error) {
	productCard, ok := s.memStore.GetProductCard(id)
	if !ok {
		return nil, catalog.ErrNotFound("no such product id: %d", id)
	}
	return productCard, nil
}
