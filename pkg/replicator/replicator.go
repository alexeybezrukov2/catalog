package replicator

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"sync/atomic"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/go-kit/kit/metrics"
	kitprometheus "github.com/go-kit/kit/metrics/prometheus"
	"github.com/prometheus/client_golang/prometheus"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

// Storage is a persistent catalog data storage.
type Storage interface {
	ForeachCategory(ctx context.Context, fn func(*catalog.Category) error) error
	ForeachProperty(ctx context.Context, fn func(*catalog.Property) error) error
	ForeachPropertyVariant(ctx context.Context, fn func(*catalog.PropertyVariant) error) error
	ForeachProduct(ctx context.Context, fn func(*catalog.Product) error) error
}

// EventQueue provides replication events.
type EventQueue interface {
	Subscribe() (<-chan catalog.Event, error)
}

// Index indexes and queries documents.
type Index interface {
	AddMapping(m *catalog.IndexMapping)
	DropMapping(m *catalog.IndexMapping)
	Insert(id uint32, doc *catalog.IndexDocument)
	Update(id uint32, doc *catalog.IndexDocument)
	Delete(id uint32)
}

// MemStore is an in-memory catalog data storage.
type MemStore interface {
	DeleteProductCard(id uint32)
	UpsertProductCard(pc *catalog.ProductCard)
	PatchProductCard(id uint32, properties []*catalog.ProductProperty)
	DeleteCategory(id uint32)
	UpsertCategory(c *catalog.Category)
	DeleteProperty(id uint32)
	UpsertProperty(p *catalog.Property)
	DeletePropertyVariant(id uint32)
	UpsertPropertyVariant(pv *catalog.PropertyVariant)
}

// Config is a replicator configuration.
type Config struct {
	Logger                 log.Logger
	Storage                Storage
	EventQueue             EventQueue
	Index                  Index
	MemStore               MemStore
	MemStoreOperationTypes []catalog.OperationType
	MetricPrefix           string
}

func (cfg Config) validate() error {
	if cfg.Logger == nil {
		return errors.New("must provide Logger")
	}
	if cfg.Storage == nil {
		return errors.New("must provide Storage")
	}
	if cfg.EventQueue == nil {
		return errors.New("must provide EventQueue")
	}
	if cfg.Index == nil && cfg.MemStore == nil {
		return errors.New("must provide Index or MemStore")
	}
	if cfg.MemStore != nil && len(cfg.MemStoreOperationTypes) == 0 {
		return errors.New("must provide MemStoreOperationTypes")
	}
	if cfg.MetricPrefix == "" {
		return errors.New("must provide MetricPrefix")
	}
	return nil
}

// Replicator replicates catalog data to MemStore or Index.
// It uses Storage to load the initial dataset and EventQueue to apply the updates.
type Replicator struct {
	logger        log.Logger
	storage       Storage
	eventQueue    EventQueue
	index         Index
	memStore      MemStore
	memStoreTypes map[catalog.OperationType]bool
	ready         uint32
	counter       metrics.Counter
}

// New creates a new replicator.
func New(cfg Config) (*Replicator, error) {
	err := cfg.validate()
	if err != nil {
		return nil, fmt.Errorf("invalid configuration: %w", err)
	}

	counter := kitprometheus.NewCounterFrom(
		prometheus.CounterOpts{
			Name: cfg.MetricPrefix + "_events",
		},
		[]string{"error"},
	)

	types := make(map[catalog.OperationType]bool, len(cfg.MemStoreOperationTypes))
	for _, ot := range cfg.MemStoreOperationTypes {
		types[ot] = true
	}

	r := &Replicator{
		logger:        cfg.Logger,
		storage:       cfg.Storage,
		eventQueue:    cfg.EventQueue,
		index:         cfg.Index,
		memStore:      cfg.MemStore,
		memStoreTypes: types,
		counter:       counter,
	}

	return r, nil
}

func (r *Replicator) setReady(val bool) {
	if val {
		atomic.StoreUint32(&r.ready, 1)
	} else {
		atomic.StoreUint32(&r.ready, 0)
	}
}

// Ready returns true if the replicator successfully completed reading catalog data from
// the storage and started to handle replication events from the event queue.
func (r *Replicator) Ready() bool {
	return atomic.LoadUint32(&r.ready) == 1
}

// Replicate starts the data replication and stops it when the provided context is canceled.
func (r *Replicator) Replicate(ctx context.Context) error {
	if err := r.loadDataFromStorage(ctx); err != nil {
		return fmt.Errorf("failed to load data from storage: %w", err)
	}
	r.setReady(true)
	if err := r.handleReplicationEvents(ctx); err != nil {
		return fmt.Errorf("failed to handle replication events from event queue: %w", err)
	}
	return nil
}

func (r *Replicator) loadDataFromStorage(ctx context.Context) error {
	if err := r.loadCategories(ctx); err != nil {
		return fmt.Errorf("failed to load categories: %w", err)
	}
	if err := r.loadProperties(ctx); err != nil {
		return fmt.Errorf("failed to load properties: %w", err)
	}
	if err := r.loadPropertyVariants(ctx); err != nil {
		return fmt.Errorf("failed to load property variants: %w", err)
	}
	if err := r.loadProducts(ctx); err != nil {
		return fmt.Errorf("failed to load products: %w", err)
	}
	return nil
}

func (r *Replicator) loadCategories(ctx context.Context) error {
	if r.memStore == nil || !r.memStoreTypes[catalog.OperationTypeCategory] {
		return nil
	}
	return r.storage.ForeachCategory(ctx, func(category *catalog.Category) error {
		return r.processOperation(&catalog.Operation{
			Type:     catalog.OperationTypeCategory,
			Method:   catalog.OperationMethodUpsert,
			Category: category,
		})
	})
}

func (r *Replicator) loadProperties(ctx context.Context) error {
	if r.index == nil && (r.memStore == nil || !r.memStoreTypes[catalog.OperationTypeProperty]) {
		return nil
	}
	return r.storage.ForeachProperty(ctx, func(property *catalog.Property) error {
		return r.processOperation(&catalog.Operation{
			Type:     catalog.OperationTypeProperty,
			Method:   catalog.OperationMethodUpsert,
			Property: property,
		})
	})
}

func (r *Replicator) loadPropertyVariants(ctx context.Context) error {
	if r.memStore == nil || !r.memStoreTypes[catalog.OperationTypePropertyVariant] {
		return nil
	}
	return r.storage.ForeachPropertyVariant(ctx, func(propertyVariant *catalog.PropertyVariant) error {
		return r.processOperation(&catalog.Operation{
			Type:            catalog.OperationTypePropertyVariant,
			Method:          catalog.OperationMethodUpsert,
			PropertyVariant: propertyVariant,
		})
	})
}

func (r *Replicator) loadProducts(ctx context.Context) error {
	if r.index == nil && (r.memStore == nil || !r.memStoreTypes[catalog.OperationTypeProduct]) {
		return nil
	}
	return r.storage.ForeachProduct(ctx, func(product *catalog.Product) error {
		return r.processOperation(&catalog.Operation{
			Type:    catalog.OperationTypeProduct,
			Method:  catalog.OperationMethodUpsert,
			Product: product,
		})
	})
}

func (r *Replicator) handleReplicationEvents(ctx context.Context) error {
	eventCh, err := r.eventQueue.Subscribe()
	if err != nil {
		return fmt.Errorf("failed to subscribe to event queue: %w", err)
	}
	for {
		select {
		case event, ok := <-eventCh:
			if !ok {
				return errors.New("event channel is closed")
			}

			err := r.processOperation(event.Operation())
			if err != nil {
				level.Error(r.logger).Log("msg", "failed to process replication operation", "err", err)
				continue
			}

			err = event.Ack()
			if err != nil {
				level.Error(r.logger).Log("msg", "failed to ack event", "err", err)
				continue
			}

		case <-ctx.Done():
			return nil
		}
	}
}

func (r *Replicator) processOperation(op *catalog.Operation) (err error) {
	defer r.recordMetrics(&err)

	if err := op.Validate(); err != nil {
		return fmt.Errorf("invalid operation: %w", err)
	}

	if r.index != nil {
		if err := r.processOperationWithIndex(op); err != nil {
			return fmt.Errorf("failed to process operation with index: %w", err)
		}
	}
	if r.memStore != nil {
		if err := r.processOperationWithMemStore(op); err != nil {
			return fmt.Errorf("failed to process operation with memstore: %w", err)
		}
	}

	return nil
}

func (r *Replicator) processOperationWithIndex(op *catalog.Operation) error {
	switch op.Type {
	case catalog.OperationTypeProperty:
		mapping, ok := propertyToIndexMapping(op.Property)
		if !ok {
			return nil
		}
		switch op.Method {
		case catalog.OperationMethodDelete:
			r.index.DropMapping(mapping)

		case catalog.OperationMethodUpsert:
			if op.Property.Indexed {
				r.index.AddMapping(mapping)
			} else {
				r.index.DropMapping(mapping)
			}
		}

	case catalog.OperationTypeProduct:
		switch op.Method {
		case catalog.OperationMethodDelete:
			r.index.Delete(op.Product.ID)

		case catalog.OperationMethodUpsert:
			doc, err := op.Product.ToIndexDocument()
			if err != nil {
				return fmt.Errorf("failed to convert product to index document: %w", err)
			}
			r.index.Insert(op.Product.ID, doc)

		case catalog.OperationMethodPatch:
			doc, err := op.Product.ToIndexDocument()
			if err != nil {
				return fmt.Errorf("failed to convert product to index document: %w", err)
			}
			if len(doc.EnumFields) > 0 || len(doc.RangeFields) > 0 {
				r.index.Update(op.Product.ID, doc)
			}
		}
	}
	return nil
}

func (r *Replicator) processOperationWithMemStore(op *catalog.Operation) error {
	if !r.memStoreTypes[op.Type] {
		return nil
	}

	switch op.Type {
	case catalog.OperationTypeProduct:
		switch op.Method {
		case catalog.OperationMethodDelete:
			r.memStore.DeleteProductCard(op.Product.ID)

		case catalog.OperationMethodUpsert:
			r.memStore.UpsertProductCard(op.Product.ToProductCard())

		case catalog.OperationMethodPatch:
			r.memStore.PatchProductCard(op.Product.ID, op.Product.Properties)
		}

	case catalog.OperationTypeProperty:
		switch op.Method {
		case catalog.OperationMethodDelete:
			r.memStore.DeleteProperty(op.Property.ID)

		case catalog.OperationMethodUpsert:
			r.memStore.UpsertProperty(op.Property)
		}

	case catalog.OperationTypePropertyVariant:
		switch op.Method {
		case catalog.OperationMethodDelete:
			r.memStore.DeletePropertyVariant(op.PropertyVariant.ID)

		case catalog.OperationMethodUpsert:
			r.memStore.UpsertPropertyVariant(op.PropertyVariant)
		}

	case catalog.OperationTypeCategory:
		switch op.Method {
		case catalog.OperationMethodDelete:
			r.memStore.DeleteCategory(op.Category.ID)

		case catalog.OperationMethodUpsert:
			r.memStore.UpsertCategory(op.Category)
		}
	}
	return nil
}

func (r *Replicator) recordMetrics(err *error) {
	labels := []string{"error", strconv.FormatBool(*err != nil)}
	r.counter.With(labels...).Add(1)
}

func propertyToIndexMapping(p *catalog.Property) (*catalog.IndexMapping, bool) {
	switch p.Type {
	case catalog.PropertyTypeEnum, catalog.PropertyTypeEnumList:
		return &catalog.IndexMapping{Type: catalog.IndexTypeEnum, Key: p.ID}, true
	case catalog.PropertyTypeInt, catalog.PropertyTypeDate:
		return &catalog.IndexMapping{Type: catalog.IndexTypeRange, Key: p.ID}, true
	default:
		return nil, false
	}
}
