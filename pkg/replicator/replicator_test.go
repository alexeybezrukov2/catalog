package replicator

import (
	"context"
	"os"
	"sync"
	"testing"

	"github.com/go-kit/kit/log"
	"github.com/stretchr/testify/assert"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

type mockStorage struct {
	onForeachCategory        func(ctx context.Context, fn func(*catalog.Category) error) error
	onForeachProperty        func(ctx context.Context, fn func(*catalog.Property) error) error
	onForeachPropertyVariant func(ctx context.Context, fn func(*catalog.PropertyVariant) error) error
	onForeachProduct         func(ctx context.Context, fn func(*catalog.Product) error) error
}

func (s *mockStorage) ForeachCategory(ctx context.Context, fn func(*catalog.Category) error) error {
	return s.onForeachCategory(ctx, fn)
}

func (s *mockStorage) ForeachProperty(ctx context.Context, fn func(*catalog.Property) error) error {
	return s.onForeachProperty(ctx, fn)
}

func (s *mockStorage) ForeachPropertyVariant(ctx context.Context, fn func(*catalog.PropertyVariant) error) error {
	return s.onForeachPropertyVariant(ctx, fn)
}

func (s *mockStorage) ForeachProduct(ctx context.Context, fn func(*catalog.Product) error) error {
	return s.onForeachProduct(ctx, fn)
}

type mockEventQueue struct {
	onSubscribe func() (<-chan catalog.Event, error)
}

func (q *mockEventQueue) Subscribe() (<-chan catalog.Event, error) {
	return q.onSubscribe()
}

type mockEvent struct {
	operation *catalog.Operation
	sequence  uint64
	ack       func() error
}

func (e *mockEvent) Operation() *catalog.Operation {
	return e.operation
}

func (e *mockEvent) Sequence() uint64 {
	return e.sequence
}

func (e *mockEvent) Ack() error {
	return e.ack()
}

type mockIndex struct {
	onAddMapping  func(m *catalog.IndexMapping)
	onDropMapping func(m *catalog.IndexMapping)
	onInsert      func(id uint32, doc *catalog.IndexDocument)
	onUpdate      func(id uint32, doc *catalog.IndexDocument)
	onDelete      func(id uint32)
}

func (idx *mockIndex) AddMapping(m *catalog.IndexMapping) {
	idx.onAddMapping(m)
}

func (idx *mockIndex) DropMapping(m *catalog.IndexMapping) {
	idx.onDropMapping(m)
}

func (idx *mockIndex) Insert(id uint32, doc *catalog.IndexDocument) {
	idx.onInsert(id, doc)
}

func (idx *mockIndex) Update(id uint32, doc *catalog.IndexDocument) {
	idx.onUpdate(id, doc)
}

func (idx *mockIndex) Delete(id uint32) {
	idx.onDelete(id)
}

type mockMemStore struct {
	onDeleteProductCard     func(id uint32)
	onUpsertProductCard     func(pc *catalog.ProductCard)
	onPatchProductCard      func(id uint32, properties []*catalog.ProductProperty)
	onDeleteCategory        func(id uint32)
	onUpsertCategory        func(c *catalog.Category)
	onDeleteProperty        func(id uint32)
	onUpsertProperty        func(p *catalog.Property)
	onDeletePropertyVariant func(id uint32)
	onUpsertPropertyVariant func(pv *catalog.PropertyVariant)
}

func (s *mockMemStore) DeleteProductCard(id uint32) {
	s.onDeleteProductCard(id)
}

func (s *mockMemStore) UpsertProductCard(pc *catalog.ProductCard) {
	s.onUpsertProductCard(pc)
}

func (s *mockMemStore) PatchProductCard(id uint32, properties []*catalog.ProductProperty) {
	s.onPatchProductCard(id, properties)
}

func (s *mockMemStore) DeleteCategory(id uint32) {
	s.onDeleteCategory(id)
}

func (s *mockMemStore) UpsertCategory(c *catalog.Category) {
	s.onUpsertCategory(c)
}

func (s *mockMemStore) DeleteProperty(id uint32) {
	s.onDeleteProperty(id)
}

func (s *mockMemStore) UpsertProperty(p *catalog.Property) {
	s.onUpsertProperty(p)
}

func (s *mockMemStore) DeletePropertyVariant(id uint32) {
	s.onDeletePropertyVariant(id)
}

func (s *mockMemStore) UpsertPropertyVariant(pv *catalog.PropertyVariant) {
	s.onUpsertPropertyVariant(pv)
}

func TestReplicator(t *testing.T) {
	storage := &mockStorage{
		onForeachCategory: func(ctx context.Context, fn func(*catalog.Category) error) error {
			return fn(&catalog.Category{ID: 111})
		},
		onForeachProperty: func(ctx context.Context, fn func(*catalog.Property) error) error {
			return fn(&catalog.Property{ID: 222, Type: catalog.PropertyTypeEnumList, Indexed: true})
		},
		onForeachPropertyVariant: func(ctx context.Context, fn func(*catalog.PropertyVariant) error) error {
			return fn(&catalog.PropertyVariant{ID: 333, PropertyID: 444, Value: "555"})
		},
		onForeachProduct: func(ctx context.Context, fn func(*catalog.Product) error) error {
			return fn(&catalog.Product{
				ID:       666,
				SchemaID: 666666,
				Properties: []*catalog.ProductProperty{
					{Indexed: true, Type: catalog.PropertyTypeEnumList, ID: 777, ValueIDs: []uint32{888}},
				},
			})
		},
	}

	events := []*mockEvent{
		{
			operation: &catalog.Operation{
				Type:     catalog.OperationTypeProperty,
				Method:   catalog.OperationMethodDelete,
				Property: &catalog.Property{ID: 1, Type: catalog.PropertyTypeEnumList, Indexed: true},
			},
		},
		{
			operation: &catalog.Operation{
				Type:     catalog.OperationTypeProperty,
				Method:   catalog.OperationMethodUpsert,
				Property: &catalog.Property{ID: 2, Type: catalog.PropertyTypeInt, Indexed: true},
			},
		},
		{
			operation: &catalog.Operation{
				Type:            catalog.OperationTypePropertyVariant,
				Method:          catalog.OperationMethodDelete,
				PropertyVariant: &catalog.PropertyVariant{ID: 1, PropertyID: 2, Value: "test3"},
			},
		},
		{
			operation: &catalog.Operation{
				Type:            catalog.OperationTypePropertyVariant,
				Method:          catalog.OperationMethodUpsert,
				PropertyVariant: &catalog.PropertyVariant{ID: 4, PropertyID: 5, Value: "test6"},
			},
		},
		{
			operation: &catalog.Operation{
				Type:   catalog.OperationTypeProduct,
				Method: catalog.OperationMethodUpsert,
				Product: &catalog.Product{
					ID:       1,
					SchemaID: 111,
					Properties: []*catalog.ProductProperty{
						{Indexed: true, Type: catalog.PropertyTypeEnumList, ID: 2, ValueIDs: []uint32{3}},
					},
				},
			},
		},
		{
			operation: &catalog.Operation{
				Type:   catalog.OperationTypeProduct,
				Method: catalog.OperationMethodPatch,
				Product: &catalog.Product{
					ID:       4,
					SchemaID: 444,
					Properties: []*catalog.ProductProperty{
						{Indexed: true, Type: catalog.PropertyTypeEnumList, ID: 5, ValueIDs: []uint32{6}},
					},
				},
			},
		},
		{
			operation: &catalog.Operation{
				Type:   catalog.OperationTypeProduct,
				Method: catalog.OperationMethodDelete,
				Product: &catalog.Product{
					ID: 7,
				},
			},
		},
		{
			operation: &catalog.Operation{
				Type:     catalog.OperationTypeCategory,
				Method:   catalog.OperationMethodDelete,
				Category: &catalog.Category{ID: 8},
			},
		},
		{
			operation: &catalog.Operation{
				Type:     catalog.OperationTypeCategory,
				Method:   catalog.OperationMethodUpsert,
				Category: &catalog.Category{ID: 8, Name: "Something"},
			},
		},
	}
	acks := make(chan struct{}, len(events))
	for _, e := range events {
		e.ack = func() error {
			acks <- struct{}{}
			return nil
		}
	}
	eq := &mockEventQueue{
		onSubscribe: func() (<-chan catalog.Event, error) {
			ch := make(chan catalog.Event, len(events))
			for _, e := range events {
				ch <- e
			}
			return ch, nil
		},
	}

	wantIndexCalls := [][]interface{}{
		{
			"AddMapping",
			&catalog.IndexMapping{Key: 222, Type: catalog.IndexTypeEnum},
		},
		{
			"Insert",
			uint32(666),
			&catalog.IndexDocument{EnumFields: map[uint32][]uint32{catalog.IndexKeySchemaID: {666666}, 777: {888}}, RangeFields: map[uint32]uint64{}},
		},
		{
			"DropMapping",
			&catalog.IndexMapping{Key: 1, Type: catalog.IndexTypeEnum},
		},
		{
			"AddMapping",
			&catalog.IndexMapping{Key: 2, Type: catalog.IndexTypeRange},
		},
		{
			"Insert",
			uint32(1),
			&catalog.IndexDocument{EnumFields: map[uint32][]uint32{catalog.IndexKeySchemaID: {111}, 2: {3}}, RangeFields: map[uint32]uint64{}},
		},
		{
			"Update",
			uint32(4),
			&catalog.IndexDocument{EnumFields: map[uint32][]uint32{catalog.IndexKeySchemaID: {444}, 5: {6}}, RangeFields: map[uint32]uint64{}},
		},
		{
			"Delete",
			uint32(7),
		},
	}

	wantMemStoreCalls := [][]interface{}{
		{
			"UpsertCategory",
			&catalog.Category{ID: 111},
		},
		{
			"UpsertProperty",
			&catalog.Property{ID: 222, Type: catalog.PropertyTypeEnumList, Indexed: true},
		},
		{
			"UpsertPropertyVariant",
			&catalog.PropertyVariant{ID: 333, PropertyID: 444, Value: "555"},
		},
		{
			"UpsertProductCard",
			(&catalog.Product{
				ID: 666,
				Properties: []*catalog.ProductProperty{
					{Indexed: true, Type: catalog.PropertyTypeEnumList, ID: 777, ValueIDs: []uint32{888}},
				},
			}).ToProductCard(),
		},
		{
			"DeleteProperty",
			uint32(1),
		},
		{
			"UpsertProperty",
			&catalog.Property{ID: 2, Type: catalog.PropertyTypeInt, Indexed: true},
		},
		{
			"DeletePropertyVariant",
			uint32(1),
		},
		{
			"UpsertPropertyVariant",
			&catalog.PropertyVariant{ID: 4, PropertyID: 5, Value: "test6"},
		},
		{
			"UpsertProductCard",
			(&catalog.Product{
				ID: 1,
				Properties: []*catalog.ProductProperty{
					{Indexed: true, Type: catalog.PropertyTypeEnumList, ID: 2, ValueIDs: []uint32{3}},
				},
			}).ToProductCard(),
		},
		{
			"PatchProductCard",
			uint32(4),
			[]*catalog.ProductProperty{
				{Indexed: true, Type: catalog.PropertyTypeEnumList, ID: 5, ValueIDs: []uint32{6}},
			},
		},
		{
			"DeleteProductCard",
			uint32(7),
		},
		{
			"DeleteCategory",
			uint32(8),
		},
		{
			"UpsertCategory",
			&catalog.Category{ID: 8, Name: "Something"},
		},
	}

	gotIndexCalls := [][]interface{}{}

	index := &mockIndex{
		onAddMapping: func(m *catalog.IndexMapping) {
			gotIndexCalls = append(gotIndexCalls, []interface{}{"AddMapping", m})
		},
		onDropMapping: func(m *catalog.IndexMapping) {
			gotIndexCalls = append(gotIndexCalls, []interface{}{"DropMapping", m})
		},
		onInsert: func(id uint32, doc *catalog.IndexDocument) {
			gotIndexCalls = append(gotIndexCalls, []interface{}{"Insert", id, doc})
		},
		onUpdate: func(id uint32, doc *catalog.IndexDocument) {
			gotIndexCalls = append(gotIndexCalls, []interface{}{"Update", id, doc})
		},
		onDelete: func(id uint32) {
			gotIndexCalls = append(gotIndexCalls, []interface{}{"Delete", id})
		},
	}

	gotMemStoreCalls := [][]interface{}{}

	memStore := &mockMemStore{
		onDeleteProductCard: func(id uint32) {
			gotMemStoreCalls = append(gotMemStoreCalls, []interface{}{"DeleteProductCard", id})
		},
		onUpsertProductCard: func(pc *catalog.ProductCard) {
			gotMemStoreCalls = append(gotMemStoreCalls, []interface{}{"UpsertProductCard", pc})
		},
		onPatchProductCard: func(id uint32, properties []*catalog.ProductProperty) {
			gotMemStoreCalls = append(gotMemStoreCalls, []interface{}{"PatchProductCard", id, properties})
		},
		onDeleteCategory: func(id uint32) {
			gotMemStoreCalls = append(gotMemStoreCalls, []interface{}{"DeleteCategory", id})
		},
		onUpsertCategory: func(c *catalog.Category) {
			gotMemStoreCalls = append(gotMemStoreCalls, []interface{}{"UpsertCategory", c})
		},
		onDeleteProperty: func(id uint32) {
			gotMemStoreCalls = append(gotMemStoreCalls, []interface{}{"DeleteProperty", id})
		},
		onUpsertProperty: func(p *catalog.Property) {
			gotMemStoreCalls = append(gotMemStoreCalls, []interface{}{"UpsertProperty", p})
		},
		onDeletePropertyVariant: func(id uint32) {
			gotMemStoreCalls = append(gotMemStoreCalls, []interface{}{"DeletePropertyVariant", id})
		},
		onUpsertPropertyVariant: func(pv *catalog.PropertyVariant) {
			gotMemStoreCalls = append(gotMemStoreCalls, []interface{}{"UpsertPropertyVariant", pv})
		},
	}

	r, err := New(Config{
		Logger:     log.NewLogfmtLogger(log.NewSyncWriter(os.Stderr)),
		Storage:    storage,
		EventQueue: eq,
		Index:      index,
		MemStore:   memStore,
		MemStoreOperationTypes: []catalog.OperationType{
			catalog.OperationTypeProduct,
			catalog.OperationTypeProperty,
			catalog.OperationTypePropertyVariant,
			catalog.OperationTypeCategory,
		},
		MetricPrefix: "test_replicator",
	})
	if err != nil {
		t.Fatal(err)
	}

	ctx, cancel := context.WithCancel(context.Background())
	var (
		wg      sync.WaitGroup
		replErr error
	)
	wg.Add(1)
	go func() {
		replErr = r.Replicate(ctx)
		wg.Done()
	}()

	for range events {
		<-acks
	}

	cancel()
	wg.Wait()

	assert.NoError(t, replErr)
	assert.Equal(t, wantIndexCalls, gotIndexCalls)
	assert.Equal(t, wantMemStoreCalls, gotMemStoreCalls)
}
