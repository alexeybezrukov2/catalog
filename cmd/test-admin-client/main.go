package main

import (
	"context"
	"log"
	"time"

	"github.com/kelseyhightower/envconfig"

	"git.wildberries.ru/lifestyle/catalog/pkg/adminsvc"
	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
)

type configuration struct {
	AdminServiceURL string        `envconfig:"ADMIN_SERVICE_URL" required:"true"`
	Delay           time.Duration `envconfig:"DELAY" default:"10s"`
}

func main() {
	var cfg configuration
	if err := envconfig.Process("", &cfg); err != nil {
		log.Fatalf("failed to load configuration: %s", err)
	}

	c, err := adminsvc.NewClient(adminsvc.ClientConfig{
		ServiceURL: cfg.AdminServiceURL,
		Timeout:    5 * time.Second,
	})
	if err != nil {
		log.Fatalf("failed to create client: %s", err)
	}

	log.Printf("sleeping %v", cfg.Delay)
	time.Sleep(cfg.Delay)

	ctx := context.Background()

	createProperties(ctx, c)
	createPropertyVariants(ctx, c)
	createSchemas(ctx, c)
	createCategories(ctx, c)
	createProducts(ctx, c)

	log.Print("done")
}

var (
	idTitle, idDesc, idImages, idPrice, idField, idTags, idDateBegin uint32 // Properties.
	idFieldIT, idFieldArt, idTagProgramming, idTagGo, idTagPython    uint32 // Property variants.
	idSchemaSport, idSchemaEducation                                 uint32 // Schemas.
)

func createProperties(ctx context.Context, c *adminsvc.Client) {
	log.Print("creating properties")

	p, err := c.CreateProperty(ctx, &catalog.PropertyInput{
		Key:  "title",
		Name: "Наименование",
		Type: catalog.PropertyTypeString,
	})
	if err != nil {
		log.Fatal(err)
	}
	idTitle = p.ID

	p, err = c.CreateProperty(ctx, &catalog.PropertyInput{
		Key:  "desc",
		Name: "Описание",
		Type: catalog.PropertyTypeString,
	})
	if err != nil {
		log.Fatal(err)
	}
	idDesc = p.ID

	p, err = c.CreateProperty(ctx, &catalog.PropertyInput{
		Key:     "images",
		Name:    "Изображения",
		Type:    catalog.PropertyTypeStringList,
		Indexed: false,
	})
	if err != nil {
		log.Fatal(err)
	}
	idImages = p.ID

	p, err = c.CreateProperty(ctx, &catalog.PropertyInput{
		Key:     "price",
		Name:    "Цена",
		Type:    catalog.PropertyTypeInt,
		Indexed: true,
	})
	if err != nil {
		log.Fatal(err)
	}
	idPrice = p.ID

	p, err = c.CreateProperty(ctx, &catalog.PropertyInput{
		Key:     "field",
		Name:    "Направление обучения",
		Type:    catalog.PropertyTypeEnum,
		Indexed: true,
	})
	if err != nil {
		log.Fatal(err)
	}
	idField = p.ID

	p, err = c.CreateProperty(ctx, &catalog.PropertyInput{
		Key:     "tags",
		Name:    "Теги",
		Type:    catalog.PropertyTypeEnumList,
		Indexed: true,
	})
	if err != nil {
		log.Fatal(err)
	}
	idTags = p.ID

	p, err = c.CreateProperty(ctx, &catalog.PropertyInput{
		Key:     "date_begin",
		Name:    "Дата начала обучения",
		Type:    catalog.PropertyTypeDate,
		Indexed: false,
	})
	if err != nil {
		log.Fatal(err)
	}
	idDateBegin = p.ID
}

func createPropertyVariants(ctx context.Context, c *adminsvc.Client) {
	log.Print("creating property variants")

	pv, err := c.CreatePropertyVariant(ctx, &catalog.PropertyVariantInput{
		PropertyID: idField,
		Value:      "IT",
	})
	if err != nil {
		log.Fatal(err)
	}
	idFieldIT = pv.ID

	pv, err = c.CreatePropertyVariant(ctx, &catalog.PropertyVariantInput{
		PropertyID: idField,
		Value:      "Искусство",
	})
	if err != nil {
		log.Fatal(err)
	}
	idFieldArt = pv.ID

	pv, err = c.CreatePropertyVariant(ctx, &catalog.PropertyVariantInput{
		PropertyID: idTags,
		Value:      "Программирование",
	})
	if err != nil {
		log.Fatal(err)
	}
	idTagProgramming = pv.ID

	pv, err = c.CreatePropertyVariant(ctx, &catalog.PropertyVariantInput{
		PropertyID: idTags,
		Value:      "Go",
	})
	if err != nil {
		log.Fatal(err)
	}
	idTagGo = pv.ID

	pv, err = c.CreatePropertyVariant(ctx, &catalog.PropertyVariantInput{
		PropertyID: idTags,
		Value:      "Python",
	})
	if err != nil {
		log.Fatal(err)
	}
	idTagPython = pv.ID
}

func createSchemas(ctx context.Context, c *adminsvc.Client) {
	log.Print("creating schemas")

	s, err := c.CreateSchema(ctx, &catalog.SchemaInput{
		Name: "Спорт и ЗОЖ",
		Properties: []*catalog.SchemaPropertyInput{
			{ID: idTitle, Required: true},
			{ID: idDesc, Required: false},
			{ID: idImages, Required: true},
			{ID: idPrice, Required: true},
		},
	})
	if err != nil {
		log.Fatal(err)
	}
	idSchemaSport = s.ID

	s, err = c.CreateSchema(ctx, &catalog.SchemaInput{
		Name: "Образование",
		Properties: []*catalog.SchemaPropertyInput{
			{ID: idTitle, Required: true},
			{ID: idDesc, Required: false},
			{ID: idImages, Required: true},
			{ID: idPrice, Required: true},
			{ID: idField, Required: true},
			{ID: idTags, Required: false},
			{ID: idDateBegin, Required: false},
		},
	})
	if err != nil {
		log.Fatal(err)
	}
	idSchemaEducation = s.ID
}

func createCategories(ctx context.Context, c *adminsvc.Client) {
	log.Print("creating categories")

	_, err := c.CreateCategory(ctx, &catalog.CategoryInput{
		Name:  "Спорт и ЗОЖ",
		Order: 1,
		Filters: []*catalog.IndexFilter{
			{
				Type:         catalog.IndexTypeEnum,
				Key:          catalog.IndexKeySchemaID,
				EnumVariants: []uint32{idSchemaSport},
			},
		},
		FacetKeys: []uint32{idPrice},
	})
	if err != nil {
		log.Fatal(err)
	}

	cat, err := c.CreateCategory(ctx, &catalog.CategoryInput{
		Name:  "Образование",
		Order: 2,
		Filters: []*catalog.IndexFilter{
			{
				Type:         catalog.IndexTypeEnum,
				Key:          catalog.IndexKeySchemaID,
				EnumVariants: []uint32{idSchemaEducation},
			},
		},
		FacetKeys: []uint32{idPrice, idField},
	})
	if err != nil {
		log.Fatal(err)
	}
	idCatEducation := cat.ID

	_, err = c.CreateCategory(ctx, &catalog.CategoryInput{
		Name:     "IT",
		ParentID: idCatEducation,
		Order:    1,
		Filters: []*catalog.IndexFilter{
			{
				Type:         catalog.IndexTypeEnum,
				Key:          catalog.IndexKeySchemaID,
				EnumVariants: []uint32{idSchemaEducation},
			},
			{
				Type:         catalog.IndexTypeEnum,
				Key:          idField,
				EnumVariants: []uint32{idFieldIT},
			},
		},
		FacetKeys: []uint32{idPrice, idTags},
	})
	if err != nil {
		log.Fatal(err)
	}

	_, err = c.CreateCategory(ctx, &catalog.CategoryInput{
		Name:     "Искусство",
		ParentID: idCatEducation,
		Order:    1,
		Filters: []*catalog.IndexFilter{
			{
				Type:         catalog.IndexTypeEnum,
				Key:          catalog.IndexKeySchemaID,
				EnumVariants: []uint32{idSchemaEducation},
			},
			{
				Type:         catalog.IndexTypeEnum,
				Key:          idField,
				EnumVariants: []uint32{idFieldArt},
			},
		},
		FacetKeys: []uint32{idPrice},
	})
	if err != nil {
		log.Fatal(err)
	}
}

func createProducts(ctx context.Context, c *adminsvc.Client) {
	log.Print("creating products")

	_, err := c.CreateProduct(ctx, &catalog.ProductInput{
		SupplierID: 1,
		SchemaID:   idSchemaSport,
		Properties: []*catalog.ProductPropertyInput{
			{ID: idTitle, Value: "Жиросжигающая тренировка"},
			{ID: idDesc, Value: "Высокоинтенсивный интервальный тренинг."},
			{ID: idImages, Value: []interface{}{"img1.jpg"}},
			{ID: idPrice, Value: 1000},
		},
	})
	if err != nil {
		log.Fatal(err)
	}

	_, err = c.CreateProduct(ctx, &catalog.ProductInput{
		SupplierID: 2,
		SchemaID:   idSchemaSport,
		Properties: []*catalog.ProductPropertyInput{
			{ID: idTitle, Value: "Железный пресс"},
			{ID: idImages, Value: []interface{}{"img2.jpg", "img22.jpg"}},
			{ID: idPrice, Value: 2000},
		},
	})
	if err != nil {
		log.Fatal(err)
	}

	_, err = c.CreateProduct(ctx, &catalog.ProductInput{
		SupplierID: 1,
		SchemaID:   idSchemaEducation,
		Properties: []*catalog.ProductPropertyInput{
			{ID: idTitle, Value: "Рисуем акварелью"},
			{ID: idImages, Value: []interface{}{"img3.jpg", "img33.jpg", "img333.jpg"}},
			{ID: idPrice, Value: 100},
			{ID: idField, ValueIDs: []uint32{idFieldArt}},
		},
	})
	if err != nil {
		log.Fatal(err)
	}

	_, err = c.CreateProduct(ctx, &catalog.ProductInput{
		SupplierID: 2,
		SchemaID:   idSchemaEducation,
		Properties: []*catalog.ProductPropertyInput{
			{ID: idTitle, Value: "Рисуем пастелью"},
			{ID: idImages, Value: []interface{}{"img4.jpg"}},
			{ID: idPrice, Value: 200},
			{ID: idField, ValueIDs: []uint32{idFieldArt}},
		},
	})
	if err != nil {
		log.Fatal(err)
	}

	_, err = c.CreateProduct(ctx, &catalog.ProductInput{
		SupplierID: 1,
		SchemaID:   idSchemaEducation,
		Properties: []*catalog.ProductPropertyInput{
			{ID: idTitle, Value: "Программирование на Python"},
			{ID: idImages, Value: []interface{}{"img5.jpg", "img55.jpg"}},
			{ID: idPrice, Value: 300},
			{ID: idField, ValueIDs: []uint32{idFieldIT}},
			{ID: idTags, ValueIDs: []uint32{idTagProgramming, idTagPython}},
			{ID: idDateBegin, Value: "2021-01-01T00:00:00+03:00"},
		},
	})
	if err != nil {
		log.Fatal(err)
	}

	_, err = c.CreateProduct(ctx, &catalog.ProductInput{
		SupplierID: 2,
		SchemaID:   idSchemaEducation,
		Properties: []*catalog.ProductPropertyInput{
			{ID: idTitle, Value: "Программирование на Go"},
			{ID: idImages, Value: []interface{}{"img6.jpg", "img66.jpg", "img666.jpg"}},
			{ID: idPrice, Value: 400},
			{ID: idDesc, Value: "Курс программирования на языке Go."},
			{ID: idField, ValueIDs: []uint32{idFieldIT}},
			{ID: idTags, ValueIDs: []uint32{idTagProgramming, idTagGo}},
			{ID: idDateBegin, Value: "2020-12-31T23:59:59Z"},
		},
	})
	if err != nil {
		log.Fatal(err)
	}
}
