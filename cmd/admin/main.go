package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/kelseyhightower/envconfig"

	"git.wildberries.ru/lifestyle/catalog/pkg/adminsvc"
	"git.wildberries.ru/lifestyle/catalog/pkg/event"
	"git.wildberries.ru/lifestyle/catalog/pkg/storage"
)

const metricPrefix = "lifestyle_catalog_admin"

type configuration struct {
	Port            string        `envconfig:"PORT" required:"true"`
	ReadTimeout     time.Duration `envconfig:"READ_TIMEOUT" default:"10s"`
	WriteTimeout    time.Duration `envconfig:"WRITE_TIMEOUT" default:"10s"`
	ShutdownTimeout time.Duration `envconfig:"SHUTDOWN_TIMEOUT" default:"10s"`

	MongoURL      string `envconfig:"MONGO_URL" required:"true"`
	MongoDatabase string `envconfig:"MONGO_DATABASE" required:"true"`

	EventQueueURL              string        `envconfig:"EVENT_QUEUE_URL" required:"true"`
	EventQueueClusterID        string        `envconfig:"EVENT_QUEUE_CLUSTER_ID" required:"true"`
	EventQueueSubject          string        `envconfig:"EVENT_QUEUE_SUBJECT" required:"true"`
	EventQueueClientName       string        `envconfig:"EVENT_QUEUE_CLIENT_NAME" required:"true"`
	EventQueueAckWait          time.Duration `envconfig:"EVENT_QUEUE_ACK_WAIT" default:"1s"`
	EventQueueReconnectTimeout time.Duration `envconfig:"EVENT_QUEUE_RECONNECT_TIMEOUT" default:"1s"`
}

func main() {
	logger := log.NewLogfmtLogger(log.NewSyncWriter(os.Stderr))
	logger = log.With(logger, "caller", log.DefaultCaller)
	logger = log.With(logger, "ts", log.DefaultTimestampUTC)

	ctx, cancel := signalContext(logger)
	defer cancel()

	level.Info(logger).Log("msg", "service is starting")
	if err := run(ctx, logger); err != nil {
		level.Error(logger).Log("msg", "service is stopped with error", "err", err)
		os.Exit(1)
	}
	level.Info(logger).Log("msg", "service is stopped")
}

func run(ctx context.Context, logger log.Logger) error {
	var cfg configuration
	if err := envconfig.Process("", &cfg); err != nil {
		return fmt.Errorf("failed to load configuration: %w", err)
	}

	storage, err := storage.New(storage.Config{
		URL:      cfg.MongoURL,
		Database: cfg.MongoDatabase,
	})
	if err != nil {
		return fmt.Errorf("failed to initialize storage: %w", err)
	}
	defer func() {
		if err := storage.Close(); err != nil {
			level.Error(logger).Log("msg", "failed to close storage", "err", err)
		}
	}()

	eventQueue, err := event.NewQueue(event.Config{
		Logger:           logger,
		NodeID:           cfg.EventQueueClientName,
		ClusterID:        cfg.EventQueueClusterID,
		SequenceNumber:   0,
		URL:              cfg.EventQueueURL,
		Subject:          cfg.EventQueueSubject,
		AckWait:          cfg.EventQueueAckWait,
		ReconnectTimeout: cfg.EventQueueReconnectTimeout,
	})
	if err != nil {
		return fmt.Errorf("failed to initialize event queue: %w", err)
	}
	defer func() {
		if err := eventQueue.Close(); err != nil {
			level.Error(logger).Log("msg", "failed to close event queue", "err", err)
		}
	}()

	srv, err := adminsvc.NewServer(adminsvc.ServerConfig{
		Logger:          logger,
		Storage:         storage,
		EventQueue:      eventQueue,
		Port:            cfg.Port,
		ReadTimeout:     cfg.ReadTimeout,
		WriteTimeout:    cfg.WriteTimeout,
		ShutdownTimeout: cfg.ShutdownTimeout,
		MetricPrefix:    metricPrefix + "_server",
	})
	if err != nil {
		return fmt.Errorf("failed to initialize server: %w", err)
	}

	level.Info(logger).Log("msg", "starting http server", "port", cfg.Port)
	if err := srv.Serve(ctx); err != nil {
		return fmt.Errorf("failed to serve http: %w", err)
	}

	return nil
}

// signalContext returns a context that is canceled if either SIGTERM or SIGINT signal is received.
func signalContext(logger log.Logger) (context.Context, context.CancelFunc) {
	ctx, cancel := context.WithCancel(context.Background())

	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGTERM, syscall.SIGINT)

	go func() {
		select {
		case sig := <-c:
			level.Info(logger).Log("msg", "received signal", "signal", sig)
			cancel()
		case <-ctx.Done():
		}
	}()

	return ctx, cancel
}
