package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/kelseyhightower/envconfig"
	"golang.org/x/sync/errgroup"

	"git.wildberries.ru/lifestyle/catalog/pkg/catalog"
	"git.wildberries.ru/lifestyle/catalog/pkg/event"
	"git.wildberries.ru/lifestyle/catalog/pkg/filtersvc"
	"git.wildberries.ru/lifestyle/catalog/pkg/index"
	"git.wildberries.ru/lifestyle/catalog/pkg/replicator"
	"git.wildberries.ru/lifestyle/catalog/pkg/storage"
)

const metricPrefix = "lifestyle_catalog_filters"

type configuration struct {
	Port            string        `envconfig:"PORT" required:"true"`
	ReadTimeout     time.Duration `envconfig:"READ_TIMEOUT" default:"10s"`
	WriteTimeout    time.Duration `envconfig:"WRITE_TIMEOUT" default:"10s"`
	ShutdownTimeout time.Duration `envconfig:"SHUTDOWN_TIMEOUT" default:"10s"`

	MongoURL      string `envconfig:"MONGO_URL" required:"true"`
	MongoDatabase string `envconfig:"MONGO_DATABASE" required:"true"`

	EventQueueURL              string        `envconfig:"EVENT_QUEUE_URL" required:"true"`
	EventQueueClusterID        string        `envconfig:"EVENT_QUEUE_CLUSTER_ID" required:"true"`
	EventQueueSubject          string        `envconfig:"EVENT_QUEUE_SUBJECT" required:"true"`
	EventQueueClientName       string        `envconfig:"EVENT_QUEUE_CLIENT_NAME" required:"true"`
	EventQueueAckWait          time.Duration `envconfig:"EVENT_QUEUE_ACK_WAIT" default:"1s"`
	EventQueueReconnectTimeout time.Duration `envconfig:"EVENT_QUEUE_RECONNECT_TIMEOUT" default:"1s"`
}

func main() {
	logger := log.NewLogfmtLogger(log.NewSyncWriter(os.Stderr))
	logger = log.With(logger, "caller", log.DefaultCaller)
	logger = log.With(logger, "ts", log.DefaultTimestampUTC)

	ctx, cancel := signalContext(logger)
	defer cancel()

	level.Info(logger).Log("msg", "service is starting")
	if err := run(ctx, logger); err != nil {
		level.Error(logger).Log("msg", "service is stopped with error", "err", err)
		os.Exit(1)
	}
	level.Info(logger).Log("msg", "service is stopped")
}

func run(ctx context.Context, logger log.Logger) error {
	var cfg configuration
	if err := envconfig.Process("", &cfg); err != nil {
		return fmt.Errorf("failed to load configuration: %w", err)
	}

	idx := index.New()
	idx.AddMapping(&catalog.IndexMapping{
		Key:  catalog.IndexKeySchemaID,
		Type: catalog.IndexTypeEnum,
	})

	storage, err := storage.New(storage.Config{
		URL:      cfg.MongoURL,
		Database: cfg.MongoDatabase,
	})
	if err != nil {
		return fmt.Errorf("failed to initialize storage: %w", err)
	}
	defer func() {
		if err := storage.Close(); err != nil {
			level.Error(logger).Log("msg", "failed to close storage", "err", err)
		}
	}()

	eventQueue, err := event.NewQueue(event.Config{
		Logger:           logger,
		NodeID:           cfg.EventQueueClientName,
		ClusterID:        cfg.EventQueueClusterID,
		SequenceNumber:   0,
		URL:              cfg.EventQueueURL,
		Subject:          cfg.EventQueueSubject,
		AckWait:          cfg.EventQueueAckWait,
		ReconnectTimeout: cfg.EventQueueReconnectTimeout,
	})
	if err != nil {
		return fmt.Errorf("failed to initialize event queue: %w", err)
	}
	defer func() {
		if err := eventQueue.Close(); err != nil {
			level.Error(logger).Log("msg", "failed to close event queue", "err", err)
		}
	}()

	replicator, err := replicator.New(replicator.Config{
		Logger:       logger,
		Storage:      storage,
		EventQueue:   eventQueue,
		Index:        idx,
		MetricPrefix: metricPrefix + "_replicator",
	})
	if err != nil {
		return fmt.Errorf("failed to initialize replicator: %w", err)
	}

	srv, err := filtersvc.NewServer(filtersvc.ServerConfig{
		Logger:          logger,
		Index:           idx,
		Port:            cfg.Port,
		ReadTimeout:     cfg.ReadTimeout,
		WriteTimeout:    cfg.WriteTimeout,
		ShutdownTimeout: cfg.ShutdownTimeout,
		ReadyFunc:       replicator.Ready,
		MetricPrefix:    metricPrefix + "_server",
	})
	if err != nil {
		return fmt.Errorf("failed to initialize server: %w", err)
	}

	g, ctx := errgroup.WithContext(ctx)

	g.Go(func() error {
		level.Info(logger).Log("msg", "starting replicator")
		if err := replicator.Replicate(ctx); err != nil {
			return fmt.Errorf("failed to replicate data: %w", err)
		}
		return nil
	})
	g.Go(func() error {
		level.Info(logger).Log("msg", "starting http server", "port", cfg.Port)
		if err := srv.Serve(ctx); err != nil {
			return fmt.Errorf("failed to serve http: %w", err)
		}
		return nil
	})

	return g.Wait()
}

// signalContext returns a context that is canceled if either SIGTERM or SIGINT signal is received.
func signalContext(logger log.Logger) (context.Context, context.CancelFunc) {
	ctx, cancel := context.WithCancel(context.Background())

	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGTERM, syscall.SIGINT)

	go func() {
		select {
		case sig := <-c:
			level.Info(logger).Log("msg", "received signal", "signal", sig)
			cancel()
		case <-ctx.Done():
		}
	}()

	return ctx, cancel
}
