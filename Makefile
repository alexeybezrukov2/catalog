.PHONY: help
help:
	@echo "usage: make [target]"
	@echo
	@echo "targets:"
	@echo "  help            show this message"
	@echo "  test            run all tests (requires docker)"
	@echo "  clean-docker    stop test docker containers"
	@echo

.PHONY: test
test:
	# Starting Mongo docker container ...
	@docker run --rm -d --name test_mongo -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=test -e MONGO_INITDB_ROOT_PASSWORD=test mongo:4.2.6 > /dev/null
	# Starting NATS Streaming docker container ...
	@docker run --rm -d --name test_nats_streaming -p 4222:4222 -p 8222:8222 nats-streaming:0.18.0-alpine > /dev/null
	# Waiting 3s before running tests...
	@sleep 3s
	# Running tests ...
	@\
		TEST_MONGO_URL='mongodb://test:test@localhost:27017' \
		TEST_MONGO_DATABASE='test' \
		TEST_NATS_URL='localhost:4222' \
		go test -mod=vendor -cover -race -count=1 ./...; \
		rc=$$?; \
		docker stop test_mongo > /dev/null; \
		docker stop test_nats_streaming > /dev/null; \
		exit $$rc
	# OK

.PHONY: clean-docker
clean-docker:
	@docker stop test_mongo >/dev/null 2>&1 || true
	@docker stop test_nats_streaming >/dev/null 2>&1 || true