# Lifestyle Catalog

This is the system that stores some items and provides filtering methods. Catalog designed to work on any product card structure. 
Final Catalog API depends on data, but you can see the [OpenAPI Specification](https://git.wildberries.ru/lifestyle/catalog/-/blob/master/api/catalog-openapi.yaml),
this will help to understand the ways of using.

### Installing

To install from source, clone the Catalog git repository:

```shell script
git clone git@git.wildberries.ru:lifestyle/catalog.git
cd ./catalog
```

Run `docker-compose up` and Compose will start the entire app:

```shell script
cd ./build
docker-compose up -d
```

### Examples:

Get all product categories

```shell script
curl --request GET \
  --url http://catalog.lifestyle.svc.k8s.devel:8080/api/v1/categories
```

Get category ID from response and use it in the query to get product list 

```shell script
curl --request GET \
  --url 'http://catalog.lifestyle.svc.k8s.devel:8080/api/v1/products?category=1&sort=price&limit=1&desc=true'
```

You see something like 

```json
{
  "products": [
    {
      "id": 100,
      "properties": {
        "price": 8781,
        ...
      }
    }
  ],
  "total": 339,
  "facets": [
    {
      "id": 17,
      "name": "Конечная цена",
      "type": "int",
      "range_min": 43,
      "range_max": 9770
    },
    {
      "id": 3,
      "name": "Возрастное ограничение",
      "type": "enum",
      "enum_variants": [
        {
          "id": 0,
          "value": "16+",
          "count": 191
        },
        ...
      ]
    },
    ...
  ]
}
```

`"products": []` - product list which satisfy all filters in the query.

`"total": 339` - total number of products that match all filters.

`"facets": []` - list of facets available for returned products.

Now you probably want to specify some filters. There are some types of filters, 
they all have different syntax in the query.

- Enum filter: `{facetID}:{enumVariantID},{anotherEnumVariantID}`
- IntRange filter: `{facetID}:{minValue}-{maxValue}`
- DateRange filter: `{facetID}:{unixDateFrom}-{unixDateTo}`

So, if you want to get products with price no less than 1000 and no more than 10000, you just need to add one query param:
`filter=17:1000-10000`

Also, if you want to get products with an age limit 16+, just add another query param:
`filter=3:0` 

The final request should look like this: 

```shell script
curl --request GET \
  --url 'http://catalog.lifestyle.svc.k8s.devel:8080/api/v1/products?category=1&sort=price&limit=1&desc=true&filter=17:1000-10000&filter=3:0'
```