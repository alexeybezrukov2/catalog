module git.wildberries.ru/lifestyle/catalog

go 1.14

require (
	github.com/RoaringBitmap/roaring v0.4.23
	github.com/go-kit/kit v0.10.0
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.7.4
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/nats-io/nats-streaming-server v0.18.0 // indirect
	github.com/nats-io/stan.go v0.7.0
	github.com/prometheus/client_golang v1.6.0
	github.com/stretchr/testify v1.4.0
	go.mongodb.org/mongo-driver v1.3.5
	golang.org/x/sync v0.0.0-20200625203802-6e8e738ad208
)
